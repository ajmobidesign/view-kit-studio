import test from 'tape';
import Nightmare from 'nightmare';
//import firebase from 'firebase';
import { firebaseApp, firebaseAuth, firebaseDb, Db } from '../../src/core/firebase';


const nConfig = { openDevTools: {mode: "detach" }, show: true};

const nightmare = Nightmare(nConfig);


test('Templates Page Delete Function test', ()=>{
	nightmare
	.goto('http://localhost:3000')
	.click('button.github-signin')
	.wait('span.user-name')
	.click("a[href='/templates']")
	.wait('table.mdl-data-table')
	.evaluate(()=>{

		var deleteButtons = document.querySelectorAll('table.mdl-data-table tbody tr i.delete-btn');
		var dlBtn = deleteButtons[0].parentElement;
		var dialog = undefined;
		var logOut = document.querySelector('button.logout-button');

		var checkDialog = ()=>{
			console.log('check dialog')
			var dlg = document.querySelector('dialog.mdl-dialog');
			console.log('Dialog', dialog)
			dialog = dlg ? true : false;

			if(dialog){
				var clnBtn = document.querySelector('div.mdl-dialog__actions button:nth-child(2)');
				clnBtn.click();

				logOut.click();
				console.log('Dialog 2', dialog)
			}
		}

		dlBtn.click()

		var st1 = setTimeout(()=>{
			checkDialog();
			clearTimeout(st1);
		},500)


		return new Promise((resolve, reject)=>{
			setTimeout(()=> resolve(dialog), 2000)
		})

	})
	.end()	
	.then((result)=>{
		
		t.equal(result, true, 'Delete dialog works')		
		t.end();

	})
	.catch((error)=>{
		console.log(error)
	})



});



/*

test('Templates Table Test', (t)=>{

	nightmare
	.goto('http://localhost:3000')
	.click('button.github-signin')
	.wait('span.user-name')
	.click("a[href='/templates']")
	.wait(500)
	.evaluate(()=>{

		var rows = document.querySelectorAll('table.mdl-data-table tbody tr');

		console.log(rows.length)

		var rSmall = document.querySelectorAll('table.mdl-data-table tbody tr small.tpl-owner');

		var resultsArr =[];

			rSmall.forEach((r, i)=>{

				var row = r.parentElement.parentElement;

				var deleteBtn = row.querySelectorAll('i.delete-btn')

				resultsArr.push(deleteBtn.length);
				
			})

		return new Promise((resolve, reject)=>{
			setTimeout(()=> resolve(resultsArr), 2000)
		})
	})
	.end()	
	.then((results)=>{
		results.forEach((r)=>{
			t.equal(r, 0, 'Delete button is not present in all instances when user is not owner of template')
		})
	})
	.catch((error)=>{
		console.log(error)
	})


});

*/