
import {
	LOAD_PROFILE_DATA,
	UNLOAD_PROFILE_DATA, 
	LOAD_GLOBALNOTIFICATIONS_DATA, 
	UNLOAD_GLOBALNOTIFICATIONS_DATA
} from './action-types';


import { Db } from 'src/core/firebase';
import _ from 'lodash';




const newState = {transList: [], tplList: [], notifications: []};

function objectWithKeyToArray(obj){


	if(obj){
			return Object.keys(obj).map((key)=>{
							return Object.assign({}, obj[key], {key: key})
					});				
	}
	else{
		return [];
	}
}


function pullVals(obj, keys, val){
	return keys.map((key)=>{
		return obj[key][val]
	})
}


export function makeState (payload, state) {

	const { transactions, notifications } = payload;

	let notificationList =  (notifications) ? objectWithKeyToArray(notifications).filter((n)=>{ return !n.delete}) : [];

	let tList = (transactions) ?  Object.keys(transactions) : [];

	let tpList = (transactions) ?  pullVals(transactions, Object.keys(transactions), 'tplId')  : [];

return Object.assign({}, state, {notifications: notificationList}, {transList : tList}, {tplList: tpList});
}


export function profileReducer(state = newState, action){
	
	switch(action.type){
		case LOAD_PROFILE_DATA:
			
			let nextState = (action.payload) ? makeState(action.payload, state) : state;
			
			return nextState;

		case UNLOAD_PROFILE_DATA:
			let s =  Object.assign({}, newState)
			
			return s;

		case UNLOAD_GLOBALNOTIFICATIONS_DATA:
			 return state;
		default:
			return state;
	}
}