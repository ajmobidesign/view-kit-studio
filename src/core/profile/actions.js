import {
	LOAD_PROFILE_DATA,
	UNLOAD_PROFILE_DATA, 
	LOAD_GLOBALNOTIFICATIONS_DATA, 
	UNLOAD_GLOBALNOTIFICATIONS_DATA
} from './action-types';


import { Db } from 'src/core/firebase';
import { checkNotifications } from './selectors';


export function getProfileData(){

	//console.log('get profile data called')

	return(dispatch, getState) =>{
		const { auth } = getState();
		if(auth.authenticated){
			let id = auth.id;
			let path = `${id}/profile`;
			let alreadySubscribing = Db.alreadySubscribing(path);

			if(!alreadySubscribing){

				Db.subscribe(path, dispatch, LOAD_PROFILE_DATA, UNLOAD_PROFILE_DATA)
			}
		}
	}
}

//This is not currently being used
//Should it be used if the auth token expires
//or will an auto log out take care of it?
/*
export function unloadProfileData(){
	return(dispatch, getState) =>{
		const { auth } = getState();
			if(auth.authenticated){
				let id = auth.id;
				let path = `${id}/profile`;
				Db.unSubscribe(path, dispatch)
		}
	}
}
*/


export function getGlobalNotifications(){
	return(dispatch, getState)=>{
		const { auth } = getState();
		if(auth.authenticated){
				let id = auth.id;
				let path = `globalNotifications`;
				//let pathList = Db._subscriberList.map((l)=>{ return l.path });
				//let notYetSubscribing = (Db._subscriberList.map((l)=>{ return l.path }).indexOf(path) ==-1);
				let alreadySubscribing = Db.alreadySubscribing(path);

				if(!alreadySubscribing){

					Db.globallNotificationsSubscribe(path, dispatch, combineNotifications, UNLOAD_GLOBALNOTIFICATIONS_DATA)
			}
		}
	}
}


export function combineNotifications(globals){

	return(dispatch, getState)=>{
		const { auth } = getState();
		if(auth.authenticated){
			let id = auth.id;
			let path = `${id}/profile/notifications`;
			let { profile } = getState();
			//console.log( profile)
			let personal = profile.notifications;

			//console.log('personal', personal)

			let newNotifications = checkNotifications(globals, personal)

			//console.log(newNotifications)

			newNotifications.map((n)=>{
				let ob = {};
				ob[n] = globals[n];
				Db.update(path, ob);
			})
		}
	}
}

export function flagAsDeleted(path){
	return(dispatch, getState)=>{
		const { auth } = getState();
		if(auth.authenticated){
			let id = auth.id;
			let p = `${id}/${path}`;
			Db.flagDelete(p)
		}
	}	
}













