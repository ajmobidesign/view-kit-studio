import {
	LOAD_PROFILE_DATA,
	UNLOAD_PROFILE_DATA, 
	LOAD_GLOBALNOTIFICATIONS_DATA, 
	UNLOAD_GLOBALNOTIFICATIONS_DATA
} from './action-types';

import { Db } from 'src/core/firebase';
import { checkNotifications } from './selectors';

import { getProfileData, loadProfileData, unloadProfileData, getGlobalNotifications, combineNotifications, flagAsDeleted } from './actions';


const mockGlobal1 = {
	'456': {
			content: "This is another welcome message.",
 			date: "2016-10-23T17:39:10-07:00",
 			title: "This is another notification"
		}
}

const mockGlobal2 = {
	'456': {
			content: "This is another welcome message.",
 			date: "2016-10-23T17:39:10-07:00",
 			title: "This is another notification"
		}, 
	'778': {
			content: "This is a second message.",
 			date: "2016-10-23T17:39:10-07:00",
 			title: "This is another notification"
		}	
}


const state = {
	auth:{
		id: '123',
		authenticated: null
	},

	profile: {
		notifications: {
			'456': {
				content: "This is another welcome message.",
	 			date: "2016-10-23T17:39:10-07:00",
	 			title: "This is another notification"
			}, 
			'886': {
				content: "This is another welcome message.",
	 			date: "2016-10-23T17:50:10-07:00",
	 			title: "This is another notification", 
	 			delete: true
			}
		}	
	}
}


const getState = function (){
	return state;
}

const dispatch = {};



describe('Get Profile Data function', () =>{
	//execute function

	state.auth.authenticated = true;

	beforeEach(()=>{
		spyOn(Db, 'subscribe')
	});


	it('should call Db.subscribe when it user is logged in', () =>{

		let profileDataFnc = getProfileData();
		profileDataFnc(dispatch, getState);

		expect(Db.subscribe).toHaveBeenCalled();
		expect(Db.subscribe).toHaveBeenCalledWith('123/profile', dispatch, LOAD_PROFILE_DATA, UNLOAD_PROFILE_DATA);
	});

});


describe('Get Global Notifications', () =>{
	state.auth.authenticated = true;

	beforeEach(()=>{
		spyOn(Db, 'globallNotificationsSubscribe')
	});

	it('should call the Db globallNotificationsSubscribe method', ()=>{
		let gNotifications = getGlobalNotifications();
		gNotifications(dispatch, getState);

		expect(Db.globallNotificationsSubscribe).toHaveBeenCalled();
		expect(Db.globallNotificationsSubscribe).toHaveBeenCalledWith('globalNotifications', dispatch, combineNotifications, UNLOAD_GLOBALNOTIFICATIONS_DATA);
	})
});


describe('Check Notifications Selector', ()=>{

	it('should retun empty array of filtered global notifications', ()=>{
		let m1 = checkNotifications(mockGlobal1, state.profile.notifications);
		
		expect(m1.length).toBe(0);
	});

	it('should retun array of filtered global notifications', ()=>{
		
		let m2 = checkNotifications(mockGlobal2, state.profile.notifications);

		
		expect(m2.length).toBe(1);
		expect(m2[0]).toBe('778')
	})

});


describe('Combine Notifications', ()=>{

	state.auth.authenticated = true;

	beforeEach(()=>{
		spyOn(Db, 'update')
	});

	it('should not call the Db update function ', () =>{

		let combineFunc = combineNotifications(mockGlobal1);
		combineFunc(dispatch, getState);

		expect(Db.update).not.toHaveBeenCalled();

	});

	it('should call the Db update function ', () =>{

		let combineFunc = combineNotifications(mockGlobal2);
		combineFunc(dispatch, getState);
		let path = '123/profile/notifications';
		let checkData = {'778': {
				content: "This is a second message.",
	 			date: "2016-10-23T17:39:10-07:00",
	 			title: "This is another notification"
			}	
		};

		expect(Db.update).toHaveBeenCalled();
		expect(Db.update).toHaveBeenCalledWith(path, checkData);

	});


})


describe('Unload Profile Data Function', ()=>{
	state.auth.authenticated = true;

	beforeEach(()=>{
		spyOn(Db, 'unSubscribe')
	})

//disabled
	xit('should call the Db unSubscribe method', ()=>{
		let unloadFnc = unloadProfileData();
		unloadFnc(dispatch, getState);

		expect(Db.unSubscribe).toHaveBeenCalled();
		expect(Db.unSubscribe).toHaveBeenCalledWith('123/profile', dispatch);
	});
})


describe('Flag As Deleted function', () =>{

	state.auth.authenticated = true;

	beforeEach(()=>{
		spyOn(Db, 'flagDelete')
	})

	it('should call the flagDelete method and pass path as arg', () =>{
		let path = '786';
		let deleteFn = flagAsDeleted(path);
		deleteFn(dispatch, getState);

		 expect(Db.flagDelete).toHaveBeenCalled();
		 expect(Db.flagDelete).toHaveBeenCalledWith('123/786');
	});
});











