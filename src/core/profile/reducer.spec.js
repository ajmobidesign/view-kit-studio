import {
	LOAD_PROFILE_DATA,
	UNLOAD_PROFILE_DATA, 
	LOAD_GLOBALNOTIFICATIONS_DATA, 
	UNLOAD_GLOBALNOTIFICATIONS_DATA
} from './action-types';

import { profileReducer, makeState } from './reducer';

const newState = {transList: [], tplList: [], notifications: []};

const mockPayload = {
	transactions : {
		'1234': {
			status: 'Completed', 
			time: "2016-12-16T22:03:51-08:00",
			tplId: "mdl_single_page"
		},  
		'445': {
			status: 'Completed', 
			time: "2016-12-16T22:03:51-08:00",
			tplId: "mdl_basic_html5"
		}
	}, 

	notifications: {
		'456': {
			content: "This is another welcome message.",
 			date: "2016-10-23T17:39:10-07:00",
 			title: "This is another notification"
		}, 
		'886': {
			content: "This is another welcome message.",
 			date: "2016-10-23T17:50:10-07:00",
 			title: "This is another notification", 
 			delete: true
		}
	}

};

const mockGlobal = {
	'456': {
			content: "This is another welcome message.",
 			date: "2016-10-23T17:39:10-07:00",
 			title: "This is another notification"
		}, 
}


describe('Make State Function', ()=>{
	let nextState = makeState(mockPayload, newState);

	it('should create new state where the number of transactions and notifications matches the data object ', () =>{
		
		expect(nextState.transList.length).toBe(2);
		expect(nextState.tplList.length).toBe(2);		
	});

	it('should filter out notifications that have a delete flag', () =>{
		expect(nextState.notifications.length).toBe(1);
	});

}); 








