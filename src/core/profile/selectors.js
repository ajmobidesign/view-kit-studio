
export function checkNotifications(globals, personals){

	let gKeys = Object.keys(globals);
	let pKeys = Object.keys(personals);
	
	return gKeys.filter((k)=>{ return (pKeys.indexOf(k) === -1)})

}
