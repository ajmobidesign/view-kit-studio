import {
	
	LOAD_TEMPLATES_LIST,
	UNLOAD_TEMPLATES_LIST, 
	ADD_SHARED_TEMPLATE, 
	UNLOAD_SHARED_TEMPLATE
} from './action-types';


import { Db } from 'src/core/firebase';
import { appUtils } from 'src/core/dataUtils';


export function getTemplateList(){

	//need to figure out a way to limit the number of templates loaded and set lazy load
	return(dispatch, getState) =>{
		const { auth } = getState();
		if(auth.authenticated){
			let id = auth.id;
			let path = `${id}/templates`;
			let pathList = Db._subscriberList.map((l)=>{ return l.path });
			//let notYetSubscribing = (Db._subscriberList.map((l)=>{ return l.path }).indexOf(path) ==-1);
			let alreadySubscribing = Db.alreadySubscribing(path)
			
			if(!alreadySubscribing){
				Db.templateListSubscribe(path, dispatch, LOAD_TEMPLATES_LIST, UNLOAD_TEMPLATES_LIST, sharedTemplateCheck)
			}
		}
	}
}


export function sharedTemplateCheck(templateList){
	
	return (dispatch, getState) =>{
		const { auth } = getState();

		if(auth.authenticated){
						
				if(templateList){

					let tplList = Object.keys(templateList).map((k)=>{ return templateList[k]});

					let sharedList = tplList.filter((f)=>{return f.isCopyOf && !f.title})

					sharedList.forEach((s)=>{
						
						let isSubscribing = appUtils.propValExistsInCollection('path', Db._subscriberList, s.path)

					//console.log('subscribing', isSubscribing)
						if(isSubscribing === false){

							Db.sharedTemplateListSubscribe(s.path, dispatch, ADD_SHARED_TEMPLATE, UNLOAD_SHARED_TEMPLATE)
						}
						else{
							Db.getSharedTemplate(s.path, dispatch, ADD_SHARED_TEMPLATE)
						}
					})
				}
		}
	}	
}



export function unloadTemplateList(){
	return(dispatch, getState) =>{
		const { auth } = getState();
			if(auth.authenticated){
				let id = auth.id;
				let path = `${id}/profile`;
				Db.unSubscribe(path, dispatch)
		}
	}
}