import {
	LOAD_TEMPLATES_LIST,
	UNLOAD_TEMPLATES_LIST, 
	ADD_SHARED_TEMPLATE, 
	UNLOAD_SHARED_TEMPLATE
} from './action-types';


import { Db } from 'src/core/firebase';
import { appUtils } from 'src/core/dataUtils';
import _ from 'lodash';


const newState = { allTemplates: [] };


export function templateListReducer(state = newState, action){


	//console.log(action.type)

	switch(action.type){


		case LOAD_TEMPLATES_LIST:


			let data = action.payload;
			let list = (data) ? appUtils.objectToArray(data) : []

			return Object.assign({}, state, {allTemplates: list});


		case ADD_SHARED_TEMPLATE:

		
			let sharedTpl = action.payload;
			let itExists = appUtils.propValExistsInCollection('key', state.allTemplates, sharedTpl.key);
			let newList;

			if(itExists){
				let oldList = state.allTemplates.filter((t) =>{
					//need to handle case where the object does not have key..
					return t.key !== sharedTpl.key;
				})
				newList = oldList.concat([sharedTpl])

			} else{
				
				let updated = [].concat(state.allTemplates)
				newList = updated.concat([sharedTpl])
			}

			return Object.assign({}, state, {allTemplates: newList});

		case UNLOAD_SHARED_TEMPLATE:

			//not sure what happens here
			//what happens when something is deleted?
		
			return state;


		case UNLOAD_TEMPLATES_LIST:
		
			return Object.assign({}, newState);			

		default: 
			return state;


	}

}