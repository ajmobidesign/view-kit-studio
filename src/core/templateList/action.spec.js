import {
	
	LOAD_TEMPLATES_LIST,
	UNLOAD_TEMPLATES_LIST, 
	ADD_SHARED_TEMPLATE, 
	UNLOAD_SHARED_TEMPLATE
} from './action-types';


import { getTemplateList, sharedTemplateCheck, getShareTemplate, unloadTemplateList } from './actions';
import { Db } from 'src/core/firebase';

const state = {
	auth:{
		id: '123',
		authenticated: null
	}
}



const getState = function (){
	return state;
}

const dispatch = {};

const mockTemplateList = {
	'123': {
		title: 'I am first'
	}, 
	'345': {
		isCopyOf: true
	}, 
	'976': {
		isCopyOf: true
	}, 
	'667':{
		title: 'I am second'
	}
}



describe('Get Template List Func ', ()=>{
	

	beforeEach(()=>{
		
		spyOn(Db, 'templateListSubscribe')
	});

	afterEach(()=>{
		state.auth.authenticated = null;
	});

	it('should call Db templateListSubscribe when user is authenticated', ()=>{

		state.auth.authenticated = true;

		let path = `${state.auth.id}/templates`;
		let action = getTemplateList();
		action(dispatch, getState);

		//expect(state.auth.authenticated).toBe(true);
		expect(Db.templateListSubscribe).toHaveBeenCalled();
		expect(Db.templateListSubscribe).toHaveBeenCalledWith(path, dispatch, LOAD_TEMPLATES_LIST, UNLOAD_TEMPLATES_LIST, sharedTemplateCheck);

	});

});



describe('Shared Template Check ', ()=>{
	beforeEach(()=>{
		
		spyOn(Db, 'sharedTemplateListSubscribe');
	});

	afterEach(()=>{
		state.auth.authenticated = null;
	});


	it('should call Db sharedTemplateListSubscribe', ()=>{
		state.auth.authenticated = true;
		let action = sharedTemplateCheck(mockTemplateList);
		action(dispatch, getState);

		expect(Db.sharedTemplateListSubscribe).toHaveBeenCalled();
		expect(Db.sharedTemplateListSubscribe).toHaveBeenCalledTimes(2);

	});

});









