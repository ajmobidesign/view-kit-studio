import {
	LOAD_TEMPLATES_LIST,
	UNLOAD_TEMPLATES_LIST, 
	ADD_SHARED_TEMPLATE, 
	UNLOAD_SHARED_TEMPLATE
} from './action-types';


import { templateListReducer } from './reducer';
const newState = { allTemplates: []};
import { objectToArray, propValExistsInCollection, propValInCollectionIndex } from 'src/core/dataUtils/utils';

const Ob = {
	'1234': {
		prop: 'prop1'
	}, 
	'3347': {
		prop: 'prop2'
	}, 
	'2247': {
		prop: 'prop3'
	}, 
	'8890': {
		prop: 'prop4'
	}
};


describe('Template List Reducer', ()=>{
	describe('LOAD_TEMPLATES_LIST', ()=>{
		it('should set state to an arry when an object is passed to it ', () =>{
			let action = {type: 'LOAD_TEMPLATES_LIST', payload: Ob};

			let state = templateListReducer(undefined, action);

			expect(state.allTemplates.length).toBe(4);

		});
	});

	describe('UNLOAD_TEMPLATES_LIST', () =>{
		it('should return the current state', ()=>{
			let action = {type: 'UNLOAD_SHARED_TEMPLATE'};
			let currentState = { allTemplates: [{tpl: '1'}, {tpl: '2'}]}
			let state = templateListReducer(currentState, action);

			expect(state).toBe(currentState);
		});
			
	});

	describe('ADD_SHARED_TEMPLATE', () =>{
		

	});



	describe('UNLOAD_SHARED_TEMPLATE', ()=>{
		it('should return newState', ()=>{
			let action = {type: 'UNLOAD_SHARED_TEMPLATE'};
			let currentState = { allTemplates: [{tpl: '1'}, {tpl: '2'}]}
			let state = templateListReducer(currentState, action);

			expect(state).toBe(state);
		});
	});
});
