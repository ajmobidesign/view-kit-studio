import {
	MAKE_PAYMENT, 
	CREATE_NEW_PAYMENT_OBJECT, 
	PAYMENT_COMPLETED,
	PAYMENT_CANCELED
} from './action-types';

import { paypal , saleOb } from './pplrest';
import { Db } from 'src/core/firebase';
import { push } from 'react-router-redux';
import moment from 'moment';
import _ from 'lodash';
import { currentEv } from 'src/core/defaults';


export function makePayment(paymentData, errorFnc){

	return(dispatch, getState)=>{
		const { auth } = getState();

		if(auth.authenticated){
			let id = auth.id;
			let path = 'transactions';
			let fbData = Object.assign({}, paymentData)

			fbData.user = id;


			Db.newTransaction(path, fbData)
			.then((transId)=>{
				let transOb = Object.assign({}, saleOb);
				let tpl = (paymentData.item_list.items[0].name).toLowerCase().trim();
				let tplId = _.replace(tpl, RegExp(" ","g"), '_');

				

				transOb.redirect_urls.return_url = `${currentEv.client}/payment/${transId}/${tplId}`;
				
				transOb.redirect_urls.cancel_url = `${currentEv.client}/payment-canceled/${transId}`;
				

				transOb.transactions.push(paymentData)

				let timePath = `transactions/${transId}`;
				let payId =  `transactions/${transId}/payId`;

				
				//paypal.payment.create({}, function (error, payment) {
					
				paypal.payment.create(transOb, function (error, payment) {
				    if (error) {

				    	errorFnc()
				    	//throw error;

				    } 
				    else {

				    	Db.update(timePath, {create_time: payment.create_time, payId: payment.id, status: 'Pending'})
				    	//Db.update(payId, payment.id)

				        //console.log("Create Payment Response", payment);

				        window.location.href= payment.links[1].href;
					}
				});
			})
		}
	}
}




export function transCompleted(transId, tplId){
	return(dispatch, getState)=>{
		
		const { auth, profile } = getState();
		if(auth.authenticated){
			let id = auth.id;
			let inList = (profile.transactions && Object.keys(profile.transactions).indexOf(tplId) !== -1)

			let path1 = `transactions/${transId}`;
			let path2 = `${id}/profile/transactions/${transId}`;

			//Db.getData(`transactions/${transId}/create_time`)
			//.then((result)=>{

				//console.log(result)
				
				if(!inList){
					let data1 = {status: 'Completed'};
			        let data2 = {status: 'Completed', tplId: tplId, time: moment().format()};
						
					Db.update(path1, data1)
					Db.update(path2, data2)
				}
			//})
		}
	}
}


export function transCanceled(transId){
	return(dispatch, getState)=>{
		const { auth } = getState();
		if(auth.authenticated){
			let id = auth.id;
			let data = {status: 'Canceled'};
			let path1 = `transactions/${transId}`;

			Db.update(path1, data)
		}
	}
}


export function getNewPayment(){
	return {
		type: CREATE_NEW_PAYMENT_OBJECT
	}
}