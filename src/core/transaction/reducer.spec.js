import {
	MAKE_PAYMENT,
	CREATE_NEW_PAYMENT_OBJECT,
	PAYMENT_COMPLETED,
	PAYMENT_CANCELED
} from './action-types';

import { saleOb } from './pplrest';
import { transactionReducer } from './reducer';


const startState = Object.assign({}, saleOb);


describe('Transaction Reducer', ()=>{

	describe('CREATE_NEW_PAYMENT_OBJECT', ()=>{
		it('should return saleOb', ()=>{
			let action = {type: CREATE_NEW_PAYMENT_OBJECT};
			let state = transactionReducer(undefined, action);

			expect(state).toEqual(saleOb);
		});		
	});

	describe('PAYMENT_CANCELED', ()=>{
		it('should return saleOb', ()=>{
			let action = {type: MAKE_PAYMENT};
			let state = transactionReducer(undefined, action);

			expect(state).toEqual(saleOb);
		});
	});

	describe('PAYMENT_COMPLETED', ()=>{
		it('should return saleOb', ()=>{
			let action = {type: MAKE_PAYMENT};
			let state = transactionReducer(undefined, action);

			expect(state).toEqual(saleOb);
		});
	});

});

