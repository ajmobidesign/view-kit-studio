import { paypalConfig } from './config';
import * as paypal from 'paypal-rest-sdk';


const testOb = {
  'mode': 'sandbox', //sandbox or live
  'client_id': 'EBWKjlELKMYqRNQ6sYvFo64FtaRLRR5BdHEESmha49TM',
  'client_secret': 'EO422dn3gQLgDbuwqTjzrFgFtaRLRR5BdHEESmha49TM'
}

paypal.configure(paypalConfig);

export { paypal };

export const saleItem = {
	                "name": "",
	                "price": "1.00",
	                "currency": "USD",
	                "quantity": 1
	            };

export const paymentItem = {
			        "item_list": {
			            "items": []
			        },
			        "amount": {
			            "currency": "USD",
			            "total": "1.00"
			        },
			        "description": "This is the payment description."
			    };


export const saleOb = {
				    "intent": "sale",
				    "payer": {
				        "payment_method": "paypal"
				    },
				    "redirect_urls": {
				        "return_url": "http://return.url",
				        "cancel_url": "http://cancel.url"
				    },
				    "transactions": []
				};



