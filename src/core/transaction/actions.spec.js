import {
	MAKE_PAYMENT, 
	CREATE_NEW_PAYMENT_OBJECT, 
	PAYMENT_COMPLETED,
	PAYMENT_CANCELED
} from './action-types';


import { makePayment, transCompleted, transCanceled, getNewPayment } from './actions';

import { Db } from 'src/core/firebase';

const state = {
	auth:{
		id: '123',
		authenticated: null
	}, 

	profile : {
		transactions :{
			'12345' : {
				tplId: "mdl_single_page"
			}
		}
	}

};

const getState = function (){
	return state;
};

const dispatch = {};

describe('Make Payment', ()=>{

});

describe('Trans Completed Function', ()=>{

		beforeEach(()=>{
		
			spyOn(Db, 'update')
		});

		afterEach(()=>{
			state.auth.authenticated = null;
		});


	it('should call Db update when user is authenticated', ()=>{
		state.auth.authenticated = true;

		let mockTransId = '12345';
		let tplId = "mdl_single_page";
		let action = transCompleted(mockTransId, tplId);

		action(dispatch, getState);

		expect(Db.update).toHaveBeenCalled();
		expect(Db.update).toHaveBeenCalledTimes(2);
		
	});
});

describe('Trans Canceled', ()=>{

		beforeEach(()=>{
		
			spyOn(Db, 'update')
		});

		afterEach(()=>{
			state.auth.authenticated = null;
		});


	it('should call Db update when user is authenticated', ()=>{
		state.auth.authenticated = true;
		let mockTransId = '12345';
		let path = `transactions/${mockTransId}`;
		let data = {status: 'Canceled'};
		let action = transCanceled(mockTransId);

		action(dispatch, getState);

		expect(Db.update).toHaveBeenCalled();
		expect(Db.update).toHaveBeenCalledWith(path, data);
		
	});
});




