import {
	MAKE_PAYMENT,
	CREATE_NEW_PAYMENT_OBJECT,
	PAYMENT_COMPLETED,
	PAYMENT_CANCELED
} from './action-types';

import { saleOb } from './pplrest';


const startState = Object.assign({}, saleOb);



export function transactionReducer(state = startState, action){
	switch(action.type){

		case CREATE_NEW_PAYMENT_OBJECT: 

			return Object.assign({}, saleOb);
		case PAYMENT_COMPLETED:
		case PAYMENT_CANCELED:
			return Object.assign({}, saleOb);
		default:
			return state;

	}

}