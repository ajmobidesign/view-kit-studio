
const middleware = () => next => action => {
  dataLayer = dataLayer || [];
  dataLayer.push({
    event: action.type,
    payload: action.payload
  });
  let result = next(action);
  return result;
};

export default middleware;