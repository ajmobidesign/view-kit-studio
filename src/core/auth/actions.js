import { firebaseAuth } from 'src/core/firebase';
import firebase from 'firebase';
import {
  INIT_AUTH,
  SIGN_IN_ERROR,
  SIGN_IN_SUCCESS,
  SIGN_OUT_SUCCESS
} from './action-types';


import { Db } from 'src/core/firebase';
import { push } from 'react-router-redux';
import axios from 'axios';






function authenticate(provider) {
  console.log('authenticate called', provider)
  
  return dispatch => {
    console.log('dispatch called')
    firebaseAuth
      .signInWithPopup(provider)
      .then(result => {

        console.log('log result', result)
           
        Db.addAndTrackUser(result.user);
        dispatch(signInSuccess(result.user));

        dispatch(push('/templates'));
        

      })
      .catch(error => dispatch(signInError(error)));
  };
}


export function authenticateUser(user){
  //console.log(user)
  return dispatch => {
    firebaseAuth.signInWithEmailAndPassword(user.email, user.pswd)
      .then(result => {
       
        Db.addAndTrackUser(result);
        dispatch(signInSuccess(result));
        dispatch(push('/templates'));
      })
      .catch(error => dispatch(signInError(error)));
  };
}

export function initAuth(user) {
  //console.log('action init', user)
  return {
    type: INIT_AUTH,
    payload: user
  };
}

export function signInError(error) {
  //console.log('error', error)
  return {
    type: SIGN_IN_ERROR,
    payload: error
  };
}

export function signInSuccess(result) {
  console.log('signin Success', result)
  return {
    type: SIGN_IN_SUCCESS,
    payload: result
  };
}

export function signInWithGithub() {
  return authenticate(new firebase.auth.GithubAuthProvider());
}


export function signInWithGoogle() {
  let provider = new firebase.auth.GoogleAuthProvider();

  //console.log(provider)
  //provider.addScope('profile')
  //provider.setCustomParameters({'inlcude_granted_scopes': true});
  return authenticate(provider);
}


export function signInWithGmail() {
  let provider = new firebase.auth.GoogleAuthProvider();

  //console.log(provider)
  provider.addScope('https://www.googleapis.com/auth/gmail.send')

  return authenticate(provider);
}





export function signInWithTwitter() {
  return authenticate(new firebase.auth.TwitterAuthProvider());
}

export function signOut() {
  return (dispatch, getState )=> {
    const { auth } = getState();

     Db.trackLogOut(auth.id);

    firebaseAuth.signOut()
      .then((r) => {
        dispatch(signOutSuccess());

        Db.unSubscribeAll(dispatch);
      })
      .catch((e)=> console.log(e))
  };
}

export function signOutSuccess() {
  //console.log('successfully signed out')
  return {
    type: SIGN_OUT_SUCCESS
  };
}
