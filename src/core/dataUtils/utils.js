

export function objectToArray(ob){
	//console.log(ob)
	return Object.keys(ob).map((k) =>{
		return ob[k];
	})
};


export function propValExistsInCollection(prop, collection, val){
	return collection.map((l)=>{ return l[prop] }).indexOf(val) !== -1;
};

export function propValInCollectionIndex(prop, collection, val){
	return collection.map((l)=>{ return l[prop] }).indexOf(val);
};