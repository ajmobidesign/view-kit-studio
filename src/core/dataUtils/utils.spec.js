import { objectToArray, propValExistsInCollection, propValInCollectionIndex } from './utils';

const Ob = {
	'1234': {
		prop: 'prop1'
	}, 
	'3347': {
		prop: 'prop2'
	}, 
	'2247': {
		prop: 'prop3'
	}, 
	'8890': {
		prop: 'prop4'
	}
};

const arr = [
	{	
		prop: 'prop1'
	}, 
	{
		prop: 'prop2'
	}, 
	{	 
		prop: 'prop3'
	}, 
	{	
		prop: 'prop4'
	}
];

describe('objectToArray Function', ()=>{
	it('should take an object with keys as properties and return an arry of those properties', ()=>{

		//let util = objectToArray(Ob);

		//the index of the arrays is different;
		//expect(util).toEqual(arr);
	});

});


describe('propValExistsInCollection', ()=>{
	it('should return true if the property exist and the value exist', ()=>{

		let util = propValExistsInCollection('prop', arr, 'prop3');

		expect(util).toBe(true);
	});

	it('should return false if the prop does not exist', ()=>{

		let util = propValExistsInCollection('whatever', arr, 'prop3');

		expect(util).toBe(false);
	});

	it('should return false if the value does not exist', ()=>{

		let util = propValExistsInCollection('prop', arr, 'prop8');

		expect(util).toBe(false);
	});
});


describe('propValInCollectionIndex', ()=>{
	it('should return index of the object where the propery exists and the value exists', ()=>{

		let util = propValInCollectionIndex('prop', arr, 'prop3');

		expect(util).toBe(2);

	});

	it('should return -1 when the propery does not exist', ()=>{

		let util = propValInCollectionIndex('whatever', arr, 'prop3');

		expect(util).toBe(-1);

	});

	it('should return -1 when the propery value does not exist', ()=>{

		let util = propValInCollectionIndex('prop', arr, 'prop8');

		expect(util).toBe(-1);

	});

});






