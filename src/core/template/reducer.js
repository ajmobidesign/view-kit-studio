
import {
	CREATE_TEMPLATE,
	UPDATE_TEMPLATE,
	LOAD_TEMPLATE,
	UNLOAD_TEMPLATE,
	UPDATE_STYLECONFIG,
	UPDATE_COMPONENTCONFIG, 
	SAVING_TEMPLATE, 
	DEMO_NOTIFICATION
} from './action-types';


//need to figure this out
//because this is differnet 
const startState =  Object.assign({}, {});



export function templateReducer(state = startState, action){


	
	
	switch(action.type){
		case CREATE_TEMPLATE:

			//fired when not logged in 
			//the default template state has to based template id
			
			return  Object.assign({}, action.payload);
		case UPDATE_TEMPLATE:

			let ob = Object.assign({}, state, action.payload);

			//console.log('reducer', ob, action.payload)
			
			return ob
		case LOAD_TEMPLATE:
			//there is not point doing this
			let { savingTemplate } = state;
			let o = (action.payload) ? Object.assign({}, action.payload, {savingTemplate: savingTemplate }) : state;

			
			
			return o
		case UNLOAD_TEMPLATE:
			
			//reset template
			return Object.assign({}, startState);
		case UPDATE_STYLECONFIG:
				
			return  Object.assign({}, state, {styleConfig: action.payload});

		case UPDATE_COMPONENTCONFIG:
				
			return  Object.assign({}, state, {componentConfig: action.payload});

		case DEMO_NOTIFICATION:
		
			return Object.assign({}, state, {demoNotification: action.payload})	

		case SAVING_TEMPLATE: 

			
		
			return Object.assign({}, state, {savingTemplate: action.payload})	
			
		default:
			
			return state;		
	}

}