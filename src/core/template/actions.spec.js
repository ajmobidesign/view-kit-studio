import {
	CREATE_TEMPLATE,
	UPDATE_TEMPLATE,
	LOAD_TEMPLATE,
	UNLOAD_TEMPLATE,
	UPDATE_STYLECONFIG,
	UPDATE_COMPONENTCONFIG
} from './action-types';


import { templateMap } from 'src/core/defaults';
import { Db } from 'src/core/firebase';
import axios from 'axios';
import { push } from 'react-router-redux';
import { replace, lowerCase} from 'lodash'; 
import moment from 'moment';

import { createViewOnlyTemplate, createNewTemplate, loadTemplate, loadShareView, loadCollabTemplate, unloadTemplate, updateTemplate, updateStyleConfig, updateComponentConfig, addSticky, removeSticky, updateSticky, addTemplateCollaborator, removeTemplateCollaborator, shareTemplate, removeShare, deleteTemplate, downloadTemplate } from './actions';

const state = {
	auth:{
		id: '123',
		displayName: null,
		authenticated: null
	}
};	

const getState = function (){
	return state;
};

const dispatch = {};



describe('Create Veiw Only Template ', () =>{
	it('should set template object based on the Id passed to action', ()=>{

		let id = 'mdl_basic_html5'
		let action = createViewOnlyTemplate(id);
		let name = templateMap[id].name;
				
		expect(action.payload.name).toBe(name);
	
	})
});

describe('Create New Template', ()=>{

	beforeEach(()=>{
		state.auth.authenticated = null;
		state.auth.displayName = 'tom@example.com';
		spyOn(Db, 'addTemplate');
	});

	afterEach(()=>{
		state.auth.authenticated = null;
	});


	it('should call Db addTemplate when user is authenticated', ()=>{
		state.auth.authenticated = true;
		let id = 'mdl_basic_html5';
		let path = `${state.auth.id}/templates`;
		let template = templateMap[id];
		template.owner = 'tom@example.com';
		template.created = moment().format();
	
		let action = createNewTemplate(id);
		action(dispatch, getState);


		expect(Db.addTemplate).toHaveBeenCalled();
		expect(Db.addTemplate).toHaveBeenCalledWith(path, template, dispatch);

	});

	it('should not call Db addTemplate when user is not authenticated', () =>{
		expect(Db.addTemplate).not.toHaveBeenCalled();
	});
});


describe('Load Template Function', ()=>{

	beforeEach(()=>{
		state.auth.authenticated = null;
		spyOn(Db, 'subscribe');
	});

	afterEach(()=>{
		state.auth.authenticated = null;
	});

	it('should call Db subscribe when user is authenticated', ()=>{
		let docId = 'kbl2';
		state.auth.authenticated = true;
		let action = loadTemplate(docId);
		let path = `${state.auth.id}/templates/${docId}`;

		action(dispatch, getState);

		expect(Db.subscribe).toHaveBeenCalled();
		expect(Db.subscribe).toHaveBeenCalledWith(path, dispatch, LOAD_TEMPLATE, UNLOAD_TEMPLATE);

	});
});

/*
describe('Load Share View Function ', ()=>{

	beforeEach(()=>{
		state.auth.authenticated = null;
		spyOn(Db, 'subscribe');
		spyOn(Db, 'checkData');

	});

	afterEach(()=>{
		state.auth.authenticated = null;
	});

	it('should call Db checkData when user is authenticated', ()=>{
		state.auth.authenticated = true;
		let sharePath = '123/templates/456';

		let action = loadShareView(sharePath);
		action(dispatch, getState);

		expect(Db.checkData).toHaveBeenCalled();
		expect(Db.subscribe).toHaveBeenCalled();

	});
});


describe('Lodad Callab Template Function', ()=>{

});

*/

describe('Unload Template', ()=>{
	beforeEach(()=>{
		state.auth.authenticated = null;
		state.collaborators = {};
		spyOn(Db, 'unSubscribe');
	});

	afterEach(()=>{
		state.auth.authenticated = null;
		state.collaborators = null;
		state.currState = null;
	});

	it('should call Db unsubscribe when user is authenticated', ()=>{
		state.auth.authenticated = true;
		let docId = '3245';		 
		let action = unloadTemplate(docId);
		action(dispatch, getState);

		let path = `${state.auth.id}/templates/${docId}`;
		expect(Db.unSubscribe).toHaveBeenCalled();
		expect(Db.unSubscribe).toHaveBeenCalledWith(path, dispatch);
	});


	//is this even being used?

/*
	it('should call Db unsubscribe when user is authenticated and pass the docId when currState is null', ()=>{
		state.auth.authenticated = true;
		state.currState= {
			'3245': {

			}
		}
		let docId = '3245';	

		let action = unloadTemplate(docId);
		action(dispatch, getState);

		let path = `${state.auth.id}/templates/${docId}`;
		expect(Db.unSubscribe).toHaveBeenCalled();
		expect(Db.unSubscribe).toHaveBeenCalledWith(path, dispatch);
	});
*/


});


describe('Update Style Config Func', ()=>{
	beforeEach(()=>{
		state.auth.authenticated = null;
		state.collaborators = {
			'445': 'userId'
		};
		spyOn(Db, 'update');
	});

	afterEach(()=>{
		state.auth.authenticated = null;
		state.collaborators = null;
	});

	it('should call Db update when user is authenticated and pass the styeOb and path as arguments', ()=>{
		state.auth.authenticated = true;
		let docId = '3245';	

		let styleOb = {style: '234'};	 
		let action = updateStyleConfig(styleOb, docId);
		action(dispatch, getState);

		let path = `${state.auth.id}/templates/${docId}/styleConfig`;
		expect(Db.update).toHaveBeenCalled();
		expect(Db.update).toHaveBeenCalledWith(path, styleOb);
	});


	it('should call Db updated when user is authenticated and pass styeOb and path as arguments', () =>{

		state.auth.authenticated = true;
		let docId = '3245';	

		state.collaborators['3245']= 'userId2'

		let styleOb = {style: '234'};	 
		let action = updateStyleConfig(styleOb, docId);
		action(dispatch, getState);

		let path = `${state.collaborators['3245']}/templates/${docId}/styleConfig`;
		expect(Db.update).toHaveBeenCalled();
		expect(Db.update).toHaveBeenCalledWith(path, styleOb);

	});

	//need to still add test for logged out version with dispatch object


});


describe('Update Component Config Func', ()=>{
	beforeEach(()=>{
		state.auth.authenticated = null;
		state.collaborators = {
			'445': 'userId'
		};
		spyOn(Db, 'update');
	});

	afterEach(()=>{
		state.auth.authenticated = null;
		state.collaborators = null;
	});

	it('should call Db update when user is authenticated and pass the styeOb and path as arguments', ()=>{
		state.auth.authenticated = true;
		let docId = '3245';	

		let componentOb = {component: '234'};	 
		let action = updateComponentConfig(componentOb, docId);
		action(dispatch, getState);

		let path = `${state.auth.id}/templates/${docId}/componentConfig`;
		expect(Db.update).toHaveBeenCalled();
		expect(Db.update).toHaveBeenCalledWith(path, componentOb);
	});


	it('should call Db updated when user is authenticated and pass styeOb and collabpath as arguments', () =>{

		state.auth.authenticated = true;
		let docId = '3245';	

		state.collaborators['3245']= 'userId2';

		let componentOb = {component: '234'};	 
		let action = updateComponentConfig(componentOb, docId);
		action(dispatch, getState);

		let path = `${state.collaborators['3245']}/templates/${docId}/componentConfig`;
		expect(Db.update).toHaveBeenCalled();
		expect(Db.update).toHaveBeenCalledWith(path, componentOb);

	});

	//need to still add test for logged out version with dispatch object


});


describe('Add Sticky Function', ()=>{

	beforeEach(()=>{
		state.auth.authenticated = null;
		state.collaborators = {
			'445': 'userId'
		};
		spyOn(Db, 'addAndSetKey');
	});

	afterEach(()=>{
		state.auth.authenticated = null;
		state.collaborators = null;
	});

	it('should call Db addAndSetKey when user is authenticated and pass path and stickyOb as args', ()=>{

		state.auth.authenticated = true;
		let docId = '3245';	

		let stickyOb = {sticky: '234'};	 
		let action = addSticky(docId, stickyOb);
		action(dispatch, getState);

		let path = `${state.auth.id}/templates/${docId}/stickys`;
		expect(Db.addAndSetKey).toHaveBeenCalled();
		expect(Db.addAndSetKey).toHaveBeenCalledWith(path, stickyOb);

	});

		it('should call Db addAndSetKey when user is authenticated and pass callabpath and stickyOb as args', ()=>{

		state.auth.authenticated = true;
		let docId = '3245';	
		state.collaborators['3245']= 'userId2';

		let stickyOb = {sticky: '234'};	 
		let action = addSticky(docId, stickyOb);
		action(dispatch, getState);

		let path = `${state.collaborators['3245']}/templates/${docId}/stickys`;
		expect(Db.addAndSetKey).toHaveBeenCalled();
		expect(Db.addAndSetKey).toHaveBeenCalledWith(path, stickyOb);

	});

});


describe('Remove Sticky Function', ()=>{

	beforeEach(()=>{
		state.auth.authenticated = null;
		state.collaborators = {
			'445': 'userId'
		};
		spyOn(Db, 'deleteValue');
	});

	afterEach(()=>{
		state.auth.authenticated = null;
		state.collaborators = null;
	});

	it('should call Db deleteValue and pass path as arg', ()=>{
		state.auth.authenticated = true;
		let docId = '3245';	
		let stickyId = '8875';

		let action = removeSticky(docId, stickyId);
		action(dispatch, getState);

		let path = `${state.auth.id}/templates/${docId}/stickys/${stickyId}`;
		expect(Db.deleteValue).toHaveBeenCalled();
		expect(Db.deleteValue).toHaveBeenCalledWith(path);
	});

	it('should call Db deleteValue and pass collabpath as arg', ()=>{
		state.auth.authenticated = true;
		let docId = '3245';	
		let stickyId = '8875';
		state.collaborators['3245']= 'userId2';

		let action = removeSticky(docId, stickyId);
		action(dispatch, getState);

		let path = `${state.collaborators['3245']}/templates/${docId}/stickys/${stickyId}`;
		expect(Db.deleteValue).toHaveBeenCalled();
		expect(Db.deleteValue).toHaveBeenCalledWith(path);
	});

});

describe('Update Sticky Function', ()=>{
	beforeEach(()=>{
		state.auth.authenticated = null;
		state.collaborators = {
			'445': 'userId'
		};
		spyOn(Db, 'update');
	});

	afterEach(()=>{
		state.auth.authenticated = null;
		state.collaborators = null;
	});

	it('should call Db addAndSetKey when user is authenticated and pass path and stickyOb as args', ()=>{

		state.auth.authenticated = true;
		let docId = '3245';	

		let stickyOb = {key: '234'};	 
		let action = updateSticky(docId, stickyOb);
		action(dispatch, getState);

		let path = `${state.auth.id}/templates/${docId}/stickys/${stickyOb.key}`;
		expect(Db.update).toHaveBeenCalled();
		expect(Db.update).toHaveBeenCalledWith(path, stickyOb);

	});

	it('should call Db addAndSetKey when user is authenticated and pass callabpath and stickyOb as args', ()=>{

		state.auth.authenticated = true;
		let docId = '3245';	
		state.collaborators['3245']= 'userId2';

		let stickyOb = {key: '234'};	 
		let action = updateSticky(docId, stickyOb);
		action(dispatch, getState);

		let path = `${state.collaborators['3245']}/templates/${docId}/stickys/${stickyOb.key}`;
		expect(Db.update).toHaveBeenCalled();
		expect(Db.update).toHaveBeenCalledWith(path, stickyOb);

	});
	
});


describe('Add Template Collaborator Func', () =>{

	beforeEach(()=>{
		state.auth.authenticated = null;
		spyOn(Db, 'update');
	});


	it('should call Db update 3 times when user is authenticated', ()=>{
		let collaborator ={
			id : '6767', 
			name: 'jason@example.com'
		};
		state.auth.authenticated = true;
		let docId = '3245';	

		let action = addTemplateCollaborator(docId, collaborator);
		action(dispatch, getState);

		expect(Db.update).toHaveBeenCalled();
		expect(Db.update).toHaveBeenCalledTimes(2);

	});
});


describe('Remove Template Collaborator Func', ()=>{

	beforeEach(()=>{
		state.auth.authenticated = null;
		spyOn(Db, 'deleteValue');
	});

	afterEach(()=>{
		state.auth.authenticated = null;		
	});

	it('should call Db deleteValue 2 times when user is authenticated', () =>{
		let collaborator ={
			id : '6767', 
			name: 'jason@example.com'
		};

		state.auth.authenticated = true;
		let docId = '3245';	

		let action = removeTemplateCollaborator(docId, collaborator);
		action(dispatch, getState);


		expect(Db.deleteValue).toHaveBeenCalled();
		expect(Db.deleteValue).toHaveBeenCalledTimes(2);
	});

});


describe('Share Template Func', ()=>{

	beforeEach(()=>{
		state.auth.authenticated = null;
		spyOn(Db, 'shareUrl');
	});

	afterEach(()=>{
		state.auth.authenticated = null;		
	});

	it('should call Db shareUrl when user is authenticated', ()=>{
		state.auth.authenticated = true;
		let docId = '3245';	
		let tplPath = `${state.auth.id}/templates/${docId}`;
		let type = 'public';
		let sharePath = `${type}Share`;

		let action = shareTemplate(docId, type);

		action(dispatch, getState);

		expect(Db.shareUrl).toHaveBeenCalled();
		expect(Db.shareUrl).toHaveBeenCalledWith(sharePath, tplPath, type);
	});

});


describe('Remove Share Func', ()=>{

	beforeEach(()=>{
		state.auth.authenticated = null;
		spyOn(Db, 'deleteValue');
	});

	afterEach(()=>{
		state.auth.authenticated = null;		
	});

	it('should call Db shareUrl when user is authenticated', ()=>{
		state.auth.authenticated = true;
		let docId = '3245';	
		let key = '7778';
		let type = 'public';
		let tplPath = `${state.auth.id}/templates/${docId}/${type}`;		
		let sharePath = `${type}Share/${key}`;

		let action = removeShare(docId, key, type);

		action(dispatch, getState);

		expect(Db.deleteValue).toHaveBeenCalled();
		expect(Db.deleteValue).toHaveBeenCalledTimes(2);
	});

});


describe('Delete Template Func', ()=>{

	beforeEach(()=>{
		state.auth.authenticated = null;
		spyOn(Db, 'deleteValue');
	});

	it('should call Db deleteValue with path as argument when user is authenticated', ()=>{
		state.auth.authenticated = true;
		let docId = '3245';	
		let path = `${state.auth.id}/templates/${docId}`;

		let action = deleteTemplate(docId);
		action(dispatch, getState);

		expect(Db.deleteValue).toHaveBeenCalled();
		expect(Db.deleteValue).toHaveBeenCalledWith(path);
	});

});


describe('Download Template', ()=>{
	//need to set up the async test
});





















