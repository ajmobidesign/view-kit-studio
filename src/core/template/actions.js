import {
	CREATE_TEMPLATE,
	UPDATE_TEMPLATE,
	LOAD_TEMPLATE,
	UNLOAD_TEMPLATE,
	UPDATE_STYLECONFIG,
	UPDATE_COMPONENTCONFIG,
	SAVING_TEMPLATE, 
	DEMO_NOTIFICATION
} from './action-types';


import { templateMap } from '../defaults';
import { Db } from '../firebase';
import axios from 'axios';
import { push } from 'react-router-redux';
import moment from 'moment';
import _ from 'lodash';
import { currentEv, localDb } from 'src/core/defaults';


export function checkLocalStorage(){

	console.log('check local storage called')

	return(dispatch, getState)=>{
		const { auth } = getState();
		let tplIdList = ['mdl_basic_html5', 'mdl_single_page', 'mdl_style_guide'];

		if(auth.authenticated){
			let id = auth.id;
			let path = `${id}/templates`;

			tplIdList.forEach((t)=>{
				let tpl = localDb.getItemValue(t)

				if(tpl){

					let tOb = JSON.parse(tpl);
					tOb.created = moment().format();
					tOb.demoNotification = null;

					Db.addAndSetKey(path, tOb);

					localDb.removeItemAndValue(t);

				}
			})
		}
	}	
}


export function createViewOnlyTemplate(tplId){

	//console.log('view only', tplId)
	
	const template = templateMap[tplId];
	return {type: CREATE_TEMPLATE, 
		payload: template
	};
}

export function createNewTemplate(tplId){
	//console.log('create template', tplId)
	return(dispatch, getState)=>{
		const { auth } = getState();
		const template = templateMap[tplId]

		if(auth.authenticated){
			let id = auth.id;
			let path = `${id}/templates`;

			template.created = moment().format();
			template.owner = auth.displayName;
			Db.addTemplate(path, template, dispatch);
		}
		else{
			//not able to unit test this appropriately
			dispatch(createViewOnlyTemplate(tplId))
			//this should be the template chosen 
			let kName = _.replace(tplId , RegExp("_","g"), '-')
			dispatch(push(`/try-a-kit/${kName}`))		
		}
	}
}

export function changeRoute(route){
	return dispatch =>{
		dispatch(push(route))
	}
}

export function loadTemplate(docId){
	return(dispatch, getState) =>{
		const { auth } = getState();
		if(auth.authenticated){
			let id = auth.id;
			let path = `${id}/templates/${docId}`;
			let alreadySubscribing = Db.alreadySubscribing(path);

			if(!alreadySubscribing){
				Db.subscribe(path, dispatch, LOAD_TEMPLATE, UNLOAD_TEMPLATE);
			}

		}
	}
}

export function loadShareView(sharePath){
	return(dispatch, getState) =>{
		
			Db.checkData(sharePath)
			.then((ob)=>{
				let path = ob.path
				Db.subscribe(path, dispatch, LOAD_TEMPLATE, UNLOAD_TEMPLATE)
			})			
	}
}


export function loadCollabTemplate(docId){

	return(dispatch, getState) =>{
		const { auth } = getState();
		if(auth.authenticated){
			let id = auth.id
			let path = `${id}/templates/${docId}/path`
	
			Db.checkData(path)
			.then((val)=>{
				
				if(val){
					let cid = val.split('/')[0] //need to check

					let ob = {}
					ob[docId] = cid

					dispatch({type: 'SET_COLLAB_TEMPLATE', payload: ob})

					Db.subscribe(val, dispatch, LOAD_TEMPLATE, 'REMOVE_COLLAB_TEMPLATE')
				}
			})
		}
	}
}


export function unloadTemplate(docId){
	return(dispatch, getState) =>{
		const { auth, collaborators } = getState();		
		const isASharedTemplate = collaborators[docId];

		if(auth.authenticated){
			let id = auth.id;
			let p = `${id}/templates/${docId}`;
			let s = `${isASharedTemplate}/templates/${docId}`;
			let path = (isASharedTemplate) ? s : p;

			//test stuff
			let isShared = (isASharedTemplate) ? true: false;

			//console.log('Is Shared', isASharedTemplate);
			//console.log('I am going to unSubscribe')
			//only unsubscribe if it is not a shared template
			if(!isShared){
				Db.unSubscribe(path, dispatch);
			}
		}
	}
}




export function updateTemplate(template, docId){
	return(dispatch, getState)=>{
		
		const { auth, collaborators } = getState();
		const isASharedTemplate = collaborators[docId];
	
		if(auth.authenticated){
			let id = auth.id
			let p = `${id}/templates/${docId}`
			let s = `${isASharedTemplate}/templates/${docId}`
			let path = (isASharedTemplate) ? s : p;

			Db.setValue(path, template)

			dispatch(savingTemplate(true))
		}
	}
}



export function updateStyleConfig(styleOb, docId){
	
	return(dispatch, getState)=>{
		const { auth, collaborators, template} = getState();
		const isASharedTemplate = collaborators[docId];

		if(auth.authenticated){
			let id = auth.id
			let p = `${id}/templates/${docId}/styleConfig`
			let s = `${isASharedTemplate}/templates/${docId}/styleConfig`
			let path = (isASharedTemplate) ? s : p;

			Db.update(path, styleOb)
			
			dispatch(savingTemplate(true))
		}
		else{
			dispatch({type: UPDATE_STYLECONFIG, payload: styleOb })

			let newTemplateVal = JSON.stringify( Object.assign({}, template, {styleConfig: styleOb}) )

			let isNew = localDb.initState;
			let seenIt = localDb.seenItState;
			let tplId = template.tplId

			localDb.setItemValue(tplId, newTemplateVal)


			if(isNew && !seenIt){
				dispatch({type: DEMO_NOTIFICATION, payload: isNew })
			}
		}
	}
}

export function updateComponentConfig(componentOb, docId){
	//console.log(componentOb, docId)
	return(dispatch, getState)=>{
	
		const { auth, collaborators } = getState();
		const isASharedTemplate = collaborators[docId];

		if(auth.authenticated){

			let id = auth.id
			let p = `${id}/templates/${docId}/componentConfig`
			let s = `${isASharedTemplate}/templates/${docId}/componentConfig`
			let path = (isASharedTemplate) ? s : p;

			Db.update(path, componentOb)

			dispatch(savingTemplate(true))
			
		}
		else{
			dispatch({type: UPDATE_COMPONENTCONFIG, payload: componentOb })
			
			let newTemplateVal = JSON.stringify( Object.assign({}, template, {styleConfig: styleOb}) )

			let isNew = localDb.initState;
			let seenIt = localDb.seenItState;
			let tplId = template.tplId

			localDb.setItemValue(tplId, newTemplateVal)



			if(isNew && !seenIt){
				dispatch({type: DEMO_NOTIFICATION, payload: isNew })
			}
		}
	}
}

export function addSticky(docId, stickyOb){

	return(dispatch, getState)=>{
		const { auth, collaborators } = getState();
		const isASharedTemplate = collaborators[docId];

		if(auth.authenticated){
			let id = auth.id;

			let p = `${id}/templates/${docId}/stickys`;
			let s = `${isASharedTemplate}/templates/${docId}/stickys`;
			let path = (isASharedTemplate) ? s : p;

			stickyOb['authorId'] = id;
			stickyOb['author'] = auth.displayName;

			Db.addAndSetKey(path, stickyOb)

			dispatch(savingTemplate(true))
		}
	}			
}

export function removeSticky(docId, stickyId){
	
	//console.log('remove sticky called', docId, stickyId)
	return(dispatch, getState)=>{
		const { auth, collaborators } = getState();
		const isASharedTemplate = collaborators[docId];

		if(auth.authenticated){
			let id = auth.id;
			let p = `${id}/templates/${docId}/stickys/${stickyId}`;
			let s = `${isASharedTemplate}/templates/${docId}/stickys/${stickyId}`;
			let path = (isASharedTemplate) ? s : p;

			//console.log(isASharedTemplate, path, stickyId)

			Db.deleteValue(path);

			dispatch(savingTemplate(true))
		}
	}			
}


export function updateSticky(docId, stickyOb){
	
	//console.log('update sticky', docId, stickyOb)
	return(dispatch, getState)=>{
		const { auth, collaborators } = getState();
		const isASharedTemplate = collaborators[docId];

		if(auth.authenticated){
			let id = auth.id;
			let stickyId = stickyOb.key

			let p = `${id}/templates/${docId}/stickys/${stickyId}`;
			let s = `${isASharedTemplate}/templates/${docId}/stickys/${stickyId}`;
			let path = (isASharedTemplate) ? s : p;

			//console.log('path', path)

			//console.log(path, isASharedTemplate, stickyOb)
			Db.update(path,  stickyOb);

			dispatch(savingTemplate(true))
		}
	}			
}


export function addTemplateCollaborator(docId, collaborator){

	//console.log(docId, collaborator)
	return(dispatch, getState)=>{
		const { auth } = getState();
		if(auth.authenticated){
			let id = auth.id;
			let name = auth.displayName;
			let myPath = `${id}/templates/${docId}`;

			let myCollabPath = `${id}/templates/${docId}/collaborators`;

			let collabPath = `${collaborator.id}/templates/${docId}`;

			let collabUser = {}
			collabUser[collaborator.id] = collaborator.name;



			//add the user to my list of collaborators
			Db.update(myCollabPath, collabUser);

			let collabTrackOb={
				user: id,
				collaborator: collaborator.id,
				action: 'added',
				time: moment().format()
			};


			Db.addAndTrackCollaborator(collabTrackOb);

			let sharedRefOb = {
				isCopyOf: true,
				path: myPath
			};
			
			Db.update(collabPath, sharedRefOb);
			//also need to add notification


			let shareNotification = {
				title: `Template Access Granted`,
				content: `${name} has shared a template with you `,
				link: `/shared-templates/${docId}`,
				date: moment().format()
			}

			let notificationPath = `${collaborator.id}/profile/notifications`;

			Db.addAndGetKey(notificationPath, shareNotification)

			dispatch(savingTemplate(true))

		}
	}	
}

export function removeTemplateCollaborator(docId, collaborator){
	return(dispatch, getState)=>{
		const { auth } = getState();
		if(auth.authenticated){
			let id = auth.id;
			let name = auth.displayName;
			let myCollabPath = `${id}/templates/${docId}/collaborators/${collaborator.id}`;
			let collabPath = `${collaborator.id}/templates/${docId}`;

			Db.deleteValue(myCollabPath);
			Db.deleteValue(collabPath);

			let shareNotification = {
				title: `Template Access Disabled`,
				content: `${name} has disabled your access to a template `,
				link: `/shared-templates/${docId}`,
				date: moment().format()
			}

			let collabTrackOb={
				user: id,
				collaborator: collaborator.id,
				action: 'removed',
				time: moment().format()
			};


			Db.addAndTrackCollaborator(collabTrackOb);


			let notificationPath = `${collaborator.id}/profile/notifications`;

			Db.addAndGetKey(notificationPath, shareNotification)

			dispatch(savingTemplate(true))

		}
	}
}


export function shareTemplate(docId, type){
	return(dispatch, getState)=>{
		const { auth } = getState();
		if(auth.authenticated){
			let id = auth.id
			let tplPath = `${id}/templates/${docId}`;
			let sharePath = `${type}Share`;

			Db.shareUrl(sharePath, tplPath, type);

			dispatch(savingTemplate(true))
		}
	}	
}

export function removeShare(docId, key, type){
	return(dispatch, getState)=>{
		const { auth } = getState();
		if(auth.authenticated){
			let id = auth.id
			let sharePath = `${type}Share/${key}`;
			let tplPath = `${id}/templates/${docId}/${type}`;

			Db.deleteValue(sharePath);
			Db.deleteValue(tplPath);

			dispatch(savingTemplate(true))
		}
	}		
}


export function deleteTemplate(docId){
	return(dispatch, getState)=>{
		const { auth } = getState();
		if(auth.authenticated){
			let id = auth.id
			let path = `${id}/templates/${docId}`;

			Db.deleteValue(path);
		}
	}	
}

export function downloadTemplate(template, downloadFnc, errorFnc){

	//console.log('download template')

	return(dispatch, getState)=>{
		const { auth } = getState();
		if(auth.authenticated){
			let id = auth.id
			let pathFrag = 'api/download';
			
			let path = `${currentEv.server}/${pathFrag}`;

			template.user = id

			axios.post(path, template)
			  .then(function (response) {
			    
			  	//console.log('API response', response)
			    downloadFnc(response)
			    
			  })
			  .catch(function (error) {
			    console.log(error);
			    errorFnc()
			  });  
		}
	}	
}


export function savingTemplate(currentState){

	return(dispatch, getState)=>{
		const { auth } = getState();
		if(auth.authenticated){
			//let payload = (currentState) ? true : null;
			
			if(currentState){
				dispatch({type: SAVING_TEMPLATE, payload: true});

				setTimeout(()=>{
					dispatch({type: SAVING_TEMPLATE, payload: null});
				}, 800)
			}
		}
	}	
}






