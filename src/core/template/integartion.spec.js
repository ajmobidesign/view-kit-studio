import {
	CREATE_TEMPLATE,
	UPDATE_TEMPLATE,
	LOAD_TEMPLATE,
	UNLOAD_TEMPLATE,
	UPDATE_STYLECONFIG,
	UPDATE_COMPONENTCONFIG
} from './action-types';

import { templateMap } from 'src/core/defaults';
import { Db } from 'src/core/firebase';
import axios from 'axios';
import { push } from 'react-router-redux';
import { replace, lowerCase} from 'lodash'; 
import moment from 'moment';

import { createViewOnlyTemplate, createNewTemplate, loadTemplate, loadShareView, loadCollabTemplate, unloadTemplate, updateTemplate, updateStyleConfig, updateComponentConfig, addSticky, removeSticky, updateSticky, addTemplateCollaborator, removeTemplateCollaborator, shareTemplate, removeShare, deleteTemplate, downloadTemplate } from './actions';