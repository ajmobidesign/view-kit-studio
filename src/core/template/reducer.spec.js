import {
	CREATE_TEMPLATE,
	UPDATE_TEMPLATE,
	LOAD_TEMPLATE,
	UNLOAD_TEMPLATE,
	UPDATE_STYLECONFIG,
	UPDATE_COMPONENTCONFIG
} from './action-types';

import { templateReducer } from './reducer';


describe('Template Reducer Function', () =>{

	describe('CREATE_TEMPLATE', ()=>{

		it('should set state to action payload', ()=>{
			let action = {
				 type: CREATE_TEMPLATE,
				 payload: {test: 'I am test'}
				};
			let state = templateReducer({}, action);

			expect(state).toEqual(action.payload);
		});

	});

	describe('UPDATE_TEMPLATE', ()=>{
		it('should update state with new action payload', ()=>{
			let action = {
				 type: UPDATE_TEMPLATE,
				 payload: {test: 'I am test2'}
				};
			let startState = {test: 'I am test'};
			
			let state = templateReducer(startState, action);

			expect(state).toEqual(action.payload);	

		});
	});

	describe('LOAD_TEMPLATE', ()=>{
		it('should set state to action payload', ()=>{
			let action = {
				 type: LOAD_TEMPLATE,
				 payload: {test: 'I am test'}
				};
			let state = templateReducer({}, action);

			expect(state).toEqual(action.payload);
		});
	});

	describe('UNLOAD_TEMPLATE', ()=>{
		it('should set state to empty object', ()=>{
			let action = {
				 type: UNLOAD_TEMPLATE
				};
			let startState = {test: 'I am test'};
				
			let state = templateReducer(startState, action);

			expect(state).toEqual({});
		});
	});

	describe('UPDATE_STYLECONFIG', ()=>{
		it('should set state.styleConfig to action payload', ()=>{
			let action = {
				 type: UPDATE_STYLECONFIG, 
				 payload: {style: 'I am a style Object'}
				};
			let startState = {test: 'I am test'};	
			let state = templateReducer(startState, action);

			expect(state.styleConfig).toEqual(action.payload)

		});
	});

	describe('UPDATE_COMPONENTCONFIG', ()=>{
		it('should set state.componentConfig to action payload', ()=>{
			let action = {
				 type: UPDATE_COMPONENTCONFIG, 
				 payload: {style: 'I am a component object'}
				};
			let startState = {test: 'I am test'};	
			let state = templateReducer(startState, action);

			expect(state.componentConfig).toEqual(action.payload)

		});
	});

})