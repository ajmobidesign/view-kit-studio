import * as templateActions from './actions';


export { templateActions };
export * from './action-types';
export { templateReducer } from './reducer';