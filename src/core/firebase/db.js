import { firebaseDb } from './firebase';
import { Record } from 'immutable';
import moment from 'moment';
import _ from 'lodash';




export class AppDb {
	constructor(actions){
		this._actions = actions;
		this._subscriberList = [];
		this._adminRef = (path)=>{return firebaseDb.ref('adminDb/'+ path)};
		this._appRef = (path)=>{return firebaseDb.ref('appDb/'+ path)};
		this._processSharedListData = (sharedTpl, path)=>{
				
				sharedTpl['isCopyOf'] = true;
				sharedTpl['path'] = path;
				sharedTpl['collabList'] = Object.keys(sharedTpl.collaborators).map((k)=>{

						let cName = sharedTpl.collaborators[k]; 
						let cid = k

					return 	{name: cName, id: cid}
				})
			return sharedTpl
		}
	}

	alreadySubscribing = (path)=>{
		return this._subscriberList.filter((o)=>{ return o.path === path}).length === 1;
	}

	update(path, data) {
		let Ref = this._appRef(path);
		return Ref.update(data);
	}

	setValue(path, data){
		let Ref = this._appRef(path);
		return Ref.set(data)
	}

	deleteValue(path){
		let Ref = this._appRef(path);
		return Ref.remove()
	}


	flagDelete(path){
		let Ref = this._appRef(path);
		return Ref.update({delete: true})
	}

	addAndGetKey(path, data){
		let Ref = this._appRef(path)
		
		return Ref.push(data)
				.then((result)=>{
					return result.key;
				});
	}

	addAndSetKey(path, data){
		let Ref = this._appRef(path);

		return Ref.push(data)
			.then((result)=>{
				return result.key;
			}, (error)=>{
				return error;
			})
			.then((key)=>{
				let ob = {key: key};
				return Ref.child(key).update(ob)
			});	
	}

	checkData(path){
				//console.log(path)
		let Ref = this._appRef(path);

		return Ref
			.once('value')
			.then((snapshot)=>{	
				//console.log(snapshot.val())
				return snapshot.val();
			})
	}



	addTemplate(path, data, dispatch){

		let Ref = this._appRef(path);
		let newItem = Ref.push(data);
		let key = newItem.key;

		return Ref.child(key+'/key' )
		.set(key)
		.then(()=>{
			return newItem.once('value');
		})
		.then((snapshot)=>{
			let dt = snapshot.val();
			let aOb = {type: 'CREATE_TEMPLATE', payload: dt};
			let url = `/templates/${key}`;

			dispatch(aOb);
			dispatch(this._actions.loadUrl(url));
		});
	}


	globallNotificationsSubscribe(path, dispatch, subscribeAction, unSubscribeAction){
		
		let subscriberList = this._subscriberList;
		let Ref = this._appRef(path);

		Ref
		.on('value', (snapshot)=>{
			let val = snapshot.val();
			let alreadySubscribing = this.alreadySubscribing(path);

				dispatch(subscribeAction(val))
				if(!alreadySubscribing){
					subscriberList.push({path: path, unSubscribeAction: unSubscribeAction});
				}
		});		
	}


	templateListSubscribe(path, dispatch, subscribeAction, unSubscribeAction, action){

		let subscriberList = this._subscriberList;
		let Ref = this._appRef(path);

		Ref
		.on('value', (snapshot)=>{
			let val = snapshot.val();
			let alreadySubscribing = this.alreadySubscribing(path);

				dispatch({type: subscribeAction, payload: snapshot.val()});
				dispatch(action(snapshot.val()));

				if(!alreadySubscribing){
					subscriberList.push({path: path, unSubscribeAction: unSubscribeAction});
				}
		});
	}


	subscribe(path, dispatch, subscribeAction, unSubscribeAction){

		let subscriberList = this._subscriberList;
		let Ref = this._appRef(path);

		Ref
		.on('value', (snapshot)=>{
			let val = snapshot.val();
			let alreadySubscribing = this.alreadySubscribing(path);

				dispatch({type: subscribeAction, payload: snapshot.val()});
				
				if(!alreadySubscribing){
					subscriberList.push({path: path, unSubscribeAction: unSubscribeAction});
				}
		})	

		//console.log(path, subscribeAction, unSubscribeAction, subscriberList)	
	}


	unSubscribe(path, dispatch){

		//console.log('Unsbscribe is subscribing', path, this.alreadySubscribing(path), this._subscriberList)
		
		let newSubscriberList = this._subscriberList.filter((ob, idx)=>{
			if(ob.path==path) {
				dispatch({type: this._subscriberList[idx].unSubscribeAction})
			};
			return ob.path !== path;
		})

		let Ref = this._appRef(path);
		Ref.off()	
		this._subscriberList = newSubscriberList;

	}

	unSubscribeAll(dispatch){
		//need to perform dispatch
		//console.log('unSubscribeAll', this._subscriberList)
		let ln = this._subscriberList.length

		this._subscriberList.forEach((ob, idx)=>{

			//if it is the user path 

			//let userPath = UNLOAD_USER			

			let Ref = (ob.unSubscribeAction === 'UNLOAD_USER') ? this._appRef(ob.path) : this._adminRef(ob.path);


			Ref.off();
			dispatch({type: this._subscriberList[idx].unSubscribeAction})

			if(ln-1 === idx){
				this._subscriberList = [];
			}
		})

	}




	//need to refector to make the flow faster
	newTransaction(path, data){
		data.status = 'Started'
		let Ref = this._appRef(path)
		return Ref.push(data)
				.then((result)=>{
					return result.key;
				});
	}


	sharedTemplateListSubscribe(path, dispatch, subscribeAction, unSubscribeAction){

		let subscriberList = this._subscriberList;
		let Ref = this._appRef(path);

		let processData = this._processSharedListData;

		Ref
		.on('value', (snapshot)=>{
			let val = snapshot.val();
			let alreadySubscribing = this.alreadySubscribing(path);

				dispatch({type: subscribeAction, payload: processData(val, path)})
				if(!alreadySubscribing){
					subscriberList.push({path: path, unSubscribeAction: unSubscribeAction})
				}
		})

		//console.log(path, subscribeAction, unSubscribeAction, subscriberList)	
	}

	getSharedTemplate(path, dispatch, subscribeAction){
		let Ref = this._appRef(path);
		let processData = this._processSharedListData;

		Ref.once('value', (snapshot)=>{
			let val = snapshot.val();
			dispatch({type: subscribeAction, payload: processData(val, path)});
		})
	}


	shareUrl(sharePath, templatePath, type){
		let shareRef = this._appRef(sharePath)
		let tplRef = this._appRef(templatePath)

		let ob = {};
		
		shareRef.push({path: templatePath})
		.then((result)=>{
			let k = result.key;
			ob[type] = k;
			tplRef.update(ob);
		})
		.catch((error)=>{
			console.log(error);
		})
	}


	//Admin Stuff

	searchUserList(query, dispatch, owner){
		
		let Ref = this._adminRef('users');

		Ref.once('value')
		.then(snapshot=>{
			let v = snapshot.val();

			let userListOb = Object.keys(v).map((k)=>{
				return {id:k , name : v[k].email }
			})

			let filteredList = userListOb.filter((u)=>{
				return (u.name === query && u.name !== owner)
				//return (u.name.indexOf(query) === 0 && u.name !== owner)
			})

			return (query.length>3) ? filteredList : [];

		})
		.then((filteredList)=>{			
			dispatch({type: 'LOAD_USER_LIST', payload: filteredList})
		});
	}



	subscribeUser(path, dispatch, subscribeAction, unSubscribeAction){


		let subscriberList = this._subscriberList;
		let Ref = this._adminRef(path)

		Ref
		.on('value', (snapshot)=>{
			let val = snapshot.val();
			let alreadySubscribing = this.alreadySubscribing(path);

			dispatch({type: subscribeAction, payload: snapshot.val()});

			if(!alreadySubscribing){
				subscriberList.push({path: path, unSubscribeAction: unSubscribeAction});
			}
		});	

		//console.log(path, subscribeAction, unSubscribeAction, subscriberList)		
	}



	addAndTrackUser(result){

		let user = {
					name: result.displayName, 
					email: result.email,
					emailVerified: result.emailVerified,
					created: moment().format(),
					id: result.uid };
	
		let Ref = this._adminRef('users/' + result.uid );
		let log = {
				letoggedInAt: moment().format(), 
				user: result.uid
			};

		Ref.once('value')
		.then((snapshot)=>{

			let itExists = snapshot.exists();
			let logPath = this._adminRef('userLogs');
			
			if(itExists){
				logPath.push(log)
			}else{

				let analyticsOb = { 'event' : 'newUserAdded' };

				dataLayer.push(analyticsOb);

				Ref.set(user, ()=>{
					logPath.push(log);
				});
			}
		})
		.catch((error)=>{
			console.log(error)
		})
	}

	trackLogOut(id){

		let Ref = this._adminRef('userLogs');
		let log = {
				loggedOutAt: moment().format(),
				user: id
			};

		Ref.push(log)
		.catch((error)=>{
			console.log(error)
		});
	}

	addAndTrackCollaborator(collabTrackOb){
		let Ref = this._adminRef('collaborations');

		Ref.push(collabTrackOb)
		.catch((error)=>{
			console.log(error)
		});
	}


	submitBug(data){
		let Ref = this._adminRef('bugReports')

		Ref.push(data)
		.catch((error)=>{
			console.log(error)
		});
	}

}