import moment from 'moment';
import { Db } from './index';
import sinon from 'sinon';
require('sinon-as-promised');





const testPath = 'test';


describe('Firebase Connection Fuction', ()=>{
	
	afterAll(()=>{
		Db._subscriberList =[];
	});

	describe('Subscriber List Check', ()=>{

		it('should return false if the path is not in the list ', ()=>{

			Db._subscriberList =[];

			let path = 'some/unique/path';
			
			let alreadySubscribing = Db.alreadySubscribing(path);			
			expect(alreadySubscribing).toBe(false);
		});

		it('should return true if the path is in the list', ()=>{

			Db._subscriberList =[{path: 'some/unique/path'}];

			let path = 'some/unique/path';

			let alreadySubscribing = Db.alreadySubscribing(path);
			expect(alreadySubscribing).toBe(true);
		});
	});


/*
	describe('Update method', ()=>{
		const path = 'some/unique/path';
		const refRep = Db._appRef(path);

		beforeEach(()=>{			
			spyOn(refRep, 'update');
		});

		it('should call the ref update', ()=>{
						
			let data = {test: 'test data'};

			Db.update(path, data);

			expect(refRep.update).toHaveBeenCalled();
			expect(refRep.update).toHaveBeenCalledWith(path);
		});
	});

	describe('Set Value method', ()=>{
		const path = 'some/unique/path';
		const refRep = Db._appRef(path);

		beforeEach(()=>{
			spyOn(refRep, 'set');
		});

		it('should call the ref set', ()=>{
			
			let data = {test: 'test data'};

			Db.setValue(path, data);

			expect(refRep.set).toHaveBeenCalled();
			expect(refRep.set).toHaveBeenCalledWith(path);
		});
	});

	describe('Delete Value method', ()=>{
		const path = 'some/unique/path';
		const refRep = Db._appRef(path);

		beforeEach(()=>{
			spyOn(refRep, 'remove');
		});

		it('should call the ref update', ()=>{
			
			let data = {test: 'test data'};
			Db.deleteValue(path, data);

			expect(refRep.remove).toHaveBeenCalled();			
		});
	});

	describe('Flag Delete method', ()=>{
		const path = 'some/unique/path';
		const refRep = Db._appRef(path);

		beforeEach(()=>{
			spyOn(refRep, 'update');
		});

		it('should call the ref update', ()=>{
			let path = 'some/unique/path';
			let refRep = Db._appRef(path);
			let dl = {delete: true};

			Db.flagDelete(path);

			expect(refRep.update).toHaveBeenCalled();
			expect(refRep.update).toHaveBeenCalledWith(dl);		
		});
	});

*/

/*
	describe('Add and Get Key', ()=>{
		deforeEach(()=>{
			spyOn(Db, 'addAndGetKey')
		});

		it('should return promise', ()=>{
			let path = 'some/unique/path';
			let refRep = Db._appRef(path);
			let data = {test: 'test data'};

			Db.deleteValue(path, data);

			expect(refRep.remove).toHaveBeenCalled();			
		});
	});*/

	describe('Add and Set Key', ()=>{

	});

	describe('Check Data', ()=>{

	});
});




/*
const DbTest = ()=>{
	console.log('Db Test called')
	let subscribeAction = 'SUBSCRIBE';
	let unSubscribeAction = 'UNSUBSCRIBE';


	const addData = ()=>{
		let data = {parent: 'I am a test frag'};
		Db.setValue(testPath, data);

		console.log('Subscribe Test', Db._subscriberList)
	};

	const addChildData = ()=>{
		let cData = {baby: 'I am child frag'}
		Db.setValue(`${testPath}/parent`, cData);
	};

	const removeData = ()=>{
		Db.deleteValue(testPath)
	}


	const subscribeTest = ()=>{
		let dispatch = (action)=>{
			console.log('action', action);
			//console.log('Subscribe Test', Db._subscriberList)
		}

		Db.subscribe(testPath, dispatch, subscribeAction, unSubscribeAction);
		
	};

	const unSubscribe = ()=>{
		let dispatch = (action)=>{
			console.log('action', action);
			
		}

		Db.unSubscribe(testPath, dispatch);
	}

	const subscribeFunc = setTimeout(()=>{

		subscribeTest();
		clearTimeout(subscribeFunc);
	}, 100);


	const addDataFunc = setTimeout(()=>{

		addData();
		clearTimeout(addDataFunc)

	}, 500);


	const addChildDataFunc = setTimeout(()=>{

		addChildData();
		clearTimeout(addChildDataFunc)

	}, 1000);


	const removeDataFunc = setTimeout(()=>{

		removeData();
		clearTimeout(removeDataFunc);

	}, 1300);


	const unSubscribeFunc = setTimeout(()=>{

		unSubscribe();
		clearTimeout(unSubscribeFunc);
	}, 2000);


	const logList = setTimeout(()=>{
		console.log('Unsubscribe Test', Db._subscriberList)
	}, 2300);
	

	const addDataFunc2 = setTimeout(()=>{

		console.log('add data called second time')
		addData();
		clearTimeout(addDataFunc2)

	}, 2800);


};


export default DbTest;
*/

