import {  
	LOAD_USER,
	UNLOAD_USER
} from './action-types';


import { Db } from 'src/core/firebase';

import { getUser, unloadUser } from './actions';

const state = {
	auth:{
		id: '123',
		authenticated: null
	}
};


const getState = function (){
	return state;
};

const dispatch = {};


describe('Get User Action', ()=>{

	beforeEach(()=>{
		
		spyOn(Db, 'subscribeUser')
	});

	afterEach(()=>{
		state.auth.authenticated = null;
	});

	it('should call Db update when user is authenticated', ()=>{
		state.auth.authenticated = true;
		let path = `users/${state.auth.id}`;
		let action = getUser();
		action(dispatch, getState);

		expect(Db.subscribeUser).toHaveBeenCalled();
		expect(Db.subscribeUser).toHaveBeenCalledWith(path, dispatch, LOAD_USER, UNLOAD_USER);

	});
});



describe('Unload User Action', ()=>{

	beforeEach(()=>{
		
		spyOn(Db, 'unSubscribe');
	});

	afterEach(()=>{
		state.auth.authenticated = null;
	});

	it('should call Db update when user is authenticated', ()=>{
		state.auth.authenticated = true;
		let path = `users/${state.auth.id}`;

		let action = unloadUser();
		action(dispatch, getState);


		expect(Db.unSubscribe).toHaveBeenCalled();
		expect(Db.unSubscribe).toHaveBeenCalledWith(path, dispatch);

	});

});