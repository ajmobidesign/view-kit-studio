import {  
	LOAD_USER,
	UNLOAD_USER
} from './action-types';


import { Db } from 'src/core/firebase';

export function getUser(){
	return(dispatch, getState) =>{
		const { auth } = getState();
		if(auth.authenticated){
			//console.log('user called', auth.id)
			let id = auth.id;
			let path = `users/${id}`;

			let pathList = Db._subscriberList.map((l)=>{ return l.path });

			let notYetSubscribing = (Db._subscriberList.map((l)=>{ return l.path }).indexOf(path) ==-1);

			//console.log(pathList, notYetSubscribing)

			if(notYetSubscribing){
				//At some point this has be replaced
				//dispatch({type: LOAD_USER, payload: {user: 'placeholder'}})

				//Db.getUserData(path, dispatch)
				Db.subscribeUser(path, dispatch, LOAD_USER, UNLOAD_USER)
			}
		}
	}
}


export function unloadUser(){
	return(dispatch, getState) =>{
		const { auth } = getState();
		if(auth.authenticated){
			let id = auth.id;
			let path = `users/${id}`;
			//this is the wrong unsubscribe --- the path prefix is admin
			Db.unSubscribe(path, dispatch)

		}
	}
}