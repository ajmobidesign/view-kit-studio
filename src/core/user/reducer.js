import {  
	LOAD_USER,
	UNLOAD_USER
} from './action-types';

import moment from 'moment';



export function userReducer(state= {}, action){

	switch(action.type){

		case LOAD_USER:

				//check if user is has created property;

				let user = action.payload

			return Object.assign({}, state, user);

		case UNLOAD_USER:

			return {};

		default:
			return state;

	}

}