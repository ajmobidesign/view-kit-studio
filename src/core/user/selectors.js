import moment from 'moment';


export function newUserPromo(user){
	return (user.created) ? moment(user.created).add(1, 'days') : null;
}


export function userIsNew(user){

		let today = moment().format();
		let start = newUserPromo(user);
		
 	return (moment(today).isBefore(start))? true: false;	 			
}


