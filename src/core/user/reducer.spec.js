import {  
	LOAD_USER,
	UNLOAD_USER
} from './action-types';

import { userReducer } from './reducer';

describe('User Reducer', ()=>{
	describe('LOAD_USER', ()=>{
		it('should set state to action payload', ()=>{
			let action = {type: LOAD_USER, payload: { name: 'John'}}
			let state = userReducer(undefined, action);

			expect(state).toEqual(action.payload);
		});
	});

	describe('UNLOAD_USER', ()=>{
		it('should set state to empty object', ()=>{
			let action = {type: UNLOAD_USER}
			let currState = { name: 'John'};
			let state = userReducer(currState, action);

			expect(state).toEqual({});
		});
	});

});