import { newUserPromo, userIsNew } from './selectors';
import moment from 'moment';


const mockUser1 = {
	name: 'John',
	created : '2016-12-28T12:37:13-08:00' 
};

const mockUser2 = {
	name: 'David'
};


describe('New User Promo Function', ()=>{
	
	it('should add a day to created date if user has a created timestamp', ()=>{

		let promo = newUserPromo(mockUser1);
		let fPromo = moment(promo).format();

		expect(fPromo).toBe('2016-12-29T12:37:13-08:00');

	});

	it('should return null if user does not have created date', ()=>{

		let promo = newUserPromo(mockUser2);

		expect(promo).toBe(null)

	});

}); 



describe('User Is New Function', () =>{
	it('should return true or false based on when user was created', ()=>{

		let today = moment().format();
		let start = moment(mockUser1.created).add(1, 'day');

		let condition = moment(today).isBefore(start);

		let newUser = userIsNew(mockUser1);

		expect(newUser).toBe(condition);
	});
});