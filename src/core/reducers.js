import { routerReducer } from 'react-router-redux';
import { combineReducers } from 'redux';
import { authReducer } from './auth';
import { templateReducer }  from './template';
import { profileReducer }  from './profile';
import { transactionReducer } from './transaction';
import { collaboratorsReducer } from './collaborators';
import { templateListReducer } from './templateList';
import { userReducer } from './user';


export default combineReducers({	
  auth: authReducer,
  routing: routerReducer,
  template: templateReducer, 
  profile: profileReducer, 
  transaction: transactionReducer, 
  collaborators: collaboratorsReducer, 
  templateList: templateListReducer,
  user: userReducer
});
