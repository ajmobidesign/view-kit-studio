import {
	SET_CURRENT_COLLABORATOR,
	REMOVE_CURRENT_COLLABORATOR,
	LOAD_USER_LIST,
	UNLOAD_USER_LIST,
	SET_COLLAB_TEMPLATE, 
	REMOVE_COLLAB_TEMPLATE
} from './action-types';

import { Db } from 'src/core/firebase';

export function setCurrentCollaborator(id){
	return {type: SET_CURRENT_COLLABORATOR, payload: id}
}


export function removeCurrentCollaborator(){
	return { type: REMOVE_CURRENT_COLLABORATOR}
}


export function removeCollabTemplate(docId){
	return {type: REMOVE_COLLAB_TEMPLATE, payload: docId}
}


export function searchUserList(query){
	return(dispatch, getState)=>{
		const { auth } = getState();
		
		if(auth.authenticated){
			Db.searchUserList(query, dispatch, auth.displayName);
		}
	}	
}


export function clearUserList(){
	return {
		type: UNLOAD_USER_LIST
	}
}





