import {
	SET_CURRENT_COLLABORATOR,
	REMOVE_CURRENT_COLLABORATOR,
	LOAD_USER_LIST,
	UNLOAD_USER_LIST,
	SET_COLLAB_TEMPLATE, 
	REMOVE_COLLAB_TEMPLATE
} from './action-types';


import { collaboratorsReducer } from './reducer';

const startState =  Object.assign({}, {collaborators:[], current: null, query: []})


describe('Collaborators Reducer', () =>{
	describe('SET_CURRENT_COLLABORATOR', () =>{
		it('should set collabState.current to the payload', ()=>{
			let state = collaboratorsReducer(undefined, {
				type: SET_CURRENT_COLLABORATOR, 
				payload: '1234'
			});
			expect(state.current).toBe('1234');
		});
		
	});

	describe('REMOVE_CURRENT_COLLABORATOR', () =>{
		it('should set collabState.current to null ', () =>{
			let currState = Object.assign({}, startState, {current: '123'})
			let state = collaboratorsReducer(currState, {
				type: REMOVE_CURRENT_COLLABORATOR
			});

			expect(state.current).toBeNull();
		});
	});

	describe('LOAD_USER_LIST', ()=>{
		it('should set collabState.query to the payload', () =>{
			let pld = [{id: '123', name: 'david@example.com'}, {id: '345', name: 'mindy@example.com'}]
			let state = collaboratorsReducer(undefined, {
				type: LOAD_USER_LIST, 
				payload: pld
			});

			expect(state.query).toBe(pld);
		});
	});

	describe('UNLOAD_USER_LIST', () =>{
		it('should set collabState.query to an empty list', () =>{
			let pld = [{id: '123', name: 'david@example.com'}, {id: '345', name: 'mindy@example.com'}];
			let currState = Object.assign({}, startState, {query : pld})
			let state = collaboratorsReducer(currState, {
				type: UNLOAD_USER_LIST
			});

			expect(state.query).toEqual([]);
		});
	});

	describe('SET_COLLAB_TEMPLATE', () =>{
		it('should set collabState.docId to the payload', () =>{
			let template = {'klm': '1234'};
			let state = collaboratorsReducer(undefined, {
				type: SET_COLLAB_TEMPLATE, 
				payload: template
			});

			expect(state.klm).toBe('1234');
		});
	});

	describe('REMOVE_COLLAB_TEMPLATE', () =>{
		it('should set collabState docId passed in as arg should be set to be null', () =>{
			let template = {'klm': '1234'};
			let currState = Object.assign({}, startState, template);
			let state = collaboratorsReducer(currState, {
				type: REMOVE_COLLAB_TEMPLATE, 
				payload: 'klm'
			});

			expect(state['klm']).toBeNull();
		})
	});


})