import {
	SET_CURRENT_COLLABORATOR,
	REMOVE_CURRENT_COLLABORATOR,
	LOAD_USER_LIST,
	UNLOAD_USER_LIST,
	SET_COLLAB_TEMPLATE, 
	REMOVE_COLLAB_TEMPLATE
} from './action-types';


const startState =  Object.assign({}, {collaborators:[], current: null, query: []}) //Object.assign({}, mdlConfig.mdl_basic_html5)



export function collaboratorsReducer(state = startState, action){
	
	switch(action.type){

		case SET_CURRENT_COLLABORATOR:
			return Object.assign({}, state, {current: action.payload});

		case REMOVE_CURRENT_COLLABORATOR:
			return Object.assign({}, state, {current: null});

		case SET_COLLAB_TEMPLATE:	
			return Object.assign({}, state, action.payload);

		case LOAD_USER_LIST:

			let r = {query : action.payload}
		
			return Object.assign({}, state, r)	

		case UNLOAD_USER_LIST: 
		
			return Object.assign({}, state, {query: []})	

		case REMOVE_COLLAB_TEMPLATE:

			let ob = {};
			ob[action.payload] = null;

			return Object.assign({}, state, ob)	

		default:
			return state;
	}
}	