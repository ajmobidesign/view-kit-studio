import {
	SET_CURRENT_COLLABORATOR,
	REMOVE_CURRENT_COLLABORATOR,
	LOAD_USER_LIST,
	UNLOAD_USER_LIST,
	SET_COLLAB_TEMPLATE, 
	REMOVE_COLLAB_TEMPLATE
} from './action-types';

//import sinon from 'sinon';

import { setCurrentCollaborator, removeCurrentCollaborator, setCollabTemplate, removeCollabTemplate, searchUserList, clearUserList } from './actions';


import { Db } from 'src/core/firebase';


describe('Search User List action', () =>{

	let dispatch, state, query, action, getState, arg1, arg2, arg3, actionResult = null;
	

	beforeEach(function() {
		
		dispatch = {};
		state = {auth: {authenticated: true, displayName: 'John'}};

		getState = function (){
			return state;
		}

		query = [];
				
	})


	it('should call Db.searchUserList if user is authenticated ', ()=>{
		
		spyOn(Db, 'searchUserList');

		action = searchUserList(query);
		actionResult = action(dispatch, getState);

		expect(state.auth.authenticated).toBe(true);
		expect(Db.searchUserList).toHaveBeenCalled();
			
		expect(Db.searchUserList).toHaveBeenCalledWith(query, dispatch, 'John')
	})
});

