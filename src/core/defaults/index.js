import * as mdlConfig from './mdl';
import { Storage } from './storage';



//this should be removed
export { mdlConfig };


export const templateMap = Object.assign({}, mdlConfig.tplMap);
export * from './selectors';
export * from './faq';


//this needs to be 
export const appTemplateList = [].concat(mdlConfig.tplList);


const localDb = new Storage();

localDb.init();

export { localDb };




const stagingClient = `https://prdxtool.firebaseapp.com`;
const localClient = `http://localhost:3000`;
const productionClient = `https://viewkitstudio.com`;
const localServer = `http://localhost:5000`;
const productionServer = `https://viewkit-admin.appspot.com`;

const stagingEv = {
	client : stagingClient, 
	server : productionServer
};

const localEv = {
	client : localClient, 
	server : localServer
};

const productionEv = {
	client: productionClient, 
	server: productionServer
};

export const currentEv = productionEv;




