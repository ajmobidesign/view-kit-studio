import { Record } from 'immutable';

export const styleParams = {
	primaryName: 'Blue', 
	accentName: 'Red', 
	primaryRgb: '100,181,246', 
	primaryLightRgb: '227,242,253',
	primaryDarkRgb: '30,136,229', 
	primaryShade: '300',
	accentRgb: '229,57,53',
	accentLightRgb: '255,235,238',
	accentShade: '', 
	primaryTextRgb: '255,255,255',
	primaryTextShade: '', 
	accentTextRgb: '255,255,255', 
	accentTextShade: ''
}

export const componentParams = {
	hero: '0', 
	features: '0',
	details: '0',
	grid: '0',
	contact: '0',
	footer: '0'
}

export const mdl_basic_html5 = {
	name: 'MDL Basic HTML5',
	price: '80.0',
	title : 'Untitled Template', 
	styleId: 'material', // has to part of default configs
	tplId:'mdl_basic_html5', //has to be part of default configs
	key: '',
	created: '',
	pics: 4,
	styleConfig: Object.assign({}, styleParams),
	componentConfig: {}
};

export const mdl_single_page = {
	name: 'MDL Single Page',
	price: '120.0',
	title : 'Untitled Template', 
	styleId: 'material', // has to part of default configs
	tplId:'mdl_single_page', //has to be part of default configs
	key: '',
	created: '',
	pics: 7,
	styleConfig: Object.assign({}, styleParams),
	componentConfig: Object.assign({}, componentParams)
};


export const mdl_style_guide = {
	name: 'MDL Style Guide',
	price: '50.0',
	title : 'Untitled Template', 
	styleId: 'material', // has to part of default configs
	tplId:'mdl_style_guide', //has to be part of default configs
	key: '',
	created: '',
	pics: 5,
	styleConfig: Object.assign({}, styleParams)
}; 



export const tplMap = {
	mdl_basic_html5 : mdl_basic_html5,  
	mdl_single_page: mdl_single_page, 
	mdl_style_guide: mdl_style_guide
};


//need to add the text configs
export const tplList = [

	{name: 'MDL Single Page', id: 'mdl_single_page', details: '', price: '120.00',  tags: ['HTML 5', 'Material Design Lite', 'Material Design'], promotion: '2017-05-13T23:55:13-08:00', pics: 7 },
	
	{name: 'MDL Basic HTML5', id: 'mdl_basic_html5', details: '', price: '80.00',  tags: ['HTML 5', 'Material Design Lite', 'Material Design'], promotion: '2017-05-13T23:55:13-08:00', pics: 4},
	{name: 'MDL Style Guide', id: 'mdl_style_guide', details: '', price: '50.00',  tags: ['HTML 5', 'Material Design Lite', 'Material Design'], promotion: '2017-05-13T23:55:13-08:00', pics: 5 }

	
];






const mdl_style_guide_Details = {

	about: 'Generate a Custom Material Design Style Guide with this template. Material Design Lite framework lets you add a Material Design look and feel to your websites. This layout includes all the main Material Design elements from Buttons, form elements to Navigation Tabs. The Material Colors Palette panel will let you create a custom combination of Material Colors for your project. Use the share panel tools to collaborate with others on your projects. Purchase the template to enable download and download a HTML5 page and customized Material Design Lite Css',

	tools:[{name: 'Material Design Palette'}, {name: 'Template Components'}, {name: 'Share Tools'}],
	techList:[{name: 'HTML5'}, {name: 'Material Design Lite', link: 'https://getmdl.io'}, {name: 'Font Awesome', link: 'http://fontawesome.io/icons/'}, {name: 'Material Icons', link: 'https://material.io/icons/'} ],

};


const mdl_basic_html5_Deails = {

	about: "This basic layout is based on Material Design Lite. Take it further with the The Material Colors Palette panel and create multiple customized color combinations base on Material Colors ", 
	techList:[{name: 'HTML5'}, {name: 'Material Design Lite', link: 'https://getmdl.io'}, {name: 'Font Awesome', link: 'http://fontawesome.io/icons/'}, {name: 'Material Icons', link: 'https://material.io/icons/'} ],
	tools:[{name: 'Material Design Palette'}]

}; 


const mdl_single_page_Details = {

	about: 'This Single Page reponsive layout template is designed to help you get a quick start on your single page projects. The template is designed with Material Design Lite framework. Material Design Lite is a lightweight framework  that does not rely on any JavaScript frameworks and aims to optimize for cross-device use, gracefully degrade in older browsers, and offer an experience that is immediately accessible.The preset layout includes multiple versions of standard website components like contact and features. Use the components panel included with the template to create a custom configuration for each of your projects. The Material Colors Palette panel will let you create a custom combination of Material Colors for your project. Use the share panel tools to collaborate with others. Purchase the template to enable download and download a HTML5 page and customized Material Design Lite Css',
	techList:[{name: 'HTML5'}, {name: 'Material Design Lite', link: 'https://getmdl.io'}, {name: 'Font Awesome', link: 'http://fontawesome.io/icons/'}, {name: 'Material Icons', link: 'https://material.io/icons/'} ],
	tools:[{name: 'Material Design Palette'}, {name: 'Template Components'}, {name: 'Share Tools'}], 


}; 

export const tplDetailsMap={
	mdl_basic_html5 : mdl_basic_html5_Deails,  
	mdl_single_page: mdl_single_page_Details, 
	mdl_style_guide: mdl_style_guide_Details
}


