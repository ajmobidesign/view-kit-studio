import moment from 'moment';



export class Storage{

	constructor(){

		this._hasInit = ()=>{
						let logged = localStorage.getItem('viewKitStudioInit');
							return (logged)? true : false;
					}

		this._hasSeenIt = ()=>{
						let seen = localStorage.getItem('viewKitStudioDemoSeen');
							return (seen) ? true : false;
					}

	}

	init(){
		let time = moment().format();
		let logged = localStorage.getItem('viewKitStudioInit');
		if(!logged){
			localStorage.setItem('viewKitStudioInit', time);
		}
	}


	get initState(){
		return this._hasInit();
	}

	get seenItState(){
		return this._hasSeenIt();
	}

	setItemValue(key, value){
		localStorage.setItem(key, value);
	}

	getItemValue(key){
		return localStorage.getItem(key);
	}

	removeItemAndValue(key){
		console.log('removed', key)
		localStorage.removeItem(key);
	}
}