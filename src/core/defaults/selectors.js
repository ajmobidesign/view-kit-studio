

export function getTplIdsWithPromotion(tplList){
	return tplList.filter((t)=>{ return t.promotion}).map((t)=>{ return t.id})
}