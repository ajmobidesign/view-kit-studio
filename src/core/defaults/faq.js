export const faqData = [
{

'question': 'What are layout templates?', 
'answer': 'View Kit Studio layout templates are web template comprised of commonly used  structural elements and interactions patterns in modern web development. They offer a faster starting point for your project by minimizing the time and effort spent coding these commonly used patterns, leaving you and your team more time to focus on the truly challenging parts of your project.'

}, 
{

'question': 'How do I create and save a new template version?', 
'answer': 'First, sign in with your google account. Then from the Select A Template page click on the CREATE NEW button on the template you would like to use. A new version will be created for you. Give the new version a title and makes changes to its configurations using the side panel. All changes you make will be autosaved.'
}, 
{

'question': 'What does it cost to create and save?', 
'answer': "Nothing. It's Free!"

}, 
{

'question': 'How many versions of a template can I save?', 
'answer': 'There are currently no limits.'

}, 
{

'question': 'How do I change colors on a template ?', 
'answer': 'All templates come with a side panel that includes a color palette. Palettes are pre-configured based on the style framework used to create the template. Click on the colors to configure the right palette for your project.'

}, 
{

'question': 'What if I want to change colors that are not in the panel?', 
'answer': 'Layout templates are meant to give you a way to quickly configure large components in a project. To make fine grained color changes not available on the palette panel, you will have to download and update the css file of your template to make the required changes.'

}, 
{

'question': 'How do I change the configurations of components in a template?', 
'answer': 'For templates that allow component configurations, you can use the components panel to select the configurations that suits your project.'

}, 
{

'question': 'How do I download the code for a saved version of a template?', 
'answer': 'There are two ways to download the saved versions. You can click on the download button in the Templates List Table or you can click on the download button in the Header, next to the template title, in the Template Page.'

}, 
{

'question': 'Where can I find more details about the template?', 
'answer': 'In the Select A Template page, click the SHOW DETAILS button to see the details of a specific template.'

}, 
/*
{

'question': 'What will the downloaded file contain?', 
'answer': 'The details of file contents of each template are listed in the details section of the template. You can see it by clicking on the Show Details button on the template in the Template List page.'

},
*/ 
{

'question': 'Where can I find information about the license?', 
'answer': 'View Kit Studio layout templates come with a Multi Use License. It allows you to create multiple products for yourself and your clients. ', 
'link': '/license'

}, 
{

'question': 'I only see a few templates? Will there be more?', 
'answer': 'Absolutely! Over the coming months we will be adding more templates to our collection. If you have suggestions, we would love hearing from you. Click on the tab on the bottom right of this page and enter your suggestion.'

}, 
{

'question': 'How can I generate a URL I can use to share the template?', 
'answer': 'On the Share Panel select the share tab to pick the kind of URL you would like to generate. There are two options. You can generate a public URL that can be viewed by anyone with the link or you can generate a private URL which can be viewed only by signed in View Kit Studio users with the link.'

}, 
{

'question': 'Can someone with a public or private URL make changes?', 
'answer': 'No. Only users added to the project as collaborators can make changes.'

},
{

'question': 'How do I add or remove a collaborator?', 
'answer': 'On the Share Panel, use the the search tab to find users by email. Collaborators have to be registered View Kit Studio users. Click the Add button next to their name. To remove a user click the Remove button next to their name from the list of collaborators tab.'

}, 
{

'question': 'How many collaborators can I add?', 
'answer': 'You can add up to 3 collaborators per template version.'

}, 
{

'question': 'Can I set permission for collaborators?', 
'answer': 'Not at this time. All collaborators automatically have full edit permissions. They cannot however delete templates or invite other collaborators.'

}, 
{

'question': 'How do I post a note?', 
'answer': 'Click the Add Sticky button on the share panel with the bookmark tab. A note will appear on the left side of the screen. You can drag the note to any other part of the screen. You can use notes to post reminders for yourself or messages to collaborators.'

}
, 
{

'question': "Can my collaborators edit my notes?",
'answer': "No. Only you can add, remove or edit a note you posted. Additionally you also do not have permission to add, remove or edit a collaborator's note."

}
/*, 
{

'question': '', 
'answer': ''

}, 
{

'question': '', 
'answer': ''

}*/
]