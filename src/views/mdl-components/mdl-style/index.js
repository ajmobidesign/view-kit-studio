import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import * as Colors from '../../styles/inline';


export class MdlStyle extends Component{

	static proptypes ={
		style: PropTypes.object.isRequired
	}


	render(){

		


		let primaryRgb = this.props.style.primaryRgb;
		let primaryLightRgb = this.props.style.primaryLightRgb;
		let primaryDarkRgb = this.props.style.primaryDarkRgb;

		let accentRgb = this.props.style.accentRgb;
		let accentLightRgb = this.props.style.accentLightRgb;

		//main colors
		let colorPrimary = `rgb(${primaryRgb})`;
		let colorPrimaryDark = `rgb(${primaryDarkRgb})`;
		let colorAccent =`rgb(${accentRgb})`;

		let colorPrimaryContrast = `rgb(${primaryLightRgb})`;
		let colorAccentContrast = `rgb(${accentLightRgb})`;

		//default button colors
		let buttonPrimaryColor = `rgb(${Colors.paletteGrey.Grey500})`;
		let buttonSecondaryColor = `rgb(${Colors.ColorBlack})`;
		let buttonHoverColor= '';//remove this
		let buttonActiveColor=`rgba(${Colors.paletteGrey.Grey500}, 0.40)`;
		let buttonFocusColor = `rgb(${Colors.ColorBlack})`;

		// colored button colors
		let buttonPrimaryColorAlt  = colorPrimary;
		
		let buttonHoverColorAlt = colorPrimary;	
		let buttonActiveColorAlt = colorPrimary;
		let buttonFocusColorAlt = buttonFocusColor;


	
		//Fab
		let buttonFabColorAlt = colorAccent;
		let buttonFabHoverColorAlt = colorAccent;
		
		let buttonFabActiveColorAlt = colorAccent;
		let buttonFabRippleColorAlt = colorAccentContrast;


		//Disabled
		let buttonPrimaryColorDisabled = `rgba(${Colors.ColorBlack}, 0.12)`;
		let buttonSecondaryColorDisabled=`rgba(${Colors.ColorBlack}, 0.26)`;

		//--updatable--//
		let buttonSecondaryColorAlt = `rgb(${this.props.style.primaryTextRgb})`;//the light color
		//Ripple Color for colored raised
		let buttonRippleColorAlt = `rgb(${this.props.style.primaryTextRgb})`;
		let buttonFabTextColorAlt = `rgb(${this.props.style.accentTextRgb})`;	



		//--Main Colors and Text--//

		let mainColorCss= `


			.mdl-color--primary {
			  background-color: ${colorPrimary} !important;
			}

			.mdl-color--primary-contrast {
			  background-color: ${colorPrimaryContrast} !important;
			}

			.mdl-color--primary-dark {
			  background-color: ${colorPrimaryDark} !important;
			}

			.mdl-color--accent {
			  background-color: ${colorAccent} !important;
			}

			.mdl-color--accent-contrast {
			  background-color: ${colorAccentContrast} !important;
			}

			.mdl-color-text--primary {
			  color: ${colorPrimary} !important;
			}

			.mdl-color-text--primary-contrast {
			  color: ${colorPrimaryContrast} !important;
			}

			.mdl-color-text--primary-dark {
			  color: ${colorPrimaryDark} !important;
			}

			.mdl-color-text--accent {
			  color: ${colorAccent} !important;
			}

			.mdl-color-text--accent-contrast {
			  color: ${colorAccentContrast} !important;
			}

		`;

		
		//--Link--//

		let linkCss = `
			a{
				color: ${colorPrimary};
			}
		`;


		//--Buttons--//

		let buttonCss = `
			.mdl-button: {
				color: ${buttonSecondaryColor};
			}

			.mdl-button:hover{
				background-color: ${buttonHoverColor};
			}

			.mdl-button:focus:not(:active){
				background-color: ${buttonFocusColor};
			}

			.mdl-button.mdl-button--colored{
				color: ${buttonPrimaryColorAlt};
			}

			.mdl-button.mdl-button--colored:focus:not(:active){
				background-color: ${buttonFocusColorAlt};
			}

			.mdl-button--raised{
				background: ${buttonPrimaryColor};
			}

			.mdl-button--raised:active{
				background-color: ${ buttonActiveColor};
			}

			.mdl-button--raised:focus:not(:active){
				background-color: ${buttonActiveColor}
			}

			.mdl-button--raised.mdl-button--colored{
				background: ${buttonPrimaryColorAlt};
				color: ${buttonSecondaryColorAlt};
			}

			.mdl-button--raised.mdl-button--colored:hover{
				background-color: ${buttonHoverColorAlt};
			}

			.mdl-button--raised.mdl-button--colored:active{
				background-color: ${buttonActiveColorAlt};
			}

			.mdl-button--raised.mdl-button--colored:focus:not(:not){
				background-color: ${buttonActiveColorAlt};
			}

			.mdl-button--raised.mdl-button--colored .mdl-ripple{
				background: ${buttonRippleColorAlt};
			}

			.mdl-button--fab{
				background: ${buttonPrimaryColor}
			}

			.mdl-button--fab:active{
				background-color: ${buttonActiveColor};
			}

			.mdl-button--fab:focus:not(:active){
				background-color: ${buttonActiveColor};
			}

			.mdl-button--fab.mdl-button--colored{
				background: ${buttonFabColorAlt};
				color: ${buttonFabTextColorAlt};
			}

			.mdl-button--fab.mdl-button--colored:hover{
				background-color: ${buttonFabHoverColorAlt};
			}

			.mdl-button--fab.mdl-button--colored:focus:not(:active){
				background-color: ${buttonFabActiveColorAlt};
			}

			.mdl-button--fab.mdl-button--colored:active{
				background-color: ${buttonFabActiveColorAlt};
			}

			.mdl-button--fab.mdl-button--colored .mdl-ripple{
				background: ${buttonFabRippleColorAlt}
			}

			.mdl-button--primary.mdl-button--primary{
				color: ${buttonPrimaryColorAlt};
			}

			.mdl-button--primary.mdl-button--primary .mdl-ripple{
				background: ${buttonSecondaryColorAlt};
			}

			.mdl-button--primary.mdl-button--primary.mdl-button--raised, 
			.mdl-button--primary.mdl-button--primary.mdl-button--fab{
				color: ${buttonSecondaryColorAlt};
				background-color: ${buttonPrimaryColorAlt};
			}

			.mdl-button--accent.mdl-button--accent {
				color: ${buttonFabColorAlt};
			}

			.mdl-button--accent.mdl-button--accent .mdl-ripple{
				background: ${buttonFabTextColorAlt};
			}

			.mdl-button--accent.mdl-button--accent.mdl-button--raised, 
			.mdl-button--accent.mdl-button--accent.mdl-button--fab{
				color: ${buttonFabTextColorAlt};
				background-color: ${buttonFabColorAlt};
			}

			.mdl-button[disabled][disabled],
			.mdl-button.mdl-button--disabled.mdl-button--disabled{
				color: ${buttonSecondaryColorDisabled};
			}

			.mdl-button--fab[disabled][disabled],
			.mdl-button--fab.mdl-button--disabled.mdl-button--disabled{
				background-color: ${buttonPrimaryColorDisabled};
				color: ${buttonSecondaryColorDisabled};
			}

			.mdl-button--colored[disabled][disabled],
			.mdl-button--colored.mdl-button--disabled.mdl-button--disabled{
				
				color: ${buttonSecondaryColorDisabled};
			}

		`;


		//--Layout--//

		// Drawer

		let layoutNavColor = '';
		let layoutDrawerBgColor = '';
		let layoutDrawerBorderColor = '';
		let layoutTextColor = '';
		let layoutDrawerNavigationColor = '';
		let layoutDrawerNavigationLinkActiveBackground = '';

		let layoutDrawerNavigationLinkActiveColor = '';


		// Header

		let layoutHeaderBgColor = colorPrimary;
		
		let layoutHeaderNavHoverColor = '';
		

		// Tabs

		let layoutHeaderTabHighlight = colorAccent;




		//--Updateable--//
		let layoutHeaderTextColor = colorPrimaryContrast //`rgb(${this.props.primaryTextColor})`
		let layoutHeaderTabTextColor = colorPrimaryContrast;



		
		let layoutCss = `
			.mdl-navigation__link{
				color: ${layoutTextColor};
			}

			.mdl-layout__drawer{
				background: ${layoutDrawerBgColor};
				color: ${layoutTextColor};
			}

			.mdl-navigation .mdl-navigation__link{
				color: ${layoutDrawerNavigationColor};
			}

			.mdl-navigation .mdl-navigation__link:hover{
				background-color: ${layoutNavColor};
			}

			.mdl-navigation .mdl-navigation__link--current{
				background-color: ${layoutDrawerNavigationLinkActiveBackground};
				color: ${layoutDrawerNavigationLinkActiveColor};
			}

			.mdl-layout__header .mdl-layout__drawer-button{
				color: ${layoutHeaderTextColor};
			}

			.mdl-layout__header{
				background-color: ${layoutHeaderBgColor};
				color: ${layoutHeaderTextColor};
			}

			.mdl-layout__header-row .mdl-navigation__link{
				color: ${layoutHeaderTextColor};
			}

			.mdl-layout__tab-bar{
				background-color: ${layoutHeaderBgColor};
			}

			.mdl-layout__tab-bar-button{
				background-color: ${layoutHeaderBgColor};
			}

			.mdl-layout__tab-bar-button.is-active{
				color: ${layoutHeaderTextColor};
			}

			.mdl-layout__tab{
				color: rgba(${this.props.style.primaryTextRgb}, 0.65)
			}

			.mdl-layout.is-upgraded .mdl-layout__tab.is-active{
				color: ${layoutHeaderTextColor};
			}

			.mdl-layout__tab .mdl-layout__tab-ripple-container .mdl-ripple{
				background-color: ${layoutHeaderTextColor};
			}

			.mdl-layout.is-upgraded .mdl-layout__tab.is-active::after{
				background: ${layoutHeaderTabHighlight}
			}

			.mdl-tabs.is-upgraded .mdl-tabs__tab.is-active:after{
				background: ${colorAccent};
			}

			.mdl-tabs__tab .mdl-tabs__ripple-container .mdl-ripple{
				background: ${colorAccent}
			}
			

		`;

		//--Badges--//

		let badgeBackground = colorAccent;
		let badgeBackgroundInverse = colorAccentContrast;
		let badgeColorInverse = colorAccent;


		//Updatable--//
		let badgeColor = `rgb(${this.props.style.accentTextRgb})`


		let badgeCss = `
			.mdl-badge[data-badge]:after{
				background: ${badgeBackground};
				color: ${badgeColor}
			}

			.mdl-badge.mdl-badge--no-background{
				color: ${badgeColorInverse};
			}

			.mdl-badge.mdl-badge--no-background[data-badge]:after{
				color: ${colorAccent};
			}
		`;

		//--Checkboxes--//

		let checkboxColor = colorPrimary;
		let checkboxFocusColor = colorPrimary;

		let checkboxCss = `
			.mdl-checkbox.is-focused .mdl-checkbox__focus-helper {
				box-shadow: box-shadow:0 0 0 8px rgba(${primaryRgb}, .1);
				background-color: ${checkboxFocusColor};
			}

			.mdl-checkbox__ripple-container .mdl-ripple{
				background: ${checkboxColor};
			}

			.mdl-checkbox.is-checked .mdl-checkbox__box-outline{
				border: 2px solid ${checkboxColor};
			}

			.mdl-checkbox.is-checked .mdl-checkbox__tick-outline{
				background: ${checkboxColor};
			}

		`;

		//--Progress--//

		let progressMainColor = colorPrimary;
		let progressSecondaryColor = `rgba(${primaryLightRgb}, 0.7)`;
		let progressFallbackBufferColor = `rgba(${primaryLightRgb}, 0.9)`;

		let progressCss = `
			.mdl-progress > .progressbar {
				background-color: ${progressMainColor};
			}

			.mdl-progress > .bufferbar {
			  background-image: linear-gradient(to right, ${progressSecondaryColor}, ${progressSecondaryColor}),
			    linear-gradient(to right, ${progressMainColor}, ${progressMainColor});
			}

			.mdl-progress.mdl-progress--indeterminate > .bar1,
			.mdl-progress.mdl-progress__indeterminate > .bar1 {
				background-color: ${progressMainColor};
			}

			.mdl-progress.mdl-progress--indeterminate > .bar3,
			.mdl-progress.mdl-progress__indeterminate > .bar3 {
				background-color:${progressMainColor}
			}

			.mdl-progress:not(.mdl-progress--indeterminate) > .auxbar,
			.mdl-progress:not(.mdl-progress__indeterminate) > .auxbar {
			  background-image: linear-gradient(to right, ${progressFallbackBufferColor}, ${progressFallbackBufferColor}),
			    linear-gradient(to right, ${progressMainColor}, ${progressMainColor});
			}

			.mdl-progress:not(.mdl-progress--indeterminate):not(.mdl-progress--indeterminate)>.auxbar, .mdl-progress:not(.mdl-progress__indeterminate):not(.mdl-progress__indeterminate)>.auxbar{

				background-image: linear-gradient(to right, ${progressFallbackBufferColor}, ${progressFallbackBufferColor}),
			    linear-gradient(to right, ${progressMainColor}, ${progressMainColor});
			}


		`;

		//--Sliders--//

		let rangeColor = colorPrimary;
		let rangeFadeColor = `rgba(${primaryRgb}, 0.26)`;

		let sliderCss = `
			.mdl-slider.is-upgraded{
				color: ${rangeColor};
			}

			.mdl-slider.is-upgraded::-ms-fill-lower {
				background: linear-gradient(to right, transparent, transparent 16px, ${rangeColor} 16px, ${rangeColor} 0);
			}

			.mdl-slider.is-upgraded::-webkit-slider-thumb{
				background: ${rangeColor};
			}

			.mdl-slider.is-upgraded::-moz-range-thumb{
				background: ${rangeColor};
			}

			.mdl-slider.is-upgraded:focus:not(:active)::-webkit-slider-thumb{
                box-shadow: 0 0 0 10px ${rangeFadeColor};
            }

			

			.mdl-slider.is-upgraded:focus:not(:active)::-moz-range-thumb {
      			box-shadow: 0 0 0 10px ${rangeFadeColor};
    		}

    		.mdl-slider.is-upgraded:active::-webkit-slider-thumb{
    			background: ${rangeColor};
    		}

    		.mdl-slider.is-upgraded:active::-moz-range-thumb {
    			background: ${rangeColor};
    		}

    		.mdl-slider.is-upgraded::-ms-thumb {
    			background: ${rangeColor};
    		}

    		.mdl-slider.is-upgraded:focus:not(:active)::-ms-thumb{
    			background: radial-gradient(circle closest-side, ${rangeColor} 0%, ${rangeColor} 37.5%, ${rangeFadeColor} 37.5%, ${rangeFadeColor} 100%);
    		}

    		.mdl-slider.is-upgraded:active::-ms-thumb{
    			background: ${rangeColor};
    		}

    		.mdl-slider__background-lower {
    			background: ${rangeColor};
    		}
		   
		`;


		//--Text Fields--//

		let inputTextHighlightColor = colorPrimary;

		let inputCss = `

			.mdl-textfield--floating-label.is-focused .mdl-textfield__label,
			.mdl-textfield--floating-label.is-dirty .mdl-textfield__label,
			.mdl-textfield--floating-label.has-placeholder .mdl-textfield__label{
				color: ${inputTextHighlightColor}
			}

			.mdl-textfield__label:after {
    			background-color: ${inputTextHighlightColor};
    		}

		`;


		//--Spinner--//

		let spinnerSingleColor = colorPrimary;

		let spinnerCss = `
			 .mdl-spinner--single-color .mdl-spinner__layer-1{
				border-color: ${spinnerSingleColor};
			}

			.mdl-spinner--single-color .mdl-spinner__layer-2{
				border-color: ${spinnerSingleColor};
			}

			.mdl-spinner--single-color .mdl-spinner__layer-3{
				border-color: ${spinnerSingleColor};
			}

			.mdl-spinner--single-color .mdl-spinner__layer-4{
				border-color: ${spinnerSingleColor};
			}


		`;

		//--Radio--//

		let radioColor = colorPrimary;


		let radioCss = `
			.mdl-radio.is-checked .mdl-radio__outer-circle{
				border: 2px solid ${radioColor};
			}

			.mdl-radio__inner-circle{
				background: ${radioColor};
			}

			.mdl-radio__ripple-container .mdl-ripple{
				background: ${radioColor};
			}
			
		`;

		//--swtich--//

		let switchColor = colorPrimary;
		let switchFadedColor = `rgba(${primaryRgb}, 0.26)`;
		let switchThumbColor = switchColor;
		let swithcTrackColor = `rgba(${primaryRgb}, 0.5)`;

		let switchCss = `
			.mdl-switch__ripple-container .mdl-ripple{
				background: ${switchColor};
			}

			.mdl-switch.is-focused.is-checked .mdl-switch__focus-helper{
				background: ${switchFadedColor};
			}

			.mdl-switch.is-checked .mdl-switch__thumb{
				background: ${switchThumbColor};
			}

			.mdl-switch.is-checked .mdl-switch__track{
				background: ${swithcTrackColor};
			}
		`;


		return (
			
				<style>
						{mainColorCss}
						{layoutCss}
						{linkCss}
						{buttonCss}
						{badgeCss}
						{checkboxCss}
						{progressCss}
						{sliderCss}
						{inputCss}
						{spinnerCss}
						{radioCss}
						{switchCss}
				</style>

			
			)
	}
}




export default MdlStyle;