import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import * as Colors from '../../styles/inline';
import { styleguideActions } from 'src/core/styleguide';
import MdlColorblock from '../mdl-colorblock';
import { templateActionions } from 'src/core/template';

export class MdlPalette extends Component{

	static propTypes = {
		updateColors: PropTypes.func.isRequired, 

		style: PropTypes.object.isRequired,
		update: PropTypes.func.isRequired
	}


	constructor(props, context){
		super(props, context);

		this.state = {primary: 'Indigo', accent: 'Green', primaryMain: '255,255,255', primaryLight: '255,255,255', primaryDark: '255,255,255', primaryShade: '', accentMain: '255,255,255', accentShade: '', accentLight: '255,255,255'};

		this.onPrimaryChange = ::this.onPrimaryChange;
		this.onAccentChange = ::this.onAccentChange;
		//this.primaryPalette = ::this.primaryPalette;
		this.applyPalette = ::this.applyPalette;

		//this.renderMainPrimaryShades = ::this.renderMainPrimaryShades;

		this.setPrimaryColor= ::this.setPrimaryColor;
		this.setPrimaryLightColor = ::this.setPrimaryLightColor;
		this.setPrimaryDarkColor = ::this.setPrimaryDarkColor;
		this.setAccentColor = ::this.setAccentColor;
		this.setAccentLightColor = ::this.setAccentLightColor;
	}

	
	

	onPrimaryChange(event){

		//should this update the store?

		this.setState({primary: event.target.value});
		this.setState({primaryMain: '255,255,255'});
		this.setState({primaryLight: '255,255,255'});
		this.setState({primaryDark: '255,255,255'});
		this.setState({accentMain: '255,255,255'});
		this.setState({accentLight: '255,255,255'});
		
	}

	onAccentChange(event){
		this.setState({accent: event.target.value});
		this.setState({accentMain: '255,255,255'});
		this.setState({accentLight: '255,255,255'});
	}


	applyPalette(){
		
		//cannot apply if the full palette is not selected.
		let primaryClrs = {main: this.state.primaryMain , mainDark: this.state.primaryDark , mainLight: this.state.primaryLight};
		let accentClrs = {main: this.state.accentMain , mainLight: this.state.accentLight};

		let palette = {primary: this.state.primary, primaryColors: primaryClrs, accent: this.state.accent, accentColors: accentClrs , primaryTextColor: this.state.primaryLight, accentTextColor: this.state.accentLight};

		this.props.updateColors(palette);


		let p = {
				primaryName: this.state.primary,
				accentName: this.state.accent, 
				primaryRgb: this.state.primaryMain, 
				primaryLightRgb: this.state.primaryLight,
				primaryDarkRgb: this.state.primaryDark, 
				primarShade: '',
				accentRgb: this.state.accentMain,
				accentLightRgb: this.state.accentLight,
				accentShade: ''
			}


		this.props.update(p)


	}

/*
	primaryPalette(){

		let color = this.state.primary;
		let main = Colors[`palette${color}`][`${color}500`];
		let mainDark = Colors[`palette${color}`][`${color}700`];
		let mainLight = Colors[`palette${color}`][`${color}50`];

		return {main: main , mainDark: mainDark , mainLight: mainLight}
	}

	accentPalette(){

		let color = this.state.accent;
		let main = Colors[`palette${color}`][`${color}A200`];
		let mainLight = Colors[`palette${color}`][`${color}A100`];

		return {main: main , mainLight: mainLight}
	}*/


	setAccentColor(clr, shade){

		this.setState({accentMain: clr});
		this.setState({accentShade: shade});

		//this.applyPalette();
	}

	setAccentLightColor(clr){
		this.setState({accentLight: clr});

		//this.applyPalette();
	}


	setPrimaryColor(clr, shade){

		this.setState({primaryMain: clr});
		this.setState({primaryShade: shade});

		//this.applyPalette();
	}

	setPrimaryLightColor(clr){

		this.setState({primaryLight: clr});

		//this.applyPalette();
	}

	setPrimaryDarkColor(clr){

		this.setState({primaryDark: clr});

		//this.applyPalette();
	}



	renderLightPrimaryShades(){

		const primaryShades = ['300', '400', '500', '600', '700', '800'];

		let currentPrimary = this.state.primary;
		let currentPrimaryShade = this.state.primaryShade;
		let blocks = undefined;

		//is there a better way to generate an array?

		
		let lightShades = ['50', '100', '200'].concat(primaryShades.filter((n)=>{ 
			if(parseInt(n)<parseInt(currentPrimaryShade)){
				return n
			}
		}))


		blocks = lightShades.map((cl, index)=>{
				let clr = Colors[`palette${currentPrimary}`][`${currentPrimary}${cl}`];
				let isCurrent = this.state.primaryLight;

				return (<MdlColorblock  setcolor={this.setPrimaryLightColor} shade={cl} current={isCurrent} color={clr} key={index}></MdlColorblock>)
			})

		//return (<div><span>Primary Light</span>{blocks}</div>);
		return (<div className="style-panel-control-block"><span className="control-label">Primary Light</span><span className="control-content">{blocks}</span></div>)
	}

	renderDarkPrimaryShades(){

		const primaryShades = ['300', '400', '500', '600', '700', '800'];

		let currentPrimary = this.state.primary;
		let currentPrimaryShade = this.state.primaryShade;
		let blocks = undefined;

		//is there a better way to generate an array?
		
		let darkShades = ['900'].concat(primaryShades.filter((n)=>{ 

			if(parseInt(n)>parseInt(currentPrimaryShade)){
				return n
			}
		}))


		blocks = darkShades.map((cl, index)=>{
				let clr = Colors[`palette${currentPrimary}`][`${currentPrimary}${cl}`];
				let isCurrent = this.state.primaryDark;
				return (<MdlColorblock  setcolor={this.setPrimaryDarkColor} current={isCurrent} shade={cl} color={clr} key={index}></MdlColorblock>)
			})

		//return (<div><span>Primary Dark</span>{blocks}</div>);
		return (<div className="style-panel-control-block"><span className="control-label">Primary Dark</span><span className="control-content">{blocks}</span></div>)
		
	}


	renderMainPrimaryShades(){

		let currentPrimary = this.state.primary;
		let blocks = undefined;
		
		const primaryShades = ['300', '400', '500', '600', '700', '800'];
		const greyShades = ['500', '600', '700', '800'];
		
		if(currentPrimary == 'Grey'|| currentPrimary == 'BlueGrey' || currentPrimary == 'Brown'){
			blocks = greyShades.map((cl, index)=>{
				let clr = Colors[`palette${currentPrimary}`][`${currentPrimary}${cl}`];
				let isCurrent = this.state.primaryMain
				return (<MdlColorblock  setcolor={this.setPrimaryColor} shade={cl} color={clr} current={isCurrent} key={index}></MdlColorblock>)
			})
			
		}
		
		else{
			

			blocks = primaryShades.map((cl, index)=>{
				let clr = Colors[`palette${currentPrimary}`][`${currentPrimary}${cl}`]
				let isCurrent = this.state.primaryMain;
				return (<MdlColorblock  setcolor={this.setPrimaryColor} current={isCurrent} shade={cl} color={clr} key={index}></MdlColorblock>)
			})
		}
		
		
		//return(<div><span>Primary Color</span>{blocks}</div>)
		return (<div className="style-panel-control-block"><span className="control-label">Primary Color</span><span className="control-content">{blocks}</span></div>)
		
	}


	renderMainAccentShades(){

		let currentAccent = this.state.accent;
		let blocks = undefined;
		
		const accentShades = ['300', '400', '500', '600', '700', '800', 'A200', 'A400'];
		
		
		blocks = accentShades.map((cl, index)=>{
				let clr = Colors[`palette${currentAccent}`][`${currentAccent}${cl}`]
				let isCurrent = this.state.accentMain;
				return (<MdlColorblock  setcolor={this.setAccentColor} current={isCurrent} shade={cl} color={clr} key={index}></MdlColorblock>)
			})
		
			
		return(<div className="style-panel-control-block"><span className="control-label">Accent Color</span><span className="control-content">{blocks}</span></div>)
		
	}


	renderLightAccentShades(){

		let currentAccent = this.state.accent;
		let currentAccentShade = this.state.accentShade;
		let blocks = undefined;
		
		const accentShades = ['300', '400', '500', '600', '700', '800', 'A200', 'A400'];

		if(currentAccentShade === 'A200' || currentAccentShade === 'A400'){


			if(currentAccentShade === 'A200'){

				let cl = 'A100';

				let clr = Colors[`palette${currentAccent}`][`${currentAccent}${cl}`];
				let isCurrent = this.state.accentLight;

				blocks = (<MdlColorblock  setcolor={this.setAccentLightColor} current={isCurrent} shade={cl} color={clr} ></MdlColorblock>)

			}
			else{

				let accentLightShades = ['A100', 'A200']

				blocks = accentLightShades.map((cl, index)=>{
					let clr = Colors[`palette${currentAccent}`][`${currentAccent}${cl}`];
					let isCurrent = this.state.accentLight;
					return (<MdlColorblock  setcolor={this.setAccentLightColor} current={isCurrent} shade={cl} color={clr} key={index}></MdlColorblock>)
				})

			}

			

		}
		else{

			let accentLightShades = ['50', '100', '200'].concat(accentShades.filter((s)=>{if(parseInt(s)< parseInt(currentAccentShade)){return s}}))

			blocks = accentLightShades.map((cl, index)=>{
				let clr = Colors[`palette${currentAccent}`][`${currentAccent}${cl}`]
				let isCurrent = this.state.accentLight;
				return (<MdlColorblock  setcolor={this.setAccentLightColor} current={isCurrent} shade={cl} color={clr} key={index}></MdlColorblock>)
			})


		}
		
	
			
		//return(<div><span>Accent Light</span>{blocks}</div>)
		return (<div className="style-panel-control-block"><span className="control-label">Accent Light</span><span className="control-content">{blocks}</span></div>)
		
		
	}

/*
	renderDarkAccentShades(){

		let currentAccent = this.state.accent;
		let currentAccentShade = this.state.accentShade;
		let blocks = undefined;
		
		const accentShades = ['300', '400', '500', '600', '700', '800', 'A200', 'A400'];

		if(currentAccentShade === 'A200' || currentAccentShade === 'A400'){

			block = ['400', '500', '600', '700', '800'].map((cl, index)=>{
				let clr = Colors[`palette${currentAccent}`][`${currentAccent}${cl}`]
				let isCurrent = this.state.accentDark;
				return (<MdlColorblock  setcolor={this.setAccentDarkColor} current={isCurrent} shade={cl} color={clr} key={index}></MdlColorblock>)
			})
		}
		else{

			//need to change
			block = ['400', '500', '600', '700', '800'].map((cl, index)=>{
				let clr = Colors[`palette${currentAccent}`][`${currentAccent}${cl}`]
				let isCurrent = this.state.accentDark;
				return (<MdlColorblock  setcolor={this.setAccentDarkColor} current={isCurrent} shade={cl} color={clr} key={index}></MdlColorblock>)
			})

		}


		return(<div></div>)
	}

*/



	render(){

		

		const fStyle = {
							padding: '10px', 
							display: 'block'
						};

		

		//let mainColors = this.primaryPalette();
		//let accentColors = this.accentPalette();

		let primaryMain = { background: `rgb(${this.state.primaryMain})` };

		let primaryDark = { background: `rgb(${this.state.primaryDark})` };

		let primaryLight = { background: `rgb(${this.state.primaryLight})` };


		let accentMain = { background: `rgb(${this.state.accentMain})` };

		let accentLight = { background: `rgb(${this.state.accentLight})` };




		return(
			<div className="style-palette" >
				<div>
					<div className="style-panel-control-block">
					<span className="control-label">
						Primary Color
					</span>	
					<span className="control-content">	
						<select value={this.state.primary} onChange={this.onPrimaryChange}>
							
							<option value="Red">Red</option>
							<option value="Pink">Pink</option>
							<option value="Purple">Purple</option>
							<option value="DeepPurple">Deep Purple</option>
							<option value="Indigo">Indigo</option>
							<option value="Blue">Blue</option>
							<option value="LightBlue">Light Blue</option>
							<option value="Cyan">Cyan</option>
							<option value="Teal">Teal</option>
							<option value="Green">Green</option>
							<option value="LightGreen">Light Green</option>
							<option value="Lime">Lime</option>
							<option value="Yellow">Yellow</option>
							<option value="Amber">Amber</option>
							<option value="Orange">Orange</option>
							<option value="DeepOrange">Deep Orange</option>
							<option value="Brown">Brown</option>
							<option value="Grey">Grey</option>
							<option value="BlueGrey">Blue Grey</option>
						</select>
					</span>
					</div>
					
						{(this.state.primary)?  this.renderMainPrimaryShades() : null}
					

					
						{(this.state.primaryMain !== '255,255,255')?  this.renderLightPrimaryShades() : null}
					

						{(this.state.primaryMain !== '255,255,255')?  this.renderDarkPrimaryShades() : null}
					


					<div className="style-panel-control-block">
					<span className="control-label">
						Accent Color
					</span>	
					<span className="control-content">	
						<select value={this.state.accent} onChange={this.onAccentChange}>
							
							<option value="Red">Red</option>
							<option value="Pink">Pink</option>
							<option value="Purple">Purple</option>
							<option value="DeepPurple">Deep Purple</option>
							<option value="Indigo">Indigo</option>
							<option value="Blue">Blue</option>
							<option value="LightBlue">Light Blue</option>
							<option value="Cyan">Cyan</option>
							<option value="Teal">Teal</option>
							<option value="Green">Green</option>
							<option value="LightGreen">Light Green</option>
							<option value="Lime">Lime</option>
							<option value="Yellow">Yellow</option>
							<option value="Amber">Amber</option>
							<option value="Orange">Orange</option>
							<option value="DeepOrange">Deep Orange</option>
						</select>
					</span>
					</div>


					
						{(this.state.accent)?  this.renderMainAccentShades() : null}
					

					
						{(this.state.accentMain !== '255,255,255')? this.renderLightAccentShades() : null}
					

					<div className="style-panel-control-block" style={{textAlign:'center'}}>
						<div className="color-block" style={primaryLight}></div>
						<div className="color-block" style={primaryMain}></div>
						<div className="color-block" style={primaryDark}></div>
						<div className="color-block" style={accentLight}></div>
						<div className="color-block" style={accentMain}></div>
					</div>

				</div>

				<div className="style-panel-control-block" style={{textAlign:'center'}}>
					<button onClick={this.applyPalette}>Apply</button>

				</div>
				
			</div>
		)
	}
}

//CONNECT

const mapDispatchToProps = Object.assign({}, styleguideActions);

/*
const mapStateToProps = (state)=>{	
}*/


export default connect(null, mapDispatchToProps)(MdlPalette)

