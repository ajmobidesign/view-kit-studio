import React, { Component, PropTypes } from 'react';
import { Icon } from 'react-mdl';


class DrawerButton extends Component{

	static propTypes = {
		ariaExpanded: PropTypes.bool.isRequired,	
	}

	render(){
		return(
			<div className="mdl-layout__drawer-button" ariaExpanded="false" role="button">
		        <Icon name="menu" />
		    </div>
		)
	}

}

export default DrawerButton;