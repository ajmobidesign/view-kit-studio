import React, { Component } from 'react';
import { Grid, Textfield,  Cell, Icon, Button, Card, CardTitle, CardText, CardActions} from 'react-mdl';



class MdlContent1 extends Component{



	render(){

		return(
			<Grid>

				<div className="mdl-cell mdl-cell--12-col  mdl-grid">
					<div className="mdl-cell mdl-cell--12-col  features-headline">
						<h4>Our Amazing Features </h4>
					</div>
				</div>

				<div className="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone">
					<div className="mdl-card mdl-shadow--2dp">
					  <div className="mdl-card__title mdl-card--expand">
					    <h2 className="mdl-card__title-text">Feature </h2>
					  </div>
					  <div className="mdl-card__supporting-text">
					    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
					    Mauris sagittis pellentesque lacus eleifend lacinia...
					  </div>
					  <div className="mdl-card__actions mdl-card--border">
					    <a className="mdl-button mdl-button--accent mdl-js-button mdl-js-ripple-effect">
					      Get Started
					    </a>
					  </div>
					</div>  
				</div>

				<div className="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone">
					<div className="mdl-card mdl-shadow--2dp">
					  <div className="mdl-card__title mdl-card--expand">
					    <h2 className="mdl-card__title-text">Feature </h2>
					  </div>
					  <div className="mdl-card__supporting-text">
					    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
					    Mauris sagittis pellentesque lacus eleifend lacinia...
					  </div>
					  <div className="mdl-card__actions mdl-card--border">
					    <a className="mdl-button mdl-button--accent mdl-js-button mdl-js-ripple-effect">
					      Get Started
					    </a>
					  </div>
					</div>  
				</div>

				<div className="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone">
					<div className="mdl-card mdl-shadow--2dp">
					  <div className="mdl-card__title mdl-card--expand">
					    <h2 className="mdl-card__title-text">Feature </h2>
					  </div>
					  <div className="mdl-card__supporting-text">
					    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
					    Mauris sagittis pellentesque lacus eleifend lacinia...
					  </div>
					  <div className="mdl-card__actions mdl-card--border">
					    <a className="mdl-button mdl-button--accent mdl-js-button mdl-js-ripple-effect">
					      Get Started
					    </a>
					  </div>
					</div>  
				</div>

    		</Grid>
		)
	}

}

export default MdlContent1;