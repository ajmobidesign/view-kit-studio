import React, { Component, PropTypes } from 'react';
import { Icon, Footer, FooterSection, FooterLinkList, FooterDropDownSection} from 'react-mdl';


class MdlLargeFooter extends Component{

	

	render(){

		return(
			<Footer size="mega">

				

				
			    <FooterSection type="middle" >

				   
			        <FooterDropDownSection title="Features">
			            <FooterLinkList>
			                <a href="#">About</a>
			                <a href="#">Terms</a>
			                <a href="#">Partners</a>
			                <a href="#">Updates</a>
			            </FooterLinkList>
			        </FooterDropDownSection>
			        <FooterDropDownSection title="Details">
			            <FooterLinkList>
			                <a href="#">Specs</a>
			                <a href="#">Tools</a>
			                <a href="#">Resources</a>
			            </FooterLinkList>
			        </FooterDropDownSection>
			        <FooterDropDownSection title="FAQ">
			            <FooterLinkList>
			                <a href="#">Questions</a>
			                <a href="#">Answers</a>
			                <a href="#">Contact Us</a>
			            </FooterLinkList>
			        </FooterDropDownSection>

			        <FooterDropDownSection title="Technology">
			            <FooterLinkList>
			                <a href="#">How it works</a>
			                <a href="#">Patterns</a>
			                <a href="#">Usage</a>
			                <a href="#">Products</a>
			                <a href="#">Contracts</a>
			            </FooterLinkList>
		        	</FooterDropDownSection>

			        


			        
			    </FooterSection>
			    <FooterSection type="bottom" >
			    	<FooterSection type="left" >
				        <FooterLinkList>
				            <a href="#">Help</a>
				            <a href="#">Privacy & Terms</a>
				        </FooterLinkList>
			        </FooterSection>

			        <FooterSection type="right" >
				    	<FooterLinkList>
							<a href="#"><i className="fa fa-twitter" aria-hidden="true"></i></a>
			                <a href="#"><i className="fa fa-facebook-official" aria-hidden="true"></i></a>
			                <a href="#"><i className="fa fa-google-plus" aria-hidden="true"></i></a>
				        </FooterLinkList>    
					</FooterSection>

			    </FooterSection>
			</Footer>
		)
	}

}

export default MdlLargeFooter;