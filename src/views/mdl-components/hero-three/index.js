import React, { Component } from 'react';
import { Grid, Textfield,  Cell, Icon, Button, Card} from 'react-mdl';



class HeroThree extends Component{

    
	render(){
		return(
			<Grid>

    			<div className="mdl-cell mdl-cell--8-col mdl-cell--12-col-tablet mdl-cell--12-col-phone">
    				<div className="hero-text-section">
    					<h3>Diamond Marketing Solutions</h3>

    				</div>
    				<div className="hero-form mdl-shadow--6dp mdl-grid">
                        <div className="mdl-cell mdl-cell--12-col">
                            <h4>Sign up for our newsletter</h4>
                        </div>
    					
    					
                        <div className="mdl-cell mdl-cell--12-col">
                            <form action="#">
                                
                                <Textfield
                                    onChange={() => {}}
                                    label="Enter Email"/>

                                <button className="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"> Join </button>
                            </form>
                        </div>   
    				</div> 
                    <div className="hero-text-section">
                        <p>See our <a href="#">Privacy Policy</a> and <a href="#">Terms of Service</a> for details.</p> 
                    </div>   				
    			</div>
    		</Grid>
		)
	}

}

export default HeroThree;