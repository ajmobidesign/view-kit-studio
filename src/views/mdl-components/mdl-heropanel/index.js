import React, { Component, PropTypes } from 'react';
import { RadioGroup, Radio} from 'react-mdl';

class MdlHeropanel extends Component{

	
	static propTypes = {
		components : PropTypes.object.isRequired,
		update: PropTypes.func.isRequired,
		docId: PropTypes.string.isRequired
	}

	constructor(props, context){
		super(props, context);

		
		this.onChange = ::this.onChange;
	}

	onChange(event){
		
		let v = event.target.value;
		let docId = this.props.docId
		
		let newOb = Object.assign({}, this.props.components, {hero: v})
		this.props.update(newOb, docId)		
		
	}



	render(){

		let hero= this.props.components.hero;

		return(
			<div className="panel-style">	
				<div className="radio-panel">
					<RadioGroup value={hero} container="ul" childContainer="li" name="navtool" onChange={this.onChange}>
						
							<Radio value="0" > Hero 1</Radio>
						
							<Radio value="1" > Hero 2</Radio>
						
							<Radio value="2" > Hero 3</Radio>
						
								
								
					</RadioGroup>	
				</div>
			</div>
		)
	}
}



export default MdlHeropanel;