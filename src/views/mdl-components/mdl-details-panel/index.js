import React, { Component, PropTypes } from 'react';
import { RadioGroup, Radio} from 'react-mdl';

class MdlDetailsPanel extends Component{

	
	static propTypes = {
		components : PropTypes.object.isRequired,
		update: PropTypes.func.isRequired,
		docId: PropTypes.string.isRequired
	}

	constructor(props, context){
		super(props, context);

		
		this.onChange = ::this.onChange;
	}

	onChange(event){
		
		let v = event.target.value;
		let docId = this.props.docId
		
		let newOb = (v == 'remove') ? Object.assign({}, this.props.components, {details: null}) : Object.assign({}, this.props.components, {details: v})
		this.props.update(newOb, docId)		
		
	}



	render(){

		let val= (this.props.components.details) ? this.props.components.details : "remove";


		return(
			<div className="panel-style">	
				<div className="radio-panel">
					<RadioGroup value={val} container="ul" childContainer="li" name="detailsradio" onChange={this.onChange}>
						
							<Radio value="0" > Tabs Layout</Radio>
						
							<Radio value="1" > Card Layout</Radio>
						
							<Radio value="2" > Video Layout</Radio>

							<Radio value="remove" > Remove this Section</Radio>
							
					</RadioGroup>	
				</div>
			</div>
		)
	}
}



export default MdlDetailsPanel;