import React, { Component } from 'react';
import { Grid, Layout, Header, Drawer, Navigation, Content, Textfield, HeaderRow, Cell, Icon, Button} from 'react-mdl';



class HeroTwo extends Component{



	render(){
		return(
			<Grid>

    			<div className="mdl-cell mdl-cell--8-col mdl-cell--10-col-tablet mdl-cell--10-col-phone">
    				<div className="hero-text-section">
    					<div className="hero-text-section">
                            <h3><i className="material-icons">business</i></h3>
                            <h3> Business Solutions for the Digital Age</h3>
                            
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin pulvinar, dolor dignissim fringilla viverra, massa dui condimentum dui, vel porta velit ante sit amet neque. Vel porta velit ante sit amet neque</p>
                            <div className="hero-button-section">
                                <button className="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">
                                    Button </button> <button className="mdl-button mdl-js-button mdl-button--raised mdl-button--accent">
                           Listen to our Podcast <i className="material-icons">play_arrow</i></button>
                            </div>
                            
                        </div>
    				</div>
    			
    				
    			</div>
    		</Grid>
		)
	}

}

export default HeroTwo;