import React, { Component } from 'react';
import { Grid, Textfield,  Cell, Icon, Button, Card} from 'react-mdl';



class MdlContact3 extends Component{



	render(){

		return(
			<Grid>
				<div className="mdl-cell mdl-cell--12-col" >
					<div className="contact-title">

					<h4>Contact Details</h4>
					255 Stark Street, Suite A5, New York, NY  11001   |    Phone : 1-888-888-8888 |  Hours : 10am - 5pm | <a href="#" className="social-button"><i className="fa fa-twitter" aria-hidden="true"></i></a>
		                <a href="#" className="social-button"><i className="fa fa-facebook-official" aria-hidden="true"></i></a>
		                <a href="#" className="social-button"><i className="fa fa-google-plus" aria-hidden="true"></i></a>

					</div>
					
				</div>
				<div className="mdl-cell mdl-cell--6-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-grid">
					<div className="mdl-cell mdl-cell--10-col map-container">


					</div>
				</div>
				<div className="mdl-cell mdl-cell--6-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-cell--12-phone mdl-grid">
					
					<div className="mdl-cell mdl-cell--12-col" >
						<form>
							<div>
								<Textfield
								    onChange={() => {}}
								    floatingLabel
								    label="Name "/>
							</div>
							<br />
							<div>
								<Textfield
								    onChange={() => {}}
								    floatingLabel
								    label="Email "/>
							</div> 
							<div>
								<Textfield
							    onChange={() => {}}
							    label="Message"
							    floatingLabel
							    rows={3} />

							</div>  

							 <p>
							 	<Button raised accent> Send Message </Button>
							 </p>   

						</form>
					</div>
				</div>
    		</Grid>
		)
	}

}

export default MdlContact3;