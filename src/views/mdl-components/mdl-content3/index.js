import React, { Component } from 'react';
import { Grid, Textfield,  Cell, Icon, Button, Card, Tab, Tabs} from 'react-mdl';



class MdlContent3 extends Component{



	constructor(props, context){
		super(props, context);

		this.state = { activeTab: 2 };
		this.renderTab = ::this.renderTab;
	}


	renderTab(){
		switch(this.state.activeTab){
			case 0:
				return(
						<div className="mdl-cell  mdl-cell--12-col mdl-cell--12-col-tablet mdl-grid">
							<div className="mdl-cell mdl-cell--6-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-card">
								<div className="mdl-card__title">
								    <p >
								    	
								    	<i className="material-icons">local_see</i>
								    </p>
								  </div>
								  <div className="mdl-card__supporting-text">
								    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin pulvinar, dolor dignissim fringilla viverra, massa dui condimentum dui, vel porta velit ante sit amet neque. Aliquam fringilla justo sed faucibus viverra. Quisque dui massa, placerat eget commodo nec, cursus vel lorem. Suspendisse ac tincidunt arcu, a feugiat eros. Proin ornare diam turpis, et suscipit neque porttitor gravida. Aenean a lobortis ipsum. Quisque a eros massa. 							    
								  </div>
							</div>
						</div>
						)
			case 1:
				return(	
						<div className="mdl-cell mdl-grid mdl-cell--12-col" >
							<div className="mdl-cell mdl-cell--6-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-card">
								<div className="mdl-card__title">
								    <p >
								    	
								    	<i className="material-icons">place</i>
								    </p>
								  </div>
								  <div className="mdl-card__supporting-text">
								    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin pulvinar, dolor dignissim fringilla viverra, massa dui condimentum dui, vel porta velit ante sit amet neque. Aliquam fringilla justo sed faucibus viverra. Quisque dui massa, placerat eget commodo nec, cursus vel lorem. Suspendisse ac tincidunt arcu, a feugiat eros. Proin ornare diam turpis, et suscipit neque porttitor gravida. Aenean a lobortis ipsum. Quisque a eros massa. 							    
								  </div>
							</div>
						</div>
						)
			case 2:
				return(	
						<div className="mdl-cell mdl-grid mdl-cell--12-col" >
							<div className="mdl-cell mdl-cell--6-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-card">
								<div className="mdl-card__title">
								    <p >
								    	
								    	<i className="material-icons">local_offer</i>
								    </p>
								  </div>
								  <div className="mdl-card__supporting-text">
								    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin pulvinar, dolor dignissim fringilla viverra, massa dui condimentum dui, vel porta velit ante sit amet neque. Aliquam fringilla justo sed faucibus viverra. Quisque dui massa, placerat eget commodo nec, cursus vel lorem. Suspendisse ac tincidunt arcu, a feugiat eros. Proin ornare diam turpis, et suscipit neque porttitor gravida. Aenean a lobortis ipsum. Quisque a eros massa. 						    
								  </div>
							</div>
						</div>
						)
			case 3:
				return(
						<div className="mdl-cell mdl-grid mdl-cell--12-col" >
							<div className="mdl-cell mdl-cell--6-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-card">
								<div className="mdl-card__title">
								    <p >
								    	
								    	<i className="material-icons">local_mall</i>
								    </p>
								  </div>
								  <div className="mdl-card__supporting-text">
								    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin pulvinar, dolor dignissim fringilla viverra, massa dui condimentum dui, vel porta velit ante sit amet neque. Aliquam fringilla justo sed faucibus viverra. Quisque dui massa, placerat eget commodo nec, cursus vel lorem. Suspendisse ac tincidunt arcu, a feugiat eros. Proin ornare diam turpis, et suscipit neque porttitor gravida. Aenean a lobortis ipsum. Quisque a eros massa. 							    
								  </div>
							</div>
						</div>
						)
		
					
			default:
				return "";	

			}
	}					



	render(){

		return(
				<Grid>
				
				<div className="mdl-cell mdl-cell--12-col  mdl-grid">
					<div className="mdl-cell mdl-cell--12-col  features-headline">
						<h4>Our Amazing Features </h4>
					</div>
				</div>
				
				<div className="mdl-cell mdl-cell--12-col  mdl-grid">
					 <Tabs activeTab={this.state.activeTab} onChange={(tabId) => this.setState({ activeTab: tabId })} ripple>
	                    <Tab>Feature 1</Tab>
	                    <Tab>Feature 2</Tab>
	                    <Tab>Feature 3</Tab>
	                    <Tab>Feature 4</Tab>
	                </Tabs>
	                <section className="tabs-container">
	                    <div className="content">

	                    	{this.renderTab()}
	                    </div>
	                </section>
                </div>
    		</Grid>)
	}

}

export default MdlContent3;