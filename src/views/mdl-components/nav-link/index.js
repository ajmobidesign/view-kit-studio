import React, { Component, PropTypes } from 'react';


class NavLink extends Component{

	static proptypes = {
		value: PropTypes.number.isRequired, 
		navChange: PropTypes.func.isRequired
	}

	constructor(props, context){
		super(props, context);

		this.onClick = ::this.onClick
	}

	onClick(event){
		event.preventDefault();
		this.props.navChange(this.props.value);
	}


	render(){
		return(
			<a className="mdl-navigation__link" href="" onClick={this.onClick}> {this.props.children} </a>
		)
	}
}

export default NavLink;