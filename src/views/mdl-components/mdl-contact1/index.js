import React, { Component } from 'react';
import { Grid, Textfield,  Cell, Icon, Button, Card} from 'react-mdl';



class MdlContact1 extends Component{




	stopChange(event){
		event.preventDefault();
	}



	render(){

		return(
			<div className="mdl-grid " >
			<div className="mdl-cell mdl-cell--12-col mdl-grid mdl-cell--12-col-tablet mdl-cell--12-col-phone contact-form-details-container mdl-shadow--2dp no-spacing" >
				<div className="mdl-cell mdl-cell--7-col mdl-cell--12-col-tablet mdl-cell--12-col-phone  contact-form" >
					
					<div className=" "> 
						
						<h4> Send us a message </h4>

						<form>
							<div>
								<Textfield
								    onChange={() => {}}
								    floatingLabel
								    label="Name "/>
							</div>
							<br />
							<div>
								<Textfield
								    onChange={() => {}}
								    floatingLabel
								    label="Email "/>
							</div> 
							<div>
								<Textfield
							    onChange={() => {}}
							    label="Message"
							    floatingLabel
							    rows={3} />

							</div>  

							 <p>
							 	<button className="mdl-button mdl-js-button mdl-button--raised mdl-button--accent" onClick={this.stopChange}>
	                            Submit </button>
							 </p>   
						</form>
					</div>


				</div>
				<div className="mdl-cell mdl-cell--5-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-grid contact-details " >
					<div className="mdl-cell mdl-cell--10-col mdl-cell--1-offset" >
						<h4>Contact Details </h4>
						<p>
							255 Stark Street <br />
							Suite A5 <br />
							New York, NY  11001
						</p>

						
						<p>
							Phone : 1-888-888-8888 <br />
							Hours : 10am - 5pm
						</p>

						<p>
							<a href="#" className="social-button"><i className="fa fa-twitter" aria-hidden="true"></i></a>
		                	<a href="#" className="social-button"><i className="fa fa-facebook-official" aria-hidden="true"></i></a>
		                	<a href="#" className="social-button"><i className="fa fa-google-plus" aria-hidden="true"></i></a>
						</p>

					</div>
				</div>

			  </div>	
    		</div>
		)
	}

}

export default MdlContact1;