import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';
import MdlPalette from '../../mdl-components/mdl-palette';
import MdlElementstyles from '../../mdl-components/mdl-elementstyles';
import ControlPanel from '../../app-components/control-panel';
import SharePanel from '../../app-components/share-panel';
import PanelTab from '../../app-components/panel-tab';
import StickyPanel from '../../app-components/sticky-panel';

import { Icon } from 'react-mdl';
import moment from 'moment';




class StylePanel extends Component{

	static proptypes ={
		style: PropTypes.object.isRequired,
		update: PropTypes.func.isRequired,
		docId: PropTypes.string.isRequired
	}

	constructor(props, context){
		super(props, context);

		this.state = {tab: 0};
		this.switchTab = ::this.switchTab;
	}


	switchTab(idx){
		this.setState({tab: idx});
	}




	
	renderTabs(){

		const tabList = [{title: 'palette'}, {title: 'format_color_text'}, {title: 'info'}];
		return tabList.map((tab, index)=>{
			let isActive = (this.state.tab === index) 
			return (<PanelTab isActive={isActive}  key={index} tabshift={this.switchTab} tab={index} title={tab.title}></PanelTab>)
		});
	}

	renderTabContent(){
		switch(this.state.tab){
			case 0: 
				return (<MdlPalette style={this.props.style} update={this.props.update} docId={this.props.docId} savingTemplate={this.props.savingTemplate}></MdlPalette>);
			case 1:
				return (<MdlElementstyles style={this.props.style} update={this.props.update} docId={this.props.docId} savingTemplate={this.props.savingTemplate}></MdlElementstyles>);
			case 2: 
				return (<div className="mdl-shadow--2dp" style={{background: '#fff', padding: '20px', width: '80%', margin: 'auto'}}> 
							<p> 
								Material Design palette comprises primary and accent colors that can be used for illustration or to develop your brand colors. They’ve been designed to work harmoniously with each other. The color palette starts with primary colors and fills in the spectrum to create a complete and usable palette.<br />
								This panel allow you to select color palettes for your projects with primary and accent colors from the Material Design Palette. Color combinations are restricted based on recommendations in the Material Design guidelines.
								<br /> 
								 <a href="https://material.google.com/style/color.html" target="_blank">Read More Here</a>

							</p>

						</div>);
							
			default:
				return (<div></div>)	
		}
	}



	render(){

		
		let toggleState = classNames({
			'toggled_open' : this.state.open,
			'toggled_close': !this.state.open
		});


		let signInMsgStyle = {
			color: 'red',
			fontSize: '11px',
			paddingTop: '8px', 
			width: '80%',
			margin: 'auto'
		}

		//let signInMsg = (this.props.loggedIn) ? null : <div style={signInMsgStyle}> Changes you make will not be saved. Sign In to create new templates and save for free.</div>;



		return(
				<div style={{height: '100%'}}>
					<div className="control-panel-tab_bar">
						{this.renderTabs()}
					</div>
					<div className="control-panel-tab_content">
						
						{this.renderTabContent()}
					</div>
				</div>	
		)
	}

}

export default StylePanel;