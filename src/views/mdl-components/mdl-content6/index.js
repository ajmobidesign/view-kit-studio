import React, { Component } from 'react';
import { Grid, Textfield,  Cell, Icon, Button, Card} from 'react-mdl';



class MdlContent6 extends Component{



	render(){

		return(
			<Grid>
				<div className="mdl-cell mdl-cell--3-col  mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-grid details-item-image-block mix-grid-left-col">

					

						
						<div className="mdl-cell mdl-cell--12-col icon-text-container">
							<h4> A Small Headline</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a felis nulla. Vivamus massa eros, commodo id felis vel, fermentum malesuada justo.   </p>
						</div>

						
						<div className="mdl-cell mdl-cell--9-col icon-text-container">
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
						</div>
						<div className="mdl-cell mdl-cell--3-col icon-container">
							<i className="material-icons">forum </i>
						</div>

						
						<div className="mdl-cell mdl-cell--9-col icon-text-container">
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
						</div>
						<div className="mdl-cell mdl-cell--3-col icon-container">
							<i className="material-icons">email </i>
						</div> 


						
						<div className="mdl-cell mdl-cell--9-col icon-text-container">
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer a consequat diam. </p>
						</div>
						<div className="mdl-cell mdl-cell--3-col icon-container">
							<i className="material-icons">security </i>
						</div>
						


					


				</div>
				<div className="mdl-cell mdl-cell--6-col  mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-grid no-spacing details-item-image-block mix-grid-center-col">
					<div className="mdl-cell mdl-cell--12-col  mdl-shadow--4dp mdl-card">
						  	<div className="mdl-card__title mdl-card--expand">
							    
							</div>

							<div className="mdl-card__supporting-text">
							    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
							    Aenan convallis. 
							     
							</div>
							<div className="mdl-card__actions ">
								    <button className="mdl-button mdl-js-button mdl-button--fab mdl-button--colored">
									  <i className="material-icons">star</i>
									</button>
							</div>
					</div>
				</div>
				<div className="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-grid  details-item-block mix-grid-right-col">

					

						<div className="mdl-cell mdl-cell--12-col">
							<h4 className="mdl-typography--display-1"> A Larger Headline </h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin pulvinar, dolor dignissim fringilla viverra, massa dui condimentum dui, vel porta velit ante sit amet neque. Aliquam fringilla justo sed faucibus viverra.</p>
						</div>

						<div className="mdl-cell mdl-cell--3-col icon-container">
							<i className="material-icons">beenhere </i>
						</div>
						<div className="mdl-cell mdl-cell--9-col icon-text-container">
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
						</div>
						<div className="mdl-cell mdl-cell--3-col icon-container">
							<i className="material-icons">beenhere </i>
						</div>
						<div className="mdl-cell mdl-cell--9-col icon-text-container">
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
						</div>
						

					

				</div>
    		</Grid>
		)
	}
}

export default MdlContent6;