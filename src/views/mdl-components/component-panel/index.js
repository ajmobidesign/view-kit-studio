import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';
import ControlPanel from '../../app-components/control-panel';
import PanelTabText from '../../app-components/panel-tab-text';
import { Icon } from 'react-mdl';

import MdlHeropanel from '../../mdl-components/mdl-heropanel';
import MdlFeaturesPanel from '../../mdl-components/mdl-features-panel';
import MdlDetailsPanel from '../../mdl-components/mdl-details-panel';
import MdlGridPanel from '../../mdl-components/mdl-grid-panel';
import MdlContactPanel from '../../mdl-components/mdl-contact-panel';
import MdlFooterPanel from '../../mdl-components/mdl-footer-panel';


class ComponentPanel extends Component{

	static proptypes ={
		
		components: PropTypes.object.isRequired,
		updateComponent: PropTypes.func.isRequired,
		docId: PropTypes.string.isRequired
	}

	constructor(props, context){
		super(props, context);

		this.state = {tab: 0};		
		this.switchTab = ::this.switchTab;
		this.updateFunc = ::this.updateFunc
	}


	switchTab(idx){
		this.setState({tab: idx});
	}

	updateFunc(arg1, arg2){
		//console.log(arg1, arg2)
		this.props.updateComponent(arg1, arg2)
	}

	
	renderTabs(){

		const tabList = [{title: 'Hero'}, {title: 'Features'}, {title: 'Details'}, {title: 'Grid'}, {title: 'Contact'},  {title: 'Footer'}];
		return tabList.map((tab, index)=>{
			let isActive = (this.state.tab === index) 
			return (<PanelTabText isActive={isActive}  key={index} tabshift={this.switchTab} tab={index} title={tab.title}></PanelTabText>)
		});
	}

	renderTabContent(){
		switch(this.state.tab){
			case 0: 
				return (<MdlHeropanel components={this.props.components}  docId={this.props.docId} update={this.updateFunc} savingTemplate={this.props.savingTemplate} ></MdlHeropanel>)	
			case 1: 
				return (<MdlFeaturesPanel components={this.props.components}  docId={this.props.docId} update={this.updateFunc} savingTemplate={this.props.savingTemplate}></MdlFeaturesPanel>)
			case 2: 
				return (<MdlDetailsPanel components={this.props.components}  docId={this.props.docId} update={this.updateFunc} savingTemplate={this.props.savingTemplate}></MdlDetailsPanel>)
			case 3: 
				return (<MdlGridPanel components={this.props.components}  docId={this.props.docId} update={this.updateFunc} savingTemplate={this.props.savingTemplate}></MdlGridPanel>)	
			case 4: 
				return (<MdlContactPanel components={this.props.components}  docId={this.props.docId} update={this.updateFunc} savingTemplate={this.props.savingTemplate}></MdlContactPanel>)

			case 5: 
				return (<MdlFooterPanel components={this.props.components}  docId={this.props.docId} update={this.updateFunc} savingTemplate={this.props.savingTemplate}></MdlFooterPanel>)		
			default:
				return (<div></div>)	
		}
	}



	render(){


		let signInMsgStyle = {
			color: 'red',
			fontSize: '11px',
			paddingTop: '8px', 
			width: '80%',
			margin: 'auto'
		}

		//let signInMsg = (this.props.loggedIn) ? null : <div style={signInMsgStyle}> Changes you make will not be saved. Sign In to create new templates and save for free.</div>;


		return(
			<div style={{height: '100%'}}>
				<div className="control-panel-tab_bar">
					{this.renderTabs()}
				</div>
				<div className="control-panel-tab_content" >
					
					{this.renderTabContent()}
				</div>
			</div>
			)
	}


}

export default ComponentPanel;