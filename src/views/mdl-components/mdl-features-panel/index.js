import React, { Component, PropTypes } from 'react';
import { RadioGroup, Radio} from 'react-mdl';

class MdlFeaturesPanel extends Component{

	
	static propTypes = {
		components : PropTypes.object.isRequired,
		update: PropTypes.func.isRequired,
		docId: PropTypes.string.isRequired
	}

	constructor(props, context){
		super(props, context);

		
		this.onChange = ::this.onChange;
	}

	onChange(event){
		
		let v = event.target.value;
		let docId = this.props.docId
		
		let newOb = (v == 'remove') ? Object.assign({}, this.props.components, {features: null}) : Object.assign({}, this.props.components, {features: v})
		this.props.update(newOb, docId)		
		
	}



	render(){

		let val= (this.props.components.features) ? this.props.components.features : "remove";


		return(
			<div className="panel-style">	
				<div className="radio-panel">
					<RadioGroup value={val} container="ul" childContainer="li" name="featuresradio" onChange={this.onChange}>
						
							<Radio value="0" > Cards </Radio>
						
							<Radio value="1" > Grid </Radio>
						
							<Radio value="2" > Tabs </Radio>
						
							<Radio value="remove" > Remove this Section</Radio>
								
					</RadioGroup>	
				</div>
			</div>
		)
	}
}



export default MdlFeaturesPanel;