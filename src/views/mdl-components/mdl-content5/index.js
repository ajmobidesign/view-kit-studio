import React, { Component } from 'react';
import { Grid, Textfield,  Cell, Icon, Button, Card, Tabs, Tab} from 'react-mdl';



class MdlContent5 extends Component{


	constructor(props, context){
		super(props, context);

		this.state = { activeTab: 0 };
		this.renderTab = ::this.renderTab;
	}


	renderTab(){
		switch(this.state.activeTab){
			case 0:

				return (

						<div className="mdl-cell mdl-cell--12-col  mdl-grid mix-grid-3-col">
							<div className="mdl-cell mdl-cell--3-col mdl-cell--8-col-tablet mdl-cell--8-col-phone  mdl-grid mix-grid-left-col">

								<div className="mdl-cell mdl-cell--12-col mdl-card">
									<div className="mdl-card__supporting-text">
									  	<h4>Headline</h4>
									    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
									    Mauris sagittis pellentesque lacus eleifend lacinia...
									</div>
								</div>
								<div className="mdl-cell mdl-cell--12-col mdl-card">
									<div className="mdl-card__supporting-text">
									  	<h4>Headline</h4>
									    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
									    Mauris sagittis pellentesque lacus eleifend lacinia...
									</div>
								</div>	

							</div>
							<div className="mdl-cell mdl-cell--6-col mdl-cell--8-col-tablet mdl-cell--8-col-phone mdl-grid mix-grid-center-col">

								<div className=" mdl-card mdl-shadow--2dp">
								  <div className="mdl-card__title mdl-card--expand">
								    <h2 className="mdl-card__title-text">Update</h2>
								  </div>
								  <div className="mdl-card__supporting-text">
								    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
								    Aenan convallis.
								  </div>
								</div>
										
							</div>
							<div className="mdl-cell mdl-cell--3-col mdl-cell--8-col-tablet mdl-cell--8-col-phone  mdl-grid mix-grid-right-col">

								<div className="mdl-cell mdl-cell--12-col mdl-card">
									<div className="mdl-card__supporting-text">
									  	<h4>Headline</h4>
									    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
									    Mauris sagittis pellentesque lacus eleifend lacinia...
									</div>
								</div>
								<div className="mdl-cell mdl-cell--12-col mdl-card">
									<div className="mdl-card__supporting-text">
									  	<h4>Headline</h4>
									    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
									    Mauris sagittis pellentesque lacus eleifend lacinia...
									</div>
								</div>
										
							</div>

					</div>)

			case 1: 

				return (<div className="mdl-cell mdl-cell--12-col   mdl-grid ">	

						
							

								<div className="mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--8-col-phone mdl-grid" >
										<div className="mdl-card mdl-cell--12-col ">
									 
											<div className="mdl-card__supporting-text ">
												
												<p>
													Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin pulvinar, dolor dignissim fringilla viverra, massa dui condimentum dui, vel porta velit ante sit amet neque. Aliquam fringilla justo sed faucibus viverra. 

												</p>
												<p>
													Nunc non urna egestas, luctus metus sit amet, vestibulum orci. Vestibulum aliquam bibendum tristique. Nullam et elementum lorem, et finibus sem. Sed venenatis faucibus eros vel condimentum. Fusce vestibulum nisi finibus tincidunt aliquam. Maecenas luctus, odio vitae suscipit rhoncus, lorem sem porttitor magna, non laoreet augue nulla sed tortor.


												</p>
											    
										  	</div>
										</div>

								</div>


								<div className="mdl-cell mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--8-col-phone mdl-cell--12-tablet mdl-cell--12-phone mdl-grid" >

									<div className="mdl-cell mdl-cell--12-col mdl-card card-with-left-border mdl-grid mdl-shadow--2dp">

										<div className="mdl-cell mdl-cell--4-col image-block">


										</div>
										<div className="mdl-cell mdl-cell--8-col">
											
											<div className="mdl-card__supporting-text">
													<strong> Jane Doe </strong><br />
												    <em> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.
												    Mauris sagittis pellentesque lacus eleifend lacinia." </em> 
											</div>

										</div>

										
										
									</div>
									<div className="mdl-cell mdl-cell--12-col mdl-card card-with-left-border mdl-shadow--2dp mdl-grid">
										<div className="mdl-cell mdl-cell--4-col image-block">


										</div>
										<div className="mdl-cell mdl-cell--8-col">
											
											<div className="mdl-card__supporting-text">
													<strong> Jane Doe </strong><br />
												    <em> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.
												    Mauris sagittis pellentesque lacus eleifend lacinia. <br /> 
												    Vivamus volutpat nunc sed erat rhoncus, eget sodales ante scelerisque."
												    </em>  
											</div>

										</div>
										
									</div>
									<div className="mdl-cell mdl-cell--12-col mdl-card card-with-left-border mdl-shadow--2dp mdl-grid">
										<div className="mdl-cell mdl-cell--4-col image-block">


										</div>
										<div className="mdl-cell mdl-cell--8-col">
											
											<div className="mdl-card__supporting-text">
													<strong> Jane Doe </strong><br />
												    <em> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.
												    Mauris sagittis pellentesque lacus eleifend lacinia. id consectetur lorem tellus ut elit." </em>
											</div>

										</div>
										
									</div>

								</div>

							


					</div>)

			case 2: 
				return (<div className="mdl-cell mdl-cell--12-col  mdl-grid ">


							<div className="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-grid"> 
									<div className="mdl-card__supporting-text ">
											<p>
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin pulvinar, dolor dignissim fringilla viverra, massa dui condimentum dui. 
											</p>
											<p>
												Nunc non urna egestas, luctus metus sit amet, vestibulum orci. Vestibulum aliquam bibendum tristique.  


											</p>
								  	</div>
							</div>


							<div className="mdl-cell mdl-cell--9-col mdl-grid">

								<div className="mdl-card mdl-cell--5-col picture-card mdl-shadow--2dp">
									<div className="mdl-card__title mdl-card--expand">
									    
									</div>  
									<div className="mdl-card__supporting-text ">
										<h4 className="mdl-card__title-text">Picture 1 </h4>
									    
								  	</div>
								</div>  

								<div className="mdl-card mdl-cell--5-col picture-card mdl-shadow--2dp">
									<div className="mdl-card__title mdl-card--expand">
									    
									</div>  
									<div className="mdl-card__supporting-text ">
										<h4 className="mdl-card__title-text">Picture 2 </h4>
									    
								  	</div>
								</div> 

								<div className="mdl-card mdl-cell--5-col picture-card mdl-shadow--2dp">
									<div className="mdl-card__title mdl-card--expand">
									    
									</div>  
									<div className="mdl-card__supporting-text mdl-shadow--2dp">
										<h4 className="mdl-card__title-text">Picture 3 </h4>
									    
								  	</div>
								</div> 

								<div className="mdl-card mdl-cell--5-col picture-card mdl-shadow--2dp">
									<div className="mdl-card__title mdl-card--expand">
									    
									</div>  
									<div className="mdl-card__supporting-text ">
										<h4 className="mdl-card__title-text">Picture 4 </h4>
									    
								  	</div>
								</div> 

								



								 
							
							</div>
								
					</div>)

			default: 
				return ""		

		}
	}



	render(){

		return(
			<Grid>
				 <Tabs activeTab={this.state.activeTab} onChange={(tabId) => this.setState({ activeTab: tabId })} ripple>
                    <Tab>How We Are Different</Tab>
                    <Tab>Reviews</Tab>
                    <Tab>Portfolio</Tab>
                </Tabs>
                <section className="tabs-container">
                    <div className="content">

                    	{this.renderTab()}
                    </div>
                </section>
    		</Grid>
		)
	}

}

export default MdlContent5;