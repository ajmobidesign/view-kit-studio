import React, { Component } from 'react';
import { Grid, Textfield,  Cell, Icon, Button, Card} from 'react-mdl';



class MdlContent2 extends Component{



	render(){

		return(
			<Grid>
					<div className="mdl-cell mdl-cell--12-col  mdl-grid">
						<div className="mdl-cell mdl-cell--12-col  features-headline">
							<h4>Our Amazing Features </h4>
						</div>
					</div>	
					<div className="mdl-cell mdl-cell--12-col  mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-grid feature-grid">
						<div className="mdl-cell mdl-cell--3-col  mdl-cell--7-col-tablet mdl-cell--12-col-phone   mdl-card">
							<div className="mdl-card__media" >
								<i className="material-icons">local_taxi</i>
							</div>
							<div className="mdl-card__title">
							    Feature 1
							</div>
							<div className="mdl-card__supporting-text">
							    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
							    Nullam nec egestas lacus.     
							</div>
							  
						</div>

						<div className="mdl-cell mdl-cell--3-col  mdl-cell--7-col-tablet mdl-cell--12-col-phone  mdl-card">
							<div className="mdl-card__media" >
								<i className="material-icons">business_center</i>
							</div>
							<div className="mdl-card__title">
							    Feature 2
							</div>
							<div className="mdl-card__supporting-text">
							    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
							    Nullam nec egestas lacus.     
							</div>
							  
						</div>

						<div className="mdl-cell mdl-cell--3-col  mdl-cell--7-col-tablet mdl-cell--12-col-phone  mdl-card">
							<div className="mdl-card__media" >
								<i className="material-icons">beach_access</i>
							</div>
							<div className="mdl-card__title">
							    Feature 3
							</div>
							<div className="mdl-card__supporting-text">
							    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
							    Nullam nec egestas lacus.     
							</div>
							  
						</div>

						<div className="mdl-cell mdl-cell--3-col  mdl-cell--7-col-tablet mdl-cell--12-col-phone  mdl-card">
							<div className="mdl-card__media" >
								<i className="material-icons">location_city</i>
							</div>
							<div className="mdl-card__title">
							    Feature 4
							</div>
							<div className="mdl-card__supporting-text">
							    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
							    Nullam nec egestas lacus.     
							</div>
							  
						</div>
						<div className="mdl-cell mdl-cell--3-col  mdl-cell--7-col-tablet mdl-cell--12-col-phone   mdl-card">
							<div className="mdl-card__media" >
								<i className="material-icons">local_see</i>
							</div>
							<div className="mdl-card__title">
							    Feature 5
							</div>
							<div className="mdl-card__supporting-text">
							    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
							    Nullam nec egestas lacus.     
							</div>
							  
						</div>

						<div className="mdl-cell mdl-cell--3-col  mdl-cell--7-col-tablet mdl-cell--12-col-phone  mdl-card">
							<div className="mdl-card__media" >
								<i className="material-icons">place</i>
							</div>
							<div className="mdl-card__title">
							    Feature 6
							</div>
							<div className="mdl-card__supporting-text">
							    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
							    Nullam nec egestas lacus.     
							</div>
							  
						</div>

						<div className="mdl-cell mdl-cell--3-col  mdl-cell--7-col-tablet mdl-cell--12-col-phone  mdl-card">
							<div className="mdl-card__media" >
								<i className="material-icons">local_offer</i>
							</div>
							<div className="mdl-card__title">
							    Feature 7
							</div>
							<div className="mdl-card__supporting-text">
							    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
							    Nullam nec egestas lacus.     
							</div>
							  
						</div>

						<div className="mdl-cell mdl-cell--3-col  mdl-cell--7-col-tablet mdl-cell--12-col-phone mdl-card">
							<div className="mdl-card__media" >
								<i className="material-icons">local_mall</i>
							</div>
							<div className="mdl-card__title">
							    Feature 8
							</div>
							<div className="mdl-card__supporting-text">
							    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
							    Nullam nec egestas lacus.     
							</div>
							  
						</div>
					
				</div>
    		</Grid>
		)
	}

}

export default MdlContent2;