import React, { Component } from 'react';
import { Grid, Textfield,  Cell, Icon, Button, Card} from 'react-mdl';



class MdlContent7 extends Component{



	render(){

		return(
			<Grid>
				
				<div className="mdl-cell mdl-cell--12-col mdl-grid ">
					<div className="mdl-cell mdl-cell--8-col mdl-cell--12-col-tablet details-item-block">
						<div className="player-container">
							<i className="material-icons">play_circle_outline </i>
						</div>
					</div>
				</div>
				<div className="mdl-cell mdl-cell--12-col mdl-cell--12-col-tablet mdl-grid ">
					<div className="mdl-cell mdl-cell--10-col  details-item-text-block">
						<h4 className="mdl-typography--display-1"> Watch the Video to Find out More </h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin pulvinar, dolor dignissim fringilla viverra, massa dui condimentum dui, vel porta velit ante sit amet neque. Aliquam fringilla justo sed faucibus viverra.</p>
						<p>
							<button className="mdl-button mdl-js-button mdl-button--raised mdl-button--accent"> Action Button </button>
						</p>
					</div>
				</div>
    		</Grid>
		)
	}

}

export default MdlContent7;