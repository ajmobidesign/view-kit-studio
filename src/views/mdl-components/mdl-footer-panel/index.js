import React, { Component, PropTypes } from 'react';
import { RadioGroup, Radio} from 'react-mdl';

class MdlFootPanel extends Component{

	
	static propTypes = {
		components : PropTypes.object.isRequired,
		update: PropTypes.func.isRequired,
		docId: PropTypes.string.isRequired
	}

	constructor(props, context){
		super(props, context);

		
		this.onChange = ::this.onChange;
	}

	onChange(event){
		
		let v = event.target.value;
		let docId = this.props.docId
		
		let newOb = Object.assign({}, this.props.components, {footer: v})
		this.props.update(newOb, docId)		
		
	}



	render(){

		let val= this.props.components.footer;


		return(
			<div className="panel-style">	
				<div className="radio-panel">
					<RadioGroup value={val} container="ul" childContainer="li" name="navtool" onChange={this.onChange}>
						
							<Radio value="0" > Mini Footer </Radio>
						
							<Radio value="1" > Large Footer </Radio>
						
							
							
					</RadioGroup>	
				</div>
			</div>
		)
	}
}



export default MdlFootPanel;