import React, { Component, PropTypes } from 'react';
import * as Colors from '../../styles/inline';
import MdlColorblock from '../mdl-colorblock';

class MdlElementstyles extends Component{

	static propTypes = {
		style: PropTypes.object.isRequired,
		update: PropTypes.func.isRequired,
		docId: PropTypes.string.isRequired
	}

	constructor(props, context){
		super(props, context);

		this.setPrimaryTextColor = ::this.setPrimaryTextColor;
		this.setAccentTextColor = ::this.setAccentTextColor;
	}




	setPrimaryTextColor(color, shade){

		let ob = Object.assign({}, this.props.style, {primaryTextRgb: color, primaryTextShade: shade})
		this.props.update(ob, this.props.docId)
	}

	setAccentTextColor(color, shade){

		let ob = Object.assign({}, this.props.style, {accentTextRgb: color, accentTextShade: shade})
		this.props.update(ob, this.props.docId)
	}


	renderPrimaryTextColors(){
		let primaryTextColors = [{rgb: this.props.style.primaryLightRgb, shade: 'light'}, {rgb: this.props.style.primaryDarkRgb, shade: 'dark'}, {rgb:'255,255,255', shade: 'white'} , {rgb:'66,66,66', shade: 'gray'}, {rgb:'0,0,0', shade: 'black'}];
		let isCurrent = this.props.style.primaryTextRgb;
		
		let blocks = primaryTextColors.map((c, index)=>{
			let clr = c.rgb;
			return(<MdlColorblock  setcolor={this.setPrimaryTextColor} shade={c.shade} color={clr} current={isCurrent} key={index}></MdlColorblock>)
		})
		
		return (<div className="style-panel-control-block"><span className="control-label">Primary Button Text</span><span className="control-content">{blocks}</span></div>)
	}


	renderAccentTextColors(){
		
		let accentTextColors = [{rgb: this.props.style.accentLightRgb, shade: 'color'}, {rgb:'255,255,255', shade: 'white'} , {rgb:'66,66,66', shade: 'gray'}, {rgb:'0,0,0', shade: 'black'}];
		let isCurrent = this.props.style.accentTextRgb;
		
		let blocks = accentTextColors.map((c, index)=>{
			let clr = c.rgb;
			

			return(<MdlColorblock  setcolor={this.setAccentTextColor} shade={c.shade} color={clr} current={isCurrent} key={index}></MdlColorblock>)
		})
		
		
		return (<div className="style-panel-control-block"><span className="control-label">Accent Button Text</span><span className="control-content">{blocks}</span></div>)

	}



	render(){

		return(
			<div className="panel-style " style={{paddingTop: '10px', paddingBottom: '10px'}} >
				<div className="mdl-shadow--2dp" style={{background: '#fff', paddingTop: '15px', paddingBottom: '15px'}}>
					{this.renderPrimaryTextColors()}
					{this.renderAccentTextColors()}
				</div>
			</div>
		)
	}
}



export default MdlElementstyles