import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';

export class MdlColorblock extends Component{

	/*
	static propTypes = {
		color: PropTypes.string.required,
		shade: PropTypes.string,
		current: PropTypes.string.required,
		setcolor:PropTypes.func.required
	}*/

	constructor(props, context){
		super(props, context);
		this.setColor = ::this.setColor;
	}

	setColor(){
		this.props.setcolor(this.props.color, this.props.shade);
	}

	render(){

		let clNames = classNames({
			'color-sqr': true,
			'selected': (this.props.color === this.props.current)
		})

		return (<div className={clNames} style={{background:`rgb(${this.props.color})`}} onClick={this.setColor}></div>)
	}

}

export default MdlColorblock;