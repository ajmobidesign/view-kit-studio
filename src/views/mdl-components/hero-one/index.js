import React, { Component } from 'react';
import { Grid, Layout, Header, Drawer, Navigation, Content, Textfield, HeaderRow, Cell, Icon, Button} from 'react-mdl';



class HeroOne extends Component{

	render(){
		return(
			<Grid>
                

                <div className="mdl-cell mdl-cell--7-col mdl-cell--12-col-tablet mdl-cell--12-col-phone">
                    <div className="player mdl-shadow--3dp">
                        <i className="material-icons">play_circle_filled </i>
                    </div>

                </div>
                <div className="mdl-cell mdl-cell--5-col mdl-cell--12-col-tablet mdl-cell--12-col-phone">
                    <div className="hero-text-section">
                        <h3>Welcome to the 21st Century Solutions</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin pulvinar, dolor dignissim fringilla viverra, massa dui condimentum dui, vel porta velit ante sit amet neque. Aliquam fringilla justo sed faucibus viverra.</p>
                        <button className="mdl-button mdl-js-button mdl-button--raised mdl-button--accent">
                          Get Started Right Now
                        </button>
                    </div>    
                </div>



    		</Grid>
		)
	}

}

export default HeroOne;