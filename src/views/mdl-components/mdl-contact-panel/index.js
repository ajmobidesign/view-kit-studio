import React, { Component, PropTypes } from 'react';
import { RadioGroup, Radio} from 'react-mdl';

class MdlContactPanel extends Component{

	
	static propTypes = {
		components : PropTypes.object.isRequired,
		update: PropTypes.func.isRequired,
		docId: PropTypes.string.isRequired
	}

	constructor(props, context){
		super(props, context);

		
		this.onChange = ::this.onChange;
	}

	onChange(event){
		
		let v = event.target.value;
		let docId = this.props.docId;

		console.log( 'contact', v)
		
		let newOb = (v == 'remove') ? Object.assign({}, this.props.components, {contact: null}) : Object.assign({}, this.props.components, {contact: v})
		this.props.update(newOb, docId)		
		
	}



	render(){

		let val= (this.props.components.contact) ? this.props.components.contact : "remove";


		return(
			<div className="panel-style">	
				<div className="radio-panel">
					<RadioGroup value={val} container="ul" childContainer="li" name="contactradio" onChange={this.onChange}>
						
							<Radio value="0" > Contact 1</Radio>
						
							<Radio value="1" > Contact 2</Radio>
						
							<Radio value="2" > Contact 3</Radio>

							<Radio value="remove" > Remove this Section</Radio>
							
					</RadioGroup>	
				</div>
			</div>
		)
	}
}



export default MdlContactPanel;