import React, { Component, PropTypes } from 'react';
import { Icon, Footer, FooterSection, FooterLinkList } from 'react-mdl';


class MdlMiniFooter extends Component{

	

	render(){

		return(
			<Footer size="mini" >
			    <FooterSection type="left" logo="Title">
			        <FooterLinkList>
			            <a href="#">Help</a>
			            <a href="#">Privacy & Terms</a>

						<a href="#"><i className="fa fa-twitter" aria-hidden="true"></i></a>
		                <a href="#"><i className="fa fa-facebook-official" aria-hidden="true"></i></a>
		                <a href="#"><i className="fa fa-google-plus" aria-hidden="true"></i></a>
				        
			        </FooterLinkList>
			    </FooterSection>
			</Footer>
		)
	}

}

export default MdlMiniFooter;