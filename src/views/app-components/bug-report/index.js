import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Button, Textfield, Snackbar } from 'react-mdl';
import { Db } from 'src/core/firebase';
import moment from 'moment';



export class BugReport extends Component{


	constructor(props, context){
		super(props, context);

		this.state = {bug: '', isSnackbarActive: false };

		this.submitData = ::this.submitData;
		this.updateTextfield = ::this.updateTextfield;
		this.handleTimeoutSnackbar =::this.handleTimeoutSnackbar;
  		this.handleClickActionSnackbar = ::this.handleClickActionSnackbar;
  		this.handleShowSnackbar = :: this.handleShowSnackbar;
	}

	handleShowSnackbar(){
		this.setState({ isSnackbarActive: true });
	}

	handleClickActionSnackbar(){
		
		console.log('close camp')
	}

	handleTimeoutSnackbar(){
		this.setState({ isSnackbarActive: false });
	}


	updateTextfield(event){

		let val = event.target.value;
		let Ob = {bug: val};

		this.setState(Ob);
	}


	submitData(){

		const { user } = this.props

		let Ob = {
			user: user.id,
			bug: this.state.bug, 
			data : moment().format(),
			tplId: this.props.data.id
		}

		if(user.authenticated){
			Db.submitBug(Ob)
		}
		
		this.setState({ bug: '' });

		this.handleShowSnackbar();
	}


	render(){


		const styleOb = {
			display: (this.props.user.authenticated) ? 'block' : 'none',
			margin: '10px auto',
			boxSizing: 'border-box', 
			width: '85%', 
			padding: '8px auto'
		}


		const formOb={
			display : (this.state.isSnackbarActive) ? 'none': 'block'
		}



		return(
			<div>
				
				{ (!this.props.user.authenticated) ?<p> You have to logged in to submit bugs </p> : null} 
				
				<div style={styleOb}>
					<div style={formOb}>
						<h5 style={{color:'rgba(0,0,0, .6)'}}> Submit your bug report</h5>
						<Textfield
							    onChange={this.updateTextfield}
							    label="Text lines..."
							    rows={3}
							    style={{width: '600px'}}
							    value={this.state.bug}
							    />
							    <br/>
						<Button raised accent style={{float: 'right'}} onClick={this.submitData}> Submit </Button>
					</div>
					<Snackbar
	          			active={this.state.isSnackbarActive}
	          			onClick={this.handleClickActionSnackbar}
	          			onTimeout={this.handleTimeoutSnackbar}
	          			action="">Thanks you sending the report
	          		</Snackbar>
				</div>
			</div>
		)
	}

}

const mapStateToProps = (state)=>{
	return {user : state.auth}
}

export default connect(mapStateToProps, null)(BugReport);