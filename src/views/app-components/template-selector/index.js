import React, { Component, PropTypes } from 'react';
import { Button, Icon, Grid, Cell } from 'react-mdl';
import TemplatelistItem from '../templatelist-item';
import { appTemplateList } from 'src/core/defaults';





class TemplateSelector extends Component{

/*
	static proptypes = {
		loggedIn : PropTypes.bool.isRequired,
		createNewTemplate : PropTypes.func.isRequired, 
		buyNow: PropTypes.func.isRequired,
		auth: PropTypes.object.isRequired
	}
*/

	constructor(props, context){
		super(props, context);

		this.state = {showall: true}
		this.toggleDisplay = :: this.toggleDisplay;
	}



	toggleDisplay(showall, id){

		if(id){
			this.setState({show: id});
		}

		this.setState({showall: showall})
	}


	renderItems(){



		const tpllist= appTemplateList

		let blocks = tpllist.map((t, idx)=>{

						let p = (this.props.auth.authenticated) ? this.props.purchasedList : [];
						let purchased= false;  
						let show = undefined;
						
					 	if (p.indexOf(t.id) !== -1){
							purchased = true;
						}
						

						if(!this.state.showall){
							show = (this.state.show == t.id) ? true : false;
						}
						else{
							show = true;
						}

						const { tags } = t;

						let createNewTemplate = this.props.createNewTemplate;
						let buyNow = (this.props.loggedIn)? this.props.buyNow : null;



						return <TemplatelistItem key={idx} idx={idx} title={t.name} id={t.id} details={t.details} createNewTemplate={createNewTemplate} loggedIn={this.props.loggedIn} purchased={purchased} toggle={this.toggleDisplay} show={show} price={t.price} buyNow={buyNow} auth={this.props.auth} promotion={t.promotion} tags={tags} num={t.pics}></TemplatelistItem>
					})	

		return blocks;
	}


	render(){

		

		return(
			<Grid>
			{/* 
				<Cell col={10} offset={1}>
					Filter goes here 
				</Cell>
				*/}
			    <Cell col={10} offset={1} >
						{this.renderItems()}
				</Cell>
			</Grid>	
		);
	}

}

export default TemplateSelector;