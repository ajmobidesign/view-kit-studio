import React, { Component, PropTypes } from 'react';
import { Grid, Cell, Icon } from 'react-mdl';
import { Link } from 'react-router';


export class SlideOut extends Component{

	static proptypes = {
		title: PropTypes.string.isRequired, 
		content: PropTypes.string.isRequired //maybe it should be something else
	}

	constructor(props, context){
		super(props, context);

		this.state = {open: false, iconName: 'add_circle_outline'}

		this.toggle = ::this.toggle

	}


	toggle(){
		
		let nextState = !this.state.open;
		let iconName = (nextState) ? 'remove_circle_outline' : 'add_circle_outline';

		this.setState({open: nextState, iconName: iconName})

	}


	render(){

		let h = (this.state.open) ? 'auto' : '0px';

		let iName = this.state.iconName;


		let contentStyle ={
			transition: 'height .28s ease-out', 
			height: h, 
			width: '90%',
			color: 'rgba(0,0,0, .7)',
			overflow: 'hidden', 
		}

		let iconStyle={
			color: '#ff4081', 
			cursor: 'pointer'
		}

		let titleStyle = {
			color: '#ff4081', 
			fontSize: '20px'
		}



		return (
			<div>
				<div className="mdl-grid mdl-cell mdl-cell--12-col">
					<Cell col={10}>
						<p style={titleStyle}>{this.props.title}</p>
						<div style={contentStyle}>
							<p style={{fontSize: '16px'}}>{this.props.content}

								{(this.props.link)? <Link to={this.props.link}>More Here.</Link>: null}
							</p>
						</div>
					</Cell>
					<Cell col={2}>
							<Icon name={iName} onClick={this.toggle} style={iconStyle} />
					</Cell>
					
				</div>	
		
			</div>	
		)
	}

};

export default SlideOut;