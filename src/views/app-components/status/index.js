import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';
import { connect } from 'react-redux';
import { Button, Icon, Tooltip, Menu, MenuItem, IconButton } from 'react-mdl';
import { Link, Router } from 'react-router';


export class Status extends Component{

	static contextTypes = {
	    router: React.PropTypes.object.isRequired
	  }; 


	constructor(props, context){
		super(props, context);

		this.state = {loggedin: false}
		this.loginG = ::this.loginG;
		this.loginE = ::this.loginE;
		this.logout = ::this.logout;
		this.changeRoute = ::this.changeRoute;
	} 


	componentWillReceiveProps(nextProps, nextContext){

		let currentAuthState = this.props.auth;
		
		if(currentAuthState !== nextProps.auth){
			if(nextProps.auth){
				this.props.profileSubscribe();
				this.props.templatesListSubscribe();
				this.props.userSubscribe();
			}
		}
	}



	loginE(){
		//let user = {email:'david@example.com' , pswd: 'daviddavid'}
		//this.props.authenticateUser(user);
	}
	


	loginG(){	
		//console.log('log in google')
		this.props.signInGoogle()
	}

	logout(){	

		
		let analyticsOb = {'event': 'logOut'};
		dataLayer.push(analyticsOb);

		this.props.signOut()
	}

	changeRoute(){
		this.context.router.push('/profile');
	}

	loggedInView(){

		let userIconStyle={
			position: 'relative', 
			display: 'inline-block'
		}

		let classOb = classNames({
			'is-template-dashboard': (this.props.isTemplateDash) ? true : false
		})


		return(
				<div style={{display: 'inline-block', float: 'right'}}>
					{/*<span className="user-name" style={{display:'inline-block', paddingTop: '5px'}}>{this.props.username}</span>*/}

					<div style={userIconStyle} className={classOb}>
					    <IconButton  name="account_circle" id="profile-icon" />

					    <Menu target="profile-icon" align="right">
					    	<MenuItem> {this.props.username} </MenuItem>
					        <MenuItem onClick={this.changeRoute}>Profile</MenuItem>
					        <MenuItem onClick={this.logout}>
						        Sign Out
							</MenuItem>
					        
					    </Menu>
					</div>
					
					
				</div>
			)
	}

	loggedOutView(){
		return(
			<div style={{display: 'inline-block', boxSizing: 'border-box', height: '100%'}}>
				
				<button onClick={this.loginG} className="google-signin" style={{opacity: '1'}} id="google-login">
					<img src="/images/google.svg" height="100%"/> Sign in with Google
				</button> 
				
				{/*<button className="github-signin" onClick={this.loginE}>Sign in with GitHub <img src="/images/GitHub.png"  /> </button>*/}
			</div>
		)
	}


	render(){

		const loggedin = this.props.auth;

		const cssOb = classNames({
			'header-content' : true,
			'status-bar': true,
			'logged-out': !loggedin
		})

		return(
			<div className="header-content status-bar" className={cssOb}>
				{(!loggedin)? this.loggedOutView(): null}
				{(loggedin)? this.loggedInView() : null}
			</div>
		)
	}
}


export default Status;
