import React, { Component, PropTypes } from 'react';
import { Tab, Tabs, Grid, Cell } from 'react-mdl';
import { mdlConfig } from 'src/core/defaults'


class TemplateAbout extends Component{



	renderTechList(){

		let dataOb = mdlConfig.tplDetailsMap[this.props.tplId]

		return dataOb.techList.map((t, i)=>{
			return <li className="mdl-list__item" key={i}>
				<span className="mdl-list__item-primary-content">
					{(t.link) ? <a href={t.link} >{t.name}</a> : <span style={{color: 'rgba(0,0,0, .7)'}}>{t.name}</span>}
				</span>
			</li>
		})
	}

	renderToolList(){
		let dataOb = mdlConfig.tplDetailsMap[this.props.tplId]

		return dataOb.tools.map((t, i)=>{
			return <li className="mdl-list__item" key={i}>
				<span className="mdl-list__item-primary-content">
					{(t.link) ? <a href={t.link} >{t.name}</a> : <span style={{color: 'rgba(0,0,0, .7)'}}>{t.name}</span>}
				</span>
			</li>
		})
	}



	render(){

		

		let dataOb = mdlConfig.tplDetailsMap[this.props.tplId]



		return(
			<Grid>
				<Cell col={12}>
					

					<p>{dataOb.about} </p>

					<h5 className="headline">Made With </h5>
					<ul className="demo-list-item mdl-list">

						{this.renderTechList()}

					</ul>

					<h5 className="headline">Tools Included</h5>
					<ul className="demo-list-item mdl-list">

						{this.renderToolList()}

					</ul>
									

				</Cell>
			</Grid>
		)
	}

}

export default TemplateAbout;