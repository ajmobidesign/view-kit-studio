import React, { Component, PropTypes } from 'react';
import { Button, Icon, Grid, Cell, Card, CardText, CardActions, CardMenu, CardTitle, IconButton, Spinner, Snackbar} from 'react-mdl';
import { Link } from 'react-router';
import { saleItem, paymentItem } from 'src/core/transaction/pplrest';
import _ from 'lodash';
import TemplateItemTabs from '../template-item-tabs';
import TemplateDetails from '../template-details';
import moment from 'moment';



class TemplatelistItem extends Component{


	static proptypes = {
		purchased: PropTypes.bool.isRequired,
		loggedIn: PropTypes.bool.isRequired,
		title: PropTypes.string.isRequired,
		id: PropTypes.string.isRequired,
		price: PropTypes.string.isRequired,
		buyNow: PropTypes.func.isRequired,
		createNewTemplate: PropTypes.func.isRequired, 
		auth: PropTypes.object.isRequired
		//,
		//tags: PropTypes.array.isRequired

	}
	

	constructor(props, context){
		super(props, context);

		this.state = {open: false}
		this.loadTemplate = ::this.loadTemplate;
		this.purchaseTemplate = ::this.purchaseTemplate;
		this.closeDetail = ::this.closeDetail;

		this.toggleItemView = ::this.toggleItemView;
		
	}


	loadTemplate(){
		console.log('load template', this.props.id)
		this.props.createNewTemplate(this.props.id)
	}

	toggleItemView(id){
		let curr = this.state.open;
		let val = (curr) ? false: true;
		this.setState({open: val});

		this.props.toggle(!val, id)
	}


	purchaseTemplate(){

		let ob = {title: this.props.title, id: this.props.id, price: this.props.price};
		let analyticsOb = {'event': 'buyButtonClick'};

		dataLayer.push(analyticsOb);

		this.props.buyNow(ob);
	}

	closeDetail(){
		this.props.toggle(true);
		this.setState({open: false});
	}



	render(){

		let pullOut = {
				
				display: 'block', 
				margin: '10px auto',
				height: '400px', 
				width: '450px'
			}

		let isAuth = this.props.loggedIn;
		let purchased = this.props.purchased;
		let purchasedStyle = {color: '#20c910', marginLeft: '10px', marginTop: '0px', marginBottom: '0px'}
		let cardStyle = (this.props.show) ? {display: 'block' } : { display: 'none'}
		

		let url = _.replace(this.props.id, RegExp("_","g"), '-')

		let tryKitUrl = `/try-a-kit/${url}`

		let dataOb = {about : this.props.details}


		const actionsButtonDiv = {
				width: '100%',
				boxSizing: 'border-box',
				padding: '5px'
		}

		const buttonStyle = {
				float: 'right',
				margin : '5px'
		}

		const pr = this.props.promotion
		const timeNow = moment().format();
		
		const isPromotion = (pr && moment(timeNow).isBefore(pr, 'day') ) ? true: false;

		
		//console.log(isPromotion)

		return(
			<div>
				
				
				<TemplateDetails id={this.props.id}  toggleState={this.toggleItemView} show={this.props.show}  open={this.state.open} auth={this.props.auth} price={this.props.price} title={this.props.title} tags={this.props.tags} promotion={this.props.promotion} details={this.props.details} num={this.props.num} promotionIsValid={isPromotion}>
					
					<div style={actionsButtonDiv}>

				
					 	{(!isAuth) ? <button className="mdl-button mdl-js-button mdl-button--colored mdl-button--raised" style={buttonStyle} onClick={this.loadTemplate}>Load & Try </button> : null}

					
						{(isAuth && !purchased && !isPromotion ) ? <Button  style={buttonStyle} raised onClick={this.loadTemplate}> Create New </Button>: null}
						{(isAuth && purchased || isAuth && isPromotion) ? <Button  style={buttonStyle} raised onClick={this.loadTemplate}> Create New </Button>: null}
						{(isAuth && !purchased && !isPromotion) ? <Button  style={buttonStyle} colored accent raised onClick={this.purchaseTemplate}> Buy </Button>: null}



					</div>
				
				</TemplateDetails>
				

			</div>
		);
	}

}

export default TemplatelistItem;