import React, { Component, PropTypes } from 'react';
import { RadioGroup, Radio, Switch, Button} from 'react-mdl';
import moment from 'moment';


class StickyPanel extends Component{

	constructor(props, context){
		super(props, context);

	
		
		this.addAStickyNote = ::this.addAStickyNote;
		this.stickyVisibility = ::this.stickyVisibility;
	}


	addAStickyNote(event){

		let docId = this.props.docId;
		let cY = event.nativeEvent.pageY
		let w = event.nativeEvent.window

		let offSet = (this.props.scroll>60)? (this.props.scroll) : 0; 

		//console.log(cY, window.pageYOffset, offSet)

		let ob ={
			x: '70px', 
			y: 60 + offSet +'px', 
			content: 'Write Something now....',
			timeStamp: moment().format()
		}

		let analyticsOb = {'event': 'addSticky'};
		dataLayer.push(analyticsOb);

		this.props.addSticky(docId, ob)
	}


	stickyVisibility(event){

		let st = this.props.showStickys ? false : true;

		this.props.toggleStickys(st)
	}



	render(){

		return(
			<div>
				<div className="control-panel-control-block">
					<Button onClick={this.addAStickyNote}> Add Sticky </Button>
				</div>
				<div className="control-panel-control-block">
					<Switch id="switch2" defaultChecked onChange={this.stickyVisibility}>Show All Notes</Switch>
				</div>

			</div>
		)
	}

}

export default StickyPanel;