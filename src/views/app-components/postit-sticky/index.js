import React, { Component, PropTypes } from 'react';
import moment from 'moment';
import { Icon } from 'react-mdl';
import Browser from 'detect-browser';


class PostitSticky extends Component{


	//static propTypes = {
		
	//}

	constructor(props, context){
		super(props, context);
		
		this.state = {isBeingDragged: false, styleOb: {top: '60px', left: '70px'}};

		this.dragStart = ::this.dragStart;
		this.onDrag = ::this.onDrag;
		this.dragStop = ::this.dragStop;
		this.deleteSticky = ::this.deleteSticky;
		this.onChange = ::this.onChange;
	}


	componentWillMount(){

		let browser = Browser.name;

		let firefox = browser === 'firefox';

		this.setState({firefox: firefox});

		let y = this.props.stickyOb.y;
		let x = this.props.stickyOb.x;
		this.setState({styleOb: { top: y, left: x} });
	}

	//This exists for a reason, not sure what


	componentWillReceiveProps(nextProps){
		let y = nextProps.stickyOb.y;
		let x = nextProps.stickyOb.x;

		//console.log('previous props', this.props.stickyOb.x, this.props.stickyOb.y);
		//console.log('next props', x, y );
		//console.log('will receive props called', this.state.isBeingDragged)

		if(!this.state.isBeingDragged){
			//this function is called on darg in firefox
			this.setState({styleOb: { top: y, left: x} });
		}		
	}
	



	dragStart(event){
		//console.log('started draging')
		
		this.setState({isBeingDragged: true});

		//for firefox
		event.nativeEvent.dataTransfer.setData('text', '')

		let cX = event.nativeEvent.pageX //event.nativeEvent.clientX;
		let cY = event.nativeEvent.pageY //event.nativeEvent.clientY;

		let diffx = cX - parseInt(this.state.styleOb.left);
		let diffy = cY - parseInt(this.state.styleOb.top);

		this.setState({shiftX: diffx , shiftY: diffy})
		//console.log(diffx, diffy)
	}


	dragStop(){

		if(!this.state.firefox){
			
			event.preventDefault();
		}
				
		//console.log('drag eneded')
		this.setState({isBeingDragged: false});
		this.setState({shiftX: null, shiftY: null});

		let updatedOb = Object.assign({}, this.props.stickyOb, {x: this.state.styleOb.left, y: this.state.styleOb.top})
		let docId = this.props.docId

		//console.log('Update object', updatedOb.x, updatedOb.y)
		//console.log('Current state', this.state.styleOb.left, this.state.styleOb.top)

		//console.log('Props sticky', this.props.stickyOb)

		this.props.updateSticky(docId, updatedOb)
		
	}

	onDrag(event){
		//console.log('drag event fired!!')
		event.preventDefault();

		//console.log(event.pageX, event.nativeEvent.pageX)

		//console.log('parent cords', this.props.cX, this.props.cY)

		let pcX = this.props.cX;
		let pcY = this.props.cY;

		let authorIsUser = this.props.stickyOb.author == this.props.user

		if(this.state.isBeingDragged && authorIsUser ){

			let cX = (pcX)?  pcX : event.nativeEvent.pageX //event.nativeEvent.clientX;
			let cY = (pcY)?  pcY : event.nativeEvent.pageY //event.nativeEvent.clientY;

			

			if(cX !== 0 && cY !== 0){
				let x =  cX - this.state.shiftX + 'px';
				let y =  cY - this.state.shiftY + 'px';

				//console.log('Cords ', x, y)
				
				this.setState({
					styleOb: {
						top: y,
						left: x
					}
				})
			}
		}
	}



	deleteSticky(){


		let docId = this.props.docId
		let stickyId = this.props.stickyOb.key

		this.props.removeSticky(docId, stickyId)
	}

	onChange(event){

		let authorIsUser = this.props.stickyOb.author == this.props.user

		let content = event.target.value
		let stickyOb = this.props.stickyOb

		let updatedOb = Object.assign({}, stickyOb, {content: content})
		let docId = this.props.docId

		if(authorIsUser){
			this.props.updateSticky(docId, updatedOb)
		}
		
	}



	render(){


		//console.log('styleOb', this.state.styleOb.left, this.state.styleOb.top, this.state.isBeingDragged);

		
		let testText =  (this.state.isBeingDragged) ?  'info from stickyOb': 'info from state'

		//console.log(this.state.isBeingDragged, testText)
		
		let author = this.props.stickyOb.author;
		let time = this.props.stickyOb.timeStamp;

		let authorIsUser = this.props.user == author;

		

		let pStyle = {
			left:  this.state.styleOb.left,
			top:  this.state.styleOb.top,
			opacity: (this.props.show) ? 1 : 0, 
			backgroundColor: (authorIsUser) ? '#FAF5AA' : '#D5F0CC', 
			cursor: (authorIsUser) ? 'move': 'auto'
		}

/*
		let fpStyle = {
			
			left: this.state.cX - this.state.shiftX + 'px',
			top: this.state.cY - this.state.shiftY + 'px',
			opacity: (this.props.show) ? 1 : 0, 
			backgroundColor: (authorIsUser) ? '#FAF5AA' : '#D5F0CC', 
			cursor: (authorIsUser) ? 'move': 'auto'
			
		}
		*/


		let showElement = {
			display: (authorIsUser) ? 'block' : 'none'
		}

		let isDraggable = (authorIsUser) ? true : null

		let onDragEvent = (authorIsUser) ? this.onDrag : null

		let onDragStartEvent = (authorIsUser) ? this.dragStart : null

		let onDragEndEvent = (authorIsUser) ? this.dragStop : null

		//let onMouseDownEvent = (authorIsUser) ? this.mouseMove : null

		//let selectedStyleOb = pStyle

		//console.log('firefox', this.state.firefox)
		//console.log(selectedStyleOb.left, selectedStyleOb.top, this.props.stickyOb)


		return(
			<div className="sticky-note mdl-shadow--2dp" style={pStyle}  draggable={isDraggable} onDrag={onDragEvent} onDragStart={onDragStartEvent}    onDragEnd={onDragEndEvent} >
				<div className="sticky-header" >
					
					<span className="close-button" onClick={this.deleteSticky} style={showElement}>
						<Icon name="clear" />
					</span>

				</div>
				<div className="sticky-content">

					<p>
						<textarea rows="7" maxLength="250" placeholder="write something.." onChange={this.onChange} value={this.props.stickyOb.content}>
							
						</textarea> 

					</p>

				</div>
				<div className="sticky-footer">
					<span className="author">
					{author}
					</span>
					<span className="time-stamp">
						{moment(time).format('MM-DD-YYYY')}
					</span>
				</div>
			</div>
		)
	}

}

export default PostitSticky;