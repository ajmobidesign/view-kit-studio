import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';
import { Icon } from 'react-mdl';


class ControlPanel extends Component{

	static proptypes ={
		title: PropTypes.string.isRequired,
		currentSelected: PropTypes.number.isRequired,
		setCurrentSelection: PropTypes.func.isRequired
	}

	constructor(props, context){
		super(props, context);

		this.state = {open: false}
		this.changeToggleState = ::this.changeToggleState;
	}

	componentWillMount(){
		if(this.props.val === 0){
			this.setState({open: true})
		}
	}


	changeToggleState(){
		this.props.setCurrentSelection(this.props.val)
	}


	componentWillReceiveProps(nextProps){
		let t = (nextProps.currentSelected==this.props.val) ? true : false;
		this.setState({open: t});
	}





	render(){

		let toggleState = classNames({
			'toggle-arrow': true,
			'open-drawer' : this.state.open,
			'closed-drawer': !this.state.open
		});

		let containerState = classNames({
			'control-panel-tab_container': true,
			'open-panel-container': this.state.open,
			'closed-panel-container': !this.state.open
		});

		function calcHeight(num){
			return (578 - 26 * num) + 'px';
		}



		let heightStyle= (this.state.open) ? { height: calcHeight(this.props.numberOfChildren) } : null;


		return(
			
				<div >
					<div className="control-panel-title_container" onClick={this.changeToggleState}> 

						<span>{this.props.title} </span>

						<span className={toggleState} > <Icon name="arrow_drop_down" /></span>

					</div>
					<div className={containerState} style={heightStyle}>
						{this.props.children}
					</div>
				</div>
			
		)
	}
}


export default ControlPanel;

