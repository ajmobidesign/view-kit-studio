import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';
import TemplateItemTabs from '../template-item-tabs';
import { Button, Icon, Chip } from 'react-mdl';
import moment from 'moment';
import ImageCycle from '../image-cycle';



class TemplateDetails extends Component{


	static proptypes = {
		id: PropTypes.string.isRequired,
		auth: PropTypes.object.isRequired, 
		price: PropTypes.string.isRequired, 
		title: PropTypes.string.isRequired
	};


	constructor(props, context){
		super(props, context);

		this.state = {open:false};
		this.changeState = ::this.changeState;

	};



	changeState(){
		this.props.toggleState(this.props.id);

		let analyticsOb = { 'event' : 'showTemplateDetailsClick' }

		dataLayer.push(analyticsOb)
	};

	renderTags(){
		let tags = this.props.tags;

		return tags.map((t, i)=>{
			return <span className="tag" key={i}> {t} </span>
		})
	}


	render(){

	
		const cssOb = classNames({
			'template-card' : true,
			'details-open': this.props.open,
		});

		let cardStyle = (this.props.show) ? {display: 'block' } : { display: 'none'};

		const leftStyle = {
			background: `#f7f7f7 url(images/${this.props.id}.png) center / 95% no-repeat`
		};

		const dataOb = {about : 'placeholder', id: this.props.id}


		let promo = (this.props.promotionIsValid) ?   (<span style={{float: 'right', color: 'rgba(0,0,0, .4)'}}><del>${this.props.price}</del><span style={{ display: 'inline-block', backgroundColor: '#3BD108', color: '#fff', padding: '8px', borderRadius: '5px',  fontSize: '12px', margin: '-6px 5px 0px 5px'}}> Free until {moment(this.props.promotion).format('MM-DD-YYYY')}</span> </span>)  : (<span style={{float: 'right', marginRight: '10px'}}>${this.props.price}</span>);
		

		//console.log(promo)

		const imgStyle = {
			width: '100%'
		}




		return(
			<div className={cssOb} style={cardStyle}>
				<div className="card-left" >

					{/*<img  src={`images/${this.props.id}.png`}  style={imgStyle}/>*/}

					<ImageCycle num={this.props.num} tplId={this.props.id}></ImageCycle>

					

					<div className="action-bar">

					
						<span className="headline" style={{display: 'block', width:'100%', padding: '5px auto', marginBottom: '25px' }}>
							<h5 style={{color: 'rgba(0,0,0, .6)'}}> <span>{ this.props.title }</span> {promo}</h5>
						</span>

					
						<span className="short-detail" style={{display: 'block', width:'100%', padding: '5px auto', marginBottom: '5px'}}>
						
							{this.props.details}   
						
							<button style={{float: 'right'}} className="mdl-button mdl-js-button mdl-button--accent"  onClick={this.changeState}> {(this.props.open)? 'Hide Details' : 'Show Details'}  </button>
						


						</span>
						

						<span style={{display: 'block', width:'100%', padding: '10px auto', marginBottom: '0px'}}>  
							{this.renderTags()}
						</span>

										

					
						{this.props.children}

						


						
					</div>
				
				</div>
				<div className="card-right">
				
					<div style={{background: '#fff', width:'100%', overflow: 'hidden'}}>
						
						<div style={{width: '100%', height: '20px', cursor: 'pointer'}}> <a style={{float: 'right', color: 'rgba(0,0,0, .4)', marginTop: '10px'}} onClick={this.changeState}><Icon name="clear" /></a></div>
						

					
						<TemplateItemTabs data={dataOb} auth={this.props.auth} tplId={this.props.id}></TemplateItemTabs>

						
					</div>
					
				</div>
				
			</div>
		);
	}

}

export default TemplateDetails;