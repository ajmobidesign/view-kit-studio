import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';
import ControlPanel from '../../app-components/control-panel';
import { Icon } from 'react-mdl';



class ToolPanel extends Component{


	constructor(props, context){
		super(props, context);

		this.state= {open: true, currentSelected: 0};
		this.changeToggleState = ::this.changeToggleState;
		this.managePanelToggle = ::this.managePanelToggle;
		this.renderChildren = ::this.renderChildren;
		
	}


	changeToggleState(){
		let t = (this.state.open) ? false : true;
		this.setState({open: t});
	}


	managePanelToggle(val){
		this.setState({currentSelected: val});
	}


	renderChildren(){

		let numberOfChildren = (this.props.children.length) ? this.props.children.length : 1

		let propList = {
			currentSelected: this.state.currentSelected,
			setCurrentSelection: this.managePanelToggle,
			numberOfChildren: numberOfChildren, 
			val: 0
		}

		function loopOverChildren(child, i){
			propList.val = i;
			return React.cloneElement(child, propList)
		}


		let childrenWithProps = React.Children.map(this.props.children, loopOverChildren);

		return <div>{childrenWithProps}</div>
	
	}


	render(){

		let toggleState = classNames({
			'toggled_open' : this.state.open,
			'toggled_close': !this.state.open
		});



		return(

			<section className={toggleState} >
				<div className="control-panel-toggle" onClick={this.changeToggleState}  >
					{(this.state.open) ?  <Icon style={{fontSize: '20px'}} name="fast_forward" /> : <Icon style={{fontSize: '20px'}} name="settings" /> }
				</div>
				<div className="control-panel-main_container">
					{this.renderChildren()}
				</div>
			</section>	
		)
	}	

}

export default ToolPanel;
