import React, { Component, PropTypes } from 'react';
import { Button, Icon, Grid, Cell, Card, CardText, CardActions, CardMenu, CardTitle, IconButton, Spinner, Snackbar} from 'react-mdl';
import { saleItem, paymentItem } from 'src/core/transaction/pplrest';
import moment from 'moment';





class Bill extends Component{

	static proptypes = {
		title: PropTypes.string.isRequired,
		id: PropTypes.string.isRequired,
		price: PropTypes.string.isRequired,
		buyNow: PropTypes.func.isRequired, 
		cancel: PropTypes.func.isRequired
	}

	constructor(props, context){
		super(props, context);

		this.state = {spinner:false, isSnackbarActive: false}
		this.buyTemplate = ::this.buyTemplate;
		this.purchaseError = ::this.purchaseError;

	}


	buyTemplate(){

		this.setState({spinner:true});

		let orderItem = Object.assign({}, saleItem);
				orderItem.price = this.props.price;
				orderItem.name = this.props.title;

		let orderPayment = Object.assign({}, paymentItem);
				orderPayment.amount.total = this.props.price;
				orderPayment.item_list.items.push(orderItem);

		let analyticsOb = {'event': 'buyWithPaypalClick'};

		dataLayer.push(analyticsOb);		

		this.props.buyNow(orderPayment, this.purchaseError)
	}

	handleTimeoutSnackbar(){
		this.setState({isSnackbarActive: false})
	}

	handleShowSnackbar(){
		this.setState({isSnackbarActive: true})
	}


	purchaseError(){

		//should there be a buy state?
		this.setState({buy: false, open: false, spinner: false});
		//this.props.toggle(true)
		//need to handle error state....

		this.handleShowSnackbar();
	}

	renderSpinner(){
		return(
			<div className="transition-spinner">
				<Spinner singleColor />
			</div>
		)
	}




	render(){

		let cardStyle={
			width: '400px', 
			height: '450px',
			margin: 'auto'
		}


		return (

			<div>
				<Snackbar
		          active={this.state.isSnackbarActive}
		          onClick={this.handleClickActionSnackbar}
		          onTimeout={this.handleTimeoutSnackbar}
		          action="">There was an Error</Snackbar>

				{(this.state.spinner)? this.renderSpinner(): null}


				<Card shadow={0} style={cardStyle}>
					<CardText style={{marginTop: '50px'}}>
						<Grid>
							<Cell col={6}>Item Name : </Cell> <Cell col={6}> {this.props.title} </Cell>
							<Cell col={6}>Price </Cell> <Cell col={4}> ${this.props.price} </Cell>
							<Grid style={{borderTop: '1px solid rgba(0,0,0, .5)'}}>
								<Cell col={6}>Total </Cell> <Cell col={2}> ${this.props.price} </Cell>
							</Grid>
						</Grid>
						
					</CardText>
					<CardActions>
						<div style={{ marginLeft: '100px'}}>
							<Button colored accent raised onClick={this.buyTemplate} style={{backgroundColor: '#009cde'}}> Buy with <strong><em>PayPal</em></strong> </Button>
							<Button onClick={this.props.cancel} style={{marginLeft: '5px'}}>Cancel</Button>
							<br />
							
						</div>
						<br />
							<small style={{color: 'rgba(0,0,0, .4)', fontSize: '12px', marginLeft: '50px'}}>You will be redirected to paypal to complete payment</small>
					</CardActions>
				</Card>

				</div>
			)
	}


}

export default Bill;