import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import { Icon, Footer, FooterSection, FooterLinkList } from 'react-mdl';


class AppFooter extends Component{

	

	render(){


		const styleOb = {
			position: 'absolute',
			boxSizing: 'border-box',
			width: '100%',
			bottom: '0px'
		}




		return(
			<Footer size="mini" style={styleOb}>
			    <FooterSection type="middle" logo="" style={{margin: 'auto'}}>
			        <FooterLinkList>
			            
			            <Link to="/privacy-policy" >Privacy & Terms</Link>
			            <Link to="/license" >License</Link>
			           
			        </FooterLinkList>
			    </FooterSection>
			</Footer>
		)
	}

}

export default AppFooter;