import React, { Component, PropTypes } from 'react';
import { Button } from 'react-mdl';


class SmartButton extends Component{

	static proptypes = {
		value: PropTypes.number.isRequired, 
		onPress: PropTypes.func.isRequired
	}

	constructor(props, context){
		super(props, context);

		this.onClick = ::this.onClick
	}

	onClick(event){
		
		event.preventDefault();
		this.props.onPress(this.props.value);
	}


	render(){
		return(
			<Button raised style={{background: '#fff', color:'#ff4081'}} onClick={this.onClick}> {this.props.children}
			</Button>
		)
	}
}

export default SmartButton;