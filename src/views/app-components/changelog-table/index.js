import React, { Component, PropTypes } from 'react';
import { DataTable, TableHeader } from 'react-mdl';



class ChangelogTable extends Component{


	render(){
		return(
			<div>
				<div style={{display: 'none'}}>
					<h5> List of Changes</h5>
					<DataTable
					    shadow={0}
					    rows={[
					        {material: 'Acrylic (Transparent)', quantity: 25, price: 2.90},
					        {material: 'Plywood (Birch)', quantity: 50, price: 1.25},
					        {material: 'Laminate (Gold on Blue)', quantity: 10, price: 2.35}
					    ]}>
					    <TableHeader name="material" >Material</TableHeader>
					    <TableHeader numeric name="quantity" >Quantity</TableHeader>
					    <TableHeader numeric name="price" >Price</TableHeader>
					</DataTable>
				</div>
			</div>
		)
	}

}

export default ChangelogTable;