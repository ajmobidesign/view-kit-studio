import React, { Component, PropTypes } from 'react';
import { Icon, Spinner } from 'react-mdl';

let clearFn = undefined;

class AppSpinner extends Component{


	static proptypes = {
		timer: PropTypes.number.isRequired
	}


	constructor(props, context){
		super(props, context);
		this.state = {showSpinner: true};
	}  


	componentDidMount(){
		
		let time = this.props.timer;
		let me = this;
		clearFn = setTimeout(function(){

			me.setState({showSpinner: false});
			window.clearTimeout(clearFn);

		}, time);
	}

	componentWillUnmount(){
		window.clearTimeout(clearFn);
	}

	
	render(){

		return(
			<div style={{textAlign: 'center'}}> 
				{(this.state.showSpinner)? <Spinner /> : this.props.children} 
			</div>
		)
	}

}

export default AppSpinner;