import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import { Icon, Spinner } from 'react-mdl';

let redirectFn = undefined;


class AppLoader extends Component{


	static contextTypes = {
	    router: React.PropTypes.object.isRequired
	  };


	constructor(props, context){
		super(props, context);

		//this.state = {timeoutConst: undefined};
	}  


	componentDidMount(){
		
		//console.log('I mounted')
		const redir = this.context.router;
		redirectFn = setTimeout(function(){
			window.clearTimeout(redirectFn);
			redir.push('/notfound');

		}, 3000);
		//this.setState({timeoutConst : window.setTimeout(function(){redir.push('/notfound')}, 3000)});

		//console.log(this.state.timeoutConst)

		//console.log(redirectFn)
	}

	componentWillUnmount(){
		//console.log('I unmounted', redirectFn)
		window.clearTimeout(redirectFn);
	}

	

	render(){


		return(
			<div className="app-page">
				<div style={{margin: 'auto', width:'80px', height: '80px'}}>
					<Spinner />
				</div>
			</div>
		)
	}

}

export default AppLoader;