import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import Status from '../status';
import HeaderFrom from '../header-form';
import { templateActions } from 'src/core/template';
import { profileActions } from 'src/core/profile';
import { authActions } from 'src/core/auth';
import { userActions } from 'src/core/user';
import { templateListActions } from 'src/core/templateList';
import { FABButton, Icon, Tooltip, Snackbar} from 'react-mdl';
import DownloadButton from '../download-button';



export class Header extends Component {

  constructor(props, context){
      super(props, context);


      this.state =  {isSnackbarActive: false};
      this.toggleHeader = ::this.toggleHeader;
      this.handleShowSnackbar = ::this.handleShowSnackbar;
      this.handleTimeoutSnackbar= ::this.handleTimeoutSnackbar;
      this.downloadError = :: this.downloadError;  
      this.renderSaveStatus = ::this.renderSaveStatus;
  }


  componentWillMount(){
    this.props.getUser();
    //this.props.getProfileData();
  }

  componentWillReceiveProps(nextProps){
    let currentState = this.props.auth.authenticated
    let newState = nextProps.auth.authenticated

    if(currentState !== newState && newState){
      this.props.checkLocalStorage();
    }

  }


  toggleHeader(){



    let st = (this.props.toggleStatus) ? false : true;

    let openOrClose = (st)? 'Open' : 'Closed';

    let analyticsOb = {'event': `navToggle${openOrClose}`};
    dataLayer.push(analyticsOb);

    this.props.toggle(st);

  }

  handleShowSnackbar(){
    this.setState({isSnackbarActive: true})
  }

  handleTimeoutSnackbar() {
      this.setState({ isSnackbarActive: false });
  } 

  downloadError(){
      this.handleShowSnackbar()
  }




  renderLinks(){
    return(
      <span>
        <Link to="/templates" activeClassName="menu-is-active">All Templates</Link>
        
      </span>
    )
  }




  renderSaveStatus(){

    let savingStatus = this.props.template.savingTemplate;

    let styleOb = {
      display:'inline-block',
      marginTop: '17px', 
      marginLeft: '25px'
    }
        if(savingStatus){

          return <span style={styleOb}>Saving .... </span>
        }
        else{
           return <span style={styleOb}> Saved </span>
            
        } 
        
  }

  render(){

    let pName = this.props.loc.pathname.split('/')[2];
    let tKey = this.props.template.key;
    let templateData = this.props.template;
    let downloadError = this.downloadError;
 
    let loc = (pName !== undefined && tKey !== undefined && pName === tKey );

    let template = this.props.template;
    let updateTemplate = this.props.updateTemplate;
    let isAuth = this.props.auth.authenticated;
    let username = this.props.auth.displayName;
    let downloadTemplate = this.props.downloadTemplate;
    let purchasedTemplates = this.props.profile.tplList;

    let hStyle = {
                    width: 'calc(100% - (100% - 36px))',
                    
                    //,
                    overflow: 'hidden'  
                }


    let headerStyle = (this.props.toggleStatus == true && loc ) ? hStyle : null;
    let leftButtonStyle = (this.props.toggleStatus == true && loc ) ? {display: 'block'} : {display: 'none'};

    let rightButtonStyle = (this.props.toggleStatus == false && loc ) ? {display: 'block'} : {display: 'none'};

    let showDownload = (purchasedTemplates.length >0 &&  purchasedTemplates.indexOf(templateData.tplId) !== -1 )? true : false;


    



    return (
        <header className="main-header" style={headerStyle}>
        <Snackbar
            active={this.state.isSnackbarActive}
            onTimeout={this.handleTimeoutSnackbar}
            action="">There was a download Error.</Snackbar>
         { (loc)? 
          (<span className="header-toggle-button closed-state" onClick={this.toggleHeader} style={leftButtonStyle}> <Tooltip label="Toggle nav open" position="right">
              <Icon name="fast_forward" /> 
             </Tooltip> 
          </span>) : null}
          
          {(!loc) ? <div className="header-content logo"><Link to="/"><img src="/images/logo_vk_color_small.png" width="50px" /></Link></div>: null}
          
          <div className="header-menu" style={(loc) ? {display: 'none'}: {display: 'inline-block'}}>
            <Link to="/how-it-works" activeClassName="menu-is-active">How it works</Link>
             <Link to="/FAQ" activeClassName="menu-is-active">FAQ</Link>
            {(isAuth) ? this.renderLinks() : null}
             <Link to="/select-template" activeClassName="menu-is-active"> Select A Template</Link> 
          </div>
          
          
          <div className="header-menu" style={(loc) ? {display: 'inline-block'}: {display: 'none'}}>
            
            {(isAuth) ? <Link to="/templates"  className="back-to-templates"><Tooltip label="Back to All Templates" position="right"><Icon  name="subject" /></Tooltip></Link> : null}
             
          </div>

         
           {(loc)? (<HeaderFrom template={template} updateTemplate={updateTemplate} savingTemplate={this.props.savingTemplate}></HeaderFrom>) : null}


           <div className="header-menu" style={(loc) ? {display: 'inline-block'}: {display: 'none'}}>
            {(isAuth && loc) ? <DownloadButton headerButton={true} onPress={downloadTemplate} data={templateData} error={downloadError} purchased={showDownload} user={this.props.user}><Icon name="cloud_download" /></DownloadButton> : null}

          </div>

          { (loc) ? this.renderSaveStatus() : null}
          
          {(loc)? 
            (<span className="header-toggle-button open-state" onClick={this.toggleHeader} style={rightButtonStyle}> 
              <Tooltip label="Toggle nav close" position="left">
                <Icon name="fast_rewind" /> 
              </Tooltip> 
            </span>) : null
          }
         
          <Status auth={isAuth} profileSubscribe={this.props.getProfileData}  templatesListSubscribe={this.props.getTemplateList} signInGoogle={this.props.signInWithGoogle} signOut={this.props.signOut} username={username} authenticateUser={this.props.authenticateUser} userSubscribe={this.props.getUser} isTemplateDash={loc}></Status>
        
        </header>
      );
    }
}

const mapStateToProps = (state)=>{
  return state;
}

const mapDispatchToProps = Object.assign(
   {}, 
   templateActions, 
   authActions, 
   profileActions,
   userActions, 
   templateListActions

);

export default connect(mapStateToProps, mapDispatchToProps)(Header);
