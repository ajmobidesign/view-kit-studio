import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';
import MdlPalette from '../../mdl-components/mdl-palette';
import MdlElementstyles from '../../mdl-components/mdl-elementstyles';
import ControlPanel from '../../app-components/control-panel';
import SharePanel from '../../app-components/share-panel';
import PanelTab from '../../app-components/panel-tab';
import StickyPanel from '../../app-components/sticky-panel';
import CollabListPanel from '../../app-components/collab-list-panel';
import CollabSearchPanel from '../../app-components/collab-search-panel';
import { Icon } from 'react-mdl';
import moment from 'moment';




class ShareToolPanel extends Component{

	static proptypes ={
		docId: PropTypes.string.isRequired
	}

	constructor(props, context){
		super(props, context);

		this.state = {tab: 0};
		this.switchTab = ::this.switchTab;
	}


	switchTab(idx){
		this.setState({tab: idx});
	}


	
	renderTabs(){

		const tabList = [ {title: 'share'}, {title: 'bookmark'}, {title: 'group'}, {title: 'person_add'}];
		return tabList.map((tab, index)=>{
			let isActive = (this.state.tab === index) 
			return (<PanelTab isActive={isActive}  key={index} tabshift={this.switchTab} tab={index} title={tab.title}></PanelTab>)
		});
	}

	renderTabContent(){

		let userIsOwner = this.props.owner === this.props.displayName

		switch(this.state.tab){
			
			case 0: 
				return (<SharePanel docId={this.props.docId} publicUrl={this.props.publicUrl} privateUrl={this.props.privateUrl} shareLink={this.props.shareLink} removeLink={this.props.removeLink} userIsOwner={userIsOwner} savingTemplate={this.props.savingTemplate}></SharePanel>)
			case 1: 
				return (<StickyPanel addSticky={this.props.addSticky} toggleStickys={this.props.toggleStickys} docId={this.props.docId} showStickys={this.props.showStickys} displayName={this.props.displayName} scroll={this.props.scroll} savingTemplate={this.props.savingTemplate}></StickyPanel>)
			case 2: 
				return <CollabListPanel docId={this.props.docId} removeTemplateCollaborator={this.props.removeTemplateCollaborator} collabList={this.props.collabList} userIsOwner={userIsOwner} owner={this.props.owner} ></CollabListPanel>
			case 3: 
				return <CollabSearchPanel docId={this.props.docId} addTemplateCollaborator={this.props.addTemplateCollaborator} searchUserList={this.props.searchUserList} queryResults={this.props.queryResults} clearUserList={this.props.clearUserList} collabList={this.props.collabList} userIsOwner={userIsOwner} savingTemplate={this.props.savingTemplate}></CollabSearchPanel>					
			default:
				return (<div></div>)	
		}
	}



	render(){

		let loggedIn = this.props.loggedIn;

		return(
				<div style={{height: '100%'}}>
					<div className="control-panel-tab_bar">
						{this.renderTabs()}
					</div>
					<div className="control-panel-tab_content" >
						{(loggedIn) ? this.renderTabContent() : <div className="control-panel-control-block"><p> Sign in to activate this tool</p></div>}
					</div>
				</div>	
		)
	}

}

export default ShareToolPanel;