import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import { Button, snackbar } from 'react-mdl';
import moment from 'moment';
import { appTemplateList } from 'src/core/defaults';
import { userIsNew, newUserPromo } from 'src/core/user';
import _ from 'lodash';
import { currentEv } from 'src/core/defaults';


class DownloadButton extends Component{

	static proptypes = {
		data: PropTypes.object.isRequired, 
		onPress: PropTypes.func.isRequired
	}

	constructor(props, context){
		super(props, context);

		this.onClick = ::this.onClick;
		this.downloadZip = ::this.downloadZip;

	}

	downloadZip(response){
		let data = response.data;
		let docId = this.props.data.key;
		let fName = (this.props.data.title).trim().replace(' ', '_');
		let fileName = _.replace(fName, RegExp(' ', 'g'), '_');
		let a = document.createElement("a");
        //let file = new Blob([data], {type: 'application/zip'});
        //let fL = new File([data], 'Sunset.zip', {type: 'application/zip'})
        //console.log(file.size)
        //let ref = 'data:attachment/zip;charset=utf-8,' + encodeURI(data);

        var clickEvent = new MouseEvent('click', {
			    'view': window,
			    'bubbles': true,
			    'cancelable': true
			  });

        a.href = `${currentEv.server}/api/download/${docId}/${fileName}.zip`;	
        a.download //= `${fileName}.zip`;

        a.dispatchEvent(clickEvent);
	}




	onClick(event){
		event.preventDefault();
		//wait for this function to return something
		this.props.onPress(this.props.data, this.downloadZip, this.props.error);
	}


	render(){

		let styleOb = {
			textAlign: 'center', 
			color: 'rgba(0,0,0, .6)'

		}

		let currentTemplate = _.find(appTemplateList, (t)=>{
			return t.id == this.props.data.tplId;
		})

		const user = this.props.user

		function checkPromo(){

			let promoOb = {exists: false , statement: null}

			const pr = currentTemplate.promotion;
			const timeNow = moment().format();
			
			//has the promotion expired?
			const promoOnList = (pr && moment(timeNow).isBefore(moment(pr).format())) ? true: false;

			const promoNewUser = userIsNew(user)

			promoOb.exists =  (promoOnList || promoNewUser) ? true : false;

			let promoDate = null;

			if(promoOnList&& promoNewUser){
				//promodate whichever is later

				let newU = newUserPromo(user).format('MM-DD-YYYY')

				promoDate = moment(pr).isBefore(newU) ? newU : moment(pr).format('MM-DD-YYYY');

			}else{
				
				if(promoOnList){
					//promodate is the free until date
					promoDate = moment(pr).format('MM-DD-YYYY');
				}
				if(promoNewUser){
					//promodate is that of the new user
					//you are adding logic in multiple places
					//
					promoDate = newUserPromo(user).format('MM-DD-YYYY')
				}
			}


			promoOb.statement = (promoDate) ? 'Free until ' + promoDate : null





			return promoOb
		}

		let promo = checkPromo();

		let downloadDisabled = (this.props.purchased || promo.exists !== undefined ) ? null : true;
		//need to fix this!!!

		let showEnableLink = (!this.props.purchased &&  promo.exists === undefined)
		
		let docId = this.props.data.tplId;

		//if(this.props.data.tplId === 'mdl_basic_html5' || this.props.data.tplId === 'mdl_style_guide'){
			//console.log(this.props.data.tplId, 'purchased', this.props.purchased, 'promo exists', promo.eixts, 'download', downloadDisabled)
		//}
		

		let ulrFragment = _.replace(docId, RegExp("_","g"), '-');


		let buttonStyle = {
			backgroundColor: (downloadDisabled) ? 'transparent' : null
		}

		let btnClass = (this.props.headerButton) ? "download-button" : null



		//console.log('promo exits', promo.exits)
		//console.log('pruchased', this.props.purchased)
		//console.log('downloadDisabled', downloadDisabled)


		/*
		//test
		if(promo.exists){
			if(!downloadDisabled){
				console.log('Download is enabled for promotion :)')
			}
			if(downloadDisabled){
				console.log('Download is disabled for promotion :(')
				console.log(this.props.data)
			}
		}

		if(this.props.purchased){
			
			if(!downloadDisabled){
				console.log('Download is enabled for a purchased template :)')
			}
			if(downloadDisabled){
				console.log('Download is enabled for a purchased template :(')
				console.log(this.props.data)
			}
		}else{

			if(downloadDisabled){
				console.log('Download is not enabled for a template that is not purchased :)')
			}

			if(!downloadDisabled){
				if(!promo.exists){
				 	console.log('Download is enabled for a template that is not purchased and does not have propmotion :(')
				 	console.log(this.props.data)
				}
			}
		}
		*/

		

		return(<span style={styleOb} className={btnClass} >
				<Button  disabled={downloadDisabled} raised accent onClick={this.onClick} style={buttonStyle}> {this.props.children} </Button><br />
				<em>{/*promo.statement*/} </em>
				<em>{(showEnableLink) ? (<Link to={`/details/${docId}`} >Enable Download</Link>) : null }</em>
			</span>
		)
	}
}

export default DownloadButton;