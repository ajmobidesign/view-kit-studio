import React, { Component, PropTypes } from 'react';



class ImageCycle extends Component {


	constructor(props, context){
		super(props, context);
		
		this.renderImages = ::this.renderImages;

	};


	renderImages(){

		let imgNum = this.props.num;
		let tplId = this.props.tplId;

		let newArr = [];

		for(var i= 1; i<=imgNum; i++){
			newArr.push(i)
		}

		let imageOb ={
			width: (100/imgNum) + '%',
			float: 'left' 			
		}

		return newArr.map((n, i)=>{
			let sr = `/images/${tplId}_${n}.png`
			return <img src={sr} style={imageOb} key={i} />

		})
			 
		
	}



	render(){

		let imgNum = this.props.num;
		let tplId = this.props.tplId;

		let totalTime = imgNum * 2 + (imgNum-1) *2 *.5


		function renderKeyFrames(num){


		


			//let totalTime = num * 2 + (num-1) *2 *.5
			let oneS = (1/totalTime) * 100;
			let halfS = oneS/2

			let transPoints = num * num

			let animatePct = 0;

			let cssString = '';

			let leftStart = 0;


			let switchBack = (num-1) * 100;
			let switched = false;
			let switchBackCount = 0;


			for(var i=0; i<=transPoints; i++){

				//if(tplId=='mdl_style_guide'){
				//	console.log('for loop', num, i, totalTime, oneS, halfS, transPoints)
				//}

				let isEven = (i % 2 === 0)

				let moveLeft = '';

				if(leftStart==switchBack){
					switchBackCount+=1;
					switched = true;
				} 


				if(i>0 && isEven){
					(leftStart< switchBack && !switched && switchBackCount==0 ) ? (leftStart +=100) : (leftStart -= 100);
					moveLeft = '-' + leftStart
				} 
				else{
					moveLeft =  (i<=1) ? leftStart : '-' +  leftStart
				}  
				
				if(!isEven && i<transPoints ){
					//console.log(i, animatePct)
					//if(tplId=='mdl_style_guide'){
					//	console.log(i, animatePct)
					//}
					animatePct+=oneS
				}
				else if(isEven && i<transPoints && i>0){
					animatePct+= halfS
				}
				//else if(i>0){
				//	animatePct = 100
				
				//	moveLeft = '0'
				//}


				cssString += animatePct + '%{ left: ' + moveLeft + '%; } '

				//if(i==transPoints && tplId =='mdl_style_guide'){
				//	console.log(cssString)
				//	}
				

			}

			return cssString;

			//console.log(totalTime, oneS, halfS, transPoints)
		}

		


		let animateCss = `

			@keyframes ${tplId}_slidy {
						
						${renderKeyFrames(imgNum)}
					}	
		`;



		let styleOb = {
			
			width: '100%', 
			height: '100%',
			overflow: 'hidden'

		}

		let figureStyleOb = {
			position: 'relative',
		  	width: (imgNum * 100) +'%',
		  	margin: '0',
		  	left: '0',
		  	textAlign: 'left',
		  	fontSize: '0',
		  	animation: `${totalTime}s ${tplId}_slidy infinite`
		}

		let imageOb ={
			width: (100/imgNum) + '%',
			float: 'left' 
			
		}


		return(
			<div style={styleOb}>
				<style>
					{ animateCss }
				</style>

				<figure style={figureStyleOb}>
					{this.renderImages()}
					
				</figure>
				
			</div>	
		)
	}

}

export default ImageCycle;