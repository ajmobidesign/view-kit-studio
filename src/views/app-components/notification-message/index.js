import React, { Component, PropTypes } from 'react';
import { Icon } from 'react-mdl';
import moment from 'moment';
import { Link } from 'react-router';


class NotificationMessage extends Component{

	static propTypes = {
	    msg: PropTypes.object.isRequired,
	    remove: PropTypes.func.isRequired
	};


	constructor(props, context){
		super(props, context);

		this.removeMsg = ::this.removeMsg;

	}

	removeMsg(){

		let k = this.props.msg.key;
		let path = `profile/notifications/${k}`

		this.props.remove(path);
	}	


	render(){

		const controls={
			width: '100%',
			height: '10px'
		}

		const linkStyle={
			float: 'right',
			color: 'rgba(0,0,0, .4)', 
			cursor: 'pointer', 
			marginTop: '-6px', 
			marginRight: '-18px'
		}

		return(
			<div className="notification-messages" >
				<div style={controls}>
					<a onClick={this.removeMsg}  style={linkStyle}><Icon name="clear"/></a>

					
				</div>
				<p>{moment(this.props.msg.date).format('MMM-Do-YY')}</p>

				<h5 className="mdl-typography--headline">{this.props.msg.title}</h5>
							
				<p>{this.props.msg.content}
				{(this.props.msg.link)? <Link to={this.props.msg.link}>{this.props.msg.link}</Link>: null}
				</p>

			</div>
		)
	}

}

export default NotificationMessage;