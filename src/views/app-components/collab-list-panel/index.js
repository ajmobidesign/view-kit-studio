import React, { Component, PropTypes } from 'react';
import { RadioGroup, Radio, Switch, Textfield, List, ListItem, Icon } from 'react-mdl';
import moment from 'moment';
import SmartButton from '../smart-button';



class CollabListPanel extends Component{

	constructor(props, context){
		super(props, context);

		
		this.removePerson = ::this.removePerson;
		
	}



	removePerson(user){
		let docId = this.props.docId;
		this.props.removeTemplateCollaborator(docId, user);

		let analyticOb = {'event': 'removeCollaborator'};
		dataLayer.push(analyticOb);
	}



	renderCollabList(){
		let cList = this.props.collabList

	 return cList.map((r, i)=>{
	 		return    <li className="mdl-list__item" key={i}><span className="mdl-list__item-primary-content" > {r.name} </span> { (this.props.userIsOwner) ? <SmartButton onPress={this.removePerson} value={r}> <small>Remove</small> </SmartButton> : null }</li> 
		})
	}


	render(){

		return(
			
			<div className="control-panel-control-block">
				<p style={{marginLeft: '13px'}}>Template Owner : {this.props.owner}</p>	

				<div className="collaborator-list mdl-shadow--2dp" >			
				<p>Collaborators </p>

				{  (this.props.collabList.length>0) ?   this.renderCollabList() : (<p><small> There are no collaborators</small></p>)}
				</div>

			</div>
		)
	}

}

export default CollabListPanel;