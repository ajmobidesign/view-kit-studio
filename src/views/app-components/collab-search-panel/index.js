import React, { Component, PropTypes } from 'react';
import { RadioGroup, Radio, Switch, Textfield, List, ListItem, Button } from 'react-mdl';
import moment from 'moment';
import SmartButton from '../smart-button';



class CollabSearchPanel extends Component{

	constructor(props, context){
		super(props, context);

		this.state = {search: '', showResults: false};
		this.addPerson = ::this.addPerson;
		this.searchUsers = ::this.searchUsers;
		this.searchEnter = ::this.searchEnter;
		this.clear = ::this.clear;
	}


	addPerson(user){

		let docId = this.props.docId;

		this.props.addTemplateCollaborator(docId, user);
		this.props.clearUserList();
		this.setState({search: '', showResults: false})

		let analyticOb = {'event': 'addCollaborator'};
		dataLayer.push(analyticOb);
	}


	clear(){
		this.props.clearUserList();
		this.setState({search: '', showResults: false});
	}


	searchUsers(event){

		let query = event.target.value;
		this.setState({search: query});

		if(query === ''){
			this.clear();
		}else{
			this.props.searchUserList(query);	
		}	
	}


	searchEnter(event){
		let code = event.charCode;
		let results = this.props.queryResults;
		let query = this.state.search;
		//console.log('search enter caller', code);

		if(code === 13){
			this.setState({showResults: true});
		}
	}



	renderSearchList(){
		let collabs = this.props.collabList.map((c)=>{ return c.name})

		let userIsOwner = this.props.userIsOwner;


		let queryR = this.props.queryResults.filter((q)=>{
			let condition = collabs.indexOf(q.name) == -1
			
			return condition
		}).slice(0, 3)

		let rlist = (queryR.length>0) ? queryR.map((r, i)=>{ return    <li className="mdl-list__item" key={i}><span className="mdl-list__item-primary-content" > {r.name} </span> <SmartButton onPress={this.addPerson} value={r}> Add </SmartButton>  </li> }) : <p className="mdl-list__item"><small> No results found </small></p>
		
			return rlist;
	}


	render(){

		let showSearch = {
			display: (this.props.userIsOwner) ? 'block' : 'none'
		}

		let showDisabled = {
			display: (this.props.userIsOwner) ? 'none' : 'block'
		}

		let numOfCollabs = this.props.collabList.length;

		let numCollbasLeft = 3 - numOfCollabs;

		let collabText = (numCollbasLeft> 0) ? `Search users. Add ${ numCollbasLeft } more collaborators`: `Collaborator limit reached. `;




		return(
			<div>
				<div className="control-panel-control-block" >

					<div className="search-box mdl-shadow--2dp" style={showSearch}>
						<div className="search-field">
							<Textfield
						    onChange={this.searchUsers}
						    onKeyPress={this.searchEnter}
						    label="Expandable Input"
						    disabled={numCollbasLeft===0}
						    value={this.state.search}
						    expandable
						    expandableIcon="search" />
						</div>
												
						    <Button onClick={this.clear}> Clear</Button>
						<p style={{marginLeft: '30px'}}><small> { collabText }</small></p>    
					</div>

					<div className="search-list-results mdl-shadow--2dp"  style={showSearch}>

						{(this.state.showResults) ? this.renderSearchList() : null}

					</div> 
					<div  style={showDisabled}>
						<p><small> Only the owner of this template can add collaborators </small></p>

					</div>    

				</div>
				
			</div>
		)
	}

}

export default CollabSearchPanel;