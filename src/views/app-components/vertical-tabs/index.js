import React, { Component, PropTypes } from 'react';



export class VertialTabs extends Component{


	static proptypes = {
		selecteTab: PropTypes.func.isRequired, 
		currentTab : PropTypes.number.isRequired,
		tabs: PropTypes.array.isRequired
	}


	constructor(props, context){
		super(props, context);

		this.renderTabs = ::this.renderTabs;
		this.toggleTab = ::this.toggleTab;
	}


	toggleTab(i){
		
		//pass this to selected tab function
		this.props.selecteTab(i)
	}


	renderTabs(){

		let tabs = this.props.tabs;
		let sTab = this.props.currentTab;

		return (
			tabs.map((t, i)=>{

				let bk = (sTab === i)? { background:'#fff', color: '#ff4081', opacity: '.95',  boxShadow: '0 1px 4px 0 rgba(0,0,0,.15)', borderRadius: '3px', minHeight:'280px'/*, borderLeft: '1px solid rgba(0,0,0, .05)', borderTop: '1px solid rgba(0,0,0, .05)', borderBottom: '1px solid rgba(0,0,0, .05)' */}  : null;

				let dk = (sTab === i)? {display: 'block', color: 'rgba(0,0,0, .6)', marginLeft: '-20px'}	: {display: 'none'};	

				let msg = (sTab === i)? {display: 'block', color: 'red', marginTop: '-5px'}	: {display: 'none'};		

				let changeTab = ()=>{
					this.toggleTab(i);
				}
				return (<div className="vertical-tab" key={i} onClick={changeTab} style={bk}>

						<h5>{t.title}</h5>

						<ul style={dk}>

							{t.content.map((l, i)=>{
								return <li key={i}>{l}</li>
							})}
						</ul>

						{(i===2) ? <span style={msg}> Downloads free during preview </span>: null}

					</div>)
			})
		)
	}


	render(){
		return(
			<div className="vertical-tabs-container">
				
				<div className="vertical-tabs-left">
					
					{this.props.children}

				</div>
				<div className="vertical-tabs-right">
					{this.renderTabs()}
					
				</div>
			</div>
		)
	};

};

export default VertialTabs;