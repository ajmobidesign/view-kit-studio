import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';
import { Button } from 'react-mdl';
import { localDb } from 'src/core/defaults';
import moment from 'moment';



export class AppNotification extends Component{

	static proptypes = {
		pos: PropTypes.object,
		show: PropTypes.bool.isRequired, 
		actionLabel: PropTypes.string.isRequired, 
		action: PropTypes.func.isRequired,
		notice: PropTypes.string.isRequired //maybe it should be something else
	}

	constructor(props, context){
		super(props, context);

		this.state = {display: false}

		this.clickEvent = ::this.clickEvent;
		this.close = ::this.close;


	}

	componentWillReceiveProps(nextProps){

		if(nextProps.show){
			this.setState({display: true})
		}
		else{
			this.setState({display: false})
		}

	}


	clickEvent(){
		this.setState({display: false})
		this.props.action();
		//localDb.itemValue('viewKitStudioDemoSeen', time);

	}

	close(){
		this.setState({display: false});
		let time = moment().format();
		//localDb.itemValue('viewKitStudioDemoSeen', time);
	}



	render(){

		let notificationState = classNames({
			'app-notification' : true, 
			'show-notification' : this.props.show
		})

		let styleOb ={
			left : (this.state.display) ? this.props.pos.left : null, 
			top: (this.state.display) ? this.props.pos.top : null, 
			opacity: (this.state.display) ? '1' : '0'
		}

		let gbutton={

			backgroundColor: '#ff4081', 

		}

	

		return (
			<div className={notificationState} style={styleOb}>
				<div className="app-notification__text">

					{this.props.notice}

					<div className="app-notification__buttons">

						

						<Button raised accent className="app-notification__action" onClick={this.clickEvent} style={gbutton}>
							{this.props.actionLabel}
						</Button>
						<br/>
						<br/>

						<Button className="app-notification__action" onClick={this.close}>
							Ok Got It
						</Button>
					</div>


				</div>
			</div>	
		)
	}

};

export default AppNotification;