import React, { Component, PropTypes } from 'react';
import { Tab, Tabs } from 'react-mdl';
import ChangelogTable from '../changelog-table';
import BugReport from '../bug-report';
import TemplateAbout from '../template-about';


class TemplateItemTabs extends Component{


	//static proptypes = {
	//	data: PropTypes.object.isRequired
	//};


	constructor(props, context){
		super(props, context);

		this.state = {activeTab:0};

	}	


	renderTabs(){
		switch(this.state.activeTab){
			case 0:
				return <TemplateAbout tplId={this.props.tplId}></TemplateAbout>
			case 1:
				return <ChangelogTable></ChangelogTable>
			case 2:
				return <div></div>
			default:
				return <p></p>;			
		}
	}

	render(){

		const styleOb = {
			height: '600px'

		}

		const contentStyleOb = {
			boxSizing: 'border-box',
			height: '550px',
			padding: '16px',
			overflowY: 'scroll', 
			color: 'rgba(0,0,0, .7)'

		}


		const { auth } = this.props

		return(
			<div className="" style={styleOb}>
			{
				(auth && auth.authenticated)?
                (<Tabs activeTab={this.state.activeTab} onChange={(tabId) => this.setState({ activeTab: tabId })} >
                    <Tab>About</Tab>
                    <Tab>Updates and Changes</Tab>
                                        
                </Tabs>) : null
            }    
                <section>
                    <div className="content" style={contentStyleOb}>
                    	{(!auth)? <section style={{paddingLeft: '20px'}}> <h5 className="mdl-typography--headline">Details </h5> </section> : null}
                    	{this.renderTabs()}
                    </div>
                </section>
            </div>
		)
	}

}

export default TemplateItemTabs;