import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';
import {Icon} from 'react-mdl';

class PanelTabText extends Component{

	static propTypes = {
		tabshift: PropTypes.func.isRequired, 
		title:PropTypes.string, 
		isActive: PropTypes.bool
	}

	constructor(props, context){
		super(props, context);
		this.changeTab = ::this.changeTab;
	}

	changeTab(){
		this.props.tabshift(this.props.tab);
	}

	render(){

		let styl = classNames({
			'control-panel-tab': true,
			'active_tab': this.props.isActive
		});

		return(
			<div className={styl} onClick={this.changeTab}>
					{this.props.title}
			</div>
		)
	}

}

export default PanelTabText;