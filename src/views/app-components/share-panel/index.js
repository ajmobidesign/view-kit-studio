import React, { Component, PropTypes } from 'react';
import { RadioGroup, Radio} from 'react-mdl';
import { currentEv } from 'src/core/defaults';

class SharePanel extends Component{

	
	static propTypes = {
		docId: PropTypes.string.isRequired
	}

	constructor(props, context){
		super(props, context);

		this.state = {fetchingUrl: false};
		this.onChange = ::this.onChange;
		this.checkStatus = ::this.checkStatus;
	}



	checkStatus(v){

		let removeLink = this.props.removeLink;
		let shareLink = this.props.shareLink;
		let docId = this.props.docId;
		let currentState = this.state.shareStatus;
		let pk = this.props.publicUrl;
		let prk = this.props.privateUrl;
		let status = this.setState;


		switch(v){
			case "0":

					if(currentState !== "0"){
						if(currentState === "2"){
							removeLink(docId, pk, 'public')
						}
						if(currentState === "1"){
							removeLink(docId, prk, 'private')
						}

						this.setState({shareStatus: v});
					}
					
				return;

			case "1": 

					if(currentState !== "1"){
						if(currentState === "2"){
							removeLink(docId, pk, 'public')
						}
						shareLink(docId, 'private');
						this.setState({shareStatus: v});
					}
					

				return;

			case "2": 

					if(currentState !== "2"){
						if(currentState === "1"){
							removeLink(docId, prk, 'private')
						}
						shareLink(docId, 'public');
						this.setState({shareStatus: v});
					}
					
				return; 

			default: 
				return ;		
		}
	}


	onChange(event){
		
		let v = event.target.value;
		let userIsOwner = this.props.userIsOwner;
		
		if(userIsOwner){
			this.checkStatus(v);

			let shareStateOb = {'0': 'Private', '1': 'PrivateUrl', '2': 'PublicUrl'};
			let anEvent = shareStateOb[this.state.shareStatus];
			let analyticsOb = {'event': `shareState${anEvent}`};

			//console.log(analyticsOb)

			dataLayer.push(analyticsOb)
		}
	}


	componentWillMount(){
		if(this.props.publicUrl || this.props.privateUrl){

			if(this.props.privateUrl){
				this.setState({shareStatus: "1"})
			}
			else{
				this.setState({shareStatus: "2"})
			}
		}
		else{			
			this.setState({shareStatus: "0"})
		}

	}


	renderUrl(){

		let baseUrl = currentEv.client;

		let sharedUrl = (this.props.privateUrl) ? baseUrl + '/shared/' + this.props.privateUrl : null
		let publicUrl = (this.props.publicUrl) ? baseUrl + '/public/' + this.props.publicUrl : null

		if(this.props.privateUrl){

			return(
					<div className="control-panel-control-block">
						<p> Private Url</p>
						<div>
							
									{ sharedUrl}
						</div>
						
						<small>Any logged in user with url can view</small><br/>

					</div>
				)

		}

		else if(this.props.publicUrl){
			return(
				<div className="control-panel-control-block">
						<p>Public Url</p>
						<div>
								
									{ publicUrl}

						</div>
						<small>Anyone with url can view</small><br/>

					</div>

				)
		}
		else{
			return;
		}
	}



	render(){

		let shareStatus = this.state.shareStatus ;

		let userIsOwner = this.props.userIsOwner;

		let showRadio = (userIsOwner) ? null : true
		
		

		return(
			<div className="panel-style">	
				<div className="radio-panel">
					<div >
						<RadioGroup value={shareStatus} container="ul" childContainer="li" name="sharestatus" onChange={this.onChange}>
							
								<Radio value="0" disabled={showRadio}> Private</Radio>
							
								<Radio value="1" disabled={showRadio}> Shared Private Link </Radio>

								<Radio value="2" disabled={showRadio}> Shared Public Link </Radio>
										
						</RadioGroup>	
					</div>
					<div className="mdl-shadow--2dp" style={{ overflow:'hidden', background: '#fff'}}>
						{this.renderUrl()}
					</div>
				</div>
			</div>
		)
	}
}



export default SharePanel;