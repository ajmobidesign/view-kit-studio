import React, { Component, PropTypes } from 'react';
import { Button } from 'react-mdl';



class HeaderForm extends Component{

	//need to set template as prop
	static propTypes = {
		template : PropTypes.object.isRequired,
		updateTemplate : PropTypes.func.isRequired
	}


	constructor(props, context){
		super(props, context);

		this.titleChange = ::this.titleChange;
	}


	titleChange(event){
		let v = event.target.value;
		
		//this should be an action
		let tplOb = this.props.template
		let docId = tplOb.key
		let ob = Object.assign({}, tplOb, {title: v})
		this.props.updateTemplate(ob, docId);
	}


	render(){

		return(
			<div className="header-content header-form">
				<span className="form-divider">
					<input type="text" onChange={this.titleChange} value={this.props.template.title}/>
				</span>
			</div>
		)
	}

}


export default HeaderForm;
