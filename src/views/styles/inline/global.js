export const ListCard = {
	CardCSSClassName: 'watermelon-ListCard',
	CardStyle:{
		width: '320px', 
		margin: '10px auto'
	},
	CardShadow: 5, 
	CardTitleStyle:{
		color: '#fff', 
		background: '#e6a1b5'
	}, 
	ListStyle:{
		width: '300px'
	}, 
	ListItemStyle: {
		borderBottom: '2px solid #f2f0f1'
	}, 
	ListItemContentStyle:{
		color:'#d66d8b'
	},
	CardActionsStyle: {
		background:'#f2f0f1', 
		color: '#bbb'
	}

}





