
const paletteRedRgb = ["255,235,238",
					"255,205,210",
					"239,154,154",
					"229,115,115",
					"239,83,80",
					"244,67,54",
					"229,57,53",
					"211,47,47",
					"198,40,40",
					"183,28,28",
					"255,138,128",
					"255,82,82",
					"255,23,68",
					"213,0,0"];


export const paletteRed ={
				Red50: paletteRedRgb[0],
				Red100: paletteRedRgb[1],
				Red200: paletteRedRgb[2],
				Red300: paletteRedRgb[3],
				Red400: paletteRedRgb[4],
				Red500: paletteRedRgb[5],
				Red600: paletteRedRgb[6],
				Red700: paletteRedRgb[7],
				Red800: paletteRedRgb[8],
				Red900: paletteRedRgb[9],
				RedA100: paletteRedRgb[10],
				RedA200: paletteRedRgb[11],
				RedA400: paletteRedRgb[12],
				RedA700: paletteRedRgb[13]
			};

const palettePinkRgb = ["252,228,236",
						"248,187,208",
						"244,143,177",
						"240,98,146",
						"236,64,122",
						"233,30,99",
						"216,27,96",
						"194,24,91",
						"173,20,87",
						"136,14,79",
						"255,128,171",
						"255,64,129",
						"245,0,87",
						"197,17,98"];


export const palettePink ={
				Pink50: palettePinkRgb[0],
				Pink100: palettePinkRgb[1],
				Pink200: palettePinkRgb[2],
				Pink300: palettePinkRgb[3],
				Pink400: palettePinkRgb[4],
				Pink500: palettePinkRgb[5],
				Pink600: palettePinkRgb[6],
				Pink700: palettePinkRgb[7],
				Pink800: palettePinkRgb[8],
				Pink900: palettePinkRgb[9],
				PinkA100: palettePinkRgb[10],
				PinkA200: palettePinkRgb[11],
				PinkA400: palettePinkRgb[12],
				PinkA700: palettePinkRgb[13]
			};

const palettePurpleRgb = ["243,229,245",
							"225,190,231",
							"206,147,216",
							"186,104,200",
							"171,71,188",
							"156,39,176",
							"142,36,170",
							"123,31,162",
							"106,27,154",
							"74,20,140",
							"234,128,252",
							"224,64,251",
							"213,0,249",
							"170,0,255"];


export const palettePurple ={
				Purple50: palettePurpleRgb[0],
				Purple100: palettePurpleRgb[1],
				Purple200: palettePurpleRgb[2],
				Purple300: palettePurpleRgb[3],
				Purple400: palettePurpleRgb[4],
				Purple500: palettePurpleRgb[5],
				Purple600: palettePurpleRgb[6],
				Purple700: palettePurpleRgb[7],
				Purple800: palettePurpleRgb[8],
				Purple900: palettePurpleRgb[9],
				PurpleA100: palettePurpleRgb[10],
				PurpleA200: palettePurpleRgb[11],
				PurpleA400: palettePurpleRgb[12],
				PurpleA700: palettePurpleRgb[13]
			};

const paletteDeepPurpleRgb = ["237,231,246",
								"209,196,233",
								"179,157,219",
								"149,117,205",
								"126,87,194",
								"103,58,183",
								"94,53,177",
								"81,45,168",
								"69,39,160",
								"49,27,146",
								"179,136,255",
								"124,77,255",
								"101,31,255",
								"98,0,234"];


export const paletteDeepPurple ={
				DeepPurple50: paletteDeepPurpleRgb[0],
				DeepPurple100: paletteDeepPurpleRgb[1],
				DeepPurple200: paletteDeepPurpleRgb[2],
				DeepPurple300: paletteDeepPurpleRgb[3],
				DeepPurple400: paletteDeepPurpleRgb[4],
				DeepPurple500: paletteDeepPurpleRgb[5],
				DeepPurple600: paletteDeepPurpleRgb[6],
				DeepPurple700: paletteDeepPurpleRgb[7],
				DeepPurple800: paletteDeepPurpleRgb[8],
				DeepPurple900: paletteDeepPurpleRgb[9],
				DeepPurpleA100: paletteDeepPurpleRgb[10],
				DeepPurpleA200: paletteDeepPurpleRgb[11],
				DeepPurpleA400: paletteDeepPurpleRgb[12],
				DeepPurpleA700: paletteDeepPurpleRgb[13]
			};


const paletteIndigoRgb = ["232,234,246",
							"197,202,233",
							"159,168,218",
							"121,134,203",
							"92,107,192",
							"63,81,181",
							"57,73,171",
							"48,63,159",
							"40,53,147",
							"26,35,126",
							"140,158,255",
							"83,109,254",
							"61,90,254",
							"48,79,254"];


export const paletteIndigo ={
				Indigo50: paletteIndigoRgb[0],
				Indigo100: paletteIndigoRgb[1],
				Indigo200: paletteIndigoRgb[2],
				Indigo300: paletteIndigoRgb[3],
				Indigo400: paletteIndigoRgb[4],
				Indigo500: paletteIndigoRgb[5],
				Indigo600: paletteIndigoRgb[6],
				Indigo700: paletteIndigoRgb[7],
				Indigo800: paletteIndigoRgb[8],
				Indigo900: paletteIndigoRgb[9],
				IndigoA100: paletteIndigoRgb[10],
				IndigoA200: paletteIndigoRgb[11],
				IndigoA400: paletteIndigoRgb[12],
				IndigoA700: paletteIndigoRgb[13]
			};

const paletteBlueRgb = ["227,242,253",
						"187,222,251",
						"144,202,249",
						"100,181,246",
						"66,165,245",
						"33,150,243",
						"30,136,229",
						"25,118,210",
						"21,101,192",
						"13,71,161",
						"130,177,255",
						"68,138,255",
						"41,121,255",
						"41,98,255"];


export const paletteBlue ={
				Blue50: paletteBlueRgb[0],
				Blue100: paletteBlueRgb[1],
				Blue200: paletteBlueRgb[2],
				Blue300: paletteBlueRgb[3],
				Blue400: paletteBlueRgb[4],
				Blue500: paletteBlueRgb[5],
				Blue600: paletteBlueRgb[6],
				Blue700: paletteBlueRgb[7],
				Blue800: paletteBlueRgb[8],
				Blue900: paletteBlueRgb[9],
				BlueA100: paletteBlueRgb[10],
				BlueA200: paletteBlueRgb[11],
				BlueA400: paletteBlueRgb[12],
				BlueA700: paletteBlueRgb[13]
			};

const paletteLightBlueRgb = ["225,245,254",
							"179,229,252",
							"129,212,250",
							"79,195,247",
							"41,182,246",
							"3,169,244",
							"3,155,229",
							"2,136,209",
							"2,119,189",
							"1,87,155",
							"128,216,255",
							"64,196,255",
							"0,176,255",
							"0,145,234"];


export const paletteLightBlue ={
				LightBlue50: paletteLightBlueRgb[0],
				LightBlue100: paletteLightBlueRgb[1],
				LightBlue200: paletteLightBlueRgb[2],
				LightBlue300: paletteLightBlueRgb[3],
				LightBlue400: paletteLightBlueRgb[4],
				LightBlue500: paletteLightBlueRgb[5],
				LightBlue600: paletteLightBlueRgb[6],
				LightBlue700: paletteLightBlueRgb[7],
				LightBlue800: paletteLightBlueRgb[8],
				LightBlue900: paletteLightBlueRgb[9],
				LightBlueA100: paletteLightBlueRgb[10],
				LightBlueA200: paletteLightBlueRgb[11],
				LightBlueA400: paletteLightBlueRgb[12],
				LightBlueA700: paletteLightBlueRgb[13]
			};

const paletteCyanRgb = ["224,247,250",
						"178,235,242",
						"128,222,234",
						"77,208,225",
						"38,198,218",
						"0,188,212",
						"0,172,193",
						"0,151,167",
						"0,131,143",
						"0,96,100",
						"132,255,255",
						"24,255,255",
						"0,229,255",
						"0,184,212"];


export const paletteCyan ={
				Cyan50: paletteCyanRgb[0],
				Cyan100: paletteCyanRgb[1],
				Cyan200: paletteCyanRgb[2],
				Cyan300: paletteCyanRgb[3],
				Cyan400: paletteCyanRgb[4],
				Cyan500: paletteCyanRgb[5],
				Cyan600: paletteCyanRgb[6],
				Cyan700: paletteCyanRgb[7],
				Cyan800: paletteCyanRgb[8],
				Cyan900: paletteCyanRgb[9],
				CyanA100: paletteCyanRgb[10],
				CyanA200: paletteCyanRgb[11],
				CyanA400: paletteCyanRgb[12],
				CyanA700: paletteCyanRgb[13]
			};

const paletteTealRgb = ["224,242,241",
						"178,223,219",
						"128,203,196",
						"77,182,172",
						"38,166,154",
						"0,150,136",
						"0,137,123",
						"0,121,107",
						"0,105,92",
						"0,77,64",
						"167,255,235",
						"100,255,218",
						"29,233,182",
						"0,191,165"];


export const paletteTeal ={
				Teal50: paletteTealRgb[0],
				Teal100: paletteTealRgb[1],
				Teal200: paletteTealRgb[2],
				Teal300: paletteTealRgb[3],
				Teal400: paletteTealRgb[4],
				Teal500: paletteTealRgb[5],
				Teal600: paletteTealRgb[6],
				Teal700: paletteTealRgb[7],
				Teal800: paletteTealRgb[8],
				Teal900: paletteTealRgb[9],
				TealA100: paletteTealRgb[10],
				TealA200: paletteTealRgb[11],
				TealA400: paletteTealRgb[12],
				TealA700: paletteTealRgb[13]
			};

const paletteGreenRgb = ["232,245,233",
						"200,230,201",
						"165,214,167",
						"129,199,132",
						"102,187,106",
						"76,175,80",
						"67,160,71",
						"56,142,60",
						"46,125,50",
						"27,94,32",
						"185,246,202",
						"105,240,174",
						"0,230,118",
						"0,200,83"];


export const paletteGreen ={
				Green50: paletteGreenRgb[0],
				Green100: paletteGreenRgb[1],
				Green200: paletteGreenRgb[2],
				Green300: paletteGreenRgb[3],
				Green400: paletteGreenRgb[4],
				Green500: paletteGreenRgb[5],
				Green600: paletteGreenRgb[6],
				Green700: paletteGreenRgb[7],
				Green800: paletteGreenRgb[8],
				Green900: paletteGreenRgb[9],
				GreenA100: paletteGreenRgb[10],
				GreenA200: paletteGreenRgb[11],
				GreenA400: paletteGreenRgb[12],
				GreenA700: paletteGreenRgb[13]
			};

const paletteLightGreenRgb = ["241,248,233",
								"220,237,200",
								"197,225,165",
								"174,213,129",
								"156,204,101",
								"139,195,74",
								"124,179,66",
								"104,159,56",
								"85,139,47",
								"51,105,30",
								"204,255,144",
								"178,255,89",
								"118,255,3",
								"100,221,23"];


export const paletteLightGreen ={
				LightGreen50: paletteLightGreenRgb[0],
				LightGreen100: paletteLightGreenRgb[1],
				LightGreen200: paletteLightGreenRgb[2],
				LightGreen300: paletteLightGreenRgb[3],
				LightGreen400: paletteLightGreenRgb[4],
				LightGreen500: paletteLightGreenRgb[5],
				LightGreen600: paletteLightGreenRgb[6],
				LightGreen700: paletteLightGreenRgb[7],
				LightGreen800: paletteLightGreenRgb[8],
				LightGreen900: paletteLightGreenRgb[9],
				LightGreenA100: paletteLightGreenRgb[10],
				LightGreenA200: paletteLightGreenRgb[11],
				LightGreenA400: paletteLightGreenRgb[12],
				LightGreenA700: paletteLightGreenRgb[13]
			};

const paletteLimeRgb = ["249,251,231",
						"240,244,195",
						"230,238,156",
						"220,231,117",
						"212,225,87",
						"205,220,57",
						"192,202,51",
						"175,180,43",
						"158,157,36",
						"130,119,23",
						"244,255,129",
						"238,255,65",
						"198,255,0",
						"174,234,0"];


export const paletteLime ={
				Lime50: paletteLimeRgb[0],
				Lime100: paletteLimeRgb[1],
				Lime200: paletteLimeRgb[2],
				Lime300: paletteLimeRgb[3],
				Lime400: paletteLimeRgb[4],
				Lime500: paletteLimeRgb[5],
				Lime600: paletteLimeRgb[6],
				Lime700: paletteLimeRgb[7],
				Lime800: paletteLimeRgb[8],
				Lime900: paletteLimeRgb[9],
				LimeA100: paletteLimeRgb[10],
				LimeA200: paletteLimeRgb[11],
				LimeA400: paletteLimeRgb[12],
				LimeA700: paletteLimeRgb[13]
			};


const paletteYellowRgb = ["255,253,231",
							"255,249,196",
							"255,245,157",
							"255,241,118",
							"255,238,88",
							"255,235,59",
							"253,216,53",
							"251,192,45",
							"249,168,37",
							"245,127,23",
							"255,255,141",
							"255,255,0",
							"255,234,0",
							"255,214,0"];


export const paletteYellow ={
				Yellow50: paletteYellowRgb[0],
				Yellow100: paletteYellowRgb[1],
				Yellow200: paletteYellowRgb[2],
				Yellow300: paletteYellowRgb[3],
				Yellow400: paletteYellowRgb[4],
				Yellow500: paletteYellowRgb[5],
				Yellow600: paletteYellowRgb[6],
				Yellow700: paletteYellowRgb[7],
				Yellow800: paletteYellowRgb[8],
				Yellow900: paletteYellowRgb[9],
				YellowA100: paletteYellowRgb[10],
				YellowA200: paletteYellowRgb[11],
				YellowA400: paletteYellowRgb[12],
				YellowA700: paletteYellowRgb[13]
			};


const paletteAmberRgb = ["255,248,225",
							"255,236,179",
							"255,224,130",
							"255,213,79",
							"255,202,40",
							"255,193,7",
							"255,179,0",
							"255,160,0",
							"255,143,0",
							"255,111,0",
							"255,229,127",
							"255,215,64",
							"255,196,0",
							"255,171,0"];


export const paletteAmber ={
				Amber50: paletteAmberRgb[0],
				Amber100: paletteAmberRgb[1],
				Amber200: paletteAmberRgb[2],
				Amber300: paletteAmberRgb[3],
				Amber400: paletteAmberRgb[4],
				Amber500: paletteAmberRgb[5],
				Amber600: paletteAmberRgb[6],
				Amber700: paletteAmberRgb[7],
				Amber800: paletteAmberRgb[8],
				Amber900: paletteAmberRgb[9],
				AmberA100: paletteAmberRgb[10],
				AmberA200: paletteAmberRgb[11],
				AmberA400: paletteAmberRgb[12],
				AmberA700: paletteAmberRgb[13]
			};

const paletteOrangeRgb = ["255,243,224",
							"255,224,178",
							"255,204,128",
							"255,183,77",
							"255,167,38",
							"255,152,0",
							"251,140,0",
							"245,124,0",
							"239,108,0",
							"230,81,0",
							"255,209,128",
							"255,171,64",
							"255,145,0",
							"255,109,0"];


export const paletteOrange ={
				Orange50: paletteOrangeRgb[0],
				Orange100: paletteOrangeRgb[1],
				Orange200: paletteOrangeRgb[2],
				Orange300: paletteOrangeRgb[3],
				Orange400: paletteOrangeRgb[4],
				Orange500: paletteOrangeRgb[5],
				Orange600: paletteOrangeRgb[6],
				Orange700: paletteOrangeRgb[7],
				Orange800: paletteOrangeRgb[8],
				Orange900: paletteOrangeRgb[9],
				OrangeA100: paletteOrangeRgb[10],
				OrangeA200: paletteOrangeRgb[11],
				OrangeA400: paletteOrangeRgb[12],
				OrangeA700: paletteOrangeRgb[13]
			};

const paletteDeepOrangeRgb = ["251,233,231",
								"255,204,188",
								"255,171,145",
								"255,138,101",
								"255,112,67",
								"255,87,34",
								"244,81,30",
								"230,74,25",
								"216,67,21",
								"191,54,12",
								"255,158,128",
								"255,110,64",
								"255,61,0",
								"221,44,0"];


export const paletteDeepOrange ={
				DeepOrange50: paletteDeepOrangeRgb[0],
				DeepOrange100: paletteDeepOrangeRgb[1],
				DeepOrange200: paletteDeepOrangeRgb[2],
				DeepOrange300: paletteDeepOrangeRgb[3],
				DeepOrange400: paletteDeepOrangeRgb[4],
				DeepOrange500: paletteDeepOrangeRgb[5],
				DeepOrange600: paletteDeepOrangeRgb[6],
				DeepOrange700: paletteDeepOrangeRgb[7],
				DeepOrange800: paletteDeepOrangeRgb[8],
				DeepOrange900: paletteDeepOrangeRgb[9],
				DeepOrangeA100: paletteDeepOrangeRgb[10],
				DeepOrangeA200: paletteDeepOrangeRgb[11],
				DeepOrangeA400: paletteDeepOrangeRgb[12],
				DeepOrangeA700: paletteDeepOrangeRgb[13]
			};

const paletteBrownRgb = ["239,235,233",
						"215,204,200",
						"188,170,164",
						"161,136,127",
						"141,110,99",
						"121,85,72",
						"109,76,65",
						"93,64,55",
						"78,52,46",
						"62,39,35"];


export const paletteBrown ={
				Brown50: paletteBrownRgb[0],
				Brown100: paletteBrownRgb[1],
				Brown200: paletteBrownRgb[2],
				Brown300: paletteBrownRgb[3],
				Brown400: paletteBrownRgb[4],
				Brown500: paletteBrownRgb[5],
				Brown600: paletteBrownRgb[6],
				Brown700: paletteBrownRgb[7],
				Brown800: paletteBrownRgb[8],
				Brown900: paletteBrownRgb[9]
			};

const paletteGreyRgb = ["250,250,250",
						"245,245,245",
						"238,238,238",
						"224,224,224",
						"189,189,189",
						"158,158,158",
						"117,117,117",
						"97,97,97",
						"66,66,66",
						"33,33,33"];


export const paletteGrey ={
				Grey50: paletteGreyRgb[0],
				Grey100: paletteGreyRgb[1],
				Grey200: paletteGreyRgb[2],
				Grey300: paletteGreyRgb[3],
				Grey400: paletteGreyRgb[4],
				Grey500: paletteGreyRgb[5],
				Grey600: paletteGreyRgb[6],
				Grey700: paletteGreyRgb[7],
				Grey800: paletteGreyRgb[8],
				Grey900: paletteGreyRgb[9]
			};

const paletteBlueGreyRgb = ["236,239,241",
							"207,216,220",
							"176,190,197",
							"144,164,174",
							"120,144,156",
							"96,125,139",
							"84,110,122",
							"69,90,100",
							"55,71,79",
							"38,50,56"];


export const paletteBlueGrey ={
				BlueGrey50: paletteBlueGreyRgb[0],
				BlueGrey100: paletteBlueGreyRgb[1],
				BlueGrey200: paletteBlueGreyRgb[2],
				BlueGrey300: paletteBlueGreyRgb[3],
				BlueGrey400: paletteBlueGreyRgb[4],
				BlueGrey500: paletteBlueGreyRgb[5],
				BlueGrey600: paletteBlueGreyRgb[6],
				BlueGrey700: paletteBlueGreyRgb[7],
				BlueGrey800: paletteBlueGreyRgb[8],
				BlueGrey900: paletteBlueGreyRgb[9]
			};



export const ColorBlack = "0,0,0";
export const ColorWhite = "255, 255, 255";

export const ColorDarkContrast = ColorWhite;
export const ColorLightContrast = ColorBlack;












