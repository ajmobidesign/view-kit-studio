import React, { Component, PropTypes } from 'react';
import MdlStyle from '../../mdl-components/mdl-style';
import StylePanel from '../../mdl-components/style-panel';
import MdlBlog from '../../mdl-templates/mdl-blog';
import MdlBlogStyle from '../../mdl-templates/mdl-blog-style';




class MdlBlogPage extends Component{

	static proptypes = {
		style: PropTypes.object.isRequired,
		update: PropTypes.func.isRequired,
		docId: PropTypes.string.isRequired
	};


	render(){
		return(
			<div className="">
				<StylePanel docId={this.props.docId} style={this.props.style} update={this.props.update}></StylePanel>

				<MdlStyle style={this.props.style} ></MdlStyle>
				<MdlBlogStyle></MdlBlogStyle>					
				<MdlBlog></MdlBlog>
			</div>
		)
	}

}

export default MdlBlogPage;