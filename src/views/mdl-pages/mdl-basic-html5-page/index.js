import React, { Component, PropTypes } from 'react';
import MdlStyle from '../../mdl-components/mdl-style';
import StylePanel from '../../mdl-components/style-panel';
import MdlBasicLayout from '../../mdl-templates/mdl-basic-layout';
import TplStyles from '../../mdl-templates/tpl-styles';
import ToolPanel from '../../app-components/tool-panel';
import ControlPanel from '../../app-components/control-panel';



class MdlBasicHtml5Page extends Component{

	static proptypes = {
		style: PropTypes.object.isRequired,
		update: PropTypes.func.isRequired,
		docId: PropTypes.string.isRequired
	};


	constructor(props, context){
		super(props, context);

		this.state = {showAllStickys : true}

		this.toggleStickys = ::this.toggleStickys;
	}

	toggleStickys(val){

		this.setState({showAllStickys: val})

	}


	render(){

		let viewOnly = this.props.viewOnly;



		return(
			<div className="">
				{ (viewOnly)? null :  (<ToolPanel><ControlPanel title="Material Design Palette" ><StylePanel docId={this.props.docId} style={this.props.style} update={this.props.update} loggedIn={this.props.loggedIn} savingTemplate={this.props.savingTemplate} ></StylePanel></ControlPanel></ToolPanel>)}

				<MdlStyle style={this.props.style} ></MdlStyle>
				<TplStyles style={this.props.style} ></TplStyles>					
				<MdlBasicLayout docId={this.props.docId} ></MdlBasicLayout>
			</div>
		)
	}

}

export default MdlBasicHtml5Page;