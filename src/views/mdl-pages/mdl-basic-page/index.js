import React, { Component, PropTypes } from 'react';
import MdlStyle from '../../mdl-components/mdl-style';
import StylePanel from '../../mdl-components/style-panel';
import MdlBasic from '../../mdl-templates/mdl-basic';
import TplStyles from '../../mdl-templates/tpl-styles';



class MdlBasicPage extends Component{

	static proptypes = {
		style: PropTypes.object.isRequired,
		update: PropTypes.func.isRequired,
		docId: PropTypes.string.isRequired
	};


	render(){
		return(
			<div className="">
				<StylePanel docId={this.props.docId} style={this.props.style} update={this.props.update} ></StylePanel>

				<MdlStyle style={this.props.style} ></MdlStyle>
				<TplStyles style={this.props.style}></TplStyles>					
				<MdlBasic></MdlBasic>
			</div>
		)
	}

}

export default MdlBasicPage;