import React, { Component, PropTypes } from 'react';
import StylePanel from '../../mdl-components/style-panel';
import MdlStyle from '../../mdl-components/mdl-style';
import MdlStyleGuideCss from '../../mdl-templates/mdl-style-guide-css';
import MdlStyleGuide from '../../mdl-templates/mdl-style-guide';
import ComponentPanel from '../../mdl-components/component-panel';
import ControlPanel from '../../app-components/control-panel';
import ToolPanel from '../../app-components/tool-panel';
import ShareToolPanel from '../../app-components/share-tool-panel';







class MdlStyleGuidePage extends Component{

	static proptypes = {
		style: PropTypes.object.isRequired,
		updateStyle: PropTypes.func.isRequired,
		docId: PropTypes.string.isRequired
	};

	constructor(props, context){
		super(props, context);

		this.state = {showAllStickys : true}

		this.toggleStickys = ::this.toggleStickys;
		this.scrollPos = ::this.scrollPos;
	}

	toggleStickys(val){

		this.setState({showAllStickys: val})

	}

	scrollPos(pos){
		this.setState({scroll: pos})
	}



	render(){

		//console.log('singlepage docId', this.props.docId )

	
		let viewOnly = this.props.viewOnly;

		return(
			<div className="">
					
				{(viewOnly)? null : (<ToolPanel><ControlPanel title="Material Design Palette" ><StylePanel docId={this.props.docId} style={this.props.style} update={this.props.updateStyle} loggedIn={this.props.loggedIn} savingTemplate={this.props.savingTemplate} ></StylePanel></ControlPanel>
					
					<ControlPanel title="Share Tools" ><ShareToolPanel docId={this.props.docId} publicUrl={this.props.publicUrl} privateUrl={this.props.privateUrl}  shareLink={this.props.shareLink} removeLink={this.props.removeLink} addSticky={this.props.addSticky} toggleStickys={this.toggleStickys} showStickys={this.state.showAllStickys} addTemplateCollaborator={this.props.addTemplateCollaborator} removeTemplateCollaborator={this.props.removeTemplateCollaborator} searchUserList={this.props.searchUserList} queryResults={this.props.queryResults} clearUserList={this.props.clearUserList} collabList={this.props.collabList} displayName={this.props.displayName} owner={this.props.owner} loggedIn={this.props.loggedIn} scroll={this.state.scroll} savingTemplate={this.props.savingTemplate} ></ShareToolPanel></ControlPanel></ToolPanel>)
				}

				<MdlStyle style={this.props.style}></MdlStyle>
				<MdlStyleGuideCss style={this.props.style}></MdlStyleGuideCss>
																
				<MdlStyleGuide stickys={this.props.stickys} updateSticky={this.props.updateSticky} removeSticky={this.props.removeSticky} showStickys={this.state.showAllStickys} displayName={this.props.displayName} docId={this.props.docId} scrollPos={this.scrollPos}></MdlStyleGuide>

			</div>
		)
	}

}

export default MdlStyleGuidePage;