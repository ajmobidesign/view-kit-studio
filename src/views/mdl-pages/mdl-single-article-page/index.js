import React, { Component, PropTypes } from 'react';
import MdlStyle from '../../mdl-components/mdl-style';
import StylePanel from '../../mdl-components/style-panel';
import MdlSingleArticle from '../../mdl-templates/mdl-single-article';
import MdlSingleArticleStyle from '../../mdl-templates/mdl-single-article-style';




class MdlSingleArticlePage extends Component{

	static proptypes = {
		style: PropTypes.object.isRequired,
		update: PropTypes.func.isRequired,
		docId: PropTypes.string.isRequired
	};


	render(){
		return(
			<div className="">
				<StylePanel docId={this.props.docId} style={this.props.style} update={this.props.update}></StylePanel>

				<MdlStyle style={this.props.style} ></MdlStyle>
				<MdlSingleArticleStyle style={this.props.style}></MdlSingleArticleStyle>					
				<MdlSingleArticle></MdlSingleArticle>
			</div>
		)
	}

}

export default MdlSingleArticlePage;