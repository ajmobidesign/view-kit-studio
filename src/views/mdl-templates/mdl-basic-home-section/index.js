import React, { Component } from 'react';
import { Grid, IconButton, Menu, MenuItem, Button} from 'react-mdl';


class MdlBasicHomeSection extends Component{

	render(){
		return(
			<div className="mdl-layout__tab-panel is-active">
				<section className="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp">

		            <header className="section__play-btn mdl-cell mdl-cell--3-col-desktop mdl-cell--2-col-tablet mdl-cell--4-col-phone highlight-background" >
		              <i className="material-icons" style={{color: '#fff'}}>play_circle_filled</i>
		            </header>
		            <div className="mdl-card mdl-cell mdl-cell--9-col-desktop mdl-cell--6-col-tablet mdl-cell--4-col-phone"   style={{position: "relative", background: '#fff'}}>
		            	<div style={{position: "relative"}}>
		            		<div style={{float: "right", marginTop: '5px', marginRight: '5px'}}>
				              	<IconButton name="more_vert" id="demo-menu-lower-right" />
								    <Menu target="demo-menu-lower-right" align="right">
								        <MenuItem>Some Action</MenuItem>
								        <MenuItem>Another Action</MenuItem>
								        <MenuItem disabled>Disabled Action</MenuItem>
								        <MenuItem>Yet Another Action</MenuItem>
								    </Menu>
							</div>    
			            </div>  
		              <div className="mdl-card__supporting-text" >
		             
		                <h4>Features</h4>
		                Dolore ex deserunt aute fugiat aute nulla ea sunt aliqua nisi cupidatat eu. Nostrud in laboris labore nisi amet do dolor eu fugiat consectetur elit cillum esse.
		                
		              </div>
		              <div className="mdl-card__actions">
		                <Button accent>Read our Features</Button>
		              </div>
		            </div>     
          		</section>

          		<section className="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp" style={{position: "relative"}}>
          			<div style={{position: "relative", width: '100%', background: '#fff'}}>
		            		<div style={{float: "right", marginTop: '5px', marginRight: '5px'}}>
				              	<IconButton name="more_vert" id="details-menu" />
								    <Menu target="details-menu" align="right">
								        <MenuItem>Some Action</MenuItem>
								        <MenuItem>Another Action</MenuItem>
								        <MenuItem disabled>Disabled Action</MenuItem>
								        <MenuItem>Yet Another Action</MenuItem>
								    </Menu>
							</div>    
			        </div> 


		            <div className="mdl-card mdl-cell mdl-cell--12-col">
		              <div className="mdl-card__supporting-text mdl-grid mdl-grid--no-spacing">
		                <h4 className="mdl-cell mdl-cell--12-col">Details</h4>
		                <div className="section__circle-container mdl-cell mdl-cell--2-col mdl-cell--1-col-phone">
		                  <div className="section__circle-container__circle mdl-color--primary"></div>
		                </div>
		                <div className="section__text mdl-cell mdl-cell--10-col-desktop mdl-cell--6-col-tablet mdl-cell--3-col-phone">
		                  <h5>Lorem ipsum dolor sit amet</h5>
		                  Dolore ex deserunt aute fugiat aute nulla ea sunt aliqua nisi cupidatat eu. Duis nulla tempor do aute et eiusmod velit exercitation nostrud quis <a href="#">proident minim</a>.
		                </div>
		                <div className="section__circle-container mdl-cell mdl-cell--2-col mdl-cell--1-col-phone">
		                  <div className="section__circle-container__circle mdl-color--primary"></div>
		                </div>
		                <div className="section__text mdl-cell mdl-cell--10-col-desktop mdl-cell--6-col-tablet mdl-cell--3-col-phone">
		                  <h5>Lorem ipsum dolor sit amet</h5>
		                  Dolore ex deserunt aute fugiat aute nulla ea sunt aliqua nisi cupidatat eu. Duis nulla tempor do aute et eiusmod velit exercitation nostrud quis <a href="#">proident minim</a>.
		                </div>
		                <div className="section__circle-container mdl-cell mdl-cell--2-col mdl-cell--1-col-phone">
		                  <div className="section__circle-container__circle mdl-color--primary"></div>
		                </div>
		                <div className="section__text mdl-cell mdl-cell--10-col-desktop mdl-cell--6-col-tablet mdl-cell--3-col-phone">
		                  <h5>Lorem ipsum dolor sit amet</h5>
		                  Dolore ex deserunt aute fugiat aute nulla ea sunt aliqua nisi cupidatat eu. Duis nulla tempor do aute et eiusmod velit exercitation nostrud quis <a href="#">proident minim</a>.
		                </div>
		              </div>
		              <div className="mdl-card__actions">
		                <Button accent>Read our Features</Button>
		              </div>
		            </div>
		           
          		</section>

          		<section className="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp">
          			<div style={{position: "relative", width: '100%', background: '#fff'}}>
		            		<div style={{float: "right", marginTop: '5px', marginRight: '5px'}}>
				              	<IconButton name="more_vert" id="tech-menu" />
								    <Menu target="tech-menu" align="right">
								        <MenuItem>Some Action</MenuItem>
								        <MenuItem>Another Action</MenuItem>
								        <MenuItem disabled>Disabled Action</MenuItem>
								        <MenuItem>Yet Another Action</MenuItem>
								    </Menu>
							</div>    
			        </div>
		            <div className="mdl-card mdl-cell mdl-cell--12-col">
		              <div className="mdl-card__supporting-text">
		                <h4>Technology</h4>
		                Dolore ex deserunt aute fugiat aute nulla ea sunt aliqua nisi cupidatat eu. Nostrud in laboris labore nisi amet do dolor eu fugiat consectetur elit cillum esse. Pariatur occaecat nisi laboris tempor laboris eiusmod qui id Lorem esse commodo in. Exercitation aute dolore deserunt culpa consequat elit labore incididunt elit anim.
		              </div>
		              <div className="mdl-card__actions">
		                 <Button accent>Read our Features</Button>
		              </div>
		            </div>
          		</section>
          		<section className="section--footer mdl-color--white mdl-grid">
		            <div className="section__circle-container mdl-cell mdl-cell--2-col mdl-cell--1-col-phone">
		              <div className="section__circle-container__circle mdl-color--accent section__circle--big"></div>
		            </div>
		            <div className="section__text mdl-cell mdl-cell--4-col-desktop mdl-cell--6-col-tablet mdl-cell--3-col-phone">
		              <h5>Lorem ipsum dolor sit amet</h5>
		              Qui sint ut et qui nisi cupidatat. Reprehenderit nostrud proident officia exercitation anim et pariatur ex.
		            </div>
		            <div className="section__circle-container mdl-cell mdl-cell--2-col mdl-cell--1-col-phone">
		              <div className="section__circle-container__circle mdl-color--accent section__circle--big"></div>
		            </div>
		            <div className="section__text mdl-cell mdl-cell--4-col-desktop mdl-cell--6-col-tablet mdl-cell--3-col-phone">
		              <h5>Lorem ipsum dolor sit amet</h5>
		              Qui sint ut et qui nisi cupidatat. Reprehenderit nostrud proident officia exercitation anim et pariatur ex.
		            </div>
          		</section>
			</div>
		)
	}
}

export default MdlBasicHomeSection;