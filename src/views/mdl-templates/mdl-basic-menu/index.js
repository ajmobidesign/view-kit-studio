import React, { Component } from 'react';
import classNames from 'classnames';
import { Tabs, Tab, Header, HeaderTabs, HeaderRow, Layout, Drawer, Content, FABButton, Icon} from 'react-mdl';
import MdlBasicHomeSection from '../mdl-basic-home-section';
import MdlBasicTechSection from '../mdl-basic-tech-section';
import MdlBasicFooter from '../mdl-basic-footer';
import PostitSticky from '../../app-components/postit-sticky';

 class MdlBasicMenu extends Component{

	constructor(props, context){
		super(props, context);

		this.state = {activeTab : 0};
		this.onTabChange = :: this.onTabChange;
	}

	onTabChange(tabId){
		this.setState({activeTab: tabId});
	}




	renderTabContent(){
		switch(this.state.activeTab) {
			case 0: 
					return <MdlBasicHomeSection></MdlBasicHomeSection>;
			case 1: 
					return <MdlBasicTechSection></MdlBasicTechSection>;
			case 2: 
					return <MdlBasicTechSection></MdlBasicTechSection>;
			case 3: 
					return <MdlBasicTechSection></MdlBasicTechSection>;
			case 4: 
					return <MdlBasicTechSection></MdlBasicTechSection>;
			default: 
					return 'No Page';														
		}

	}

	render(){


		return(
		        <Layout fixedHeader className="has-scrolling-header">
                    <Header scroll>
                        <HeaderRow className="mdl-layout--large-screen-only" />
                        <HeaderRow className="mdl-layout--large-screen-only">
                                <h3>MDL Basic HTML5</h3>
                        </HeaderRow>
                        <HeaderRow className="mdl-layout--large-screen-only" />  
                        <HeaderTabs ripple activeTab={this.state.activeTab} onChange={this.onTabChange} className="mdl-layout__tab-bar-container">
                            <Tab>Overview</Tab>
                            <Tab>Features</Tab>
                            <Tab>Details</Tab>
                            <Tab>Tehnology</Tab>
                            <Tab>FAQ</Tab>
                            <button className="mdl-button mdl-js-button mdl-button--fab  mdl-button--colored mdl-shadow--4dp " id="add">
				            <i className="material-icons" role="presentation">add</i>
				            <span className="visuallyhidden">Add</span>
				          </button>
                        </HeaderTabs>
                    </Header>
                    <main className="mdl-layout__content mdl-color--grey-100 mdl-color-text--grey-700">
                        {this.renderTabContent()}
                        <MdlBasicFooter></MdlBasicFooter>
                    </main>
		        </Layout>
		)
	}
}

export default MdlBasicMenu;