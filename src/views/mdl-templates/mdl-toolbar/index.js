import React, { Component } from 'react';
import classNames from 'classnames';
import { Tabs, Tab, Header, HeaderTabs, HeaderRow, Layout, Drawer, Content} from 'react-mdl';
import StylePage from '../style-page';


 class MdlToolbar extends Component{

	constructor(props, context){
		super(props, context);

		this.state = {activeTab : 1};
		this.onTabChange = :: this.onTabChange;
	}

	onTabChange(tabId){
		this.setState({activeTab: tabId});
	}

	renderTabContent(){
		switch(this.state.activeTab) {
			case 0: 
					return 'Home Page';
			case 1: 
					return 'Style Page';
			case 2: 
					return 'Page 2';
			case 3: 
					return 'Page 3';
			case 4: 
					return 'Page 4';
			case 5: 
					return 'Page 5';
			default: 
					return 'No Page';														
		}

	}

	render(){
		return(
		        <Layout fixedHeader>
                    <Header>
                        <HeaderRow className="mdl-layout--large-screen-only" />
                            <HeaderRow className="mdl-layout--large-screen-only">
                                <h3>Name &amp; Title</h3>
                            </HeaderRow>
                        <HeaderRow className="mdl-layout--large-screen-only" />  
                        <HeaderTabs ripple activeTab={this.state.activeTab} onChange={this.onTabChange}>
                            <Tab>Home</Tab>
                            <Tab>Styles</Tab>
                            <Tab>Tab3</Tab>
                            <Tab>Tab4</Tab>
                            <Tab>Tab5</Tab>
                            <Tab>Tab6</Tab>
                        </HeaderTabs>
                    </Header>
                    <Drawer title="Title" />
                    <Content>
                        <div className="page-content">{this.renderTabContent()}</div>
                    </Content>
		        </Layout>
		)
	}
}

export default MdlToolbar;