import React, { Component, PropTypes} from 'react';
import classNames from 'classnames';
import { Grid, Layout, Header, Drawer, Navigation, Content, Textfield, HeaderRow, Cell, Icon, Button, IconButton, MenuItem, Menu} from 'react-mdl';
import HeroOne from '../../mdl-components/hero-one';
import HeroTwo from '../../mdl-components/hero-two';
import HeroThree from '../../mdl-components/hero-three';
import HeroFour from '../../mdl-components/hero-four';
import MdlContent1 from '../../mdl-components/mdl-content1';
import MdlContent2 from '../../mdl-components/mdl-content2';
import MdlContent3 from '../../mdl-components/mdl-content3';
import MdlContent4 from '../../mdl-components/mdl-content4';

import MdlContent5 from '../../mdl-components/mdl-content5';
import MdlContent6 from '../../mdl-components/mdl-content6';
import MdlContent7 from '../../mdl-components/mdl-content7';

import MdlGridblock1 from '../../mdl-components/mdl-gridblock1';
import MdlGridblock2 from '../../mdl-components/mdl-gridblock2';
import MdlGridblock3 from '../../mdl-components/mdl-gridblock3';

import MdlContact1 from '../../mdl-components/mdl-contact1';
import MdlContact2 from '../../mdl-components/mdl-contact2';
import MdlContact3 from '../../mdl-components/mdl-contact3';

import MdlMiniFooter from '../../mdl-components/mdl-mini-footer';
import MdlLargeFooter from '../../mdl-components/mdl-large-footer';


import PostitSticky from '../../app-components/postit-sticky';
import Browser from 'detect-browser';


class MdlSinglePage extends Component{


	static proptypes = {
		components: PropTypes.object.isRequired
		
	};


	constructor(props, context){
		super(props, context);

		this.state = {showShadow: false, cx: null, cY: null}
		this._scrollListener = ::this._scrollListener;
		//this.renderHero = ::this.renderHero;
		this.passScroll = ::this.passScroll;
		this.renderNavs = ::this.renderNavs;
		this.renderInnerNav = ::this.renderInnerNav;
		this.onDragOver = ::this.onDragOver;
	}


	passScroll(){

	}

	_scrollListener(){

		let elem = document.querySelector('#singlepage-content')

		let scrollPos = (elem.scrollTop>0) ? true: false;
		
		this.props.scrollPos(elem.scrollTop);
		this.setState({showShadow: scrollPos})

	}

	componentDidMount(){
		let elem = document.querySelector('#singlepage-content');

		let browser = Browser.name;

		let firefox = browser === 'firefox';

		this.props.scrollPos(elem.scrollTop);
		this.setState({firefox: firefox, scroll: elem.scrollTop});
	}


	renderNavs (){

		function capitalizeFirstLetter(string) {
			    return string.charAt(0).toUpperCase() + string.slice(1);
			}

		let navs = Object.keys(this.props.components)

		let navList = ["hero", "features", "details", "grid", "contact"]

		return navList.filter((n)=>{ return navs.indexOf(n) !== -1} ).map((n, i)=>{
			let navLinkText = capitalizeFirstLetter(n);
			
				return <a href={"#"+ n} key={i}>{navLinkText}</a>
			
		})
	}

	renderInnerNav(){
		return this.renderNavs()
					.map((n, i)=>{
							return <MenuItem key={i}> {n} </MenuItem>
						})
	}


	renderStickys(){

		function objectWithKeyToArray(obj){
				if(obj){
					return Object.keys(obj).map((key)=>{
						return Object.assign({}, obj[key])
					});
				}
				else{
					return [];
				}
			}

		let stickyList = objectWithKeyToArray(this.props.stickys);

		let updateSticky = this.props.updateSticky;
		let removeSticky = this.props.removeSticky;
		let showSticky = this.props.showStickys;

		let xCord = (this.state.firefox) ? this.state.cX : null;
		let yCord = (this.state.firefox) ? this.state.cY: null;


		return 	stickyList.map((s, i)=>{
					return <PostitSticky  key={i} stickyOb={s}   removeSticky={removeSticky} updateSticky={updateSticky} docId={this.props.docId} show={showSticky} user={this.props.displayName} cX={xCord} cY={yCord}></PostitSticky>
			})

	}


	renderHero(){
		
		switch(this.props.components.hero){
			case "0":
				return <div className="hero-section"><a name="hero" /><HeroOne></HeroOne></div>;
			case "1":
				return <div className="hero-section"><a name="hero" /><HeroTwo></HeroTwo></div>;
			case "2":
				return <div className="hero-section"><a name="hero" /><HeroThree></HeroThree></div>	
			case "3":
				return <div className="hero-section"><a name="hero" /><HeroFour></HeroFour></div>;		
			default: 
				return ;	
		}
	}


	renderFeatures(){
		
		switch(this.props.components.features){
			case "0":
				return <div className="features-section"><a name="features" /><MdlContent1></MdlContent1></div>;
			case "1":
				return <div className="features-section"><a name="features" /><MdlContent2></MdlContent2></div>;
			case "2":
				return <div className="features-section"><a name="features" /><MdlContent3></MdlContent3></div>;	
					
			default: 
				return ;	
		}
	}


	renderDetails(){
		
		switch(this.props.components.details){
			case "0":
				return <div className="details-section"><a name="details" /><MdlContent5></MdlContent5></div>;
			case "1":
				return <div className="details-section"><a name="details" /><MdlContent6></MdlContent6></div>;
			case "2":
				return <div className="details-section"><a name="details" /><MdlContent7></MdlContent7></div>;		
			default: 
				return ;	
		}
	}


	renderGrid(){
		
		switch(this.props.components.grid){
			case "0":
				return <div className="grid-section"><a name="grid" /><MdlGridblock1></MdlGridblock1></div>;
			case "1":
				return <div className="grid-section"><a name="grid" /><MdlGridblock2></MdlGridblock2></div>;
			case "2":
				return <div className="grid-section"><a name="grid" /><MdlGridblock3></MdlGridblock3></div>;		
			default: 
				return ;	
		}
	}


	renderContact(){
		switch(this.props.components.contact){
			case "0":
				return <div className="contact-section"><a name="contact" /><MdlContact1></MdlContact1></div>;
			case "1":
				return <div className="contact-section"><a name="contact" /><MdlContact2></MdlContact2></div>;
			case "2":
				return <div className="contact-section"><a name="contact" /><MdlContact3></MdlContact3></div>;		
			default: 
				return ;	
		}
	}

	renderFooter(){
		switch(this.props.components.footer){
			case "0":
				return <MdlMiniFooter></MdlMiniFooter>;
			case "1":
				return <MdlLargeFooter></MdlLargeFooter>;	
			default:
				return ;	
		}
	}

	onDragOver(event){

		let cX = event.nativeEvent.pageX;
		let cY = event.nativeEvent.pageY;

		this.setState({cX: cX});
		this.setState({cY: cY});
	}



	render(){


		const cssNames = classNames({
			'mdl-layout__header' : true,
			'mdl-layout__header--waterfall' : true,
			'is-casting-shadow': this.state.showShadow
		});

		

		
		let dragOverEvent = (this.state.firefox) ? this.onDragOver : null;



		return(
			    <Layout fixedHeader onDragOver={dragOverEvent}>
			        <header className={cssNames}>
			        	<div className="mdl-layout__header-row" >
			        		<div className="logo">
			        			<Icon name="whatshot"  /> 
			        		</div>
			        		

				        	<span className="mdl-layout-title"> Company </span>
				        	<div className="mdl-layout-spacer"></div>
				        	<Navigation>
			                    {this.renderNavs()}
			                </Navigation>

			                <div className="dropdown-menu-container" >
							    <IconButton name="more_vert" id="demo-menu-lower-right" />
							    <Menu target="demo-menu-lower-right" align="right">
							        {this.renderInnerNav()}
							    </Menu>
							</div>
		                </div>
			        	
			        </header>
			        <div className="mdl-layout__content" id="singlepage-content" onScroll={this._scrollListener}>
			        	<div style={{position: 'relative'}}>
			        		{this.renderStickys()}
			        	</div>

			        {this.renderHero()}	
			        {this.renderFeatures()}
			        {this.renderDetails()}
			        {this.renderGrid()}
			        {this.renderContact()}
			        {this.renderFooter()}
			        </div>
			        
    		</Layout>

		)
	}

}

export default MdlSinglePage;