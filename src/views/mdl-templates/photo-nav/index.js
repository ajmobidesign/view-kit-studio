import React, { Component } from 'react';
import classNames from 'classnames';
import { Layout, Header, Navigation, Drawer, Content, Icon} from 'react-mdl';
import StylePage from '../style-page';
import NavLink from '../../mdl-components/nav-link';



class PhotoNav extends Component{


	constructor(props, context){
		super(props, context);

		this.state = {page : 1};
		this.onNavChange = :: this.onNavChange;
	}

	onNavChange(navId){
		console.log(navId)
		this.setState({page: navId});
	}

	renderPageContent(){
		switch(this.state.page) {
			case 0: 
					return 'Home Page';
			case 1: 
					return <StylePage></StylePage>;
			case 2: 
					return 'Page 2';
			case 3: 
					return 'Page 3';
			case 4: 
					return 'Page 4';
			case 5: 
					return 'Page 5';
			default: 
					return 'No Page';														
		}

	}


	render(){
		return(
			<Layout style={{background: 'url(http://www.getmdl.io/assets/demos/transparent.jpg) center / cover'}}>
		        <Header transparent title="Title" style={{color: 'white'}}>
		            <Navigation>
		                <NavLink value={0} navChange={this.onNavChange}> Home </NavLink>
		                <NavLink value={1} navChange={this.onNavChange}> Style Page </NavLink>
		                <NavLink value={2} navChange={this.onNavChange}> Page 2 </NavLink>
		                <NavLink value={3} navChange={this.onNavChange}> Page 3 </NavLink>
		                <NavLink value={4} navChange={this.onNavChange}> Page 4 </NavLink>
		                <NavLink value={5} navChange={this.onNavChange}> Page 5 </NavLink>
		            </Navigation>
		        </Header>
		        <Drawer title="Title">
		            <Navigation>
		                 <NavLink value={0} navChange={this.onNavChange}> Home </NavLink>
		                <NavLink value={1} navChange={this.onNavChange}> Style Page </NavLink>
		                <NavLink value={2} navChange={this.onNavChange}> Page 2 </NavLink>
		                <NavLink value={3} navChange={this.onNavChange}> Page 3 </NavLink>
		                <NavLink value={4} navChange={this.onNavChange}> Page 4 </NavLink>
		                <NavLink value={5} navChange={this.onNavChange}> Page 5 </NavLink>	
		            </Navigation>
		        </Drawer>
		       	 <div className="mdl-layout__drawer-button" aria-expanded={false} role="button">
		        	<Icon name="menu" />
		        </div>
		        <Content>
		        	{this.renderPageContent()}
		        </Content>
		    </Layout>
		);
	}

}

export default PhotoNav;