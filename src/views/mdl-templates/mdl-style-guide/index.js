import React, { Component } from 'react';
import classNames from 'classnames';
import { Layout, Navigation, IconButton, MenuItem, Menu} from 'react-mdl';
import StylePage from '../style-page';

import PostitSticky from '../../app-components/postit-sticky';
import Browser from 'detect-browser';


class MdlStyleGuide extends Component{

	constructor(props, context){
		super(props, context);

		this.state = {showShadow: false}
		this._scrollListener = ::this._scrollListener;
		this.renderNavs = ::this.renderNavs;
		this.renderInnerNav = ::this.renderInnerNav;
		this.renderStickys = ::this.renderStickys;
	}



	_scrollListener(){

		let elem = document.querySelector('#singlepage-content')
		let scrollPos = (elem.scrollTop>0) ? true: false;

		this.props.scrollPos(elem.scrollTop);
		this.setState({showShadow: scrollPos, scroll: elem.scrollTop});
	}

	componentDidMount(){
		let elem = document.querySelector('#singlepage-content');
	
		let browser = Browser.name;

		let firefox = browser === 'firefox';

		this.props.scrollPos(elem.scrollTop);
		this.setState({firefox: firefox, scroll: elem.scrollTop});
	}


	renderNavs (){

		let navList = ["Badges and Buttons", "Form and Loading", "Navs and Tabs"];

		function capitalizeFirstLetter(string) {
			    return string.charAt(0).toUpperCase() + string.slice(1);
			}

		return navList.map((n, i)=>{
			//let navLinkText = capitalizeFirstLetter(n);
			
				return <a href={"#"+ n} key={i}>{n}</a>
		})
	}

	renderInnerNav(){
		return this.renderNavs()
					.map((n, i)=>{
							return <MenuItem key={i}> {n} </MenuItem>
					})
	}


	renderStickys(){

		//console.log('render stickys called', this.state.scroll)

		function objectWithKeyToArray(obj){
				if(obj){
					return Object.keys(obj).map((key)=>{
						return Object.assign({}, obj[key])
					});
				}
				else{
					return [];
				}
			}

		let stickyList = objectWithKeyToArray(this.props.stickys);

		//console.log('sticky list', this.props.stickys)

		let updateSticky = this.props.updateSticky;
		let removeSticky = this.props.removeSticky;
		let showSticky = this.props.showStickys;

		let xCord = (this.state.firefox) ? this.state.cX : null;
		let yCord = (this.state.firefox) ? this.state.cY: null;


		return 	stickyList.map((s, i)=>{

						
					return <PostitSticky  key={i} stickyOb={s}   removeSticky={removeSticky} updateSticky={updateSticky} docId={this.props.docId} show={showSticky} user={this.props.displayName} cX={xCord} cY={yCord} scroll={this.state.scroll}></PostitSticky>
			})

	}





	render(){


		const cssNames = classNames({
			'mdl-layout__header' : true,
			'mdl-layout__header--waterfall' : true,
			'is-casting-shadow': this.state.showShadow
		});

		


		return(
			    <Layout fixedHeader >
			        <header className={cssNames}>
			        	<div className="mdl-layout__header-row" >
			        				        		
				        	<span className="mdl-layout-title"> MDL Custom Style Guide </span>
				        	<div className="mdl-layout-spacer"></div>
				        	<Navigation>
			                    {this.renderNavs()}
			                </Navigation>

			                <div className="dropdown-menu-container" >
							    <IconButton name="more_vert" id="demo-menu-lower-right" />
							    <Menu target="demo-menu-lower-right" align="right">
							        {this.renderInnerNav()}
							    </Menu>
							</div>
		                </div>
			        	
			        </header>
			        <div className="mdl-layout__content" id="singlepage-content" onScroll={this._scrollListener}>

			        	<div className="main-style-content">
			        		
			        		<StylePage></StylePage>

			        		{this.renderStickys()}
			        	</div>	
			        </div>
			        
    		</Layout>

		)
	}

}

export default MdlStyleGuide;