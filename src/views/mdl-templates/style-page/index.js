import React, { Component } from 'react';
import { Grid, Card, CardTitle, CardText, CardActions, Button, IconButton, Switch, Checkbox, List, ListItem, ListItemContent, ListItemAction, Icon, Textfield, Badge, FABButton, ProgressBar, RadioGroup, Radio, Slider, Spinner, Tabs, Tab, Header, HeaderTabs, HeaderRow, Layout, Drawer, Content, Navigation} from 'react-mdl';


class StylePage extends Component{

	constructor(props, context){
		super(props, context);

		this.state = {activeTab: 0, activeRippleTab: 0, activeNavTabSmall: 0, activeNavTabLarge: 0}

	}



	render(){





		return(
			<div className="mdl-grid mdl-cell--12-col ">

					<div className="mdl-grid mdl-cell--12-col ">
						<a name="Badges and Buttons"></a>
						<h4 className="mdl-typography--display-1 button-title"> Badges, Buttons and Icons </h4>
					</div>

					<div className="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-title-container">
				  		<p> Badges </p>
				  	</div>
				  	<div className="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-container">
				  		<div className="inline">
							<Badge text="4">Inbox</Badge>
						</div>
						<div className="inline">
							<Badge text="♥" overlap>
							    <Icon name="account_box" />
							</Badge>
						</div>
						<div className="inline">
							<Badge text="1" overlap>
							    <Icon name="account_box" />
							</Badge>
						</div>
						<div className="inline">
							<Badge text="♥" noBackground>Mood</Badge>
						</div>

				  	</div>

					<div className="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-title-container">
				  		<p> Flat Buttons </p>
				  	</div>
				  	<div className="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-container">
				  		<div className="inline">
				  			
							<Button>Button</Button> 
							<p>
								<small> Default Button </small>
							</p>
						</div>
						<div className="inline">
							
							<Button ripple>Button</Button>
							<p>
								<small> Button with ripple </small>
							</p>	
						</div>
						<div className="inline">
							
							<Button disabled>Button</Button> 
							<p>
								<small> Disabled Button </small>
							</p>	
						</div>
						<div className="inline">
							
							<Button primary>Button</Button>
							<p>
								<small> Button Primary </small>
							</p>	
						</div>
						<div className="inline">
							
							<Button accent>Button</Button>
							<p>
								<small> Button Accent </small>
							</p>	
						</div>
					</div>

					<div className="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-title-container">
				  		<p> Raised Buttons </p>
				  	</div>
				  	<div className="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-container">
				  		<div className="inline">
							<Button raised>Button</Button>
							<p>
								<small> Raised Button Default</small>
							</p>
						</div>
						<div className="inline">
							<Button raised ripple>Button</Button>
							<p>
								<small> Raised Button with ripple </small>
							</p>
						</div>
						<div className="inline">
							<Button raised disabled>Button</Button>
							<p>
								<small> Raised Button disabled </small>
							</p>	
						</div>

				  	</div>
				
					

				  	<div className="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-title-container">
				  		<p> Rasied Colored Buttons </p>
				  	</div>
				  	<div className="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-container">
				  		<div className="inline">
							<Button raised colored>Button</Button>
							<p>
								<small> Raised Primary </small>
							</p>
						</div>
						<div className="inline">
							<Button raised colored ripple>Button</Button>
							<p>
								<small> Raised Primary with ripple   </small>
							</p>
						</div>
					

						<div className="inline">
							<Button raised accent>Button</Button>
							<p>
								<small> Raised Accent </small>
							</p>
						</div>
						<div className="inline">
							<Button raised accent ripple>Button</Button>
							<p>
								<small> Raised Accent ripple </small>
							</p>
						</div>
						

				  	</div>



				  	<div className="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-title-container">
				  		<p> FAB Buttons </p>
				  	</div>
				  	<div className="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-container">
				  		<div className="inline">
							<FABButton >
							    <Icon name="add" />
							</FABButton>
							<p>
								<small> Default FAB  </small>
							</p>
						</div>
						<div className="inline">
							<FABButton ripple>
							    <Icon name="add"  />
							</FABButton>
							<p>
								<small> FAB with ripple </small>
							</p>
						</div>
						<div className="inline">
							<FABButton disabled>
							    <Icon name="add" />
							</FABButton>
							<p>
								<small> FAB disabled </small>
							</p>
						</div>
						
				  	</div>



				  	<div className="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-title-container">
				  		<p> Colored FAB buttons </p>
				  	</div>
				  	<div className="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-container">
				  		<div className="inline">
							<FABButton colored>
						    	<Icon name="add" />
							</FABButton>
							<p>
								<small> FAB colored </small>
							</p>
						</div>
						<div className="inline">
							<FABButton colored ripple>
						    	<Icon name="add" />
							</FABButton>
							<p>
								<small> FAB colored with ripple </small>
							</p>
						</div>
						
				  	

				  	</div>
				  	<div className="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-title-container">
				  		<p> Colored FAB buttons </p>
				  	</div>
				  	<div className="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-container">

				  		<div className="inline">
				  			<FABButton mini>
							    <Icon name="add" />
							</FABButton>
							<p>
								<small> FAB mini </small>
							</p>
				  		</div>
				  		<div className="inline">
				  			<FABButton mini colored>
							    <Icon name="add" />
							</FABButton>
							<p>
								<small> FAB mini colored</small>
							</p>
				  		</div>
				  	</div>


				  	<div className="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-title-container">
				  		<p> Icon Buttons </p>
				  	</div>
				  	<div className="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-container">
				  		<div className="inline">
							<IconButton name="mood" />

							<p>
								<small> Icon Button default </small>
							</p>
						</div>
						<div className="inline">
							<IconButton name="mood" colored />

							<p>
								<small> Icon Button colored </small>
							</p>
						</div>

				  	</div>

				  	<div className="mdl-grid mdl-cell--12-col ">
						<a name="Form and Loading"></a>
						<h4 className="mdl-typography--display-1"> Form Elements and Loading </h4>
					</div>


				  	<div className="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-title-container">
				  		<p> Expandable Iput with Icon </p>
				  	</div>
				  	<div className="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-container">

				  		<div className="inline">
							<Textfield
								onChange={() => {}}
								label="Expandable Input"
								expandable
								expandableIcon="search"/>
						</div>

				  	</div>


				  	<div className="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-title-container">
				  		<p> Simple text Input </p>
				  	</div>
				  	<div className="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-container">
				  		<div className="inline">
							<Textfield
							    onChange={() => {}}
							    label="Text..."
							    style={{width: '200px'}}/>
						</div>


				  	</div>


				  	<div className="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-title-container">
				  		<p> Input with floating label </p>
				  	</div>
				  	<div className="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-container">
				  		
						<div className="inline">
							<Textfield
							    onChange={() => {}}
							    label="Text..."
							    floatingLabel
							    style={{width: '200px'}}/>
						</div>


				  	</div>
				
				
				  	<div className="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-title-container">
				  		<p> Checkbox </p>
				  	</div>
				  	<div className="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-container">

				  		<div className="inline">
							<Checkbox label="With ripple" ripple defaultChecked />
						</div>
						<div className="inline">
							<Checkbox label="Without ripple" defaultChecked />
						</div>
				  						  		
				  	</div>

				  	<div className="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-title-container">
				  		<p> Progress Bars </p>
				  	</div>
				  	<div className="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-container">
				  		<div className="inline">
				  			Default
							<ProgressBar progress={44} />
						</div>
						<div className="inline">
							Indeterminate
							<ProgressBar indeterminate />
						</div>

						<div className="inline">
							Buffer
							<ProgressBar progress={33} buffer={87} />
						</div>
				  	</div>



				  	<div className="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-title-container">
				  		<p> Radio </p>
				  	</div>
				  	<div className="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-container">
				  		<RadioGroup name="demo" value="opt1">
						    <Radio value="opt1" ripple>Opt 1</Radio>
						    <Radio value="opt2" ripple>Opt 2</Radio>
						</RadioGroup>
				  	</div>

				  	<div className="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-title-container">
				  		<p> Slider </p>
				  	</div>
				  	<div className="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-container">
				  		<Slider min={0} max={100} defaultValue={25} />

				  	</div>

				  	<div className="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-title-container">
				  		<p> Spinner </p>
				  	</div>
				  	<div className="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-container">
				  		<Spinner singleColor />
				  	</div>

				  	<div className="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-title-container">
				  		<p> Switch </p>
				  	</div>
				  	<div className="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-container">
				  		
				  		
				  		
				  			<label className="mdl-switch mdl-js-switch mdl-js-ripple-effect" htmlFor="switch-2">
							  <input type="checkbox" id="switch-2" className="mdl-switch__input" />
							  <span className="mdl-switch__label"> Ripple Switch</span>
							</label>

				  		
				  	</div>


				  	<div className="mdl-grid mdl-cell--12-col ">
						<a name="Navs and Tabs"></a>
						<h4 className="mdl-typography--display-1"> Tabs and Navigation </h4>
					</div>


				  	<div className="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-title-container">
				  		<p> Navbar </p>
				  	</div>
				  	<div className="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-container">
				  		<div className="nav-display-container">

				  			<div className="mdl-layout mdl-js-layout mdl-layout--fixed-header">
							  <header className="mdl-layout__header">
							    <div className="mdl-layout__header-row">
							      
							      <span className="mdl-layout-title">Title</span>
							      
							      <div className="mdl-layout-spacer"></div>
							      
							      <nav className="mdl-navigation mdl-layout--large-screen-only">
							        <a className="mdl-navigation__link" href="#">Link</a>
							        <a className="mdl-navigation__link" href="#">Link</a>
							        <a className="mdl-navigation__link" href="#">Link</a>
							        <a className="mdl-navigation__link" href="#">Link</a>
							      </nav>
							    </div>
							  </header>
							  <div className="mdl-layout__drawer">
							    <span className="mdl-layout-title">Title</span>
							    <nav className="mdl-navigation">
							      <a className="mdl-navigation__link" href="#">Link</a>
							      <a className="mdl-navigation__link" href="#">Link</a>
							      <a className="mdl-navigation__link" href="#">Link</a>
							      <a className="mdl-navigation__link" href="#">Link</a>
							    </nav>
							  </div>
							  <main className="mdl-layout__content">
							    <div className="page-content"></div>
							  </main>
							</div>



						   
						</div>
				  	</div>


				  	<div className="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-title-container">
				  		<p> Tabs with ripple</p>
				  	</div>
				  	<div className="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-container">
				  		<div className="nav-display-container">

					  		<Tabs activeTab={this.state.activeRippleTab} onChange={(tabId) => this.setState({ activeRippleTab: tabId })}  ripple>
				                    <Tab> Ripple Tab 1</Tab>
				                    <Tab> Ripple Tab 2</Tab>
				                    <Tab> Ripple Tab 3</Tab>
				            </Tabs>

				            <section>
			                    <div className="content">Content for the tab: {this.state.activeRippleTab + 1}</div>
			                </section>
		                </div>
				  	</div>
				  	<div className="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-title-container">
				  		<p> Nav with small Tabs </p>
				  	</div>
				  	<div className="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-container">
				  		<div className="nav-display-container">
				  			<div className="mdl-layout mdl-js-layout mdl-layout--fixed-header mdl-layout--fixed-tabs">
							  <header className="mdl-layout__header">
							    <div className="mdl-layout__header-row">
							     
							      <span className="mdl-layout-title">Title</span>
							    </div>
							    
							    <div className="mdl-layout__tab-bar mdl-js-ripple-effect">
							      <a href="#fixed-tab-1" className="mdl-layout__tab is-active">Tab 1</a>
							      <a href="#fixed-tab-2" className="mdl-layout__tab">Tab 2</a>
							      <a href="#fixed-tab-3" className="mdl-layout__tab">Tab 3</a>
							    </div>
							  </header>
							  <div className="mdl-layout__drawer">
							    <span className="mdl-layout-title">Title</span>
							  </div>
							  <main className="mdl-layout__content">
							    <section className="mdl-layout__tab-panel is-active" id="fixed-tab-1">
							      <div className="page-content">Content for tab: 1</div>
							    </section>
							    <section className="mdl-layout__tab-panel" id="fixed-tab-2">
							      <div className="page-content">Content for tab: 2</div>
							    </section>
							    <section className="mdl-layout__tab-panel" id="fixed-tab-3">
							      <div className="page-content">Content for tab: 3</div>
							    </section>
							  </main>
							</div>

				  			
						    
						</div>
				  	</div>

				  	<div className="mdl-cell mdl-cell--3-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-title-container">
				  		<p> Nav with large Tabs </p>
				  	</div>
				  	<div className="mdl-cell mdl-cell--9-col mdl-cell--12-col-tablet mdl-cell--12-col-phone elements-container">
				  		<div className="nav-display-container">

				  			<div className="mdl-layout mdl-js-layout mdl-layout--fixed-header">
							  <header className="mdl-layout__header">
							    <div className="mdl-layout__header-row">
							     
							      <span className="mdl-layout-title">Title</span>
							    </div>
							    
							    <div className="mdl-layout__tab-bar mdl-js-ripple-effect">
							      <a href="#scroll-tab-1" className="mdl-layout__tab is-active">Tab 1</a>
							      <a href="#scroll-tab-2" className="mdl-layout__tab">Tab 2</a>
							      <a href="#scroll-tab-3" className="mdl-layout__tab">Tab 3</a>
							      <a href="#scroll-tab-4" className="mdl-layout__tab">Tab 4</a>
							      <a href="#scroll-tab-5" className="mdl-layout__tab">Tab 5</a>
							      <a href="#scroll-tab-6" className="mdl-layout__tab">Tab 6</a>
							    </div>
							  </header>
							  <div className="mdl-layout__drawer">
							    <span className="mdl-layout-title">Title</span>
							  </div>
							  <main className="mdl-layout__content">
							    <section className="mdl-layout__tab-panel is-active" id="scroll-tab-1">
							      <div className="page-content">Content for tab: 1</div>
							    </section>
							    <section className="mdl-layout__tab-panel" id="scroll-tab-2">
							      <div className="page-content">Content for tab: 2</div>
							    </section>
							    <section className="mdl-layout__tab-panel" id="scroll-tab-3">
							      <div className="page-content">Content for tab: 3</div>
							    </section>
							    <section className="mdl-layout__tab-panel" id="scroll-tab-4">
							      <div className="page-content">Content for tab: 4</div>
							    </section>
							    <section className="mdl-layout__tab-panel" id="scroll-tab-5">
							      <div className="page-content">Content for tab: 5</div>
							    </section>
							    <section className="mdl-layout__tab-panel" id="scroll-tab-6">
							      <div className="page-content">Content for tab: 6</div>
							    </section>
							  </main>
							</div>

			                
			            </div>
				  		
				  	</div>

					
			</div>
		)
	}
}

export default StylePage;