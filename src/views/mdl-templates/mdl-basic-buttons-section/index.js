import React, { Component } from 'react';
import { Grid, Button, IconButton, FABButton, Cell, Icon, Card } from 'react-mdl';


class MdlBasicButtonsSection extends Component{

	render(){
		return(
			<div className="mdl-layout__tab-panel is-active" style={{marginBottom: '48px'}}>
				<section className="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp">
					<Cell col={12}>
						<h5 className="mdl-cell mdl-cell--12-col">Fab Buttons</h5>

						<Grid>
							<Cell col={4} style={{textAlign: 'center'}}>
								<FABButton >
								    <Icon name="add" />
								</FABButton>

								<p> Fab Button </p>
							</Cell>
							<Cell col={4} style={{textAlign: 'center'}}>
								<FABButton>
								    <Icon name="add" />
								</FABButton>

								<p> Fab Button </p>
							</Cell>
							<Cell col={4} style={{textAlign: 'center'}}>
								<FABButton disabled>
								    <Icon name="add" />
								</FABButton>

								<p> Disabled Fab Button </p>
							</Cell>
						</Grid>
					</Cell>
				</section>
				<section className="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp">
					<Cell col={12}>
						<Grid>
							<Cell col={6}>
								<FABButton colored>
							    	<Icon name="add" />
								</FABButton>
							</Cell>
							<Cell col={6}>
								<FABButton colored ripple>
							    	<Icon name="add" />
								</FABButton>
							</Cell>
						</Grid>
					</Cell>
				</section>
				<section className="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp">
					<Cell col={12}>
						<Grid>
							<Cell col={6}>
								<Button primary>Button</Button>
							</Cell>
							<Cell col={6}>
								<Button accent>Button</Button>
							</Cell>
						</Grid>
					</Cell>
				</section>	
				<section className="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp">
					<Cell col={12}>
						<Grid>
							<Cell col={4}>
								<Button>Button</Button>
							</Cell>
							<Cell col={4}>
								<Button ripple>Button</Button>
							</Cell>
							<Cell col={4}>
								<Button disabled>Button</Button>
							</Cell>
						</Grid>
					</Cell>
				</section>

				<section className="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp">
					<Cell col={12}>

						<div className="inline">
								<Button raised colored>Button</Button>
							</div>
							<div className="inline">
								<Button raised colored ripple>Button</Button>
							</div>
						
							<div className="inline">
								<Button raised accent ripple>Button</Button>
							</div>
							<div className="inline">
								<Button raised accent>Button</Button>
							</div>

					</Cell>
				</section>
				<section className="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp">
					<Cell col={12}>
							<div className="inline">
								<Button raised>Button</Button>
							</div>
							<div className="inline">
								<Button raised ripple>Button</Button>
							</div>
							<div className="inline">
								<Button raised disabled>Button</Button>
							</div>
					</Cell>
				</section>
				<section className="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp">
					<Cell col={12}>
							<div className="inline">
								<Button raised>Button</Button>
							</div>
							<div className="inline">
								<Button raised ripple>Button</Button>
							</div>
							<div className="inline">
								<Button raised disabled>Button</Button>
							</div>
						
					</Cell>
				</section>
				<section className="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp">
					<Cell col={12}>
							<div className="inline">
								<Button raised>Button</Button>
							</div>
							<div className="inline">
								<Button raised ripple>Button</Button>
							</div>
							<div className="inline">
								<Button raised disabled>Button</Button>
							</div>
						
					</Cell>
				</section>

			</div>	
		)
	}
}

export default MdlBasicButtonsSection;