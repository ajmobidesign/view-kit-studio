import React, { Component } from 'react';
import { Grid, Button, IconButton, FABButton, Cell, Icon} from 'react-mdl';


class MdlBasicDatatableSection extends Component{

	render(){
		return(
			<div className="mdl-layout__tab-panel is-active" style={{marginBottom: '48px'}}>
				<section className="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp">
					<Cell col={10}>
						<h4> Data Table</h4>

						<table className="mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp">
						  <thead>
						    <tr>
						      <th className="mdl-data-table__cell--non-numeric">Material</th>
						      <th>Quantity</th>
						      <th>Unit price</th>
						    </tr>
						  </thead>
						  <tbody>
						    <tr>
						      <td className="mdl-data-table__cell--non-numeric">Acrylic (Transparent)</td>
						      <td>25</td>
						      <td>$2.90</td>
						    </tr>
						    <tr>
						      <td className="mdl-data-table__cell--non-numeric">Plywood (Birch)</td>
						      <td>50</td>
						      <td>$1.25</td>
						    </tr>
						    <tr>
						      <td className="mdl-data-table__cell--non-numeric">Laminate (Gold on Blue)</td>
						      <td>10</td>
						      <td>$2.35</td>
						    </tr>
						  </tbody>
						</table>
						
					</Cell>

				</section>

			</div>	
		)
	}
}

export default MdlBasicDatatableSection;