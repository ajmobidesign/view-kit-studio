import React, { Component, PropTypes } from 'react';
import * as Colors from '../../styles/inline';


class MdlSinglePageStyles extends Component {

  static proptypes ={
    style: PropTypes.object.isRequired, 
    components: PropTypes.object.isRequired
  };




  render(){

        
        let primaryRgb = this.props.style.primaryRgb;
        let primaryLightRgb = this.props.style.primaryLightRgb;
        let primaryDarkRgb = this.props.style.primaryDarkRgb;

        let accentRgb = this.props.style.accentRgb;
        let accentLightRgb = this.props.style.accentLightRgb;

        //main colors
        let colorPrimary = `rgb(${primaryRgb})`;
        let colorPrimaryDark = `rgb(${primaryDarkRgb})`;
        let colorAccent =`rgb(${accentRgb})`;

        let colorPrimaryContrast = `rgb(${primaryLightRgb})`;
        let colorAccentContrast = `rgb(${accentLightRgb})`;


        const commonCss = `

            .logo{
                margin-right: 15px;
            }

            div.mdl-layout.is-small-screen .mdl-navigation{
                display: none;
            }

            .dropdown-menu-container{
                position: relative;
                display: none;
                margin-right: 50px;
            }

            .mdl-menu__item a{
                text-decoration: none;
            }

            div.mdl-layout.is-small-screen .mdl-layout__header--waterfall{
                overflow:visible;
            }

            div.mdl-layout.is-small-screen .dropdown-menu-container{
                display: inline-block;
            }

            div.mdl-layout.is-small-screen .dropdown-menu-container .mdl-menu__item > a{
                color: ${colorPrimary};
            }

            .social-button{
                margin: 5px;
            }

            footer > div{
                max-width: 1044px;
                margin:auto;
            }




        `;




        const hero1Css = `

            header.mdl-layout__header:not(.is-casting-shadow){
                background-color: #fff;
                color: ${colorPrimary};
            }

            header.mdl-layout__header:not(.is-casting-shadow) nav a{
                color: ${colorPrimary};
            }


            .hero-section{
                position:relative;
                width:100%;
                height: 60vh;
                background-color: ${colorPrimaryContrast};
                color: ${colorPrimary};
                padding-top: 80px;
                
            }

            .hero-section > .mdl-grid{
                max-width: 1044px;
            }


            .hero-section .hero-text-section{
                width: 90%;
                margin: auto;
               
            }

            .hero-text-section h3{
                font-weight: 300;
                font-size: 31px;
                margin: 0px auto 30px auto;
            }

            .hero-text-section p{
                color: rgba(0,0,0, .54);
            }

           
            .player{
                width: 95%;
                height: 350px;
                margin: 0px auto 20px auto;
                background-color: #fff;
                display: flex;
                -webkit-align-items: center;
                align-items: center;
                justify-content: center;
                -webkit-justify-content: center;
                color:rgba(0,0,0, .4);
            }

            .player .material-icons{
                color: ${colorAccent};
                cursor: pointer;
            }


        `;


        const hero2Css = `

            header.mdl-layout__header:not(.is-casting-shadow){
                background-color: ${colorPrimaryContrast};
                color: ${colorPrimary};
            }

            header.mdl-layout__header:not(.is-casting-shadow) nav a{
                color: ${colorPrimary};
            }
           

            .hero-section{
                position:relative;
                width:100%;
                height: 60vh;
                background-color: ${colorPrimaryContrast};
                color: ${colorPrimary};
                padding-top: 80px;
                
            }

            .hero-section > .mdl-grid{
                max-width: 1044px;
            }

            .hero-section > .mdl-grid > .mdl-cell{
                margin: auto;
            }

            .hero-section .hero-text-section{
               
                text-align: center;
                margin:auto;
            }

            .hero-text-section h3{
                font-size: 31px;
                margin: 24px auto 20px auto;
                color: ${colorPrimaryDark};
            }

            .hero-section .hero-text-section h3 > i.material-icons{
                font-size: 60px;
                color: ${colorAccent};
            }

            .hero-section .hero-button-section{
                margin-top: 25px;
            }

            

        `;


        const hero3Css = `
             .hero-section{
                position:relative;
                width:100%;
                height: 60vh;
                background-color: ${colorPrimary};
                padding-top: 80px;
                
            }

            .hero-section > .mdl-grid{
                max-width: 1044px;
            }

            .hero-section > .mdl-grid > .mdl-cell{
                margin: auto;
            }

            .hero-section .hero-text-section{
                text-align: center;
                color: #fff; 
            }

            .hero-section .hero-text-section p{
                color: #fff; 
            }

            .hero-section .hero-text-section a{
                color: #fff;
            }


            .hero-section .hero-text-section h3{
                font-weight: 300;
                font-size: 40px;
                margin-bottom: 54px;
            }


            .hero-form{
                background-color: #fff;
                text-align: center;
                margin-bottom: 8px;
            }


            .hero-form h4{
                color: rgba(0,0,0, .7);
                font-weight: 300;
                margin-top: 10px;
                margin-bottom: 10px;
            }

            .hero-form button{
                width: 120px;
                margin-left: 20px;
                vertical-align: top;
                margin-top: 10px;
            }


        `;



        const hero4Css = `

            header.mdl-layout__header:not(.is-casting-shadow){
                background-color: #fff;
                color: ${colorPrimary};
            }

            header.mdl-layout__header:not(.is-casting-shadow) nav a{
                color: ${colorPrimary};
            }


            .hero-section{
                position:relative;
                width:100%;
                height: 60vh;
                background-color: ${colorPrimary};
                color: #fff;
                
            }

            .hero-section > .mdl-grid{
                max-width: 1044px;
            }

            .hero-section > .mdl-grid > .mdl-cell{
                margin: auto;
            }

            .hero-section .hero-text-section{
                text-align: center;
                color: ${colorAccent};

            }

            .hero-section .hero-text-section h3{
                font-weight: 300;
            }


            .hero-section .hero-text-section button{
                width:300px;
                background-color: #fff;
                color: ${colorAccent};
            }


        `;


        const features1Css =`
             .features-section{
                position:relative;
                width: 100%;
                height: 500px;
                padding-top:80px;
                padding-bottom: 80px;
            }

            .features-headline{
                text-align: center;
            }

            .features-headline h4{
                color: rgba(0,0,0, .54);
                font-weight: 300;
                font-size: 30px;
                margin-bottom: 30px;
            }

            .features-section> .mdl-grid{
                max-width : 1044px;
            }

            .features-section> .mdl-grid >.mdl-cell{
                margin: auto;
            }

            .features-section .mdl-card{
                height: 370px;
                margin: 10px auto;
                width: 270px;
            }

            .features-section .mdl-card__title{
                background: url(/images/dog.png) bottom right 15% no-repeat;
                
                background-color: ${colorPrimaryContrast};
                color: ${colorPrimaryDark};
            }

        `;


         const features2Css =`
             .features-section{
                position:relative;
                width: 100%;
                height: 500px;
                padding-top:80px;
                padding-bottom: 80px;
            }

            .features-section> .mdl-grid{
                max-width : 1044px;
            }


            .features-headline{
                text-align: center;
            }

            .features-headline h4{
                color: rgba(0,0,0, .54);
                font-weight: 300;
                font-size: 30px;
            }

            .features-headline p{
                color: rgba(0,0,0, .6);
            }

            .feature-grid .mdl-cell--3-col{
                margin: auto;
            }

            .features-section .material-icons{
                color: ${colorPrimaryDark};
                font-size: 30px;
            }

            .features-section .mdl-card{
                background-color: inherit;
            }

            .features-section .mdl-card .mdl-card__media{
                height: 50px;
                padding-left: 16px;
                background-color: inherit;
            }

            .features-section .mdl-card .mdl-card__media i.material-icons{
                margin: auto;
                font-size: 50px;
                color: ${colorAccent};
            }

            .features-section .mdl-card .mdl-card__title{
                padding-bottom: 0px;
                color: rgba(0,0,0, .7)
            }


            .features-section .mdl-card .mdl-card__supporting-text{
                padding-top:5px;
            }

        `; 


        const features3Css =`
             .features-section{
                position:relative;
                width: 100%;
                height: 500px;
                padding-top:80px;
                padding-bottom: 80px;
            }

            .features-section> .mdl-grid{
                max-width : 1044px;
            }

            .features-headline{
                text-align: center;
                
            }

            .features-headline h4{
                color: rgba(0,0,0, .54);
                font-weight: 300;
                font-size: 30px;
                margin-bottom: 30px;
            }


            .features-section .mdl-tabs__tab-bar{
                border:1px solid #efefef;
            }


            .features-section .tabs-container{
                background-color: #fff;
                border-left: 1px solid #efefef;
                border-right: 1px solid #efefef;
                border-bottom: 1px solid #efefef;
            }

            .features-section .tabs-container .content > .mdl-grid{
                margin-top:0px;
            }


            .features-section .material-icons{
                color: ${colorAccent};
                font-size: 80px;
            }

            .features-section .mdl-card{
                background-color: inherit;
                height: 280px;
                margin-left: auto;
                margin-right: auto;
            }


            .features-section .mdl-card .mdl-card__title p{
                text-align: center;
                margin: auto;
                font-weight: 300;
                font-size: 24px; 

            }

            .features-image.mdl-card {
              width: 100%;
              height: 250px;
              background-image: url(/images/basic-pic2.svg);
              background-size: 50%;
              background-repeat: no-repeat;
              background-position: center;
              background-color: ${colorPrimaryContrast};
            }

        `; 


        const features4Css =`
             .features-section{
                position:relative;
                width: 100%;
                height: 450px;
            }

            .features-section> .mdl-grid{
                max-width : 1044px;
            }

            .features-section .material-icons{
                color: ${colorPrimaryDark};
                font-size: 30px;
            }

        `; 


        const details1Css = `
            .details-section{
                position: relative;
                width: 100%;
                height: 650px;
                padding-top: 80px;
                padding-bottom: 80px;
            }

            .details-section > .mdl-grid{
                max-width: 1044px;
            }

            .details-section .mdl-tabs__tab-bar{
                border:1px solid #efefef;
                background-color: #fcfcfc;
            }

        
            .details-section .mdl-tabs.is-upgraded .mdl-tabs__tab.is-active:after{
                background: ${colorPrimary};
            }

            .mdl-tabs__tab .mdl-tabs__ripple-container .mdl-ripple{
                background: ${colorPrimary}
            }


            .details-section .tabs-container{ 
                min-height:550px;
                border-left: 1px solid #efefef;
                border-right: 1px solid #efefef;
                border-bottom: 1px solid #efefef;
            }

            details-section .tabs-container .content > .mdl-grid{
                margin-top:0px;
            }


            .details-section  h2.mdl-card__title-text{

                color: rgba(0,0,0 , .54);
                font-size: 30px;
            }


            .details-item-block{
                height: 300px;
    
            }

            .details-item-image-block{
                height: 300px;                
            }


            .details-item-block > div.mdl-cell,
            .details-item-image-block > div.mdl-cell{
                margin: 10px auto;
            }

            .content{
                height:100%;
            }

            

            .mix-grid-3-col .mix-grid-left-col, 
            .mix-grid-3-col .mix-grid-right-col{
                padding-top: 50px;
                padding-bottom: 50px;
            }


            .mix-grid-3-col .mix-grid-left-col .mdl-card,
            .mix-grid-3-col .mix-grid-right-col .mdl-card,
            .mix-grid-3-col .mix-grid-center-col .mdl-card{
                margin: auto;
            }

            .mix-grid-3-col .mix-grid-left-col .mdl-card{
                text-align: right;
            }

            .mix-grid-3-col .mix-grid-left-col .mdl-card h4,
            .mix-grid-3-col .mix-grid-right-col .mdl-card h4{
                font-weight: 300;
                color: ${colorAccent};
            }

            .mix-grid-3-col .mix-grid-center-col .mdl-card div.mdl-card__title.mdl-card--expand{
                background: url(/images/dog.png) bottom right 15% no-repeat;
                background-color: ${colorPrimaryContrast};
            }


            .mix-grid-3-col .mix-grid-center-col .mdl-card{
                height: 500px;
                width: 400px;
            }


            .details-item-image-block img{
                width:100%;
                height: auto;
                background-color: ${colorPrimaryContrast};
            }


            .mdl-card.card-with-left-border{
                width: 100%;
                min-height: 120px;
                padding-top: 8px;
                padding-bottom: 8px;
                margin: 10px auto; 
                border-left: 3px solid ${colorAccent};             
            }

            .mdl-card.card-with-left-border strong{
                color: ${colorAccent};
            }


            .mdl-card.card-with-left-border  .image-block{
                border-radius: 50px;
                width: 90px;
                height: 90px;
                background: url(/images/user.jpg) 100% no-repeat;
                background-color: ${colorPrimaryContrast};
            }


            .picture-card {
                margin: 10px auto;
            }


            .picture-card  div.mdl-card__title.mdl-card--expand{
                height: 200px;
                background: url(/images/dog.png) bottom right 15% no-repeat;
            }


            .picture-card div.mdl-card__supporting-text{
                border-top: 1px solid rgba(0,0,0, .1);
            }

            .picture-card div.mdl-card__supporting-text h4{
                font-size: 22px;
                color: ${colorAccent};
            }

        `;

        const details2Css = `
            .details-section{
                position: relative;
                width: 100%;
                height: 650px;
                padding-top:80px;
                padding-bottom: 80px;
            }

            .details-section > .mdl-grid{
                max-width: 1044px;
            }

            .details-section > .mdl-grid > .details-item-image-block{
                height: 600px;
                margin:auto;
            }

            .details-section .mdl-card  .mdl-card__title{                
                background: url(/images/iphone.png) center no-repeat;
                background-color: ${colorPrimaryContrast};
            }

            .details-section .mdl-card .mdl-button--fab{
                float:right;
                margin-top: -90px;
            }

            .details-section > .mdl-grid > .details-item-block{
                
                margin: auto;

            }


            h4.mdl-typography--display-1{
                font-weight: 300;
                color: ${colorAccent};

            }

            .details-section p{
                color: rgba(0,0,0,.54);
                font-size: 1rem;
                line-height: 20px;
            }

            .icon-container{

                display: -webkit-flex;
                display: flex;
                -webkit-flex-wrap: wrap;
                flex-wrap: wrap;
                -webkit-align-content: center;
                align-content: center;
                color: ${colorPrimary};
                
            }

            .icon-container .material-icons{
                font-size: 40px;                
            }

            .icon-text-container p{
                
                color: rgba(0,0,0, .54);
            }

            .details-item-image-block{
                height: 100%;
                
            }

            .details-item-image-block img{
                width:100%;
                height: auto;

            }


            .mix-grid-left-col .icon-text-container h4{
                font-weight: 300;
                color: ${colorAccent};
            }

            .mix-grid-left-col .icon-container{
                text-align: center;
            }

            .mix-grid-left-col .icon-container i{
                float: right;
            }

            .mix-grid-right-col .icon-text-container{
                
            }

        `;


        const details3Css = `
            .details-section{
                position: relative;
                width: 100%;
                height: 650px;
                padding-top:80px;
                padding-bottom: 80px;
            }

            .details-section> .mdl-grid{
                max-width: 1044px;
            }

            .details-item-text-block{ 
                margin: auto;
                text-align: center;
                color: rgba(0,0,0, .54);
            }

            .mdl-cell.details-item-block{
                margin: auto;
            }

            .player-container{
                width: 100%;
                min-height: 380px;
                margin: auto;
                display: -webkit-flex;
                display: flex;
                -webkit-flex-wrap: wrap;
                flex-wrap: wrap;
                -webkit-align-content: center;
                align-content: center;
                background-color: ${colorPrimaryContrast};
            }

            .player-container .material-icons{
                font-size: 2rem;
                color: ${colorPrimary};
                margin: auto;
            }





            .details-item-text-block p{
                margin-bottom: 25px;
            }

            .details-item-text-block button{
                margin-top: 10px;
            }

            .details-item-text-block h4{
                margin-top: 10px;
                font-size: 30px;
                color: ${colorPrimaryDark};
                font-weight: 300;
                
            }

        `;


        const grid1Css = `
            .grid-section{
                position: relative;
                width: 100%;
                height: 480px;
                background-color: ${colorPrimaryContrast};
                padding-top: 80px;
                padding-bottom:80px;
            }

            .grid-section > .mdl-grid{
                max-width: 1044px;
            }

            .grid-item{
                
               
            }

            .grid-text-item{

                background-color: #fff;
            }


        `;

        const grid2Css = `
            .grid-section{
                position: relative;
                width: 100%;
                height: 480px;                
                background-color: ${colorPrimaryContrast};
                padding-top: 80px;
                padding-bottom:80px;

            }

            .grid-section > .mdl-grid{
                max-width: 1044px;
            }


        `;


        const grid3Css = `
            .grid-section{
                position: relative;
                width: 100%;
                height: 480px;
                background-color: ${colorPrimaryContrast};
                padding-top: 80px;
                padding-bottom:80px;
            }

            .grid-section > .mdl-grid{
                max-width: 1044px;
            }

            .grid-item{
                max-height:200px;
            }


            .grid-item-long{
                height:100%;
            }
        `;








        const contact1Css = `
            .contact-section{
                position: relative;
                width: 100%;
                height: 500px;
                padding-top: 80px;
                padding-bottom:80px;
            }

            .contact-section> .mdl-grid{
                max-width: 1044px;
            }

            .contact-section form{
                max-width: 550px;
                margin: auto;
            }

            .contact-section form .mdl-textfield{
                width: 100%;
            }


            .contact-form-details-container{
                padding: 0px;
                background-color: ${colorPrimaryContrast};
            }

            .contact-form-details-container .contact-form{
                
                padding: 30px;
                background-color: #fff;
            }

            .contact-form h4{
                color: rgba(0,0,0, .54);
            }

            .contact-form-details-container .contact-details{
                padding: 30px;
                
            }

            .contact-details{                 
                color: ${colorPrimary};

            }

            .contact-details p{
                color: ${colorPrimary};
                opacity: .8;
            }

            
        `;

        const contact2Css = `
            .contact-section{
                position: relative;
                width: 100%;
                height: 500px;
                padding-top: 80px;
                padding-bottom:80px;
                background-color: ${colorPrimaryContrast};
            }

            .contact-section> .mdl-grid{
                max-width: 1044px;
            }

            .contact-section .contact-form {
                padding-top: 10px;
            }


            .contact-section .contact-form .contact-form-container{
                width: 85%;
                margin: auto;
            }

            .contact-section form .mdl-textfield{
                width: 100%;
            }

            .contact-section .contact-form h4{
                font-size: 19px;
                color: rgba(0,0,0, .45);
                margin-top: 10px;
                margin-bottom: 35px;
            }


            .contact-section .contact-form button{
                margin: 20px;
                float: right;
            }


            .contact-section .contact-details{
                padding-top: 10px;
                color: rgba(0,0,0, .6);
            }


            .contact-details .mdl-cell--10-col{
                margin: auto;
            }


            .map-container{
                box-sizing: border-box;
                min-height:270px;
                margin: auto;
                background-image: url(/images/map.png);
                background-size: 100%;
                background-repeat: no-repeat;
                background-color: ${colorPrimaryContrast};
            }


        `;




        const contact3Css = `
            .contact-section{
                position: relative;
                width: 100%;
                height: 500px;
                padding-top: 80px;
                padding-bottom:80px;
            }

            .contact-section> .mdl-grid{
                max-width: 1044px;
            }

            .contact-title{
                text-align:center;
                color: rgba(0,0,0, .6);
            }

            .contact-info{
                font-size:13px;
                margin: 3px;
                font-weight: 400;
            }

            .contact-section form{
                max-width: 550px;
                margin: auto;
            }

            .contact-section form .mdl-textfield{
                width: 100%;
            }

            .map-container{
                box-sizing: border-box;
                min-height:250px;
                margin: auto;
                background-image: url(/images/map.png);
                background-size: 100%;
                background-repeat: no-repeat;
                background-color: ${colorPrimaryContrast};
            }


        `;


        const mediQs = `

            @media (max-width: 900px) {

                .hero-section{
                    height: auto;
                    padding-bottom: 30px;
                }


                .features-section{
                    height: auto;
                }


                .details-section{
                    height: auto
                }


                .grid-section{
                    height: auto;
                }

                .contact-section{
                    height : auto;
                }
            }



        `;


        const HeroCss = {
            "0": hero1Css, 
            "1": hero2Css, 
            "2": hero3Css
        }



        const featuresCss = {
            "0": features1Css,
            "1": features2Css,
            "2": features3Css
        }


        const detailsCss = {
            "0" : details1Css,
            "1" : details2Css,
            "2" : details3Css,
        }

        const gridCss = {
            "0" : grid1Css,
            "1" : grid2Css,
            "2" : grid3Css
        }


        const contactCss = {
            "0": contact1Css,
            "1": contact2Css,
            "2": contact3Css
        }




        return(
            <style>
                {commonCss}
                {HeroCss[this.props.components.hero]}
                {featuresCss[this.props.components.features]}
                {detailsCss[this.props.components.details]}
                {gridCss[this.props.components.grid]}
                {contactCss[this.props.components.contact]}
                {mediQs}
            </style>
            )
    }

}

export default MdlSinglePageStyles;
