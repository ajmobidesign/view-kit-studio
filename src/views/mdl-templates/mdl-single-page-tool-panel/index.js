import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';
import ToolPanel from '../../app-components/tool-panel';
import ControlPanel from '../../app-components/control-panel';



class MdlSinglePageToolPanel extends Component{


	render(){
		return(
			<ToolPanel>
					<ControlPanel title="Template Components" ></ControlPanel>
					<ControlPanel title="Material Design" ></ControlPanel>
					<ControlPanel title="Share Tools" ></ControlPanel>
			</ToolPanel>
		)
	}

}

export default MdlSinglePageToolPanel;