import React, { Component } from 'react';



class MdlStyleGuideCss extends Component{


	render(){

		let primaryRgb = this.props.style.primaryRgb;
		let colorPrimary = `rgb(${primaryRgb})`;


		let styleGuideCss = `

            div.mdl-layout.is-small-screen .mdl-navigation{
                display: none;
            }

            .dropdown-menu-container{
                position: relative;
                display: none;
                margin-right: 50px;
            }

            .mdl-menu__item a{
                text-decoration: none;
            }

            div.mdl-layout.is-small-screen .mdl-layout__header--waterfall{
                overflow:visible;
            }

            div.mdl-layout.is-small-screen .dropdown-menu-container{
                display: inline-block;
            }

            div.mdl-layout.is-small-screen .dropdown-menu-container .mdl-menu__item > a{
                color: ${colorPrimary};
            }

            .main-style-content{
            	max-width: 1024px;
            	margin: 10px auto;
            	overflow:hidden;
            }

            .elements-title-container, .elements-container{
            	margin-top: 20px;
            }

            .elements-title-container p{
            	font-size: 17px;
            }

            h4.mdl-typography--display-1{
            	margin-top: 160px;
            	margin-bottom: 50px;
            	color: rgba(0,0,0, .7)
            }

            h4.mdl-typography--display-1:first-of-type.button-title{
            	margin-top: 70px;
            }

            .inline {
			    display: inline-block;
			    vertical-align: top;
			    margin: 8px;
			    text-align: center;
			}

			.elements-container .inline p{
				margin-top: 8px;
			}

			.nav-display-container{
                position:relative;
                height: 200px;
            }


		`;

		let reactCss = `

			.nav-display-container .mdl-layout{
				overflow-y: auto;
				overflow-x: hidden;
			}

            @media screen and (max-width: 1024px){
               .nav-display-container  .mdl-layout--fixed-header>.mdl-layout__header {
                    display: -webkit-flex;
                    display: -ms-flexbox;
                    display: flex;
                }
            }    

		`;




		return(
			<style>
				{styleGuideCss}
				{reactCss}
			</style>
		)
	}

}


export default MdlStyleGuideCss