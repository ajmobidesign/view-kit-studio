import React, { Component } from 'react';
import classNames from 'classnames';
import { connect } from 'react-redux';
import MdlBasicMenu from '../mdl-basic-menu';



class MdlBasicLayout extends Component{

	render(){

		console.log('basic layout ')

		return(
			<section className={classNames('mdl-demo', 'mdl-base')} >
				<MdlBasicMenu docId={this.props.docId}></MdlBasicMenu>
			</section>
		)
	}
}

export default MdlBasicLayout;