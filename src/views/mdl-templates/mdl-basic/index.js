import React, { Component } from 'react';
import classNames from 'classnames';
import { connect } from 'react-redux';
import MdlToolbar from '../mdl-toolbar';
//import PhotoNav from '../photo-nav';


class MdlBasic extends Component{

	render(){
		return(
			<section className={classNames('mdl-demo', 'mdl-base')} >
				<MdlToolbar></MdlToolbar>
			</section>
		)
	}
}

export default MdlBasic;