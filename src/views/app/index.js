import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { authActions, getAuth } from 'src/core/auth';
import { profileActions } from 'src/core/profile';
import { paths } from '../routes';
import Header from '../app-components/header';
//import ReactCSSTransitionGroup from 'react-addons-css-transition-group';



export class App extends Component {
  static contextTypes = {
    router: React.PropTypes.object.isRequired
  };

  static propTypes = {
    //auth: PropTypes.object.isRequired,
    children: PropTypes.object.isRequired
    //,
    //signOut: PropTypes.func.isRequired
  };

  constructor(props, context){
      super(props, context);

      this.state = {headerClosed: false}

      this.toggleHeader = :: this.toggleHeader
      
  }

  componentWillReceiveProps(nextProps) {

    const { router } = this.context;
    const { auth } = this.props;

    if (auth.authenticated && !nextProps.auth.authenticated) {
      router.replace(paths.SELECT_TEMPLATE);
    }
  }


  toggleHeader(state){
    this.setState({headerClosed: state});
  }



  render() {

     
    let mainStyle = (this.state.headerClosed) ? {marginTop: '0px'} : null;


    return (
      <div >
        <Header loc={this.props.location} toggle={this.toggleHeader} toggleStatus={this.state.headerClosed}/>
          <main className="main" style={mainStyle}>
            {this.props.children}
          </main>
      </div>
    );
  }
}


//=====================================
//  CONNECT
//-------------------------------------
/*
const mapStateToProps = createSelector(
  getAuth,
  auth => ({auth})
);

export default connect(
  mapStateToProps,
  authActions
)(App);
*/

const mapStateToProps = (state)=>{
  return state;
}

export default connect(mapStateToProps, null)(App);

