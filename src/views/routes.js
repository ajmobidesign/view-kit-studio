import { isAuthenticated } from 'src/core/auth';
import App from './app';
import ProfilePage from './app-pages/profile-page';
import TemplatesPage from './app-pages/templates-page';
import TemplatePage from './app-pages/template-page';
import HomePage from './app-pages/home-page';
import AboutPage from './app-pages/about-page';
import ViewPage from './app-pages/view-page';
import SharePage from './app-pages/share-page';
import HowtoPage from './app-pages/howto-page';
import PaymentCanceled from './app-pages/payment-canceled';
import PaymentCompleted from './app-pages/payment-completed';
import NotfoundPage from './app-pages/notfound-page';
import PrivacyPage from './app-pages/privacy-page';
import DetailsPage from './app-pages/details-page';
import FaqPage from './app-pages/faq-page';
import WhymdlPage from './app-pages/whymdl-page';
import LicensePage from './app-pages/license-page';

//import BasicDocsPage from './app-pages/basic-docs-page';



//import TestPage from './app-pages/test-page';




export const paths = {
  ROOT: '/',
  SELECT_TEMPLATE: '/select-template', 
  TEMPLATES: '/templates',
  PROFILE: '/profile',
  TEMPLATE: '/templates/:id', 
  SHARED_TEMPLATE: '/shared-templates/:sharedId',
  DETAILS: '/details/:id',
  ABOUT: '/about',
  FAQ: '/FAQ',
  WHYMDL: '/material-design',
  LICENSE: '/license',
  HOW_TO: '/how-it-works',
  TRY_A_KIT: '/try-a-kit/:name', 
  CANCELED: '/payment-canceled(/:id)', 
  COMPLETED: '/payment(/:id)(/:tplId)',
  PRIVACY: '/privacy-policy', 
  PRIVATE_SHARE:'/shared/:privateId',
  PUBLIC_SHARE:'/public/:publicId',

  TEST:'/test',

  PAGE_NOTFOUND: '*'

};


const requireAuth = getState => {
  return (nextState, replace) => {
    if (!isAuthenticated(getState())) {
      replace(paths.SELECT_TEMPLATE);
    }
  };
};

const requireUnauth = getState => {
  return (nextState, replace) => {
    if (isAuthenticated(getState())) {
      replace(paths.SELECT_TEMPLATE);
    }
  };
};




export const getRoutes = getState => {
  return {
    path: paths.ROOT,
    component: App,
    childRoutes: [
      {
        indexRoute: {
          component: HomePage
          //onEnter: requireAuth(getState)
        }
      },
      {
        path: paths.TEMPLATE,
        component: ViewPage
        , 
        onEnter: requireAuth(getState)
      },
      {
        path: paths.SHARED_TEMPLATE,
        component: ViewPage
        , 
        onEnter: requireAuth(getState)
      },
      {
        path: paths.TRY_A_KIT,
        component: ViewPage
        , 
        onEnter: requireUnauth(getState)
      },
      {
        path: paths.PRIVATE_SHARE,
        component: SharePage
      },
      {
        path: paths.PUBLIC_SHARE,
        component: SharePage
      },
      {
        path: paths.TEMPLATES,
        component: TemplatesPage
        , 
        onEnter: requireAuth(getState)
      },
      {
        path: paths.PROFILE,
        component: ProfilePage
        , 
        onEnter: requireAuth(getState)
      },
      {
        path: paths.SELECT_TEMPLATE,
        component: TemplatePage
      },
      {
        path: paths.DETAILS,
        component: DetailsPage
        ,
        onEnter: requireAuth(getState)
      },
      {
        path: paths.HOW_TO,
        component: HowtoPage
      },
      {
        path: paths.FAQ,
        component: FaqPage
      }, 
      {
        path: paths.WHYMDL,
        component: WhymdlPage
      }, 
      {
        path: paths.CANCELED,
        component: PaymentCanceled
        ,
        onEnter: requireAuth(getState)
      }, 
      {
        path: paths.COMPLETED,
        component: PaymentCompleted
        ,
        onEnter: requireAuth(getState)
      },
      {
        path: paths.PRIVACY,
        component: PrivacyPage
      },
      {
        path: paths.LICENSE,
        component: LicensePage
      },
      /*
      {
        path: paths.TEST, 
        component: TestPage
      },*/

      {
        path: paths.PAGE_NOTFOUND,
        component: NotfoundPage
      }

    ]
  };
};
