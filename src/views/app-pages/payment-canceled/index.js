import React, { Component, PropTypes } from 'react';
import { Router } from 'react-router';
import { connect } from 'react-redux';
import { transactionActions } from 'src/core/transaction';
import moment from 'moment';
import { Grid, Cell} from 'react-mdl';
import { Db } from 'src/core/firebase';


export class PaymentCanceled extends Component{

	static contextTypes = {
	    router: React.PropTypes.object.isRequired
	  };

	constructor(props, context){
		super(props, context);

		this.state = {redirectTimer: null, isNotInList: false};
	}


	componentWillMount(){

		const { query } = this.props.location;
		const { id } = this.props.params;
		const redir = this.context.router;
		let path = `transactions/${id}`;

		let prps = this.props;
		let me = this;

		Db.checkData(path)
			.then((t)=>{

			//console.log(t)
			
			if(t && t.status=='Pending'){

				prps.transCanceled(id);
				me.setState({isNotInList: true});

				const redirectFn = window.setTimeout(function(){redir.push('/select-template')}, 5000);

				me.setState({redirectTimer: moment().add(5, 's')});
				me.setState ({timeoutConst : redirectFn});


			}
			else{
				redir.push('/notfound')
			}
		})
		
	}

/*
	componentDidMount(){

		console.log('component did mount')

		if(this.state.isNotInList){

			console.log('fired')
			const redir = this.context.router;
			const redirectFn = window.setTimeout(function(){redir.push('/select-template')}, 5000);

			this.setState({redirectTimer: moment().add(5, 's')});
			this.setState ({timeoutConst : redirectFn});
		}
	}
*/

	componentWillUnMount(){
		if(this.state.isNotInList){
			window.clearTimeout(this.state.timeoutConst);
		}
	}
	


	render(){
		return(
			<section className="app-page" >
				<Grid>
				    <div className="app-page-inner">
				    	<Cell col={10} offset={1}>
							<h5 > Payment Canceled </h5>
							
						</Cell>
					</div>
				</Grid>
			</section>
		)
	}

}

const mapStateToProps= (state)=>{
	return state;
}

const mapDispatchToProps = Object.assign({}, transactionActions);

export default connect(mapStateToProps, mapDispatchToProps)(PaymentCanceled);