import React, { Component } from 'react';
import { connect } from 'react-redux';
import { templateActions } from 'src/core/template';
import _ from 'lodash';
import MdlBlogPage from '../../mdl-pages/mdl-blog-page';
import MdlBasicPage from '../../mdl-pages/mdl-basic-page';
import MdlBasicHtml5Page from '../../mdl-pages/mdl-basic-html5-page';
import MdlSingleArticlePage from '../../mdl-pages/mdl-single-article-page';
import MdlSinglePagesitePage from '../../mdl-pages/mdl-single-pagesite-page';
import AppLoader from '../../app-components/app-loader';


export class SharePage extends Component{


	componentWillMount(){
		
		let params = this.props.params;
		
		if(params.publicId){
			const { publicId } = params;

			let path = `publicShare/${publicId}`;
			this.props.loadShareView(path);
		}

		if(params.privateId){

			const { privateId } = params;
			const { auth } = this.props;

			let path = `privateShare/${privateId}`;

			if(auth.authenticated){
				this.props.loadShareView(path);
			}
		}
	}




	render(){

		let params  = this.props.params;
		let loggedIn = this.props.auth.authenticated  ;
	
		let tplId = this.props.template.tplId;

		let isAllowedToView = ((params.privateId && loggedIn) || params.publicId)
		

		let docId = this.props.params.id || '';
		let style = this.props.template.styleConfig;
		let componentOb = this.props.template.componentConfig;
		let update = this.props.updateStyleConfig;//should change on all the components
		let updateComponent = this.props.updateComponentConfig;
		let publicUrl = this.props.template.public;
		let privateUrl = this.props.template.private;
		let shareLink = this.props.shareTemplate;
		let removeLink = this.props.removeShare;


		const renderTemplate = function(){

	
				switch(tplId){

					case 'mdl_single_page':
						return <MdlSinglePagesitePage style={style} docId={docId} updateStyle={update} updateComponent={updateComponent} components={componentOb} privateUrl={privateUrl} publicUrl={publicUrl} shareLink={shareLink} removeLink={removeLink} viewOnly={true}></MdlSinglePagesitePage>;

					case 'mdl_basic_html5':
						
						return <MdlBasicHtml5Page style={style} docId={docId} update={update} privateUrl={privateUrl} publicUrl={publicUrl} shareLink={shareLink} removeLink={removeLink} viewOnly={true}></MdlBasicHtml5Page>;

					default:
						return (loggedIn) ? <AppLoader></AppLoader> : (<section className="app-page" ><div className="app-page-inner"><h2>404 Error </h2><h4>Page Not Found</h4></div></section>) ;	
				}
			}
		
		return(
			<div>
				{ (isAllowedToView) ?  renderTemplate() : (<div> You must be logged in to view this page </div>) }
			</div>
		)
	}

}

const mapStateToProps = state =>{
	
	return {template: state.template, auth: state.auth};
};

const mapDispatchToProps = Object.assign({}, templateActions);

export default connect(mapStateToProps, mapDispatchToProps)(SharePage);