import React, { Component } from 'react';
import { Router, Link } from 'react-router';
import { connect } from 'react-redux';
import { templateActions } from 'src/core/template';
import { collaboratorsActions } from 'src/core/collaborators';
import { profileActions } from 'src/core/profile';
import _ from 'lodash';
import MdlBlogPage from '../../mdl-pages/mdl-blog-page';
import MdlBasicPage from '../../mdl-pages/mdl-basic-page';
import MdlBasicHtml5Page from '../../mdl-pages/mdl-basic-html5-page';
import MdlSingleArticlePage from '../../mdl-pages/mdl-single-article-page';
import MdlSinglePagesitePage from '../../mdl-pages/mdl-single-pagesite-page';
import MdlStyleGuidePage from '../../mdl-pages/mdl-style-guide-page';
import AppLoader from '../../app-components/app-loader';
import AppNotification from '../../app-components/app-notification';



export class ViewPage extends Component{


	static contextTypes = {
	    router: React.PropTypes.object.isRequired
	  };


	constructor(props, context){
      super(props, context); 


      this.state = {showNotification : false} 

      this.renderNotification = ::this.renderNotification;
      this.loginButton = ::this.loginButton;


    }  


	componentWillMount(){
		
		let params = this.props.params;
		let tpl = this.props.template;

		let { name } = params;
		let str = _.replace(name, RegExp("-","g"), '_');

		this.props.getProfileData()


		if(params.sharedId){

			let docId = this.props.params.sharedId;

			this.props.loadCollabTemplate(docId);
		}


		if(params.id){
			let docId = this.props.params.id;
			//what happens when the doc does not exist anymore??

			//if this is a copy -use a different load
			this.props.loadTemplate(docId);
		}

		if(params.name && str !== tpl.tplId){
			
			this.props.createViewOnlyTemplate(str);
			//this throws an error no clue why
		}
	}




	componentWillUnmount(){
		//need to remove the shared view
		
		let docId = this.props.template.key;
		
		let isShared = (this.props.collaborators[docId]);

		this.props.unloadTemplate(docId);

		if(isShared){
			this.props.removeCollabTemplate(docId);
		}
	}


	componentWillReceiveProps(nextProps){

		const redir = this.context.router;

		if(this.props.auth.authenticated !== nextProps.auth.authenticated && nextProps.auth.authenticated){
			redir.push('/select-template')
		}

		let showNotification = nextProps.template.demoNotification;

		console.log('show notification', showNotification)

		if(showNotification){
			this.setState({showNotification : true })
		}

	}

	loginButton(){

		document.querySelector('#google-login').click()
	}

	

	renderNotification(){


		let notice = `This is a demo. Changes you make will not be saved. Sign in to save changes`;

		let show = this.state.showNotification;

		let pos = { 
			top: '40%'
		}

		let label = 'Sign In with Google'

		return(
			<AppNotification notice={notice} pos={pos} show={show} actionLabel={label} action={this.loginButton}></AppNotification>
		)
	}


	render(){

		function objectToArray(collaborators){

			if(collaborators){
				return Object.keys(collaborators).map((k)=>{

						let cName = collaborators[k]; 
						let cid = k

					return 	{name: cName, id: cid}
							
					})
				}
				else{
					return[];
				}
			}

		let { name } = this.props.params;
		let str = _.replace(name, RegExp("-","g"), '_');	

		let tplIdAlt = (str)? str : '';
		
		let tplId = this.props.template.tplId ;

		
		let docId = this.props.params.id || this.props.params.sharedId || '';
		let style = this.props.template.styleConfig;
		let componentOb = this.props.template.componentConfig;
		let update = this.props.updateStyleConfig;//should change on all the components
		let updateComponent = this.props.updateComponentConfig;
		let publicUrl = this.props.template.public;
		let privateUrl = this.props.template.private;
		let shareLink = this.props.shareTemplate;
		let removeLink = this.props.removeShare;
		let stickys = this.props.template.stickys;
		let addSticky = this.props.addSticky;
		let removeSticky = this.props.removeSticky;
		let updateSticky = this.props.updateSticky;
		let addTemplateCollaborator = this.props.addTemplateCollaborator;
		let removeTemplateCollaborator = this.props.removeTemplateCollaborator;
		let searchUserList = this.props.searchUserList;
		let queryResults = this.props.collaborators.query;
		let clearUserList = this.props.clearUserList;
		let collabList = (this.props.template.collabList) ? this.props.template.collabList : objectToArray(this.props.template.collaborators);
		let owner = this.props.template.owner;
		let displayName = this.props.auth.displayName;
		let loggedIn = this.props.auth.authenticated;
		let savingTemplate = this.props.savingTemplate;


		





		const renderTemplate = function(){

			//console.log('view page', tplId)
	
				switch(tplId){

					case 'mdl_single_page':
						return <MdlSinglePagesitePage style={style} docId={docId} updateStyle={update} updateComponent={updateComponent} components={componentOb} privateUrl={privateUrl} publicUrl={publicUrl} shareLink={shareLink} removeLink={removeLink} stickys={stickys} addSticky={addSticky}  removeSticky={removeSticky} updateSticky={updateSticky} addTemplateCollaborator={addTemplateCollaborator} removeTemplateCollaborator={removeTemplateCollaborator} searchUserList={searchUserList} queryResults={queryResults} clearUserList={clearUserList} collabList={collabList} displayName={displayName} owner={owner} loggedIn={loggedIn} savingTemplate={savingTemplate} ></MdlSinglePagesitePage> 

					case 'mdl_style_guide':
						return <MdlStyleGuidePage style={style} docId={docId} updateStyle={update}  privateUrl={privateUrl} publicUrl={publicUrl} shareLink={shareLink} removeLink={removeLink} stickys={stickys} addSticky={addSticky}  removeSticky={removeSticky} updateSticky={updateSticky} addTemplateCollaborator={addTemplateCollaborator} removeTemplateCollaborator={removeTemplateCollaborator} searchUserList={searchUserList} queryResults={queryResults} clearUserList={clearUserList} collabList={collabList} displayName={displayName} owner={owner} loggedIn={loggedIn} savingTemplate={savingTemplate} ></MdlStyleGuidePage> 	

					case 'mdl_basic_html5':
						
						return <MdlBasicHtml5Page style={style} docId={docId} update={update} loggedIn={loggedIn} savingTemplate={savingTemplate}></MdlBasicHtml5Page> 
	
					default:
						return (name) ?  (<section className="app-page" ><div className="app-page-inner"><h2>404 Error </h2><h4>Page Not Found</h4></div></section>) : <AppLoader></AppLoader>;	
				}
			}
		
		return(
			<div >
				{ (!loggedIn) ?  this.renderNotification() : null }	
				{ renderTemplate()}
				
				

			</div>
		)
	}

}

const mapStateToProps = state =>{
	
	return {template: state.template, collaborators: state.collaborators, auth: state.auth};
};

const mapDispatchToProps = Object.assign({}, templateActions, collaboratorsActions, profileActions);

export default connect(mapStateToProps, mapDispatchToProps)(ViewPage);