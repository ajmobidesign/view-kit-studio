import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import AppFooter from '../../app-components/app-footer';
import { Link } from 'react-router';
import { Icon, Grid } from 'react-mdl';


export class HomePage extends Component{



	addAnalytics(){
		let analyticOb = {'event': 'learnHowItWorks'};
		dataLayer.push(analyticOb);
	}

	componentDidMount(){

		window.scrollTo(0,0);
	}



	render(){


		const styleOb = {
			background: `url(images/logo.png) no-repeat bottom center,linear-gradient(rgba(181,2,63, 0), rgba(255,64,129, 1))`
		}




		return(
			<section className="home-page" >
				<div className="home-hero">
					<div className="hero-inner" >
					
						
						
						<img src="images/logo_vk_white_mid.png" width="100px" />
						
						<div className="ribbon-container">
							<div className="github-ribbon"><p>Preview</p></div>
							<h2 className="typo-styles typo-styles__demo mdl-typography--display-4" style={{fontSize: '55px', marginBottom: '5px'}}>View Kit Studio </h2>
						</div>

						<div className="hero-callout">
							<h5 className="headline">LAYOUTS IN CODE </h5>
							<div className="hero-block">
								<img  src="images/template-combo.png" />
								<p> Pick A Hand Crafted Layout Template</p>
							</div>
							<div className="hero-block">
								<img  src="images/template-collab.png" />
								<p> Collaborate and Configure</p>
							</div>
							<div className="hero-block">
								<img  src="images/template-home-download.png" />
								<p> Download Customized Layout Code</p>
							</div>
						</div>

						
						<Link className="hero-button" to="/how-it-works" onClick={this.addAnalytics} style={{marginTop: '55px', display: 'block'}}>
							Learn How it Works
						</Link>
						

					</div>
				</div>

				<AppFooter></AppFooter>
			</section>
		);
	}

};

export default connect(null, null)(HomePage);