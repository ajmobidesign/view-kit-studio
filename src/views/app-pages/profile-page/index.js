import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { templateActions } from 'src/core/template';
import { profileActions } from 'src/core/profile';
import { templateMap } from 'src/core/defaults';

import { Link } from 'react-router';
import { DataTable, Button, TableHeader, FABButton, Icon, Grid, Cell, Dialog, DialogTitle, DialogContent, DialogActions, Tabs, Tab} from 'react-mdl';
import moment from 'moment';
import NotificationMessage from '../../app-components/notification-message';
import AppFooter from '../../app-components/app-footer';



export class ProfilePage extends Component{

	constructor(props, context){
		super(props, context);

		this.state = {currentTpl: null, activeTab: 0};
		this.getData = ::this.getData;
		this.renderTable = ::this.renderTable;
	}

	componentWillMount(){
		this.props.getProfileData();
		this.props.getGlobalNotifications();
	}

	componentDidMount(){

		window.scrollTo(0,0);
	}

	getData(){
		//maybe this shoud be different
		let purchaseList = this.props.profile.tplList;


		let data = purchaseList.map((t, i)=>{
	
			let tpl = templateMap[t];
			
			return {id: i, name: tpl.name , price: tpl.price, time: moment(tpl.time).format("MM-DD-YYYY")};
		})
		return data
	}


	renderTable(){
		let rowData = this.getData()
		
		return(
				<Cell col={10} style={{margin: 'auto'}}>
					<DataTable
					    shadow={0}
					    rowKeyColumn="id"
					    rows={rowData}>
					   <TableHeader name="name">View kit Name</TableHeader>
					   
					   <TableHeader name="price">Price</TableHeader>
					   <TableHeader name="time">Purchase Date</TableHeader>	   
					</DataTable>
				</Cell>	
			)
	}

	renderNotifications(){
		const notificationList = this.props.profile.notifications;

		function sortByLatest(a, b) {

				if(moment(a.date).isBefore(b.date)) return 1;
				if(moment(a.date).isAfter(b.date)) return -1;
				return 0;
			}

		return notificationList.sort(sortByLatest).map((d, i)=>{
					return (<NotificationMessage key={i} msg={d} remove={this.props.flagAsDeleted} ></NotificationMessage>)
				});
	}


	renderTab(){

		const datalist = this.props.profile.tplList;
		const notificationList = this.props.profile.notifications;

		switch(this.state.activeTab){
			case 0:
				return (notificationList.length>0) ? <div> {this.renderNotifications()} </div>: <div style={{textAlign: 'center', padding: '20px'}}><p>You have no new notifications</p></div>;
			case 1:
				return (datalist.length>0) ? this.renderTable(): <div style={{textAlign: 'center', padding: '20px'}}><p>You have no Purchases</p></div>;
			default:
				return <div></div>		
		}
	}



	render(){

		const unreadNotification = this.props.profile.notifications.length >0

		const alertStyle= {
			color: (unreadNotification) ? 'red' : 'rgba(0,0,0, .3)', 
			fontSize: '17px'

		}

	
		return(
			<section className="app-page" >				
				<Grid>
				    <div className="app-page-inner">
				    	<Cell col={12}  className="profile-page">
				    		<Tabs className="profile-tabs" activeTab={this.state.activeTab} onChange={(tabId) => this.setState({ activeTab: tabId })} >
			                    <Tab>Notifications <Icon style={alertStyle} name="notifications" /></Tab>
			                    <Tab>Purchases</Tab>
			                    
			                </Tabs>
			                <section style={{width: '100%'}}>
			                    <div className="profile-content">{this.renderTab()}</div>
			                </section>
				    		
				    	</Cell>
				    </div>
				</Grid>
			    <AppFooter ></AppFooter>
			</section>
		);
	}

};

const mapStateToProps = (state)=>{
	return state;
}

const mapDispatchToProps = Object.assign({}, templateActions, profileActions)

export default connect(mapStateToProps, mapDispatchToProps )(ProfilePage);