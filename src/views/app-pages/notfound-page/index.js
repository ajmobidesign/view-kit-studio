import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';


export class NotfoundPage extends Component{

	render(){
		return(
			<section className="app-page" >
				<div className="app-page-inner">
				<h2>404 Error </h2>
				<h4>Page Not Found</h4>
				</div>
			</section>
		);
	}

};

export default connect(null, null)(NotfoundPage);