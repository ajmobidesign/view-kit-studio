import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import AppFooter from '../../app-components/app-footer';
import { Grid, Cell } from 'react-mdl';



export class PrivacyPage extends Component{

	componentDidMount(){

		window.scrollTo(0,0);
	}


	render(){
		return(
			<section className="app-page" >
				<Grid>
					<div className="app-page-inner">
					
				    	<Cell col={10} offset={1} >

				    		<p>Listed below are the rules and restrictions that govern your use of our website(s), products, services and applications (the “Services”). By using these Services and when purchasing a product or service through these Services, you acknowledge that you have read and agreed to the following terms of use and our Refund Policy as well as the specific terms of use pertaining to the deal that you are purchasing. 

								We reserve the right to amend these terms from time to time. You must agree to and accept all of the Terms of Use, or you don’t have the right to use the Services.</p>

							<h4>SERVICES TERMS OF USE</h4>

							<h5>Overview</h5>

							<p>The right to use the Services is personal to you and is not transferable to any other person or entity. You may not transfer your account to anyone else without our prior written permission. You are responsible for any activity associated with your account.</p>

							<p>Your use of the Services is subject to the following additional restrictions:</p>

							<h5>Liability</h5>

							<p>We do not take any responsibility and we are not liable for any damage caused through use of the Services, be it indirect, special, incidental or consequential damages (including but not limited to damages for loss of business, loss of profits, interruption or the like). If you have any questions regarding the terms of use outlined here, please do not hesitate to contact us.</p>

							<h5>Indemnification</h5>

							<p>You agree to indemnify, defend and hold harmless the Services and its members, managers, officers, employees, agents and the assigns of same, from and against any and all loss, costs, expenses (including reasonable attorneys’ fees and expenses), claims, damages and liabilities related to or associated with your use of the Web Site.</p>

							<h5>Children’s Issues</h5>

							<p>We do not knowingly collect or solicit personally identifiable information from children under 18; if you are a child under 18, please do not attempt to register for the Services or send any personal information about yourself to us. If we learn we have collected personal information from a child under 18, we will delete that information as quickly as possible. If you believe that a child under 18 may have provided us personal information, please contact us at support@viewkitstudio.com.</p>


							<h5>Trademarks</h5>
							<p>View Kit Studio is a trademark or service mark.</p>

							<h5>No Representations or Warranties; Limitations on Liability</h5>

							<p>viewkitstudio.com does not make any express or implied warranties, representations or endorsements whatsoever (including, without limitation, the implied warranties of merchantability or fitness for a particular purpose) with regard to the web site, the materials, any products, information or service provided through the Services, or any sites listed therein, and the Services will not be liable for any cost or damage arising either directly or indirectly from any such transaction. The Services does not warrant that it will be uninterrupted or error-free or that defects in it will be corrected. the web site is provided on an “as is, as available” basis. In no event will the Services or its members, managers, officers, employees, agents and the assigns of same be liable for (1) any incidental, consequential or indirect damages (including, but not limited to, damages for loss of profits, business interruption, loss of programs or information, and the like) arising out of the use of or inability to use the Services, the materials or any information, or transactions provided on the web site or downloaded from the web site, even if the web site or its authorized representatives have been advised of the possibility of such damages, or (2) any claim attributable to errors, omissions or other inaccuracies in the web site, the materials and/or listings or information downloaded through the web site. Because some countries, states or provinces, etc do not allow the exclusion or limitation of liability for consequential or incidental damages, the above limitation may not apply to you. In such places, the web site’s liability is limited to the greatest extent permitted by law.</p>

							<h5>Termination </h5>

							<p>We may terminate or suspend access to the Service immediately, without prior notice or liability, for any reason whatsoever, including without limitation if you breach the Terms. All provisions of the Terms which by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability.</p>

							<h5>Payments</h5>

							<p>You represent and warrant that if you are purchasing something from us that (i) any credit information you supply is true and complete, (ii) charges incurred by you will be honored by your credit card company, and (iii) you will pay the charges incurred by you at the posted prices, including any applicable taxes.</p>


							<h5>Refund</h5>
							<p>View Kit Studio does not refund purchases on the basis of buyer remorse or unmet expectations.</p>

							<h5>Information protection</h5>
							<p>We use the highest security standard available to protect your personally identifiable information while it is in transit to us. All data stored on our servers is protected by a secure “firewall” so that no unauthorized use or activity can take place. Although we will make every effort to safeguard your personal information from loss, misuse or alteration by third parties, you should be aware that there is always some risk that thieves may find a way to thwart our security system or that transmissions over the Internet may in some highly unusual cases be intercepted.</p>

							<h5>Contact Information</h5>
							<p>Questions or comments regarding these Terms and Conditions should be sent by e-mail to terms@viewkitstudio.com</p>

							<h5>Privacy Policy</h5>

							<p>Your privacy is very important to us. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. The following outlines our privacy policy.</p>

							<h5>Google User Identity</h5>

							<ul>
								<li>We uses Google Identity Services to manage user identity.</li>
								<li>We request explicit permission from users before requesting their data from Google Identity Services.</li>
								<li>The Information is used solely for the purposes of providing Services and improving user experience.</li>
								<li>You change access permission settings in your account settings.</li>
							</ul>
	



							<h5>Google Analytics Tracking</h5>
							<ul>
								<li>Viewkitstudio.com uses Google Analytics to track and analyze website visits and usage. Google Analytics uses a unique User ID when a user is authenticated and logged in.</li>
								<li>On Behalf of viewkitstudio.com, Google Analytics tracks users flow, location by country, language, browser and operating system, internet service provider and devices.</li>
								<li>Viewkitstudio.com will not upload any data that allows Google to personally identify an individual (such as name, Social Security Number, email address, or any similar data), or data that permanently identifies a particular device (e.g. IP address).</li>
								<li>To opt-out of Analytics for the web, visit the Google Analytics opt-out page and install the add-on for your browser. For more details on installing and uninstalling the add-on, please see the relevant help resources for your specific browser.</li>
							</ul>


						</Cell>
					</div>
				</Grid>
				<AppFooter></AppFooter>
			</section>
		)
	}

}


export default connect(null, null)(PrivacyPage);