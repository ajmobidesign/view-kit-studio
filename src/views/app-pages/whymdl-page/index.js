import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import SlideOut from '../../app-components/slide-out';
//import { faqData } from 'src/core/defaults';
import { Button, Grid, Cell, Textfield, Switch } from 'react-mdl';
import { Link } from 'react-router';




export class WhymdlPage extends Component{

	constructor(props, context){
		super(props, context);

		this.state = {mdlBtn: false, btsBtn: false, mdlIn: false, btsIn: false, switchBtn: false}

		this.showMdlBtnMsg = ::this.showMdlBtnMsg;
		this.showBtsBtnMsg = ::this.showBtsBtnMsg;
		this.showMdlInMsg = ::this.showMdlInMsg;
		this.showBtsInMsg = ::this.showBtsInMsg;
		this.showSwitchBtn = ::this.showSwitchBtn;

	}

	showSwitchBtn(){
		this.setState({switchBtn: true})
	}


	showMdlBtnMsg(){

		this.setState({mdlBtn: true})
	}

	showBtsBtnMsg(){
		this.setState({btsBtn: true})
	}

	showMdlInMsg(){

		this.setState({mdlIn: true})
	}

	showBtsInMsg(){
		this.setState({btsIn: true})
	}



	componentDidMount(){

		window.scrollTo(0,0);
	}


	renderQuestions(){

		let data = [
				{

				'question': 'What is Material Design Lite?', 
				'answer': 'View Kit Studio layout templates are web template comprised of commonly used  structural elements and interactions patterns in modern web development. They offer a faster starting point for your project by minimizing the time and effort spent coding these commonly used patterns, leaving you and your team more time to focus on the truly challenging parts of your project.'

				}, 
				{

				'question': 'How is it different from Bootstrap?', 
				'answer': 'First, sign in with your google account. Then from the Select A Template page click on the CREATE NEW button on the template you would like to use. A new version will be created for you. Give the new version a title and makes changes to its configurations using the side panel. All changes you make will be autosaved.'
				}
		]

		return data.map((d, i)=>{
			return <SlideOut key={i} title={d.question} content={d.answer} link={d.link}></SlideOut>
		})


	}


	render(){

		let titleStyle = {
			color: '#ff4081', 
			fontSize: '22px', 
			marginBottom: '30px'
			
		};

		let title2Style = {
			color: 'rgba(0,0,0, .5)', 
			fontSize: '16px'
		};

		let contentStyle ={
			transition: 'height .28s ease-out',  
			width: '90%',
			color: 'rgba(0,0,0, .7)',
			overflow: 'hidden', 
			height: 'auto'
			//, 
			//textAlign: 'center'
		};

		let pStyle={
			fontSize: '17px'
		};

		let msgStyle={
			marginTop: '8px'

		};







		return(
			<section className="app-page" >
				<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
				<div className="app-page-inner">
					<div className="mdl-grid mdl-cell mdl-cell--12-col" style={{marginTop: '-40px'}}>
						<Cell col={12} >
							<h3 style={{color: 'rgba(0,0,0, .5)', textAlign: 'center'}}>What is the difference between Material Design Lite and Bootstrap ?</h3>

							<h5 style={{textAlign: 'center', color: 'rgba(0,0,0, .8)', fontSize: '16px', fontWeight: '300'}}> Let us compare a couple of elements </h5>
						</Cell>
					</div>

					<div className="mdl-grid mdl-cell mdl-cell--12-col" >
						<Cell col={6} >
								<p style={title2Style}>This is a Material Design Lite Button. Go ahead click it</p>
								<Button raised colored ripple onClick={this.showMdlBtnMsg}>Button</Button>
								{(this.state.mdlBtn ) ? <p style={msgStyle}>Yup! That is a cool ripple effect.</p>: <p></p>}
						</Cell>
						<Cell col={6} >
								<p style={title2Style}>This is a Bootstrap Button. Try it.</p>
								<button type="button" className="btn btn-primary" onClick={this.showBtsBtnMsg}>Button</button>
								{(this.state.btsBtn ) ? <p style={msgStyle}>Nope! No cool effects.</p>: <p></p>}
						</Cell>
					</div>
					<div className="mdl-grid mdl-cell mdl-cell--12-col" >
						<Cell col={6} >
								<style>
									{
										`label.mdl-textfield__label{
											margin-bottom: 0px !important;
											font-weight: 400;
										}

										.mdl-switch__label{
											font-weight: 300;
										}
										`
									}

								</style>
								<p style={title2Style}>This is a Material Design Input Element</p>
								<Textfield
							    onFocus={this.showMdlInMsg}
							    label="Go on Type Something"
							    floatingLabel
							    style={{width: '200px'}}/>


							    {(this.state.mdlIn ) ? <p style={msgStyle}>Another cool effect.</p>: <p></p>}

								
						</Cell>
						<Cell col={6} >
								<p style={title2Style}>This is a Bootstrap Input Element</p>
								<div className="form-group">
								    <input onFocus={this.showBtsInMsg} type="text" className="form-control" placeholder="Go ahead try it" />
								 </div>

								 {(this.state.btsIn ) ? <p style={msgStyle}>No! Nothing to see</p>: <p></p>}
								
						</Cell>
					</div>
					<div className="mdl-grid mdl-cell mdl-cell--12-col" >
						<Cell col={6} >
								<p style={title2Style}>This is a Material Design Lite Switch. Go ahead click it</p>
								<Switch ripple id="switch1"  onChange={this.showSwitchBtn}>Ripple switch</Switch>
								{(this.state.switchBtn ) ? <p style={msgStyle}>Yet another cool ripple effect.</p>: <p></p>}
						</Cell>
						<Cell col={6} >
								<p style={title2Style}>This is a Bootstrap.... Actually Bootstrap does not have one</p>
								
						</Cell>
					</div>
					
					<div className="mdl-grid mdl-cell mdl-cell--12-col" style={{marginTop: '30px'}}>
					<Cell col={12} >
						<span style={{textAlign: 'center'}}>
							<p style={titleStyle}>
								
								More on Material Design Lite at View Kit Studio.
								
							</p>
						</span>
						<div style={contentStyle}>
							<p style={pStyle}>
								<a href="https://getmdl.io/index.html">Material Design Lite</a> is a new css framework from Google. It is a responsive framework with a modern look that incorporates motion design into its animations and transitions.
								<br />
								<br />

								At View Kit Studio we have built a platform that makes it easy for you to create and customize templates, save unlimited versions of templates and invite others to collaborate with you. All for Free! Pay only when you are ready to enable unlimited downloads. 
								<br />
								<br />

								This is an early release Preview version of our platform. <strong>All our templates are free to download during the preview period.</strong> Including our <strong>Material Design Lite Style Guide </strong> with all the material elements.
								<br />
								<br />
								Try it out now!
								
							</p>
						</div>
					</Cell>
					{/*
					<Cell col={12} >
						<p style={titleStyle}>
							What is the difference between Bootstrap and Material Design Lite?
						</p>
						<div style={contentStyle}>
							<p style={pStyle}>
								Like Bootstrap Material Design Lite is an open-source design framework with easy to use components created with css, html and javascript. Unlike Bootstrap, MDL is lighter framework with a set of essential commonly used components. MDL components have a more modern look and incorporates motion design into its animations and transitions. It is quickly gaining in popularity due to bold styles and ease of use.
								
							</p>
						</div>
					</Cell>
					<Cell col={12} >
						<p style={titleStyle}>
							What is View Kit Studio?
						</p>
						<div style={contentStyle}>
							<p style={pStyle}>
								At View Kit Studio we want to make it easy for you to quickly create customized templates for your projects from our hand crafted layouts. You can create unlimited versions of templates and invite others to collaborate with you all for Free! Pay only when you are ready to enable unlimited downloads. 
								<br />

								This is an early release Preview version of our platform. <strong>All our templates are free to download during the preview period.</strong> So don’t wait try it out now!
								
							</p>
							
						</div>
					</Cell>
					*/}
					
					
				</div>	
					

					

					<div className="mdl-grid mdl-cell mdl-cell--12-col" style={{marginTop: '-20px'}}>
						<Cell col={12} >
							<p style={titleStyle}>
								
							</p>

							<div style={{textAlign: 'center', width: '100%' }}>
								<Link className="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" to='/try-a-kit/mdl-style-guide' style={{textTransform: 'capitalize'}}>Check Out All Material Elements with our MDL Style Guide Template</Link>

							</div>

							
						</Cell>
						
					</div>

				</div>
			</section>	
		)
	}

};

export default connect(null, null)(WhymdlPage);