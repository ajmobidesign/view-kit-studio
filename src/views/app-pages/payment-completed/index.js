import React, { Component, PropTypes } from 'react';
import { Router, Link } from 'react-router';
import { connect } from 'react-redux';
import { transactionActions } from 'src/core/transaction';
import { profileActions } from 'src/core/profile';
import { tplMap } from 'src/core/defaults/mdl';
import moment from 'moment';
import { Grid, Cell, Icon, Button } from 'react-mdl';
import { Db } from 'src/core/firebase';


export class PaymentCompleted extends Component{

	static contextTypes = {
	    router: React.PropTypes.object.isRequired
	  };

	constructor(props, context){
		super(props, context);

		this.state = { alreadyPurchased : false, isNotInList: false, purchaseDate: ''};
		this.startNewTemplate = ::this.startNewTemplate;
	}


	componentWillMount(){
		const { query } = this.props.location;
		const { id, tplId } = this.props.params;

		const { paymentId } = query;
		const redir = this.context.router;
		this.setState({tranId: id});

		let path = `transactions/${id}`
	
		this.props.getProfileData();

		let prps = this.props;
		let me = this;

		Db.checkData(path)
			.then((t)=>{
					
				if(t && t.payId === paymentId && t.status=='Pending'){

					prps.transCompleted(id, tplId);
					me.setState({purchaseDate: t.create_time});

					const redirectFn = window.setTimeout(function(){redir.push('/templates')}, 8000);

					me.setState({redirectTimer: moment().add(5, 's')});
					me.setState ({timeoutConst : redirectFn});

				}
				else{
					redir.push('/notfound')
				}
		})
	}


/*
	componentWillReceiveProps(nextProps){

		//console.log('next props', nextProps)
		//console.log(nextProps.location, this.props.location)
		const redir = this.context.router;
		const { id, tplId } = this.props.params;


	}





	componentDidMount(){

		const redir = this.context.router

		console.log(this.state)

		if(this.state.isNotInList){
			const redirectFn = window.setTimeout(function(){redir.push('/select-template')}, 10000)

			this.setState({redirectTimer: moment().add(5, 's')})
			this.setState ({timeoutConst : redirectFn})
		}

		

	}

	*/

	componentWillUnMount(){
		if(this.state.isNotInList){
			window.clearTimeout(this.state.timeoutConst)
		}
		
	}


	startNewTemplate(){
		const { tplId } = this.props.params;
		this.props.createNewTemplate(tplId);
	}



	renderDetails(){

		const { id, tplId } = this.props.params;
		const dt = this.state.purchaseDate
		//let transId = this.state.transId;
		//let trans = this.props.profile.transactions[id];

		//console.log(tarnsId, trans, this.profile)

		const pic = `images/${tplMap[tplId].tplId}.png`;

		console.log(tplMap[tplId].tplId)

		return(
			<div>
				<h4>Payment Completed <Icon style={{color: 'red', fontSize: '35px'}} name="done" /></h4> 
				 <p>{tplMap[tplId].name}</p>

				 <p> <img src={pic} /> ${tplMap[tplId].price}</p>
				 <p>Completed on  {moment(dt).format('MM-DD-YYYY')}</p>
				 <p>Redirecting in a few seconds</p>
				 <p>Get Started with new version of this template<Button onClick={this.startNewTemplate}>Create New</Button></p>
				 <small>This page will redirect in a few seconds ...</small>
			</div>	 
		)
	}



	render(){
		const { id, tplId } = this.props.params;

		return(
			<section className="app-page" >
				<Grid>
					<div className="app-page-inner">    
				    	<Cell col={10} offset={1}>
							{(id)? this.renderDetails(): null}
						</Cell>
					</div>		
				</Grid>
			</section>
		)
	}

}

const mapStateToProps= (state)=>{
	return state;
}

const mapDispatchToProps = Object.assign({}, transactionActions, profileActions);

export default connect(mapStateToProps, mapDispatchToProps)(PaymentCompleted);