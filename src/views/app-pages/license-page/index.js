import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import AppFooter from '../../app-components/app-footer';
import { Grid, Cell } from 'react-mdl';



export class LicensePage extends Component{

	componentDidMount(){

		window.scrollTo(0,0);
	}


	render(){
		return(
			<section className="app-page" >
				<Grid>
					<div className="app-page-inner">
					
				    	<Cell col={10} offset={1} >

				    		<h3>Multi Use License</h3>

				    		

				    		<ul>
				    			<li>The Multi Use License grants you, the purchaser, an ongoing, non-exclusive, worldwide license to make use of the Layout Template (Item) you have selected. </li>
				    			<li>You are licensed to use the Item to create Multiple End Products for yourself or for a client, and the End Products may be Sold.</li>
				    			

				    		</ul>



				    		<h5>Permitted Use</h5>

				    		<ul>
				    			<li>You can create the End Products for a client, and this license is then transferred from you to your client.</li>
				    			<li>You can Sell and make any number of copies of the End Product.</li>
				    			<li>You can modify or manipulate the Item. You can combine the Item with other works and make a derivative work from it. The resulting works are subject to the terms of this license. </li>

				    		</ul>

				    		<h5>Not Permitted</h5>

				    		<ul>
				    			
				    			<li>You can’t re-distribute the Item as stock, in a tool or template, or with source files. You can’t do this with an Item either on its own or bundled with other items, and even if you modify the Item. You can’t re-distribute or make available the Item as-is or with superficial modifications.</li>
				    			<li>You can’t use the Item in any application allowing an end user to customise a digital or physical product to their specific needs, such as an “on demand”, “made to order” or “build it yourself” application. </li>
				    			<li>You must not permit an end user of the End Product to extract the Item and use it separately from the End Product.</li>
				    			

				    		</ul>

				    		<h5>Additional Terms</h5>

				    		<ul>
				    			<li>You can only use the Item for lawful purposes. </li>
				    			<li>Items that contain digital versions of real products, trademarks or other intellectual property owned by others have not been property released. These Items are licensed on the basis of editorial use only. It is your responsibility to consider whether your use of these Items requires a clearance and if so, to obtain that clearance from the intellectual property rights owner.</li>
				    			<li>This license can be terminated if you breach it. If that happens, you must stop making copies of or distributing the End Product until you remove the Item from it.</li>

				    		</ul>


						</Cell>
					</div>
				</Grid>
				<AppFooter></AppFooter>
			</section>
		)
	}

}


export default connect(null, null)(LicensePage);