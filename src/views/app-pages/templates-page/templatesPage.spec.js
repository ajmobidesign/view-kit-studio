import React, { Component, PropTypes } from 'react';
import moment from 'moment';
import { DataTable, Button, TableHeader, FABButton, Icon, Grid, Cell, Dialog, DialogTitle, DialogContent, DialogActions, Card, CardTitle, CardActions, Snackbar, Spinner} from 'react-mdl';
import SmartButton from '../../app-components/smart-button';
//import DownloadButton from '../../app-components/download-button';
import TemplatesPage from './index';


import { shallow, mount, render } from 'enzyme';

const smartButtonProps = {
	data: 8, 
	onPress: ()=>{}
}

const minProps = {
				store: {
					getState : ()=>{
						let state = {};
						return state;
					}
				}
			};


const userWithPurchases = {
				store: {
					getState : ()=>{
						let state = {
							profile: {
								tplList : ["mdl_single_page"]
							}, 
							templateList: [
									{
										created : "2016-11-13T14:23:41-08:00",
										key: '',
										name: "MDL Basic HTML5",
										price: "120.0",
										title: "Template2",
										tplId: "mdl_basic_html5"
									}, 
									{
										created : "2016-11-10T14:23:41-08:00",
										key: '',
										name: "MDL Basic HTML5",
										price: "120.0",
										title: "Template2",
										tplId: "mdl_basic_html5"
									},
									{
										created : "2016-11-19T14:23:41-08:00",
										key: '',
										name: "MDL Singe Page",
										price: "120.0",
										title: "Template2",
										tplId: "mdl_single_page"
									}

								],
							user:{
								created : "2016-01-16T13:38:10-08:00",
								email :"tom@example.com",
								id :"ssLlsGF"
							}
						};
						return state;
					}
				}
			};

const newUser = {
				store: {
					getState : ()=>{
						let state = {
							profile: {
								tplList : []
							}, 
							templateList: [
									{
										created : moment().subtract(1, 'days').format("MM-DD-YYYY"),
										key: '',
										name: "MDL Basic HTML5",
										price: "120.0",
										title: "Template2",
										tplId: "mdl_basic_html5"
									}, 
									{
										created : moment().subtract(1, 'days').format("MM-DD-YYYY"),
										key: '',
										name: "MDL Basic HTML5",
										price: "120.0",
										title: "Template2",
										tplId: "mdl_basic_html5"
									},
									{
										created : moment().subtract(1, 'days').format("MM-DD-YYYY"),
										key: '',
										name: "MDL Singe Page",
										price: "120.0",
										title: "Template2",
										tplId: "mdl_single_page"
									}

							], 
							user:{
								created : moment().subtract(1, 'days').format("MM-DD-YYYY"),
								email :"tom@example.com",
								id :"12345"
							}
						};
						return state;
					}
				}
			};			


describe('Templates Page Component', () =>{
	describe('Basic Functions', ()=>{
		it('Component should render', ()=>{


			let wrapper = shallow(<TemplatesPage {...minProps} />);
			expect(wrapper.length).toEqual(1); 
			//expect(wrapper.type()).toEqual('section');
		});
	});

	describe('Existing User State', ()=>{
		it('Should render the table', ()=>{

			let wrapper = shallow(<SmartButton {...smartButtonProps} />);
			
			expect(wrapper.find('span').length).toBe(0); 
		});
	});


});