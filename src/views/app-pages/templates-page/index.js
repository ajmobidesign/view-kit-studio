import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import { templateMap, appTemplateList, getTplIdsWithPromotion } from 'src/core/defaults';
import { templateActions } from 'src/core/template';
import { profileActions } from 'src/core/profile';
import { templateListActions } from 'src/core/templateList';
import { userIsNew } from 'src/core/user';
import { Link } from 'react-router';
import { DataTable, Button, TableHeader, FABButton, Icon, Grid, Cell, Dialog, DialogTitle, DialogContent, DialogActions, Card, CardTitle, CardActions, Snackbar, Spinner} from 'react-mdl';
import SmartButton from '../../app-components/smart-button';
import DownloadButton from '../../app-components/download-button';
import AppSpinner from '../../app-components/app-spinner';

import _ from 'lodash';



export class TemplatesPage extends Component{

	constructor(props, context){
		super(props, context);

		this.state = {currentTpl: null, openDialog: null, isSnackbarActive: false};
		this.getData = ::this.getData;
		this.handleOpenDialog = ::this.handleOpenDialog;
		this.handleCloseDialog = ::this.handleCloseDialog;
		this.deleteTpl = :: this.deleteTpl;
		this.handleShowSnackbar = :: this.handleShowSnackbar;
		this.handleTimeoutSnackbar = ::this.handleTimeoutSnackbar;
		this.downloadError = :: this.downloadError;
		this.sortByTime = ::this.sortByTime;
		
	}

	componentWillMount(){

		this.props.getProfileData();
		this.props.getTemplateList();

	}

	componentDidMount(){

		window.scrollTo(0,0);
	}


	handleOpenDialog(docId) {
		//console.log('open dialog', docId)
		this.setState({ openDialog: true});
		if(docId) { this.setState({currentTpl : docId})};
		//console.log('open dialog', this.state.openDialog)

		let analyticsOb = {'event': 'deleteTemplateCalled'};

		dataLayer.push(analyticsOb);
	}

	handleCloseDialog() {
	    this.setState({ openDialog: null });
	    this.setState({currentTpl: null});

	    let analyticsOb = {'event': 'deleteTemplateCanceled'};

		dataLayer.push(analyticsOb);
	}

	deleteTpl(){
		this.props.deleteTemplate(this.state.currentTpl)
		this.handleCloseDialog();

		let analyticsOb = {'event': 'deleteTemplateConfirmed'};

		dataLayer.push(analyticsOb);
	}

	handleShowSnackbar(){
		this.setState({isSnackbarActive: true})
	}

	handleTimeoutSnackbar() {
    	this.setState({ isSnackbarActive: false });
  	}	

  	downloadError(){
  		this.handleShowSnackbar()
  	}


	getData(){

		let me = this;

		let data = this.props.templateList.allTemplates.filter((f)=>{
				return f.title;
			}).sort(me.sortByTime).map((t, i)=>{

				//add the filter here
				let templateHasPromotion = getTplIdsWithPromotion(appTemplateList).indexOf(t.tplId) !== -1

				function shareIcon(t){
						return (t.isCopyOf) ? <span> <Icon name="done" /> <br /> <small className="tpl-owner">{t.owner}</small> </span> : <Icon name="done" />
					}	

				function setPublicPrivate(t){
					return (t.public) ?  'Public'   : 'Private';
				}

				let showDownload = (me.props.profile.tplList.indexOf(t.tplId) !== -1)

				let shareStatus = (t.isCopyOf || t.collaborators) ? shareIcon(t) : null;

				let urlLink = (t.isCopyOf) ? `/shared-templates/${t.key}` : `/templates/${t.key}`;

				let publicPrivateUrl = (t.public || t.private) ?  ( setPublicPrivate(t) ): null;

				let showDelete = (t.isCopyOf) ? null : <SmartButton  onPress={me.handleOpenDialog} value={t.key}> <Icon className="delete-btn" name="delete" /> </SmartButton>

				let tableOb = {id: t.key, template: <Link to={urlLink}>{t.title}</Link>, delete: showDelete, download: <DownloadButton data={t} purchased={showDownload} onPress={me.props.downloadTemplate} error={me.downloadError} user={me.props.user}> <Icon name="get_app"/></DownloadButton>, templateId: templateMap[t.tplId].name, created: moment(t.created).format("MM-DD-YYYY"), shared: shareStatus, url: publicPrivateUrl}

				//Test Stuff
				/*
				if(t.isCopyOf){
					if(!tableOb.delete) {
						console.log('Delete Button is not present for shared template', 'Everything is working :)')
					}
					if(tableOb.delete){
						console.log(t)
						console.log('Delete Button is present for shared template', 'Something is wrong :(')
					}
				}*/


					return tableOb;

			})
		return data;
	}




	sortByTime(A, B){
		let a = A.created
		let b = B.created

		if(a > b) return -1;
		if(a < b) return 1;
		return 0;
	}



	renderTable(){

		//console.log('table render function called')
		
		let rowData = this.getData()
		//.sort(this.sortByLatest);

		return(
				<DataTable		
				    shadow={1}
				    rowKeyColumn="id"
				    rows={rowData}>
				   <TableHeader name="template">Title</TableHeader>
				   <TableHeader name="created">Created </TableHeader>
				   <TableHeader name="templateId">Template Name</TableHeader>	
				   <TableHeader name="shared">Shared</TableHeader>
				   <TableHeader name="url">URL</TableHeader>
					<TableHeader name="download">Download</TableHeader>

					<TableHeader name="delete">Delete</TableHeader>
				</DataTable>
			)
		
	}




	render(){

			const tpllist =  this.props.templateList.allTemplates;

			const cardContainer = {
									display: 'flex',
									display: '-webkit-flex', 
									width: '100%',
									height: '100%'
								};

			const cardLeft = {		
									boxSizing: 'border-box', 
									height: '100%',
									borderRight: '1px solid rgba(0,0,0, .12)',
									background: '#fff',
									flex: '3'
								};

			const cardRight = {
									boxSizing: 'border-box',
									padding: '30px 10px 10px 10px',
									height: '100%',
									textAlign: 'center',
									flex: '1'
								};

			const dialogStyle = {
									zIndex: '100149',
									position: 'fixed', 
									display: (this.state.openDialog) ? 'block' : null
								};

			let dStatus = (this.state.openDialog) ? true : null;										
	
		return(

			<section className="app-page" >
			<div className="app-page-inner">
				<Dialog onCancel={this.handleCloseDialog} role="dialog" style={dialogStyle} open={dStatus}>
		          <DialogTitle>Delete this file?</DialogTitle>
		          <DialogContent>
		            <p>The file {this.state.title} will be deleted</p>
		          </DialogContent>
		          <DialogActions>
		            <Button type='button' onClick={this.deleteTpl}>OK</Button>
		            <Button type='button' onClick={this.handleCloseDialog}>Cancel</Button>
		          </DialogActions>
				</Dialog>

				<Snackbar
	          	active={this.state.isSnackbarActive}
	          	onTimeout={this.handleTimeoutSnackbar}
	         	 action="">There was a download Error.</Snackbar>
				<Grid>
			        <div className="mdl-cell mdl-cell--12-col mdl-shadow--2dp mdl-grid no-spacing" style={{background: '#fff'}}>
			        	
						   
						    	<div className="mdl-cell mdl-cell--9-col">
						    		
				    				<h4 style={{ fontSize: '18px', color:'rgba(0,0,0, .7)', margin: '10px'}}>Select a template to get started.
				    				</h4>
						    	
						    	</div>
						    	<div className="mdl-cell mdl-cell--3-col">
						    		<Link to="/select-template" className="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" style={{marginTop: '10px'}}>
								        Select A Template
									</Link>
						    	</div>
						  					    
						
					</div>
				</Grid>
			    <Grid>
			    	<Cell col={12} >
				        
				        <AppSpinner timer={500}> {this.renderTable() } </AppSpinner>

			    	</Cell>
			    </Grid>
			    </div>
			</section>
		);
	}

};

const mapStateToProps = (state)=>{
	return {profile: state.profile, templateList: state.templateList, user: state.user};
}

const mapDispatchToProps = Object.assign({}, templateActions, profileActions, templateListActions)

export default connect(mapStateToProps, mapDispatchToProps )(TemplatesPage);