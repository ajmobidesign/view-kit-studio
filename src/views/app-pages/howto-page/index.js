import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import AppFooter from '../../app-components/app-footer';
import VerticalTabs from '../../app-components/vertical-tabs';
import status from '../../app-components/status';
import { authActions } from 'src/core/auth';
import { Button } from 'react-mdl';





export class HowtoPage extends Component{


	constructor(props, context){
		super(props, context);


		this.state = {currentTab : 0}

		this.selecteTab = ::this.selecteTab;
		this.signInUser = ::this.signInUser;
		
	}



	componentDidMount(){

		window.scrollTo(0,0);
	}


	selecteTab(i){
		this.setState({currentTab: i})
	}

	signInUser(){
		
		document.querySelector('#google-login').click()

	}



	render(){

		let tabsArray = [{title: 'Confirgurable Layouts!', content: ['Pick the right layout template for your project', 'Create and save multiple versions of each template', "Use the configuration panels to change colors and components of your template. It's all Free" ]}, {title: "Collaborate! It's Free!", content: ['Generate URLs to share your work', 'Add people to collaborate with', 'Use notes to communicate with collaborators']}, {title: 'Unlimited Downloads!', content: [ 'Pay when you are ready to enable unlimited downloads', 'Purchasing a template will automatically enable download on all new and saved versions of that template' ]}, {title: 'Watch Video', content: []}];

		let style_0 = {
			display: (this.state.currentTab === 0) ? 'flex' : 'none', 
			display: (this.state.currentTab === 0) ? '-webkit-flex' : 'none'
		};

		let style_1 = {
			display: (this.state.currentTab === 1) ? 'flex' : 'none', 
			display: (this.state.currentTab === 1) ? '-webkit-flex' : 'none'
		};

		let style_2 = {
			display: (this.state.currentTab === 2) ? 'flex' : 'none', 
			display: (this.state.currentTab === 2) ? '-webkit-flex' : 'none'
		}

		let style_3 = {
			display: (this.state.currentTab === 3) ? 'flex' : 'none', 
			display: (this.state.currentTab === 3) ? '-webkit-flex' : 'none'
		}

		let signInStyle = {
			display : (this.props.auth.authenticated) ? 'none': 'block',
			float: 'right', 
			width: '100%',
			marginRight: '5px'
		}



		return(
			<section className="app-page">
				<div className="app-page-inner">

					<div className="app-page-content-box" style={{marginBottom: '100px', marginTop: '30px'}}>
						<h3 style={{textAlign: 'center', color: '#ff4081', fontWeight: '300'}}> The Quick Way to Get Started</h3>
						<div  style={{textAlign: 'center'}}>
								
								
									<iframe width="560" height="315" src="https://www.youtube.com/embed/qKqjpmVprKY" frameBorder="0" allowFullScreen></iframe>
								
								
							</div>

					</div>



					
					<div className="app-page-content-box">

						<h3 style={{textAlign: 'center', color: '#ff4081', fontWeight: '300'}}> Do More with View Kit Studio</h3>

						<VerticalTabs tabs={tabsArray} selecteTab={this.selecteTab} currentTab={this.state.currentTab}>

							<div className="content-container" style={style_0} >
								
								<div className="content-block" >
									<img  src="images/palette-large2.png" />
								</div>
								
							</div>
							<div className="content-container" style={style_1}>
								
								<div className="content-block" >
									<img src="images/share-window.png" />
								</div>
								
							</div>
							<div className="content-container" style={style_2}>

								
								<div className="content-block" >
									<img src="images/download-enabled-combo.png" />
								</div>
								
							</div>
							


						</VerticalTabs>
					</div>
					
					<div className="app-page-content-box" style={{textAlign: 'center', padding: '20px 0px'}}>

						
	
						<span style={signInStyle}>

						{/*
							<span style={{ display: 'block', color: '#ff4081', marginBottom: '10px', marginTop: '20px',fontSize: '30px', fontWeight: '300'}}>Sign In to get started now! </span>
						*/}

							
							
							<br/> 
							<Button raised accent onClick={this.signInUser}> Sign In with Google </Button>
							
							
						</span>

						

					</div>
					

				</div>
				<AppFooter></AppFooter>
			</section>
		)
	}

};

const mapStateToProps = (state) =>{
	return {auth: state.auth}
}

const mapDispatchToProps = (state) =>{
	return Object.assign({}, authActions)
}

export default connect(mapStateToProps, mapDispatchToProps)(HowtoPage);