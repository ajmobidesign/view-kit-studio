import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import AppFooter from '../../app-components/app-footer';
import { Grid, Cell } from 'react-mdl';

export class BasicDocsPage extends Component{



	render(){


		const headerStyle = {
			color: 'rgba(0,0,0, .5)', 
			textAlign: 'center',
			marginTop: '30px'
		}

		const titleStyle = {
			color: 'rgba(0,182,242,1)', 
			fontSize: '20px', 
			fontWeight: '500',
			marginLeft: '15px',
			marginTop: '20px',
			marginBottom: '30px'
		}

		const imgStyle = {
			textAlign: 'center', 
			marginLeft: '10%'
		}

		const cellStyle ={
			boxSizing: 'border-box',
			margin: '40px auto',
			padding: '35px 25px',
			paddingBottom: '10px',
			backgroundColor: '#fff',
			boxShadow:'0 0 2px rgba(0,0,0, .14)',
			boxShadow: '0 2px 2px 0 rgba(0,0,0,0.14),0 3px 1px -2px rgba(0,0,0,0.2),0 1px 5px 0 rgba(0,0,0,0.12)'

		}

		const textSection = {
			width: '100%',
			boxSizing: 'border-box',
			marginTop: '15px',
			padding: '20px',
			color: 'rgba(0,0,0, .7)'

		}


		const containerLeft = {
			height:'550px'
		}

		const containerRight = {
			height: '550px', 
			overflowY: 'scroll'
		}

		const imgWarpper= {
			textAlign: 'center'
		}




		return(
			<section className="app-page" >
				<Grid>
				    <div className="app-page-inner">
				    <div className="mdl-grid mdl-cell mdl-cell--12-col ">
				    	<Cell col={2} style={containerLeft}>
				    		

				    		<ul className="demo-list-item mdl-list">
				    		  <li className="mdl-list__item">
							    <span className="mdl-list__item-primary-content">
							      <a href="#about" >About</a>
							    </span>
							  </li>
							  <li className="mdl-list__item">
							    <span className="mdl-list__item-primary-content">
							      <a href="#try" >Try a Template</a>
							    </span>
							  </li>
							  <li className="mdl-list__item">
							    <span className="mdl-list__item-primary-content">
							      <a href="#save" >Login and Save Versions</a>
							    </span>
							  </li>
							  <li className="mdl-list__item">
							    <span className="mdl-list__item-primary-content">
							      <a href="#purchase" >Purchase and Enable Download</a>
							    </span>
							  </li>
							  <li className="mdl-list__item">
							    <span className="mdl-list__item-primary-content">
							      <a href="#share" >How to Use Share Tools</a>
							    </span>
							  </li>
							</ul>

				    		
				    	</Cell>

				    	<Cell col={10} style={containerRight}>

				    		<Cell col={12} >
					    		<a name="about"></a>
					    		<h4 style={headerStyle} >Welcome</h4>
					    	</Cell>
					    	<Cell col={10} style={cellStyle}>

					    		<div style={textSection}>
					    			<p>Welcome to the Preview version of View Kit Studio. We make it easy for you to go from wireframes to code. Our layout templates are built with standard frameworks like Material Design Lite. Choose a layout template. Create unlimited customized versions for free. Purchase and enable unlimited downloads. It is that simple.
					    				
					    			</p>
					    		</div>
					    	</Cell>

					    	<Cell col={12} >
					    		<a name="try"></a>
					    		<h4 style={headerStyle} >Try a Template</h4>
					    	</Cell>
					    	<Cell col={10} style={cellStyle}>
					    		

					    		<img src="images/select-template.png" width="50%" style={{marginLeft: '25%'}}/>

					    		<div style={textSection}>
					    			<p>You can try out different templates without signing in. Start by going the <strong>Select Template</strong> page.
					    			</p>
					    		</div>
					    	</Cell>
					    	
					    	<Cell col={10}  style={cellStyle}>
					    		<div style={imgWarpper}>
					    			<img src="images/load-button.png"  />
					    		</div>
					    		<div style={textSection}>
					    			<p>Click the <strong>Load & Try</strong> button of the template you would like to try. This will load the template.

					    			<br/>
					    			
					    			</p>
					    		</div>
					    	</Cell>
					    	<Cell col={10}  style={cellStyle}>
					    		<img src="images/template-view.png" width="80%" style={imgStyle}/>

					    		<div style={textSection}>
					    			<p>You can now try different configurations for the template using the panels on the right.
					    			<em>The changes you make will not be saved unless you log in</em>
					    			</p>
					    		</div>
					    	</Cell>
					    	<Cell col={12} >
					    		<a name="save"></a>
					    		<h4 style={headerStyle} >Login and Save Versions</h4>
					    	</Cell>
					    	
				    		<Cell col={10} offset={1} style={cellStyle}>
					    		
					    		<img src="images/create-button.png" width="80%" style={imgStyle}/>

					    		<div style={textSection}>
					    			<p> On the Select A Template page, click the <strong>Create New</strong> button for the template you want to use. This will automatically create a new version of the template and save it.
					    			</p>
					    		</div>
				    		</Cell>
				    		<Cell col={10} offset={1} style={cellStyle}>
					    		
					    		<img src="images/template-title-small.png" width="80%" style={imgStyle}/>
					    		<div style={textSection}>
					    			<p>In the template page you can now give your template version a custom title. All changes you make to your version including color and component changes will be <em>auto saved</em>.
					    			</p>
					    		</div>

				    		</Cell>
				    		<Cell col={10} offset={1} style={cellStyle}>
					    		
					    		<img src="images/my-template-menu.png" width="80%" style={imgStyle}/>
					    		<div style={textSection}>
					    			<p>To view saved versions of your templates you can access them by going to the <strong>My Templates</strong> page.
					    			</p>
					    		</div>
				    		</Cell>
				    		<Cell col={10} offset={1} style={cellStyle}>
					    		
					    		<img src="images/template-table-small.png" width="100%" />
					    		<div style={textSection}>
					    			<p>Click on the title of the version you would like to see to load it.  
					    			</p>
					    		</div>

				    		</Cell>

				    		
					    	<Cell col={12} >
					    		<a name="purchase"></a>
					    		<h4 style={headerStyle} >Purchase and Enable Download</h4>
					    	</Cell>
					    	<Cell col={10} offset={1} style={cellStyle}>
					    		
					    		<img src="images/buy-button.png"  style={imgStyle}/>
					    		<div style={textSection}>
					    			<p>To enable download of the template versions you have created, you have to first purchase it. Go to the Select A Template page and click on the <strong>Purchase</strong> button of the template. You will be redirected to <strong style={{color: 'rgba(0,182,242,1)'}}><i className="fa fa-paypal" aria-hidden="true"></i> Paypal </strong> to complete purchase.
					    			</p>
					    		</div>

				    	</Cell>

				    	<Cell col={10} offset={1} style={cellStyle}>
				    		
				    		<img src="images/download-table-small.png" />
				    		<div style={textSection}>
				    			<p>Once you have completed your purchase of the template, all versions of that template will be downloadable. This includes all new versions of the template.
				    			<em>There are <strong> No Limits </strong> on the number of versions you can create or the number of downloads.</em>
				    			</p>
				    		</div>

				    	</Cell>
					    <Cell col={12} >
					    		<a name="share"></a>
					    		<h4 style={headerStyle} >How to use Share Tools</h4>
					    </Cell>
					    <Cell col={10} offset={1} style={cellStyle}>
				    		<div style={imgWarpper}>
				    			<img src="images/share-url.png"  />
				    		</div>	
				    		<div style={textSection}>
				    			<p>Some templates inclue a <strong>Share Tools</strong> panel. You can use the panel to create shareable URLs for your templates. You can create a private or public url for each version of your template.
				    			</p>
				    		</div>

				    	</Cell>
				    	
				    	<Cell col={10} offset={1} style={cellStyle}>
				    		<div style={imgWarpper}>
				    			<img src="images/share-search.png" />
				    		</div>
				    		<div style={textSection}>
				    			<p>You can add any View Kit Studio user as a collaborator. To add a collaborator, search and click add next to the name. Collborators are added per version. If you want to add collaborators to a different template version, you must add them using the share panel of that version.
				    			</p>
				    		</div>

				    	</Cell>
				    	<Cell col={10} offset={1} style={cellStyle}>
				    		<div style={imgWarpper}>
				    			<img src="images/share-collab.png" />
				    		</div>
				    		<div style={textSection}>
				    			<p> The collaborator list panel will show all the users who are currently working on the template. Collaborartors have full edit privilages. They cannot however add or remove other collaborators. Nor can they delete the original shared template.
				    			</p>
				    		</div>

				    	</Cell>
				    	<Cell col={10} offset={1} style={cellStyle}>
				    		<div style={imgWarpper}>
				    			<img src="images/share-sticky.png" />
				    		</div>
				    		<div style={textSection}>
				    			<p>You can add notes for yourself or for your collaborators using sticky notes. Sticky notes can be dragged to any part of the template.
				    			</p>
				    		</div>
				    		<div style={imgWarpper}>
				    			<img src="images/sticky.png"  />
				    		</div>

				    	</Cell>



				    	



				    	</Cell>

				
					</div>
					</div>
				</Grid>
			    <AppFooter ></AppFooter>
			</section>
		);
	}

};

export default connect(null, null)(BasicDocsPage);