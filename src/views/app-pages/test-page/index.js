import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { profileActions } from 'src/core/profile';
import { templateActions } from 'src/core/template';
import { authActions } from 'src/core/auth';
import { Db } from 'src/core/firebase';




export class TestPage extends Component{

	constructor(props, context){
		super(props, context);

		this.runDbTest = ::this.runDbTest;
		this.runDavidLoggedIn = ::this.runDavidLoggedIn;
		this.runMindyLoggedIn = ::this.runMindyLoggedIn;
		this.runAndyLoggedIn = ::this.runAndyLoggedIn;
		this.runLoggedOut = ::this.runLoggedOut;
	}


	runDbTest(){
		let testPath = 'test/update';
		let data = {data: 'test'};
		let ob = {path: testPath, data: data};
		let dispatch = {};


		let genExp = function *(){

			
			
			let fn1 = Db.update(ob.path, ob.data);

			fn1
			.catch((error)=>{
				 console.log(`Step 1: Update Function : ${error}`)
			});

			yield 'Set Update Function';


			let fn2 = Db.setValue(ob.path, ob.data);

			fn2.catch((error)=>{
					console.log(`Step 2: Ref set: ${error}`)
				});			
			yield 'Set Value Function';

			let fn3 = Db.deleteValue(testPath);

			fn3.catch((error)=>{
					console.log(`Step 3: Ref remove: ${error}`)
				});

			yield 'Delete Value Function';

			let fn4 = Db.flagDelete(testPath);

			fn4.catch((error)=>{
				console.log(`Step 4: Ref flag delete: ${error}`)
			});

			yield 'Flag Delete Function';

			let fn5 = Db.addAndGetKey(ob.path, ob.data);

			fn5.catch((error)=>{
				console.log(`Step 5: Add and Get Key ${error}`)
			});

			yield 'Add and Get Key';


			let fn6 = Db.addAndSetKey(ob.path, ob.data);

			fn6
			.catch((error)=>{
				console.log(`Step 6: Add and Get key: ${error}`)
			})


			yield 'Add and Set Key';

			let fn7 = Db.checkData(ob.path);

			fn7
			.then((error)=>{
				console.log(`Step 7: And and Set Key ${error}`)
			})

			yield 'Check Data';


			let fn8 = Db.addTemplate(ob.path, ob.data, dispatch);


			fn8
			.catch((error)=>{
				console.log(`Step 8: Add Template ${error}`)
			})


			yield 'Add Template';



			let fn9 = Db.newTransaction(ob.path, ob.data);

			fn9
			.catch((error)=>{
				console.log(`Step 9: New Transaction ${error}`)
			})

			yield 'New Transaction';




			return 'Finished';
		}
		

		let dbMethodTest = genExp();
		let finished = false;
		let step = 0;


		do{

			var r = dbMethodTest.next();

			finished = r.done;

			step++;

			console.log(`Step ${step} : ${r.value}`)

		}while(finished === false);
		
	}


	runDavidLoggedIn(){

		const  { auth } = this.props;
		const {id, authenticated } = auth;
		let testTplId = 'kolppl_l';

		console.log('I am David')

		//should be able to hit all the right endpoints	
		/*	
		const first = setTimeout(()=>{

			console.log('first call should pass')

			//create a template
			this.props.createNewTemplate(testTplId);
			
			clearTimeout(first);
		}, 500);
		*/

		const second = setTimeout(()=>{
			//console.log('second call should pass')
			//this could change if the template does not exist
			const { auth } = this.props
			let tempDocId = '-KXEUWRTUWD8tdOVuNfR';
			let path = `${auth.id}/templates/${tempDocId}`;
			
			//delete template
			//this.props.loadTemplate(tempDocId);

			let fn10 = Db.checkData(path);

			fn10
			.then((val)=>{
				if(val){
					console.log('I have access to my template data :)')
				}
			})
			.catch((error)=>{
				console.log('I cannot access my template data :( ' , error)
			})
			
			clearTimeout(second);
		}, 1000);

		const third = setTimeout(()=>{
			//console.log('Third call should pass')

			let { auth } = this.props;
			let path = `${auth.id}/profile`

			let fn11 = Db.checkData(path);

			fn11
			.then((val)=>{
				if(val){
					console.log('I have access to my profile data :)')
				}
			})
			.catch((error)=>{
				console.log("I don't have access to my profile data :( ", error)
			})

			
			clearTimeout(third);
		}, 1500);

		const fourth = setTimeout(()=>{
			//console.log('Fourth call should throw error')

			let otherId = 'WiY7ga5kKPhDm6JFWxvfJ8qDDXv2';
			let otherPath = `${otherId}/templates`;
			let template = {
				special: 'none'
			}

			let dispatch = {};

			let fn5 = Db.addAndGetKey(otherPath, template)

			fn5
			.then((val)=>{
				if(val){
					console.log("I was able to access Young's data I should not be able to :( " )
				}
			})
			.catch((error)=>{
				console.log("I was not able to access Young's user's data :) " + error)
			})

			//access 
			
			clearTimeout(fourth);
		}, 2000);


		const fifth = setTimeout(()=>{
			console.log('Fifth call, collaborator check should pass')

			let path = "Sl5CoyitrmNIkPGjbH0fp3A9rle2/templates/-KVc52NirkVKpKlWBztl"

			let tempDocId = '-KVc52NirkVKpKlWBztl';

			//this.props.loadCollabTemplate(tempDocId);

			let fn12 = Db.checkData(path)

			fn12
			.then((val)=>{
				if(val){
					console.log("I was able access Mindy's shared file :) " )
				}
			})
			.catch((error)=>{
				console.log("I was not able to access Mindy's shared file :( " + error)
			})

			clearTimeout(fifth);
		}, 2500)

	}

	runMindyLoggedIn(){

		const  { auth } = this.props;
		const {id, authenticated } = auth;
		let testTplId = 'kolppl_l';

		console.log('I am Mindy')

		//should be able to hit all the right endpoints	
		
		const first = setTimeout(()=>{
	
			const { auth } = this.props
			
			let path = `${auth.id}/templates`;

			let fn10 = Db.checkData(path);

			fn10
			.then((val)=>{
				if(val){
					console.log('I was able to get all my templates :)')
				}
			})
			.catch((error)=>{
				console.log('I cannot access my templates :( ' , error)
			})

			
			clearTimeout(first);
		}, 500);
		

		const second = setTimeout(()=>{
			//console.log('second call should pass')
			//this could change if the template does not exist
			const { auth } = this.props
			let tempDocId = '-KVc52NirkVKpKlWBztl';
			
			let path = `${auth.id}/templates/${tempDocId}`;
			
			//delete template
			//this.props.loadTemplate(tempDocId);

			let fn10 = Db.checkData(path);

			fn10
			.then((val)=>{
				if(val){
					console.log('I have access to my template data :)')
				}
			})
			.catch((error)=>{
				console.log('I cannot access my template data :( ' , error)
			})
			
			clearTimeout(second);
		}, 1000);

		const third = setTimeout(()=>{
			//console.log('Third call should pass')

			let { auth } = this.props;
			let path = `${auth.id}/profile`

			let fn11 = Db.checkData(path);

			fn11
			.then((val)=>{
				if(val){
					console.log('I have access to my profile data :)')
				}
			})
			.catch((error)=>{
				console.log("I don't have access to my profile data :( ", error)
			})

			
			clearTimeout(third);
		}, 1500);

		const fourth = setTimeout(()=>{
			//console.log('Fourth call should throw error')

			let otherId = 'WiY7ga5kKPhDm6JFWxvfJ8qDDXv2';
			let otherPath = `${otherId}/templates`;
			let template = {
				special: 'none'
			}

			let dispatch = {};

			let fn5 = Db.addAndGetKey(otherPath, template)

			fn5
			.then((val)=>{
				console.log('val', val)
				if(val){
					console.log("I was able to access Young's data I should not be able to :( " )
				}
			})
			.catch((error)=>{
				console.log("I was not able to access Young's data :) " + error)
			})

			//access 
			
			clearTimeout(fourth);
		}, 2000);


		const fifth = setTimeout(()=>{
			
			let path = "LJlx0jMWbbYfxJSbIga8Bmah0vH3/templates/-KWebLpcz0mha7ZGKwcl"

			//let tempDocId = '-KVc52NirkVKpKlWBztl';

			//this.props.loadCollabTemplate(tempDocId);

			let fn12 = Db.checkData(path)

			fn12
			.then((val)=>{
				if(val){
					console.log("I was able access David's shared file :) " )
				}
			})
			.catch((error)=>{
				console.log("I was not able to access David's shared file :( " + error)
			})

			clearTimeout(fifth);
		}, 2500)

		const sixth = setTimeout(()=>{
			
			let path = "LJlx0jMWbbYfxJSbIga8Bmah0vH3/templates/-KWebLpcz0mha7ZGKwcl"

			let data = {title: 'I have changed'}

			//let tempDocId = '-KVc52NirkVKpKlWBztl';

			//this.props.loadCollabTemplate(tempDocId);

			let fn12 = Db.update(path, data)

			fn12
			.then((val)=>{
				//if(val){
					console.log("I was able update David's shared file :) " )
				//}
			})
			.catch((error)=>{
				console.log("I was not able update David's shared file :( " + error)
			})

			clearTimeout(sixth);
		}, 3000)


		const seventh = setTimeout(()=>{
			

			let path = "transactions"

			let data = {test : 'I am a test'}


			let fn12 = Db.addAndGetKey(path, data)

			fn12
			.then((val)=>{
				//if(val){
					console.log("I was able to add a transaction :) " )
				//}
			})
			.catch((error)=>{
				console.log("I was not able to add a transaction  :( " + error)
			})

			clearTimeout(seventh);
		}, 3500)


		const eight = setTimeout(()=>{
			

			let path = "transactions/-KVNRnw7QkCYBzEi_uEk"

			let fn12 = Db.checkData(path)

			fn12
			.then((val)=>{
				//if(val){
					console.log("I was able to get my transaction :) " )
				//}
			})
			.catch((error)=>{
				console.log("I was not able to get my transaction  :( " + error)
			})

			clearTimeout(eight);
		}, 3500)

		const nine = setTimeout(()=>{

			let { auth } = this.props;
			
			let path = `privateShare/-K_a6zmgVVtQS9sg1cNG/path`

			

			let fn12 = Db.checkData(path)

			fn12
			.then((val)=>{
				//if(val){
					console.log("I have acces to the private share path :) " )

					return Db.checkData(val)
				//}
			})
			.then((val)=>{
				console.log("I have access the actual private share template :) " )
			})
			.catch((error)=>{
				console.log("I was not able to get a private share :( " + error)
			})

			clearTimeout(eight);
		}, 4000)

	}

	runAndyLoggedIn(){
		const  { auth } = this.props;
		const {id, authenticated } = auth;
		let testTplId = 'kolppl_l';

		console.log('I am Andy')

		//should be able to hit all the right endpoints	
		
		const first = setTimeout(()=>{
	
			const { auth } = this.props
			
			let path = `${auth.id}/templates`;

			let fn10 = Db.checkData(path);

			fn10
			.then((val)=>{
				//if(val){
					console.log('I was able to get all my templates :)')
				//}
			})
			.catch((error)=>{
				console.log('I cannot access my templates :( ' , error)
			})

			
			clearTimeout(first);
		}, 500);
		

		const second = setTimeout(()=>{
			//console.log('second call should pass')
			//this could change if the template does not exist
			const { auth } = this.props
			let tempDocId = '-KXdZUAQ2zWTLHueXfmW';
			
			let path = `${auth.id}/templates/${tempDocId}`;
			
			//delete template
			//this.props.loadTemplate(tempDocId);

			let fn10 = Db.checkData(path);

			fn10
			.then((val)=>{
				//console.log('val', val)
				if(val){
					console.log('I have access to my template data :)')
				}
			})
			.catch((error)=>{
				console.log('I cannot access my template data :( ' , error)
			})
			
			clearTimeout(second);
		}, 1000);

		const third = setTimeout(()=>{
			//console.log('Third call should pass')

			let { auth } = this.props;
			let path = `${auth.id}/profile`

			let fn11 = Db.checkData(path);

			fn11
			.then((val)=>{
				if(val){
					console.log('I have access to my profile data :)')
				}
			})
			.catch((error)=>{
				console.log("I don't have access to my profile data :( ", error)
			})

			
			clearTimeout(third);
		}, 1500);

		const fourth = setTimeout(()=>{
			//console.log('Fourth call should throw error')

			let otherId = 'WiY7ga5kKPhDm6JFWxvfJ8qDDXv2';
			let otherPath = `${otherId}/templates`;
			let template = {
				special: 'none'
			}

			let dispatch = {};

			let fn5 = Db.addAndGetKey(otherPath, template)

			fn5
			.then((val)=>{
				if(val){
					console.log("I was able to access Young's data I should not be able to :( " )
				}
			})
			.catch((error)=>{
				console.log("I was not able to access Young's data :) " + error)
			})

			//access 
			
			clearTimeout(fourth);
		}, 2000);


		const fifth = setTimeout(()=>{

			let path = "LJlx0jMWbbYfxJSbIga8Bmah0vH3/templates/-KWebLpcz0mha7ZGKwcl"

			//let tempDocId = '-KVc52NirkVKpKlWBztl';

			//this.props.loadCollabTemplate(tempDocId);

			let fn12 = Db.checkData(path)

			fn12
			.then((val)=>{
				if(val){
					console.log("I was able access David's shared file :( " )
				}
			})
			.catch((error)=>{
				console.log("I was not able to access David's shared file :) " + error)
			})

			clearTimeout(fifth);
		}, 2500)

		const sixth = setTimeout(()=>{
			
			let path = "LJlx0jMWbbYfxJSbIga8Bmah0vH3/templates/-KWebLpcz0mha7ZGKwcl"

			let data = {title: 'I have changed'}

			//let tempDocId = '-KVc52NirkVKpKlWBztl';

			//this.props.loadCollabTemplate(tempDocId);

			let fn12 = Db.update(path, data)

			fn12
			.then((val)=>{
				//if(val){
					console.log("I was able update David's shared file :( " )
				//}
			})
			.catch((error)=>{
				console.log("I was not able update David's shared file :) " + error)
			})

			clearTimeout(sixth);
		}, 3000)


		const seventh = setTimeout(()=>{
			
			let path = "LJlx0jMWbbYfxJSbIga8Bmah0vH3/profile"

			let data = {title: 'I have changed'}

			//let tempDocId = '-KVc52NirkVKpKlWBztl';

			//this.props.loadCollabTemplate(tempDocId);

			let fn12 = Db.checkData(path)

			fn12
			.then((val)=>{
				//if(val){
					console.log("I was able access David profile data :( " )
				//}
			})
			.catch((error)=>{
				console.log("I was not able access David profile data :) " + error)
			})

			clearTimeout(seventh);
		}, 3500)


		const eight = setTimeout(()=>{

			let { auth } = this.props;
			
			let path = `${auth.id}/profile/notifications`

			

			let fn12 = Db.checkData(path)

			fn12
			.then((val)=>{
				//if(val){
					console.log("I have access to my profile and notifications :) " )
				//}
			})
			.catch((error)=>{
				console.log("I don't have access to my profile and notifications :( " + error)
			})

			clearTimeout(eight);
		}, 4000)

		
	}




	componentDidMount(){
		/*
		const first = setTimeout(()=>{

			console.log('first call')
			let profileData = this.props.getProfileData;
			
			profileData();
			
			clearTimeout(first);
		}, 500);

		const second = setTimeout(()=>{
			console.log('second call')
			let profileData = this.props.getProfileData;
			
			profileData();

			
			clearTimeout(second);
		}, 1000);

		const third = setTimeout(()=>{
			console.log('Third call')

			let profileData = this.props.getProfileData;
			
			profileData();

			
			clearTimeout(third);
		}, 1500);
		*/
	}

	runLoggedOut(){
		const  { auth } = this.props;
		const {id, authenticated } = auth;
		let testTplId = 'kolppl_l';

		console.log('I am Anony')

		//should be able to hit all the right endpoints	
		
		const first = setTimeout(()=>{
	
			let { auth } = this.props;
			
			let path = `publicShare/-KXMkAtqPHMj-toJPo5N/path`			

			let fn12 = Db.checkData(path)

			fn12
			.then((val)=>{
				//if(val){
					console.log("I have acces to the public share path :) " )

					return Db.checkData(val)
				//}
			})
			.then((val)=>{
				console.log("I have access the actual public share template :) " )
			})
			.catch((error)=>{
				console.log("I was not able to get a public share :( " + error)
			})

			
			clearTimeout(first);
		}, 500);


		const second = setTimeout(()=>{

			let user = {email: 'tom@example.com', pswd: 'tomtom'};	
			this.props.authenticateUser(user);



		}, 1000);
	}



	render(){

		return(
			<section className="app-page" >
				<div className="app-page-inner">

				<button onClick={this.runDbTest}>Check Db Not Logged in</button>
				<br />

				<button onClick={this.runDavidLoggedIn}>David is logged in </button>
				<br />
				<button onClick={this.runMindyLoggedIn}>Mindy is logged in </button>
				<br />
				<button onClick={this.runAndyLoggedIn}>Andy is logged in </button>
				<br />

				<button onClick={this.runLoggedOut}>Public Access Check</button>
				<br />

				</div>
			</section>	
		)
	}

}


const mapStateToProps = (state)=>{
	return state;
}

const mapDispatchToProps = Object.assign({}, profileActions, templateActions, authActions);

export default connect(mapStateToProps, mapDispatchToProps)(TestPage);