import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import SlideOut from '../../app-components/slide-out';
import { faqData } from 'src/core/defaults';




export class FaqPage extends Component{



	componentDidMount(){

		window.scrollTo(0,0);
	}


	renderQuestions(){

		let data = faqData;

		return data.map((d, i)=>{
			return <SlideOut key={i} title={d.question} content={d.answer} link={d.link}></SlideOut>
		})


	}


	render(){
		return(
			<section className="app-page" >
				<div className="app-page-inner">
				<div className="mdl-grid mdl-cell mdl-cell--12-col" >
					<h3 style={{color: 'rgba(0,0,0, .5)'}}>Frequenty Asked Questions </h3>
				</div>
				

				{this.renderQuestions()}

				</div>
			</section>	
		)
	}

};

export default connect(null, null)(FaqPage);