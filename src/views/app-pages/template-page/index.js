import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { templateActions } from 'src/core/template';
import { profileActions } from 'src/core/profile';
import { transactionActions } from 'src/core/transaction';
import TemplateSelector from '../../app-components/template-selector';
import Bill from '../../app-components/bill';
import AppFooter from '../../app-components/app-footer';



export class TemplatePage extends Component{


	constructor(props, context){
		super(props, context);


		this.state = {showBill: false, buy: false};
		this.initiatePurchase = ::this.initiatePurchase;
		this.renderBill = ::this.renderBill;
		this.cancelPurchase = ::this.cancelPurchase;

	}

	componentWillMount(){
		this.props.getProfileData();
		//this.props.getTemplateList();
	}

	componentDidMount(){

		window.scrollTo(0,0);
	}


	initiatePurchase(data){

		this.setState({showBill: true})
		this.setState({purchaseOb: data})
	}

	cancelPurchase(){

		let analyticsOb = {'event': 'buyWithPaypalCancel'};

		dataLayer.push(analyticsOb);

		this.setState({showBill: false})
		this.setState({purchaseOb: null})
	}


	renderBill(){
		let ob = this.state.purchaseOb;
		return (<Bill title={ob.title} id={ob.id} price={ob.price} buyNow={this.props.makePayment} cancel={this.cancelPurchase}></Bill>)
	}


	render(){

		const isAuth = this.props.auth.authenticated;
		const profile = this.props.profile;		

		return(
			<section className="app-page" >
					{(this.state.showBill)? this.renderBill() : null}
				
					{(!this.state.showBill) ? <TemplateSelector loggedIn={isAuth} createNewTemplate={this.props.createNewTemplate} purchasedList={this.props.profile.tplList} buyNow={this.initiatePurchase} auth={this.props.auth}></TemplateSelector>: null}
					<AppFooter></AppFooter>	
			</section>
		);
	}
};

const mapStateToProps = (state)=>{
	//console.log(state)
	return state;
}

const mapDispatchToProps = Object.assign({}, templateActions, profileActions, transactionActions)

export default connect(mapStateToProps, mapDispatchToProps)(TemplatePage);