import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Router, Link } from 'react-router';
import { replace } from 'lodash';
import { templateMap, appTemplateList } from 'src/core/defaults';
import { Grid, Cell, Button, Snackar, Spinner } from 'react-mdl';
import Bill from '../../app-components/bill';
import { transactionActions } from 'src/core/transaction';


import ImageCycle from '../../app-components/image-cycle';

import TemplateAbout from '../../app-components/template-about';

export class DetailsPage extends Component{



	constructor(props, context){
		super(props, context);

		this.state = {buy: false}
		this.initiatePurchase = ::this.initiatePurchase;
		this.cancelPurchase= ::this.cancelPurchase;
	}

	componentDidMount(){

		window.scrollTo(0,0);
	}


	initiatePurchase(){
		this.setState({buy: true});

		let analyticsOb = {'event': 'buyNowDetailsPage'};

		dataLayer.push(analyticsOb);
	}

	cancelPurchase(){
		this.setState({buy: false});

		let analyticsOb = {'event': 'buyWithPaypalCancel'};

		dataLayer.push(analyticsOb);
	}


	render(){


		let params = this.props.params;

		let { id } = params;
		let str = replace(id, RegExp("-","g"), '_');

		let template = templateMap[str];


		let picStyle= {
			height: '300px', 
			marginBottom: '30px',
			boxShadow: '1px 2px 5px rgba(0,0,0, .3)'
		}

		let headingColor = {
			color: 'rgba(0,0,0, .6)'
		}



		return(
			<section className="app-page" >
				<div className="app-page-inner">

					{ (this.state.buy) ?<Bill title={template.name} id={template.tplId} price={template.price} buyNow={this.props.makePayment} cancel={this.cancelPurchase}></Bill>: null}

					{ (!this.state.buy) ? <div className="mdl-grid mdl-cell--12-col">
						<Cell col={9}>

						
							<Cell col={12} style={picStyle}>

								
								<ImageCycle num={template.pics} tplId={template.tplId}></ImageCycle >

							</Cell>
							<Cell col={12} style={headingColor}>
								{/*<TemplateItemTabs  tplId={template.tplId}></TemplateItemTabs>*/}
								<TemplateAbout tplId={template.tplId}></TemplateAbout>
							</Cell>


						</Cell>
						<Cell col={3}>
							<h3 className="mdl-typography--headline" style={headingColor}>{template.name}</h3>
							<p style={headingColor}>${template.price}</p>

							<Button colored accent raised onClick={this.initiatePurchase}> Buy Now</Button>

						</Cell>
						
					</div>: null}
				</div>
			</section>

		)
	}

}


const mapStateToProps = null;

const mapDispatchToProps = Object.assign({}, transactionActions);

export default connect(mapStateToProps, mapDispatchToProps )(DetailsPage);
