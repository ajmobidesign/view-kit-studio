// Polyfills
import 'babel-polyfill';

// React Redux
import 'react';
import 'react-dom';
import 'react-hot-loader';
import 'react-redux';
import 'react-router';
import 'react-router-redux';
import 'redux';
import 'redux-thunk';

// Third Party
import 'classnames';
import 'firebase';
import 'immutable';
import 'reselect';
import 'axios';
import 'paypal-rest-sdk';
import 'moment';
import 'detect-browser';