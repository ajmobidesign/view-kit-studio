.mdl-layout__drawer{
				background: ${layoutDrawerBgColor};
				color: ${lapyoutTextColor};
			}

			.mdl-navigation .mdl-navigation__link{
				color: ${layoutDrawerNavigationColor};
			}

			.mdl-navigation .mdl-navigation__link:hover{
				background-color: ${layoutNavColor};
			}

			.mdl-navigation .mdl-navigation__link--current{
				background-color: ${layoutDrawerNavigationLinkActiveBackground};
				color: ${layoutDrawerNavigationLinkActiveColor};
			}

			.mdl-layout__header .mdl-layout__drawer-button{
				color: ${layoutHeaderTextColor};
			}

			.mdl-layout__header{
				background-color: ${layoutHeaderBgColor};
				color: ${layoutHeaderTextColor};
			}

			.mdl-layout__header-row .mdl-navigation__link{
				color: ${layoutHeaderTextColor};
			}

			.mdl-layout__tab-bar{
				background-color: ${layoutHeaderBgColor};
			}

			.mdl-layout__tab-bar-button{
				background-color: ${layoutHeaderBgColor};
			}

			.mdl-layout__tab-bar-button.is-active{
				color: ${layoutHeaderTextColor};
			}

			.mdl-layout__tab{
				color: ${layoutHeaderTextColor};
			}

			.mdl-layout.is-upgraded .mdl-layout__tab.is-active{
				color: ${layoutHeaderTextColor};
			}

			.mdl-layout__tab .mdl-layout__tab-ripple-container .mdl-ripple{
				background-color: ${layoutHeaderTextColor};
			}