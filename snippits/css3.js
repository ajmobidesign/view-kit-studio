

			.mdl-slider.is-upgraded:focus:not(:active)::-webkit-slider-thumb{
                box-shadow: 0 0 0 10px ${rangeFadeColor};
            }

			

			.mdl-slider.is-upgraded:focus:not(:active)::-moz-range-thumb {
      			box-shadow: 0 0 0 10px ${rangeFadeColor};
    		}

    		.mdl-slider.is-upgraded:active::-webkit-slider-thumb{
    			background: ${rangeColor};
    		}

    		.mdl-slider.is-upgraded:active::-moz-range-thumb {
    			background: ${rangeColor};
    		}

    		.mdl-slider.is-upgraded::-ms-thumb {
    			background: ${rangeColor};
    		}

    		.mdl-slider.is-upgraded:focus:not(:active)::-ms-thumb{
    			background: radial-gradient(circle closest-side, ${rangeColor} 0%, ${rangeColor} 37.5%, ${rangeFadeColor} 37.5%, ${rangeFadeColor} 100%);
    		}

    		.mdl-slider.is-upgraded:active::-ms-thumb{
    			background: ${rangeColor};
    		}