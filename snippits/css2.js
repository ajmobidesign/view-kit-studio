.mdl-slider.is-upgraded::-ms-fill-lower {
				background: linear-gradient(to right, transparent, transparent 16px, ${rangeColor} 16px, ${rangeColor} 0);
			}

			.mdl-slider.is-upgraded::-ms-fill-upper {
				background: linear-gradient(to right, transparent, transparent 16px, ${rangeBgColor} 16px, ${rangeBgColor} 0);
			}

			.mdl-slider.is-upgraded::-webkit-slider-thumb{
				background: ${rangeColor};
			}

			.mdl-slider.is-upgraded::-moz-range-thumb{
				background: ${rangeColor};
			}

			.mdl-slider.is-upgraded:focus:not(:active)::-webkit-slider-thumb{
				box-shadow: 0 0 0 10px ${rangeFadedColor};
			}

			.mdl-slider.is-upgraded:focus:not(:active)::-moz-range-thumb {
      			box-shadow: 0 0 0 10px ${rangeFadedColor};
    		}

    		.mdl-slider.is-upgraded:active::-webkit-slider-thumb{
    			background: ${rangeColor};
    		}

    		.mdl-slider.is-upgraded:active::-moz-range-thumb {
    			background: ${rangeColor};
    		}

    		.mdl-slider.is-upgraded::-ms-thumb {
    			background: ${rangeColor};
    		}

    		.mdl-slider.is-upgraded:focus:not(:active)::-ms-thumb{
    			background: radial-gradient(circle closest-side, ${rangeColor} 0%, ${rangeColor} 37.5%, ${rangeFadedColor} 37.5%, ${rangeFadedColor} 100%);
    		}

    		.mdl-slider.is-upgraded:active::-ms-thumb{
    			background: ${rangeColor};
    		}

    		.mdl-slider.is-upgraded.is-lowest-value::-webkit-slider-thumb {
			      border: 2px solid ${rangeBgColor};
			}

			.mdl-slider.is-upgraded.is-lowest-value::-moz-range-thumb{
				border: 2px solid ${rangeBgColor};
			}

			.mdl-slider.is-upgraded.is-lowest-value:focus:not(:active)::-webkit-slider-thumb {
		      box-shadow: 0 0 0 10px ${rangeBgFocusColor};
		      background: ${rangeBgFocusColor};
		    }

		    .mdl-slider.is-upgraded.is-lowest-value:focus:not(:active)::-moz-range-thumb {
			      box-shadow: 0 0 0 10px ${rangeBgFocusColor};
			      background: ${rangeBgFocusColor};
			}

			.mdl-slider.is-upgraded.is-lowest-value:active::-webkit-slider-thumb {
		      border: 1.6px solid ${rangeBgColor};
		    }

		    .mdl-slider.is-upgraded.is-lowest-value:active::-moz-range-thumb {
		      border: 1.5px solid ${rangeBgColor};

		    }

		    .mdl-slider.is-upgraded.is-lowest-value::-ms-thumb{
		    	background: radial-gradient(circle closest-side, transparent 0%, transparent 66.67%, ${rangeBgColor} 66.67%, ${rangeBgColor} 100%);
		    }

		    .mdl-slider.is-upgraded.is-lowest-value:focus:not(:active)::-ms-thumb {
		      background: radial-gradient(circle closest-side, ${rangeBgFocusColor} 0%, ${rangeBgFocusColor} 25%, ${rangeBgFocusColor} 25%, ${rangeBgFocusColor} 37.5%, ${rangeBgFocusColor} 37.5%, ${rangeBgFocusColor} 100%);
		    }

		    .mdl-slider.is-upgraded.is-lowest-value:active::-ms-thumb{
		    	background: radial-gradient(circle closest-side, transparent 0%, transparent 77.78%, ${rangeBgColor} 77.78%, ${rangeBgColor} 100%);
		    }