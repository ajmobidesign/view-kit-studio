 <div style={{height: '200px', width: '800px'}}>
		                <Layout fixedHeader>
		                    <Header>
		                        <HeaderRow title="Title" />
		                        <HeaderTabs ripple activeTab={2} >
		                            <Tab>Tab1</Tab>
		                            <Tab>Tab2</Tab>
		                            <Tab>Tab3</Tab>
		                            <Tab>Tab4</Tab>
		                            <Tab>Tab5</Tab>
		                            <Tab>Tab6</Tab>
		                        </HeaderTabs>
		                    </Header>
		                    <Drawer title="Title" />
		                    <Content>
		                        <div className="page-content">Content for the tab: 2 </div>
		                    </Content>
		                </Layout>
            	
            	</div> 
				
				<div className="pos">
					<div className="inline">
						<Textfield
						    onChange={() => {}}
						    label="Text..."
						    floatingLabel
						    style={{width: '200px'}}/>
					</div>
					<div className="inline">
						<Textfield
							onChange={() => {}}
							label="Expandable Input"
							expandable
							expandableIcon="search"/>
					</div>	
				</div>
				<div className="pos">
					<div className="inline">
						<Badge text="4">Inbox</Badge>
					</div>
					<div className="inline">
						<Badge text="♥" overlap>
						    <Icon name="account_box" />
						</Badge>
					</div>
					<div className="inline">
						<Badge text="1" overlap>
						    <Icon name="account_box" />
						</Badge>
					</div>
					<div className="inline">
						<Badge text="♥" noBackground>Mood</Badge>
					</div>
					
					
				</div>
				
				<div className="pos">
					<div className="inline">
						<FABButton >
						    <Icon name="add" />
						</FABButton>
					</div>
					<div className="inline">
						<FABButton>
						    <Icon name="add" />
						</FABButton>
						
					</div>
					<div className="inline">
						<FABButton disabled>
						    <Icon name="add" />
						</FABButton>
					</div>
				</div>
				
				<div className="pos">
					
					<div className="inline">
						<FABButton colored>
					    	<Icon name="add" />
						</FABButton>
						
					</div>
					<div className="inline">
						<FABButton colored ripple>
					    	<Icon name="add" />
						</FABButton>
						
					</div>

					
					<div className="inline"></div>
					<div className="inline"></div>
				
				</div>
				<div className="pos">
					<div className="inline">
						<Button raised colored>Button</Button>
					</div>
					<div className="inline">
						<Button raised colored ripple>Button</Button>
					</div>
				
					<div className="inline">
						<Button raised accent ripple>Button</Button>
					</div>
					<div className="inline">
						<Button raised accent>Button</Button>
					</div>
				
				</div>
				<div className="pos">
					<div className="inline">
						<Button raised>Button</Button>
					</div>
					<div className="inline">
						<Button raised ripple>Button</Button>
					</div>
					<div className="inline">
						<Button raised disabled>Button</Button>
					</div>
				</div>
				<div className="pos">
					<div className="inline">
						<Button>Button</Button>
					</div>
					<div className="inline">
						<Button ripple>Button</Button>
					</div>
					<div className="inline">
						<Button disabled>Button</Button>
					</div>
				</div>
				<div className="pos">
					<div className="inline">
						<Button primary>Button</Button>
					</div>
					<div className="inline">
						<Button accent>Button</Button>
					</div>
				</div>
				<div className="pos">
					<div className="inline">
						<IconButton name="mood" />
					</div>
					<div className="inline">
						<IconButton name="mood" colored />
					</div>
				</div>
				<div className="pos">
					<div className="inline">
						<Checkbox label="With ripple" ripple defaultChecked />
					</div>
					<div className="inline">
						<Checkbox label="Without ripple" defaultChecked />
					</div>
					
				</div>
				<div className="pos">
					<ProgressBar progress={44} />
				</div>
				<div className="pos">
					<ProgressBar indeterminate />
				</div>
				<div className="pos">
					<ProgressBar progress={33} buffer={87}/>
				</div>
				<div className="pos">
					<RadioGroup name="demo" value="opt1">
					    <Radio value="opt1" ripple>Ripple option</Radio>
					    <Radio value="opt2">Other option</Radio>
					</RadioGroup>
				</div>
				<div className="pos">
					<Slider min={0} max={100} defaultValue={25} />
				</div>
				<div className="pos">
					<Spinner singleColor />
				</div>
				<div className="pos">
					<Switch ripple id="switch1" defaultChecked>Ripple switch</Switch>
				</div>