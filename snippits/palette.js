import React, { Component, PropTypes } from 'react';
import * as Colors from '../../styles/inline';
//import { styleguideActions } from 'src/core/styleguide';
import MdlColorblock from '../mdl-colorblock';
import { templateActionions } from 'src/core/template';


export class MdlPalette extends Component{

	static proptypes ={
		style: PropTypes.object.isRequired,
		update: PropTypes.func.isRequired,
		docId: PropTypes.string.isRequired
	}



	constructor(props, context){
		super(props, context);

		this.onPrimaryChange = ::this.onPrimaryChange;
		this.onAccentChange = ::this.onAccentChange;
		//this.primaryPalette = ::this.primaryPalette;
		//this.applyPalette = ::this.applyPalette;

		//this.renderMainPrimaryShades = ::this.renderMainPrimaryShades;

		this.setPrimaryColor= ::this.setPrimaryColor;
		this.setPrimaryLightColor = ::this.setPrimaryLightColor;
		this.setPrimaryDarkColor = ::this.setPrimaryDarkColor;
		this.setAccentColor = ::this.setAccentColor;
		this.setAccentLightColor = ::this.setAccentLightColor;
	}


	onPrimaryNameChange(event){
		let name = event.target.value;
		let ob = Object.assign({}, this.props.style, {primaryName: name})

		this.props.update(ob, this.props.docId)	
	}

	onAccentNameChange(event){
		let name = event.target.value;
		let ob = Object.assign({}, this.props.style, {accentName: name})

		this.props.update(ob, this.props.docId)	
	}

	setPrimaryColor(clr, shade){
		let ob = Object.assign({}, this.props.style, {primaryRgb: clr, primaryShade: shade})
		this.props.update(ob, this.props.docId)	
	}

	setPrimaryLightColor(clr, shade){
		let ob = Object.assign({}, this.props.style, {primaryLightRgb: clr})
		this.props.update(ob, this.props.docId)	
	}

	setPrimaryDarkColor(clr, shade){
		let ob = Object.assign({}, this.props.style, {primaryDarkRgb: clr})
		this.props.update(ob, this.props.docId)	
	}

	setAccentColor(clr, shade){
		let ob = Object.assign({}, this.props.style, {accentRgb: clr, accentShade: shade})
		this.props.update(ob, this.props.docId)	
	}

	setAccentLightColor(clr){
		let ob = Object.assign({}, this.props.style, {accentLightRgb: clr})
		this.props.update(ob, this.props.docId)	
	}



	renderMainPrimaryShades(){

		let currentPrimary = this.props.style.primaryName;
		let blocks = undefined;
		let isCurrent = this.props.style.primaryRgb;
		
		const primaryShades = ['300', '400', '500', '600', '700', '800'];
		const greyShades = ['500', '600', '700', '800'];
		
		if(currentPrimary == 'Grey'|| currentPrimary == 'BlueGrey' || currentPrimary == 'Brown'){
			blocks = greyShades.map((cl, index)=>{
				let clr = Colors[`palette${currentPrimary}`][`${currentPrimary}${cl}`];
				return (<MdlColorblock  setcolor={this.setPrimaryColor} shade={cl} color={clr} current={isCurrent} key={index}></MdlColorblock>)
			})
		}
		
		else{
			blocks = primaryShades.map((cl, index)=>{
				let clr = Colors[`palette${currentPrimary}`][`${currentPrimary}${cl}`];
				return (<MdlColorblock  setcolor={this.setPrimaryColor} current={isCurrent} shade={cl} color={clr} key={index}></MdlColorblock>)
			})
		}
		
		return (<div className="style-panel-control-block"><span className="control-label">Primary Color</span><span className="control-content">{blocks}</span></div>)
	}



	renderLightPrimaryShades(){

		const primaryShades = ['300', '400', '500', '600', '700', '800'];

		let currentPrimary =  this.props.style.primaryName; 
		let currentPrimaryShade = this.props.style.primaryShade; 
		let blocks = undefined;
		let isCurrent = this.props.style.primaryLightRgb;

		//is there a better way to generate an array?
		
		let lightShades = ['50', '100', '200'].concat(primaryShades.filter((n)=>{ 
			if(parseInt(n)<parseInt(currentPrimaryShade)){
				return n;
				}
			}))


		blocks = lightShades.map((cl, index)=>{
				let clr = Colors[`palette${currentPrimary}`][`${currentPrimary}${cl}`];
				return (<MdlColorblock  setcolor={this.setPrimaryLightColor} shade={cl} current={isCurrent} color={clr} key={index}></MdlColorblock>)
			})

		return (<div className="style-panel-control-block"><span className="control-label">Primary Light</span><span className="control-content">{blocks}</span></div>)
	}


	renderDarkPrimaryShades(){

		const primaryShades = ['300', '400', '500', '600', '700', '800'];

		let currentPrimary = this.props.style.primaryName;
		let currentPrimaryShade = this.props.style.primaryShade;
		let blocks = undefined;
		let isCurrent = this.props.style.primaryDarkRgb;

		//is there a better way to generate an array?
		
		let darkShades = ['900'].concat(primaryShades.filter((n)=>{ 
			if(parseInt(n)>parseInt(currentPrimaryShade)){
				return n
			}
		}))


		blocks = darkShades.map((cl, index)=>{
				let clr = Colors[`palette${currentPrimary}`][`${currentPrimary}${cl}`];		
				return (<MdlColorblock  setcolor={this.setPrimaryDarkColor} current={isCurrent} shade={cl} color={clr} key={index}></MdlColorblock>)
			})


		return (<div className="style-panel-control-block"><span className="control-label">Primary Dark</span><span className="control-content">{blocks}</span></div>)
		
	}


	renderMainAccentShades(){

		let currentAccent = this.props.style.accentName;
		let blocks = undefined;
		let isCurrent = this.props.style.accentRgb;
		
		const accentShades = ['300', '400', '500', '600', '700', '800', 'A200', 'A400'];
			
		blocks = accentShades.map((cl, index)=>{
				let clr = Colors[`palette${currentAccent}`][`${currentAccent}${cl}`]
				return (<MdlColorblock  setcolor={this.setAccentColor} current={isCurrent} shade={cl} color={clr} key={index}></MdlColorblock>)
			})
			
		return(<div className="style-panel-control-block"><span className="control-label">Accent Color</span><span className="control-content">{blocks}</span></div>)
	}


	renderLightAccentShades(){

		let currentAccent = this.props.style.accentName;
		let currentAccentShade = this.props.style.accentShade;
		let blocks = undefined;
		let isCurrent = this.props.stule.accentLightRgb;
		
		const accentShades = ['300', '400', '500', '600', '700', '800', 'A200', 'A400'];

		if(currentAccentShade === 'A200' || currentAccentShade === 'A400'){

			if(currentAccentShade === 'A200'){
				let cl = 'A100';
				let clr = Colors[`palette${currentAccent}`][`${currentAccent}${cl}`];
				
				blocks = (<MdlColorblock  setcolor={this.setAccentLightColor} current={isCurrent} shade={cl} color={clr} ></MdlColorblock>)
			}
			else{

				let accentLightShades = ['A100', 'A200']
				blocks = accentLightShades.map((cl, index)=>{
					let clr = Colors[`palette${currentAccent}`][`${currentAccent}${cl}`];
					
					return (<MdlColorblock  setcolor={this.setAccentLightColor} current={isCurrent} shade={cl} color={clr} key={index}></MdlColorblock>)
				})
			}
		}
		else{

			let accentLightShades = ['50', '100', '200'].concat(accentShades.filter((s)=>{if(parseInt(s)< parseInt(currentAccentShade)){return s}}))

			blocks = accentLightShades.map((cl, index)=>{
				let clr = Colors[`palette${currentAccent}`][`${currentAccent}${cl}`]
				return (<MdlColorblock  setcolor={this.setAccentLightColor} current={isCurrent} shade={cl} color={clr} key={index}></MdlColorblock>)
			})
		}
		
		return (<div className="style-panel-control-block"><span className="control-label">Accent Light</span><span className="control-content">{blocks}</span></div>)
	}









	render(){

			//Styles for palette block
		let primaryMain = { background: `rgb(${this.props.style.primaryRgb})` };

		let primaryDark = { background: `rgb(${this.props.style.primaryDarkRgb})` };

		let primaryLight = { background: `rgb(${this.props.style.primaryLightRgb})` };

		let accentMain = { background: `rgb(${this.props.style.accentRgb})` };

		let accentLight = { background: `rgb(${this.props.style.accentLightRgb})` };




		return(
			<div className="style-palette" >
				<div>
					<div className="style-panel-control-block">
					<span className="control-label">
						Primary Color
					</span>	
					<span className="control-content">	
						<select value={this.props.style.primaryName} onChange={this.onPrimaryNameChange}>
							
							<option value="Red">Red</option>
							<option value="Pink">Pink</option>
							<option value="Purple">Purple</option>
							<option value="DeepPurple">Deep Purple</option>
							<option value="Indigo">Indigo</option>
							<option value="Blue">Blue</option>
							<option value="LightBlue">Light Blue</option>
							<option value="Cyan">Cyan</option>
							<option value="Teal">Teal</option>
							<option value="Green">Green</option>
							<option value="LightGreen">Light Green</option>
							<option value="Lime">Lime</option>
							<option value="Yellow">Yellow</option>
							<option value="Amber">Amber</option>
							<option value="Orange">Orange</option>
							<option value="DeepOrange">Deep Orange</option>
							<option value="Brown">Brown</option>
							<option value="Grey">Grey</option>
							<option value="BlueGrey">Blue Grey</option>
						</select>
					</span>
					</div>
					
						{this.renderMainPrimaryShades()}

						{this.renderLightPrimaryShades() }
					
						{ this.renderDarkPrimaryShades() }
					

					<div className="style-panel-control-block">
					<span className="control-label">
						Accent Color
					</span>	
					<span className="control-content">	
						<select value={this.props.style.accentName} onChange={this.onAccentNameChange}>
							
							<option value="Red">Red</option>
							<option value="Pink">Pink</option>
							<option value="Purple">Purple</option>
							<option value="DeepPurple">Deep Purple</option>
							<option value="Indigo">Indigo</option>
							<option value="Blue">Blue</option>
							<option value="LightBlue">Light Blue</option>
							<option value="Cyan">Cyan</option>
							<option value="Teal">Teal</option>
							<option value="Green">Green</option>
							<option value="LightGreen">Light Green</option>
							<option value="Lime">Lime</option>
							<option value="Yellow">Yellow</option>
							<option value="Amber">Amber</option>
							<option value="Orange">Orange</option>
							<option value="DeepOrange">Deep Orange</option>
						</select>
					</span>
					</div>

						{this.renderMainAccentShades() }
					
						{ this.renderLightAccentShades() }
					

					<div className="style-panel-control-block" style={{textAlign:'center'}}>
						<div className="color-block" style={primaryLight}></div>
						<div className="color-block" style={primaryMain}></div>
						<div className="color-block" style={primaryDark}></div>
						<div className="color-block" style={accentLight}></div>
						<div className="color-block" style={accentMain}></div>
					</div>

				</div>

				
			</div>
		)
	}



}

export default MdlPalette;
